package Arg;
our $VERSION = '----.20190118.1216';	# 再構築
our $VERSION = '---1.20190204.0256';	# とりあえず完了
#
# 入力バッファ制御
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
use Param;
use Naoit0::ChrConv;
use Debug;

#-------------------------------------------------
# ■コンストラクタ
# new( socket );
#	socket: ソケットハンドラ（エコーバック時に呼び出すため）
#
sub new {
	my $class = shift;
	my $socket = shift;
	return unless ( defined($socket) );				# ソケットの指定がなければエラー

	my $self = { @_ };
		$self->{'socket'} = $socket;				# ソケットオブジェクトを記憶
	bless($self, $class);
	$self->_init();
	return $self;
}

#-------------------------------------------------
# ■プロパティの初期化
# _init();
#
sub _init {
	my $self = shift;

	$self->{'_ChrConv'}			= new Naoit0::ChrConv;				# 文字置換

	# バッファ
	$self->{'buf_push'}			= undef;		# 一時退避バッファ
												# recvでぶつ切りしたデータを補正するために、最後尾の一文字を一時保管する
	$self->{'buf_store'}		= [];			# 蓄積バッファ（配列）
	$self->{'buf_line'}			= [];			# ライン入力バッファ（配列）

	# モード
	my $m = new Param;
		$m->set( 'line' => 0 );				# ライン入力モード
		$m->set( 'mask' => 0 );				# マスク処理モード
		$m->set( 'echo' => 1 );				# エコーバックモード
		$self->{'mode'} = $m;

	# パラメータ
	my $p = new Param;
		$p->set( 'store_charset'	=> 'cp932' );						# 蓄積バッファのエンコード
		$p->set( 'line_charset'		=> 'cp932' );						# 入力バッファのエンコード（※現在未使用）
		$p->set( 'line_size'		=> 80 );							# 入力バッファサイズ
		$p->set( 'maskchr'			=> '*' );							# マスク文字
		$p->set( 'deny_chrs'		=> "\x08|\x0d|[\x00-\x1f]" );		# 入力バッファの取り込みを拒否する文字パターン
																			# デフォルトで <BS>, <CR>, <ESC> は入力バッファに取り込まない
		$p->set( 'allow_chrs'		=> "" );							# 入力バッファの取り込みを許可する文字パターン
																			# 例えば、エスケープシーケンスを取り込みたい場合は <ESC> を指定する
																			# ※文字をdenyとallowに同時指定した場合はallowを優先
																			# ヌルにするとデフォルト値がセットされる

	# ステータス
		$p->set( 'line_cr'			=> 0 );					# 入力モードを確定状態 ( 0: 未確定, 1: 確定 )
		$p->set( 'store_length'		=> 0 );					# 蓄積バッファのデータ長
		$p->set( 'line_length'		=> 0 );					# 入力バッファのデータ長
	$self->{'param'} = $p;
}

#--------------------------------------------------
# ■モードの参照・設定
# mode( name => value, ... );				# 設定(複数設定可)
# value = mode( name );							# 参照
#
sub mode {
	#print "\n(mode)\n";			# ????
	my $self = shift;
	my $m = $self->{'mode'};
	my @arg = @_;
	my $r;

	#printf "#arg=(%d)\n", $#arg;								# ????
	#print "[arg]\n".Dumper( @arg )."\n";						# ????
	if ( $#arg >= 1 ) {											# 設定
		#print "[A]\n".Dumper( $self->{'mode'} )."\n";			# ????
		while (@arg) {
			my $n = shift(@arg);
			my $v = shift(@arg);
			$m->set( $n => $v ) if ( $m->defined($n) );
		}
		#print "[B]\n".Dumper( $self->{'mode'} )."\n";			# ????
	}
	elsif ($#arg == 0) {										# 参照
		my $n = shift(@arg);
		#printf "n=[%s]\n", $n;									# ????
		$r = $m->query($n) if ( $m->defined($n) );
	}

	#print "(/mode)\n";			# ????
	return $r;
}

#--------------------------------------------------
# ■パラメータの参照・設定
# param( name, value );						# 設定(複数設定可)
# value = param( name );						# 参照
# 
sub param {
	#print "\n(param)\n";		# ????
	my $self = shift;
	my $p = $self->{'param'};
	my @arg = @_;
	my $r;

	#printf "#arg=(%d)\n", $#arg;								# ????
	#print "[arg]\n".Dumper( @arg )."\n";						# ????
	if ( $#arg >= 1 ) {											# 設定
		#print "[A]\n".Dumper( $self->{'param'} )."\n";			# ????
		while (@arg) {
			my $n = shift(@arg);
			my $v = shift(@arg);
			if ( ($n eq 'deny_chrs') && ($v eq '') ) {
				$p->set( 'deny_chrs' => "\x08|\x0d|[\x00-\x1f]" );			# デフォルトの取り込み拒否文字パターン
			}
			elsif ( ($n eq 'deny_chrs') && ($v eq '') ) {
				$p->set( 'allow_chrs' => "" );								# デフォルトの取り込み許可文字パターン
			}
			else {
				$p->set( $n => $v ) if ( $p->defined($n) );
			}
		}
		#print "[B]\n".Dumper( $self->{'param'} )."\n";			# ????
	}
	elsif ($#arg == 0) {										# 参照
		my $n = shift(@arg);
		#printf "n=[%s]\n", $n;									# ????
		$r = $p->query($n) if ( $p->defined($n) );
	}

	#print "(/param)\n";		# ????
	return $r;
}

#--------------------------------------------------
# ■ステータスの確認
# r = status( name );
#
sub status {
	#print "\n(status)\n";		# ????
	my $self = shift;
	my $name = shift;

	return if ($name eq '');
	$name = lc($name);					# 名前は全て小文字

	# 【蓄積バッファ】

	# ●格納されている文字数は？
	if ($name eq 'store_count') {
		return $#{ $self->{'buf_store'} } + 1;
	}

	# ●格納されているデータ長は？
	elsif ($name eq 'store_length') {
		return $self->param($name);
	}

	# 【入力バッファ】

	# ●格納されている文字数は？
	elsif ($name eq 'line_count') {
		return $#{ $self->{'buf_line'} } + 1;
	}

	# ●格納されているデータ長は？
	elsif ($name eq 'line_length') {
		return $self->param($name);
	}

	# ●入力バッファの内容は？
	elsif ($name eq 'line') {
		return join('', @{ $self->{'buf_line'} });
	}

	# ●ラインモードで改行が入力された？
	elsif ($name eq 'line_cr') {
		return $self->param($name);
	}

	#print "(/status)\n";		# ????
}

#--------------------------------------------------
# ■蓄積バッファに取り込む
# r = store( data, [ mode ] );
# 
# data : 取り込むデータ（ＣＰ９３２）
# r    : バッファのデータ長
#
# ※onAcessイベントが発生したときに呼び出す。
# ※onIdleイベントが発生したときは、rawに保管されている場合に、store処理を行う。
# 
sub store {
	#print "\n(store)\n";							# ????
	my $self = shift;
	my $data = shift;								# 取り込むデータ
														# データあり(onAccessから呼び出し)
														# データなし(onIdleから呼び出し)
	my $mode = shift;								# 取り込む位置( 0,undef: 後方, 1: 前方 )
		$mode = defined($mode) ? $mode : 0;
	my $charset = $self->param('store_charset');	# 蓄積バッファの文字コード

	# ●退避データがあれば取り込むデータに統合する
	my $_push = $self->{'buf_push'};							# 退避データ
	if ( defined($_push) ) {
		$data = defined($data) ? $_push . $data : $_push;		# 退避したデータを取り込むデータに統合
		$self->{'buf_push'} = undef;							# 退避バッファをクリア
	}

	if ( defined($data) ) {
		#printf "*** defined.data=(%d)\n", 1;						# ???? 取り込むデータ
		#printf "*** data=[%s]\n", $data;							# ???? 取り込むデータの内容
		#printf "*** length.data=(%d)\n", length($data);				# ???? 
	}

	unless ( defined($data) ) {									# 取り込むデータが空なら終了
		return;
		#print "(/store)\n";										# ????
	}

	# ●従来処理（とりあえず保留（削除待機））
	# #printf "*** data.cp932=[%s]\n", $data;						# ???? エンコード前
	# $data = decode( 'cp932', $data );							# 一度ＵＴＦ８でデコード
	# #printf "*** data.utf8=[%s]\n", $data;						# ???? エンコード後
	# my @datas = split( '', $data );								# 配列に変換して
	# @datas = map { encode( $charset, $_ ); } @datas;				# もう一度エンコード
	# #print "[datas]\n".Dumper(@datas)."\n";						# ????

	# ●ぶつぎりパケット対応処理
	my @datas = split( '', $data);
	my @d;

	#-------------------------------------------------------
	#	文字の種類			SJIS	MS932	補足
	#	JIS第1水準			○		○		
	#	JIS第2水準			○		○		
	#	NEC特殊文字			×		○		0x8740～0x879c
	#	NEC選定IBM拡張文字	×		○		0xed40～0xeefc
	#	ユーザ外字			×		○		0xf040～0xf9fc
	#	IBM拡張文字			×		○		0xfa40～0xfc4b
	#
	#	１バイト目： 0x81～0x9f， 0xe0～0xef （MS932 は～0xfc）
	#	２バイト目： 0x40～0x7e， 0x80～0xfc
	#-------------------------------------------------------

	while (@datas) {
		# printf"** length.datas=(%d)\n", $#datas;															# ????
		my $c1 = shift(@datas);
		return unless defined($c1);

		#printf "*** hex.c1=(%d)(%s)\n", length($c1), sprintf("%02x", ord($c1));								# ????
		# ●１文字目が全角上位？
		if ( $c1 =~ /[\x81-\x9f]|[\xe0-\xef]/ ) {															# ( z1 )
			my $c2 = shift(@datas);
			# ●２文字目がデータあり？
			if ( defined($c2) ) {
				#printf "*** hex.c2=(%d)(%s)\n", length($c2), sprintf("%02x", ord($c2));						# ????
				# ●２文字目が全角下位
				if ( $c2 =~ /[\x40-\x7e]|[\x80-\xfc]/ ) {													# ( z1 z2 )
					push( @d, $c1.$c2 );
					#printf "*** (1) [ %s %s ]\n", sprintf("%02x", ord($c1)), sprintf("%02x", ord($c2));
				}
				# ●２文字目が全角下位でない
				else {																						# ( z1 ?? )
					push( @d, $c1 );
					unshift( @datas, $c2 );
					#printf "*** (2) [ %s ] [ %s ]\n", sprintf("%02x", ord($c1)), sprintf("%02x", ord($c2));		# ????
				}
			}
			# ●２文字目がデータなし？
			else {																	# ( ?? )	split
					$self->{'buf_push'} = $c1;
					#printf "*** (2) [ %s ]\n", sprintf("%02x", ord($c1));		# ????
			}
		}
		# ●１文字目が半角または全角下位？
		else {																								# ( z2/h )
				# ●１文字目が全角下位（※データ破損の可能性あり）
				if ( $c1 =~ /[\x40-\x7e]|[\x80-\xfc]/ ) {													# ( z2 )
					push( @d, $c1 );
					#printf "*** (4) [ %s ]\n", sprintf("%02x", ord($c1));									# ????
				}
				# ●１文字目が半角
				else {																						# ( h )
					push( @d, $c1 );
					#printf "*** (5) [ %s ]\n", sprintf("%02x", ord($c1));									# ????
				}
		}
	}
	#print "\n-----------------------------\n";				# ????
	@datas = @d;

	unless ( $charset eq 'cp932') {								# 蓄積バッファの文字コードがＣＰ９３２でければ
		@datas = map { encode( $charset, $_ ); } @datas;			# 定義の文字コードで蓄積バッファをエンコード
	}

	if ( $mode == 1 ) {											# 取り込みフラグ＝１なら
		unshift( @{$self->{'buf_store'}}, @datas );				# バッファの先頭に保存
	}
	else {
		push( @{$self->{'buf_store'}}, @datas );				# バッファに後方に保存
	}

	my $datalen = length( join('', @datas) );											# 追加するデータのデータ長
	$self->param( 'store_length' => ( $self->param('store_length') + $datalen ) );		# バッファのデータ長の更新
	#print "[buf_store]\n".Dumper($self->{'buf_store'});								# ????
	#printf "*** store_length=(%d)\n", $self->param('store_length');					# ????

	#print "(/store)\n";					# ????
	return $self->param('store_length');	# バッファのデータ長を返す
}


#--------------------------------------------------
# ■蓄積バッファから１文字取り出す
# chr = arg();
#
# chr : 取り出した文字（ＣＰ９３２）
#
sub arg {
	#print "\n(arg)\n";		# ????
	my $self = shift;

	if ( $self->param('line_cr') == 1 ) {					# すでにラインモードで改行コードを受け取っていたら
		$self->flush();											# ライン入力バッファを消去
	}

	my $chr = $self->_takeout();									# 蓄積バッファから一文字取り出し
	my $r = $chr;
	if ( defined($chr) ) {											# 文字がなければ終了
		#printf "*** chr=[%s]\n", $chr;									# ???? 取り出した文字

		if ( $self->mode('line') == 1 ) {								# ラインモード有効時
			$chr = $self->_linemode( $chr );								# ライン入力処理に渡す
		}
		#printf "*** mode.echo=(%d)\n", $self->mode('echo');			# ????
		#printf "*** echochr=[%s]\n", $chr;								# ????

		if ( $self->mode('echo') == 1 ) {								# エコーバック有効時
			if ( defined($chr) ) {
				#Debug::dump(undef, $chr);
				if ($chr =~ /\x0d/) {
					#print "<CR>\n";
					$chr =~ s/\x0d/\r\n/;
					#printf "chr=[%s]\n", $chr;
				}
				$self->{'socket'}->send( $chr );							# エコーバックする
			}
		}
	}

	#print "(/arg)\n";		# ????
	return $r;				# 取り出した文字を返す
}

#--------------------------------------------------
# ■蓄積バッファから１文字取り出す
# chr = _takeout( [ mode ] );
# 
# chr : 取り出した文字（ＣＰ９３２）
#
# ※arg()から呼び出す
# 
sub _takeout {
	#print "\n(_takeout)\n";		# ????
	my $self = shift;
	my $mode = shift;				# 取り出す位置( 0,undef: 前方, 1: 後方 )
		$mode = defined($mode) ? $mode : 0;
	my $chr;						# 取り出した文字

	#printf "++ store_length=(%d)\n", $self->param('store_length');					# ???? 取り出し前
	return if ( $self->status('store_length') == 0 );								# 蓄積バッファが空なら終了
	if ( $mode == 1 ) {																# 取り込みフラグ＝１なら
		$chr = pop( @{$self->{'buf_store'}} );											# 蓄積バッファの後方から取り出す
	}
	else {
		$chr = shift( @{$self->{'buf_store'}} );										# 蓄積バッファの先頭から取り出す
	}
	my $chrlen = length( $chr );													# 取り出した文字のデータ長
	$self->param( 'store_length' => ( $self->param('store_length') - $chrlen ) );	# バッファのデータ長の更新
	#printf "++ store_length=(%d)\n", $self->param('store_length');					# ???? 取り出し後
	#print "[buf_store]\n".Dumper($self->{'buf_store'});							# ????

	#print "(/_takeout)\n";			# ????
	return $chr;					# 取り出した文字を返す
}

#--------------------------------------------------
# ■ライン入力処理
# _linemode( chr );
# 
# ラインバッファに取り込まない文字をあらかじめ指定しておき、
# 呼び出されたときに受け取った文字を判定しながらバッファに取り込む。
# 
sub _linemode {
	#print "\n(_linemode)\n";		# ????
	my $self = shift;
	my $chr = shift;						# 文字（ＣＰ９３２）
	my $socket = $self->{'socket'};

	my $chr_echo;													# エコーバック文字
	my $line_length = $self->status('line_length');					# 入力バッファのデータ長

	# ●後退処理(ＢＳ)
	if ($chr eq "\x08") {
		if ( $self->status('line_count') > 0 ) {									# 入力バッファが空でなければ
			#print "** backward buffer\n";												# ????
			my $chr = pop( @{ $self->{'buf_line'} } );									# 入力バッファから１文字取り出し
			$self->param( 'line_length' => ( $line_length - length($chr) ) );			# 入力バッファのデータ長の更新
			#printf "++ chr.length=(%d)\n", length($chr);								# ???? 取り出した文字
			if ( $self->mode('echo') == 1 ) {												# エコーバックが有効なら
				$chr_echo = ("\x08\x20\x08") x length($chr);								# データサイズ分の後退文字を生成
			}
		}
	}

	# ●改行処理(ＣＲ)
	# 	入力バッファの内容は次にarg()を呼び出すまで保持される
	elsif ($chr eq "\x0d") {
		$self->param( 'line_cr' => 1 );												# 改行コードを受け取ったフラグ＝１
	}

	# ●取り込み処理
	else {
		my $line_size = $self->param('line_size');					# 入力バッファサイズ
		if ( ( $line_length + length($chr) ) <= $line_size ) {		# 入力バッファのデータ長＋取り込む文字長が入力バッファサイズを超えていない
			my $deny_chrs   = $self->param('deny_chrs');			# 取り込み拒否する文字パターン
			my $allow_chrs  = $self->param('allow_chrs');			# 取り込み許可する文字パターン
			my $r_deny      = ( $deny_chrs ne '' ) ? 1 : 0;			# 取り込み拒否定義フラグ
			my $r_allow     = ( $allow_chrs ne '' ) ? 1 : 0;		# 取り込み許可定義フラグ
			#printf "** allow_chrs=[%s]\n", $allow_chrs;			# ????
			#printf "** deny_chrs=[%s]\n", $deny_chrs;				# ????
			#printf "** r_allow=(%d)\n", $r_allow;					# ????
			#printf "** r_deny=(%d)\n", $r_deny;					# ????
			#print "\n";											# ????

			# ●取り込み判定条件
			#------------------------
			#	(d) (a)
			#	 0	 0		評価なし
			#	 1	 0		拒否パターンで評価
			#	 0	 1		許可パターンで評価
			#	 1	 1		許可パターンで評価
			#------------------------

			# 取り込み判定
			my $r;
			if ( $r_allow == 1 ) {						# allow=1
				$r = ( $chr =~ /$allow_chrs/ );				# 許可パターンで評価
			}
			else {
				if ( $r_deny == 1 ) {					# allow=0, deny=1
					$r = ( $chr !~ /$deny_chrs/ );			# 拒否パターンで評価
				}
				else {									# allow=0, deny=0
					$r = 1;									# 評価なし
				}
			}

			if ($r) {
				#print "** allow character\n";												# ???? 取り込み許可
				push( @{ $self->{'buf_line'} }, $chr );										# 入力バッファに文字を取り込む
				$self->param( 'line_length' => ( $line_length + length($chr) ) );			# 入力バッファのデータ長の更新
				if ( $self->mode('echo') == 1 ) {											# エコーバックが有効なら
					$chr_echo = $chr;
					if ( $self->mode('mask') == 1 ) {										# 文字マスクが有効なら
						$chr_echo = ( $self->param('maskchr') ) x length($chr_echo);		# 文字をマスク
					}
				}
			}
			else {
				#print "** deny character\n";												# ???? 取り込み拒否
			}
		}
		else {
			#print "** overflow\n";														# ???? オーバーフローしている
		}
	}

	# chr_echoが定義されていたら、エコーバックする
	#printf "** chr=[%s], chr_echo=(%d)[%s]\n", $chr, defined($chr_echo), $chr_echo;						# ????


	#print "[buf_line]\n".Dumper($self->{'buf_line'});														# ????
	#printf "++ line_length=(%d)\n", $self->status('line_length');											# ????
	#printf "++ line_length.free=(%d)\n", ( $self->param('line_size') - $self->status('line_length') );		# ????

	#printf "*** chr_echo=[%s]\n", $chr_echo if defined($chr_echo);			# ????
	#print "(/_linemode)\n";		# ????
	return $chr_echo;			# 取り込んだ文字を返す
}

#--------------------------------------------------
# ■ライン入力バッファの消去
# flush();
# 
# ※入力中、ESCやETXで終了するような場合は入力バッファの内容が保持されているので、
# 入力途中でキャンセルする場合に呼び出す。
# 
sub flush {
	my $self = shift;
		$self->{'buf_line'} = [];								# 入力バッファをクリア
		$self->param( 'line_length'=>0, 'line_cr'=>0  );		# 入力バッファの文字長と改行フラグをリセット
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
##-----------------------------------------------
## 検証用ソケット
##
#package Arg::Test::Socket;
#use strict;
#use warnings;
#use utf8;
#use Encode qw( encode decode );
#use Naoit0::ChrConv;
#use Data::Dumper;
#
#sub new {
#	my $class = shift;
#	my $self = {};
#	bless($self, $class);
#	$self->_init();
#	return $self;
#}
#
#sub _init {
#	my $self = shift;
#		$self->{'_ChrConv'} = new Naoit0::ChrConv;
#}
#
#sub send {
#	print "\n(send)\n";				# ????
#	my $self = shift;
#	my $data = shift;
#
#	if ( defined($data) ) {
#		$data = decode('cp932', $data);						# ＵＴＦ８にエンコード
#		my @datas = split('', $data);						# データを配列型に
#		$self->dump(\@datas);
#	}
#	print "(/send)\n";				# ????
#}
#
#sub recv {
#	print "\n(recv)\n";				# ????
#	my $self = shift;
#
#	my $data = shift( @{ $self->{'data'} } );
#	if ( defined($data) ) {
#		my @datas = split('', $data);						# データを配列型に
#		$self->dump(\@datas);
#	}
#	print "(/recv)\n";				# ????
#	return encode('cp932', $data) if ( defined($data) );	# ＣＰ９３２にエンコード
#	return;
#}
#
#sub dump {
#	my $self = shift;
#	my $data = shift;
#	my @datas = @{$data};
#
#	while ( @datas ) {
#		my $chr = shift(@datas);										# 文字を取り出す
#		$chr = encode('cp932', $chr);									# ＣＰ９３２にエンコード
#		my $chr_hex = ord($chr);										# 文字コードに変換
#
#		#printf "*1* chr=[%s], chr_hex=(%s)\n", $chr, $chr_hex;			# ???? 置換前
#		if ($chr =~ /[\x00-\x1f]|\x7f/ ) {
#			($chr) = $self->{'_ChrConv'}->toHex( $chr, 'tocode' );			# 制御文字に変換
#			$chr_hex = sprintf("%x", $chr_hex);								# 10進→16進変換
#			$chr_hex = '0'.$chr_hex if ( length($chr_hex) == 1 );
#		}
#		else {
#			$chr_hex = unpack("H*", $chr );									# 10進→16進変換
#		}
#		#printf "*2* chr=[%s], chr_hex=(%s)\n", $chr, $chr_hex;			# ???? 置換後
#		printf "| { %-4s } - [%s]\n", $chr_hex , $chr;
#	}
#}
#
## *** 通信データはシフトＪＩＳ（ＣＰ９３２）、
## *** 内部バッファはＵＴＦ８で取り扱うので、送信前と受信後に、
## *** 適切にエンコードした後で処理すること。
## recv()したときは、受信データをＵＴＦ８でエンコード→store()。
## send()するときは、送信データをＣＰ９３２でエンコード。
#
1;