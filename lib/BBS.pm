package BBS;
our $VERSION = "----.20190117.1511";		# 再構築
#
# コアシステム
#
use strict;
use warnings;
use Time::HiRes;
use IO::Select;
use IO::Socket;
use DBI;
use Handler;				# ハンドラ管理
use Param;					# パラメータ管理
use Data::Dumper;
use Debug;

# ■メソッドリスト
#--------------------------------------------------
# start( port )											サーバの開始
# from()												応対中ソケットのオブジェクトを返す
# load()												ユーティリティのローダを呼び出す

# ■パラメータ関連
# config( name => value )								システムパラメータの参照・設定

# param( fileno, name => value, ... )					パラメータの参照・設定
# param_tocookie( fileno, name )						パラメータの値をクッキーに保存
# from_param( name, value, ... )						応対中ソケットのパラメータの参照・設定
# from_param_tocookie( name )							応対中ソケットのパラメータの値をクッキーに保存

# cookie( fileno, name => value, ... )					クッキーの参照・設定
# cookie_toparam( fileno, name )						クッキーの値をパラメータに保存
# from_cookie( name => value, ... )						応対中ソケットのクッキーの参照・設定
# from_cookie_toparam( name )							応対中ソケットのクッキーの値をパラメータに保存


# ■ハンドラ関連
# sethandler( func ) 									クライアントハンドラ(onIdle)の設定
# setsyshandler( type, func, id )						システムハンドラの設定

# ■イベント関連
# _onIdle()												onIdleイベント処理
# _onConnect()											onConnectイベント処理
# _onDisconnect( $socket )								onDisconnectイベント処理
# disconnect( $socket )									onDisconnectイベントを呼び出す

# ■通信関連
# recv()												受信バッファのデータを得る
# sendcancel( func )									送信キャンセル
# sendreq( data, func )									送信要求
# _send()												送信イベント処理

#--------------------------------------------------
# ■コンストラクタ
#
sub new {
	my $class = shift;
	my $self = { @_ };
	bless($self, $class);
	$self->_init();
	return $self;
}

#--------------------------------------------------
# ■属性の初期化
#
sub _init {
	my $self = shift;
		# パラメータ
		$self->{'sigterm'}								= 0;						# サーバ停止フラグ
		$self->{'lockfile'}								= ".lock";					# ロックファイル
		$self->{'select'}								= undef;					# セレクタ
		$self->{'from'}									= undef;					# 応対中のソケットオブジェクト
		$self->{'buf_recv'}								= '';						# 受信バッファ
		$self->{'nodes'}								= [];						# ノードリスト
		$self->{'config'}								= new Param;				# システムパラメータ（システム変数）
			$self->{'config'}->set('TASKS' => [] );									# タスクリスト
		$self->{'db'} =
				['dbi:mysql:dbname=bigmodel;host=127.0.0.1', 'bigmodel', '']; 		# ＤＢパラメータ

		# システムハンドラ
		$self->{'_onConnect'}							= new Handler;				# onConnectハンドラ
		$self->{'_onDisconnect'}						= {};						# onDisconnectハンドラ
																						# 各ユーティリティから登録できるようにする
		$self->{'_onAccess'}							= new Handler;				# onAccessハンドラ
		$self->{'_onIdle'}								= [];						# onIdleハンドラ
			$self->{'_onIdle'}->[0]						= new Handler;					# $self->{'_onIdle'}->[0]はシステムが使用

		# ソケットテーブル（ファイル番号順に並べる）
		$self->{'socket'}								= [];						# ソケットオブジェクト
			#$self->{'socket'}->[fileno]				= $socket;

		$self->{'buf_send'}								= [];						# 送信バッファ
			#$self->{'buf_send'}->[fileno]				= ();
		#$self->{'_onIdle'}								= [];						# onIdleハンドラ
			#$self->{'_onIdle'}->[fileno]				= new Handler;

		$self->{'param'}								= [];						# ユーザパラメータ（ユーザ変数）
			#$self->{'param'}->[fileno]					= new Param;
}

#--------------------------------------------------
# ■ユーティリティのロード
# load()
#
sub load {
	#print "(load)\n";												# ????
	my $self = shift;
	foreach my $pkg ( sort(values(%INC)) ) {
		$pkg =~ s/\.pm//;
		$pkg =~ s/\//::/g;
		if ($pkg =~ /^(BIGModel)/ ) {
			#printf "*** pkg=[%s]\n", $pkg;							# ????
			my $load = $pkg.'::load($self)';
			#printf "*** load=[%s]\n", $load;						# ????
			my $r = eval("$load");									# ユーティリティにload()があれば呼び出す
			#printf "*** load.r=(%d)\n", $r if defined($r);			# ????
		}
	}
	#print "\n(load)\n";											# ????
}

#--------------------------------------------------
# ■受信データを得る
# data = recv();
# 
sub recv {
	#print "\n(recv)\n";						# ????
	my $self = shift;
	my $data = $self->{'buf_recv'};				# 受信データを得る
	$self->{'buf_recv'} = '';					# バッファをクリア
	#print "(/recv)\n";							# ????
	return $data;
}

#--------------------------------------------------
# ■応対中ソケットのオブジェクトを返す
# $socket = from();
#
sub from {
	#print "\n(from)\n";						# ????
	my $self = shift;
	#print "(/from)\n";							# ????
	return $self->{'from'};
}

#--------------------------------------------------
# ■ハンドラの設定(onIdleハンドラ限定)
# sethandler( func );
#
sub sethandler {
	#print "\n(sethandler)\n";			# ????
	my $self = shift;
	my $func = shift;
	if ( ref($func) eq 'CODE' ) {
		my $fileno = $self->from->fileno;							# 応対中ソケットのファイル番号を得る
		if ( defined($fileno) ) {									# ファイル番号が得られないならエラー
			$self->{'_onIdle'}->[$fileno]->set($func);
			#print "[_onIdle]".Dumper($self->{'_onIdle'})."\n";		# ????
		}
	}
	#print "(/sethandler)\n";			# ????
}

#--------------------------------------------------
# ■システムハンドラの設定
# setsyshandler( type, func, id );
#
sub setsyshandler {
	#print "\n(setsyshandler)\n";				# ????
	my $self = shift;
	my $type = shift;
	my $func = shift;
	return unless ( defined($type) && defined($type) );			# 引数が未定義
	return unless ( ref($func) eq 'CODE' );						# funcがサブルーチンでない

	if ( $type =~ /onidle/i ) {
		$self->{'_onIdle'}->[0]->set($func);
	}
	elsif ( $type =~ /onaccess/i ) {
		$self->{'_onAccess'}->set($func);
	}
	if ( $type =~ /onconnect/i ) {
		$self->{'_onConnect'}->set($func);
	}
	if ( $type =~ /ondisconnect/i ) {
		my $id = shift;
		return unless ( defined($id) );							# 引数が未定義
		$self->{'_onDisconnect'}->{$id} = $func;
	}
	#print "(/setsyshandler)\n";				# ????
}

#--------------------------------------------------
# ■システムパラメータの参照・設定
# config( name => value, ... );				# 設定
# config( -name );							# 削除
# value = config( name );					# 参照
# ( name, ... ) = config();					# 名前リストを返す
#
# システムが使用するパラメータ
#
sub config {
	#print "\n(config)\n";												# ????
	my $self   = shift;
	my $config = $self->{'config'};										# パラメータオブジェクト

	my $r;
	if ( $#_ == 0 ) {
		my $n = shift;

		if ( $n =~ /^-/ ) {												# ■削除（マイナス付き）
			$n =~ s/^-//;
			$r = $config->unset($n);
		}
		else {															# ■参照（マイナスなし）
			$r = $config->query($n);
		}
	}
	else {
		if ( $#_ == -1 ) {												# ■名前リストを返す
			return $config->keys();
			#print "\n(/config)\n";										# ????
		}
		else {
			while (@_) {
				my $n = shift;
				my $v = shift;
				if ( defined($n) && defined($v) ) {
					$config->set( $n => $v ) if ( $n !~ /^-/ );			# ■設定（名前にマイナスがついていないなら）
				}
			}
		}
	}
	#print "(/config)\n";												# ????
	return $r;
}

#--------------------------------------------------
# ■パラメータの参照・設定
# param( fileno, name => value, ... );		# 設定
# param( fileno, -name );					# 削除
# value = param( fileno, name );			# 参照
# ( name, ... ) = param( fileno );			# 名前リストを返す
#
sub param {
	#print "\n(param)\n";												# ????
	my $self = shift;
	my $fileno = shift;

	my $param = $self->{'param'}->[$fileno];							# パラメータオブジェクト
	return unless defined($param);										# オブジェクトがなければ戻る

	my $r;
	if ( $#_ == 0 ) {
		my $n = shift;

		if ( $n =~ /^-/ ) {												# ■削除（マイナス付き）
			$n =~ s/^-//;
			$r = $param->unset($n);
		}
		else {															# ■参照（マイナスなし）
			$r = $param->query($n);
		}
	}
	else {
		if ( $#_ == -1 ) {												# ■名前リストを返す
			return $param->keys();
			#print "\n(/param)\n";										# ????
		}
		else {
			while (@_) {
				my $n = shift;
				my $v = shift;
				if ( defined($n) && defined($v) ) {
					$param->set( $n => $v ) if ( $n !~ /^-/ );			# ■設定（名前にマイナスがついていないなら）
				}
			}
		}
	}
	#print "\n(/param)\n";												# ????
	return $r;
}

#--------------------------------------------------
# ■パラメータの値をクッキーに保存
# param_tocookie( fileno, name );
# 
# ※クッキー側で定義済みの場合、パラメータの値が上書きされるので、呼び出すときは注意する。
#
sub param_tocookie {
	#print "\n(param_tocookie)\n";					# ????
	my $self = shift;
	my $fileno = shift;
	my $name = shift;

	#print "(/param_tocookie)\n";					# ????
	return if ( (defined($fileno) == 0) && (defined($name) == 0) );
	return $self->cookie( $fileno, $name => $self->param( $fileno, $name ) );
}

#--------------------------------------------------
# ■応対中ソケットのパラメータの参照・設定
# from_param( name => value, ... );			# 設定
# from_param( -name );						# 削除
# value = from_param( name );				# 参照
# ( name, ... ) = from_param();				# 名前リストを返す
# 
sub from_param {
	#print "\n(from_param)\n";				# ????
	my $self = shift;
	my $fileno = $self->from->fileno;		# 応対中ソケットのファイル番号
	return unless defined($fileno);			# ファイル番号が得られなければエラー
	#print "(/from_param)\n";				# ????
	return $self->param( $fileno, @_ );		# 参照結果
}

#--------------------------------------------------
# ■応対中ソケットのパラメータの値をクッキーに保存
# from_param_tocookie( name );
# 
# ※クッキー側で定義済みの場合、パラメータの値が上書きされるので、呼び出すときは注意する。
#
sub from_param_tocookie {
	#print "\n(from_param_tocookie)\n";				# ????
	my $self = shift;
	my $name = shift;
	my $fileno = $self->from->fileno;		# 応対中ソケットのファイル番号

	#print "(/from_param_tocookie)\n";				# ????
	return if ( (defined($fileno) == 0) && (defined($name) == 0) );
	return $self->param_tocookie( $fileno, $name );
}

#--------------------------------------------------
# ■クッキーの参照・設定
# cookie( fileno, name => value, ... );		# 設定
# cookie( fileno, -name );					# 削除
# value = cookie( fileno, name );			# 参照
# ( name, ... ) = cookie( fileno );			# キーリストを得る
#
# ＤＢ上で管理するパラメータ
# データはユーザＩＤに紐づけされているので、登録されているユーザに対して操作可能。
# このメソッドではユーザＩＤを指定するのではなく、
# 接続したソケットのファイル番号から、ユーザＩＤを参照するため、
# ファイル番号を指定する。
#
sub cookie {
	#print "\n(cookie)\n";							# ????
	my $self = shift;
	my $fileno = shift;
	#printf "** fileno=(%d)\n", $fileno;			# ????
	return unless ( defined($fileno) );
	my $userid = $self->param( $fileno, 'ID' );		# クライアントのユーザID
	#printf "** userid=[%s]\n", $userid;			# ????
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	my $r;
	if ( $#_ == 0 ) {
		my $n = shift;
		my $v;
		# ●削除（マイナス付き）
		if ( $n =~ /^-/ ) {
			#print "** (delete)\n";											# ????
			$n =~ s/^-//;
			my $q = "DELETE FROM `userpref` WHERE userid = BINARY '$userid' AND name='$n'";
			#printf "**[q]------------\n%s\n-----------------\n", $q;		# ????
			my $r = $dbh->do($q);
		}
		# ●参照（マイナスなし）
		else {
			#print "** (query)\n";											# ????
			my $q = "SELECT value FROM `userpref` WHERE userid = BINARY '$userid' AND name='$n'";
			#printf "**[q]------------\n%s\n-----------------\n", $q;		# ????
			my $sth = $dbh->prepare( $q );
			$sth->execute();
			if ( $sth->rows ) {
				($v) = $sth->fetchrow_array();
			}
			$sth->finish();
		}
		$dbh->disconnect();
		return $v;
	}
	else {
		#print "** (keylist)\n";										# ????
		my $names;
		my $q = "SELECT name, value FROM `userpref` WHERE userid = BINARY '$userid'";
		#printf "**[q]------------\n%s\n-----------------\n", $q;		# ????
		my $sth = $dbh->prepare( $q );
		$sth->execute();
		if ( $sth->rows ) {
			while ( my ($n, $v) = $sth->fetchrow_array ) {
				#printf "** name=[%s], value=[%s]\n", $n, $v;			# ????
				$names->{$n} = $v;
			}
		}
		$sth->finish();

		if ( $#_ == -1 ) {												# ■名前リストを返す
			$dbh->disconnect();
			#print "[names]\n".Dumper($names)."[/names]\n";				# ????
			my @keys = keys(%{ $names });
			return @keys;
		}
		else {
			while (@_) {
				my $n = shift;
				my $v = shift;
				if ( defined($n) && defined( $v ) ) {
					 if ( $n !~ /^-/ ) {												# 名前にマイナスがついていないなら
						#printf "** %s.exists=(%d)\n", $n, exists($names->{$n});		# ????
						# ●更新
						if ( exists($names->{$n}) ) {
							#print "** (update)\n";										# ????
							my $q = "UPDATE `userpref` SET value = '$v' WHERE userid = BINARY '$userid' AND name = '$n'";
							#printf "**[q]------------\n%s\n-----------------\n", $q;	# ????
							my $r = $dbh->do($q);
						}
						# ●追加
						else {
							#print "** (insert)\n";										# ????
							my $q = "INSERT INTO `userpref` (userid, name, value) VALUES (?, ?, ?)";
							#printf "**[q]------------\n%s\n-----------------\n", $q;	# ????
							my $sth = $dbh->prepare( $q );
							$sth->execute($userid, $n, $v);
							$sth->finish();
						}
						$r = 1;
					}
				}
			}
			$dbh->disconnect();
		}
	}
	#print "(/cookie)\n";							# ????
	return $r;
}

#--------------------------------------------------
# ■クッキーの値をパラメータに保存
# cookie_toparam( fileno, name );
# 
# ※パラメータ側で定義済みの場合、クッキーの値が上書きされるので、呼び出すときは注意する。
#
sub cookie_toparam {
	#print "\n(cookie_toparam)\n";					# ????
	my $self = shift;
	my $fileno = shift;
	my $name = shift;
	
	#print "(/cookie_toparam)\n";					# ????
	return if ( (defined($fileno) == 0) && (defined($name) == 0) );
	return $self->param( $fileno, $name => $self->cookie( $fileno, $name ) );
}

#--------------------------------------------------
# ■応対中ソケットのクッキーの参照・設定
# from_cookie( name => value, ... );		# 設定
# from_cookie( -name );						# 削除
# value = from_cookie( name );				# 参照
# ( name, ... ) = from_cookie();			# キーリストを得る
#
# ＤＢ上で管理するパラメータ
# データはユーザＩＤに紐づけされているので、登録されているユーザに対して操作可能。
# このメソッドではユーザＩＤを指定するのではなく、
# 接続したソケットのファイル番号から、ユーザＩＤを参照するため、
# ファイル番号を指定する。
#
sub from_cookie {
	#print "\n(from_cookie)\n";							# ????
	my $self = shift;
	my $fileno = $self->from->fileno;

	return unless ( defined($fileno) );
	#print "(/from_cookie)\n";							# ????
	return $self->cookie( $fileno, @_ );
}

#--------------------------------------------------
# ■応対中ソケットのクッキーの値をパラメータに保存
# from_cookie_toparam( name );
# 
# ※パラメータ側で定義済みの場合、クッキーの値が上書きされるので、呼び出すときは注意する。
#
sub from_cookie_toparam {
	#print "\n(from_cookie_toparam)\n";				# ????
	my $self = shift;
	my $name = shift;
	my $fileno = $self->from->fileno;		# 応対中ソケットのファイル番号
	
	#print "(/from_cookie_toparam)\n";				# ????
	return if ( (defined($fileno) == 0) && (defined($name) == 0) );
	return $self->cookie_toparam( $fileno, $name );
}

#--------------------------------------------------
# ■サーバの開始
# start( port )
#
sub start {
	my $self = shift;
	my $port = shift;

	# ロックファイル作成
	open(my $fh, ">$self->{'lockfile'}");
	close($fh);

	# ユーティリティをローダを呼び出す
	$self->load();

	my $listen = new IO::Socket::INET(Listen => 1, LocalPort => $port);				# ソケットリスナー
	$self->{'select'} = new IO::Select($listen);									# セレクタ

	$| = 1;
	print "**** server started\n";
	while ( $self->{'sigterm'} == 0 ) {
		my @ready = $self->{'select'}->can_read(0.0001);			# 通信イベントを監視
		#printf "ready=(%d)\n", $#ready+1;							# ???? 行列待ちに入っているソケット数

		if ( $#ready >= 0 ) {										# 通信イベント発生
			foreach my $socket (@ready) {								# 行列待ちを捌く
				$self->{'from'} = $socket;									# 応対中のソケットを設定

				#------------------------------------------------------
				# ■ソケット接続処理(onConnect)
				if ($socket == $listen) {
					#print "\n-----[ onConnect ]-----\n";							# ????
					$socket = $listen->accept;										# リスナーをacceptして新しいソケットを作り
					$self->{'select'}->add($socket);								# そのソケットをセレクタに追加
					$self->{'from'} = $socket;										# 応対中のソケットをacceptしたソケットに変更
					my $fileno = $socket->fileno;									# 応対中ソケットのファイル番号を得る

					# ●ノードリストに追加
					push(@{ $self->{'nodes'} }, $fileno);							# ノードリストに応対中ソケットのファイル番号を後方追加
					#printf "*** nodes: [ %s ]\n"
					#					, join(' , ', @{ $self->{'nodes'} });		# ???? ノードリストの内容

					my $r = $self->_onConnect();									# [[[ onConnectイベント処理へ ]]]
					#printf "*** event._onConnect.r = (%d)\n", $r;					# ???? イベントの戻り値
					#print "-----[ /onConnect ]-----\n";								# ????
				}

				#------------------------------------------------------
				# ■データ通信処理(onAccess)
				else {
					#print "\n-----[ onAccess ]-----\n";											# ????
					$socket->recv($self->{'buf_recv'}, 256);										# データ受信
					if ($self->{'buf_recv'} eq '') {												# 受信バッファが空（ソケット切断）
						my $r = $self->_onDisconnect( $socket );									# onDisconnectイベント処理へ
						#printf "*** event._onDisconnect.r = (%d)\n", $r;							# ???? イベントの戻り値
						$self->{'from'} = undef;													# 応対中ソケットを消去
					}
					else {																		# 受信バッファにデータがある
						#printf "*** socket.fileno=(%d)\n", $socket->fileno;						# ???? 応対中ソケットのファイル番号(ok)
						#printf "*** from.fileno=(%d)\n",   $self->{'from'}->fileno;				# ???? 応対中ソケットのファイル番号(ok)
						#printf "*** rbuf.length=(%d)\n",   length( $self->{'buf_recv'} );			# ???? 受信バッファのデータ長
						#printf "*** rbuf=[%s]\n",          $self->{'buf_recv'};					# ???? 受信データ
						my $r = $self->{'_onAccess'}->call($self);									# {{{ onAccessハンドラを実行 }}}
						#printf "*** handle._onAccess.r=(%d)\n", $r;									# ???? ハンドラの実行結果
					}
					#print "-----[ /onAccess ]-----\n";											# ????
				}

				#------------------------------------------------------
				# ■onIdleイベント処理（単発）
				#print "\n-----[ onIdle_once ]-----\n";					# ????
				my $r = $self->_onIdle();								# [[[ onIdleイベント処理へ ]]]
				#printf "*** event.onIdle_once.r = (%d)\n", $r;			# ???? イベントの戻り値
				$self->{'from'} = undef;								# 応対中のソケットをリセット
				#print "-----[ /onIdle_once ]-----\n";					# ????
			}
		}

		#------------------------------------------------------
		# ■onIdle待機イベント処理（ループ）
		else {
			#print "\n-----[ onIdle_loop ]-----\n";					# ????
			my $r = $self->_onIdle();								# [[[ onIdleイベント処理へ ]]]
			#printf "*** event.onIdle_loop.r = (%d)\n", $r;			# ???? イベントの戻り値
			#print "-----[ /onIdle_loop ]-----\n";					# ????
		}
	}
	print "**** server teaminated\n";
	return 1;				# サーバ停止
}

#--------------------------------------------------
# ■待機イベント(onIdle)
# _onIdle()
#
sub _onIdle {
	my $self = shift;
	$self->{'sigterm'} = 1 unless ( -e "$self->{'lockfile'}" );				# ロックファイルが消失したらサーバを停止

	my $r = $self->{'_onIdle'}->[0]->call($self);							# {{{ onIdleシステムハンドラを実行 }}}
	#printf "*** handler._onIdle->[0].r=(%d)\n", $r;						# ???? 実行結果

	return -1 if ( $#{ $self->{'nodes'} } == -1 );							# onIdle呼び出し行列に何もない場合は処理を行わない
	#print "\n(event._onIdle)\n";											# ????
	my $from = $self->from;													# 応対中のソケット
	my $running;															# 実行中のクライアント（順番）

	# ●最初に呼び出すクライアントを選択
	if ( defined($from) ) {																			# ファイル番号が定義済み
		my $fileno = $from->fileno;																		# 応対中ソケットのファイル番号を得る
		#printf "*** (1) from.fileno=(%d)\n", $from->fileno;											# ???? 応対中ソケットを得る
		if ( defined($fileno) ) {
			($running) = grep { $self->{'nodes'}->[$_] == $fileno } 0..$#{ $self->{'nodes'} };				# ノードリストから順番を得る	(**warn)
		}
		else {
			$running = $#{ $self->{'nodes'} };																# ノードリストの最後の順番を得る
		}
	}
	else {																							# ファイル番号が未定義（ソケット消失）
		#print "*** (0)\n";																				# ????
		$running = $#{ $self->{'nodes'} };																# ノードリストの最後の順番を得る
	}

	#printf "*** start=(%d)\n", $running;								# ???? 最初に呼び出すクライアントの順番
	#printf "*** fileno=(%d)\n", $self->{'nodes'}->[$running];			# ???? 最初に呼び出すクライアントのファイル番号
	#printf "*** nodes: [ %s ]\n\n"
	#			, join(' , ', @{ $self->{'nodes'} });					# ???? ノードリスト

	my $exit = 0;															# 終了フラグ
	my $step = 0;															# 呼び出し回数（通算）

	# ●接続クライアントの巡回呼び出し
	until( $exit ) {														# 終了フラグが立つまでループ
		my $fileno = $self->{'nodes'}->[$running];								# 応対するクライアントのファイル番号を得る	(**warn)
		$self->{'from'} = $self->{'socket'}->[$fileno];							# 応対するソケットを設定

		#printf "*** running=(%d)\n", $running;									# ???? 呼び出すのノードリストの順番
		#printf "*** fileno.from=(%d)\n", $self->from->fileno;					# ???? 呼び出すクライアントのファイル番号（ソケットから取得）
		#printf "*** fileno=(%d)\n", $self->{'nodes'}->[$running];				# ???? 呼び出すクライアントのファイル番号
		#printf "*** step=(%d)\n", $step;										# ???? 実行した回数

		my $handler = $self->{'_onIdle'}->[$fileno];							# ハンドラオブジェクトを得る
		if ( defined($handler) ) {												# ハンドラが定義されているなら
			my $r = $handler->call($self);											# {{{ onIdleハンドラを実行 }}}
			#printf "*** _onIdle.r = (%d)\n", $r;									# ???? ルーチンの戻り値
		}
    
		# **** _send()をここに追加する
		#$self->_send($fileno);													# データ送信処理（※予定）
    
		$running = ( defined($running) && $running <= 0 ) ? $#{ $self->{'nodes'} } : $running-1;
		#if ( $running == 0 ) {													# 実行中のソケットのオーダ番号が－１になったら
		#	$running = $#{ $self->{'nodes'} };										# オーダリストの最大値（最後尾）
		#}
		#else {																	# そうでなければ
		#	$running--;																# オーダ番号を－１する
		#}
    
		$step++;																# 呼び出し回数＋１
		$exit = 1 if ( $step > $#{ $self->{'nodes'} } );						# 実行回数が現在のオーダ数を超えたら終了フラグ＝１（ループを抜ける）
		#print "\n";															# ????
	}

	#print "(/event._onIdle)";													# ????
	#print "\n";																	# ????
	return;
}

# ※onIdle処理中にソケットが切断されたときに、一巡を確認できない恐れがあると思われる。

#--------------------------------------------------
# ■ソケット接続イベント(onConnect)
# _onConnect()
#
sub _onConnect {
	#print "\n(event._onConnect)\n";										# ????
	my $self = shift;
	my $fileno = $self->from->fileno;								# 応対中ソケットのファイル番号を得る

	# ●ソケットテーブルに追加

		# ●各オブジェクトと送信バッファを準備
		$self->{'socket'}->[$fileno]		= $self->from;			# ソケットオブジェクト
		$self->{'_onIdle'}->[$fileno]		= new Handler;			# ハンドラオブジェクト
		$self->{'param'}->[$fileno]			= new Param;			# パラメータオブジェクト（パラメータ用）
		$self->{'buf_send'}->[$fileno]		= '';					# 送信バッファ

		# ●パラメータの定義
		$self->from_param( 'CONNECTED_TIME' => time );				# 接続開始時間
		$self->from_param( 'SEND_TIMER' => undef );					# 待機制限タイマー
		$self->from_param( 'FLOW_CTRL'  => 0 );						# フロー制御フラグ

	# ●onConnectハンドラを呼び出す
	my $r = $self->{'_onConnect'}->call($self);						# {{{ onConnectハンドラを実行 }}}
																	# 	onIdleハンドラの初期定義は、onConnectハンドラから行う
	#printf "*** handle.onConnect.r=(%d)\n", $r;							# ???? ハンドラの戻り値
	#print "(/event._onConnect)\n";										# ????
	return 58585;
}

#--------------------------------------------------
# ■ソケット切断イベント(onDisconnect)
# _onDisconnect( socket )
#
sub _onDisconnect {
	#print "\n(event._onDisconnect)\n";									# ???? 実行モード
	my $self = shift;
	my $socket = shift;
	return -1 unless defined($socket);								# 引数が指定されていなければエラー

	my $fileno =  $socket->fileno;									# 対象ソケットのファイル番号
	$self->{'from'} = $socket;										# 応対中ソケット（ハンドラで使用するため仮設定）

	# ●onDisconnectハンドラを呼び出す
	my $handlers = $self->{'_onDisconnect'};
	#print "[handlers]\n".Dumper($handlers)."\n";					# ????
	foreach my $id ( keys(%{$handlers}) ) {
		#printf "** id=(%s)\n", $id;									# ????
		my $func = $handlers->{ $id };
		my $r = $self->$func;											# {{{ onDisconnectハンドラを実行 }}}
		#printf "*** handle.onDisconnect.%s.r=(%d)\n", $id, $r;			# ???? ハンドラの戻り値
	}

	$socket->close;													# 応答中のソケットを閉じる
	$self->{'select'}->remove($socket);								# セレクタからソケットを削除

	# ●ソケットテーブルから削除
	$self->{'buf_send'}->[$fileno] = undef;							# 送信バッファ
	$self->{'param'}->[$fileno] = undef;							# パラメータオブジェクト
	$self->{'_onIdle'}->[$fileno] = undef;							# ハンドラオブジェクト
	$self->{'socket'}->[$fileno] = undef;							# ソケットオブジェクト

	# ●ノードリストから削除
	@{ $self->{'nodes'} } =
				grep { $_ != $fileno } @{ $self->{'nodes'} };		# onIdleハンドラオーダリストから削除
	#printf "*** nodes: [ %s ]\n"
	#					, join(' , ', @{ $self->{'nodes'} });		# ???? onIdle呼び出し行列（削除後）

	#print "\n(/event._onDisconnect)\n";									# ???? 実行モード
	return 88888;
}

#--------------------------------------------------
# ■ソケット切断処理
# disconnect( socket )
#
sub disconnect {
	my $self = shift;
	return $self->_onDisconnect(@_);
}

#--------------------------------------------------
# ■応対中のソケットへ送信【※調整中】
# send( data, fileno );
# 
sub send {
	#print "\n(send)\n";						# ????
	my $self = shift;
	my $data = shift;														# 送信データ
	my $fileno = shift;														# 送信先のファイル番号
		$fileno = defined($fileno) ? $fileno : $self->from->fileno;		# 送信先が未定義なら応対中ソケットのファイル番号をあてる
	my $socket = $self->{'socket'}->[$fileno];
	$socket->send( $data );
	#print "(/send)\n";							# ????

	#----------------------------------------------
	## _send()
	##
	#sub _send {
	#	print "\n(_send)\n";								# ???? 実行モード
	#	my $self = shift;
	#	my $fileno = shift;
	#	#print map { "[$_]\n"; } @_;
	#	printf "*** fileno=(%d)\n", $fileno;
	#	printf "*** sbuf_timer=(%f)\n", $self->{'sbuf_timer'}->[$fileno];
	#	print "(/_send)\n";									# ???? 実行モード
	#}
	#
	#
	#
	##エミュレーションモードが有効でない
	#
	##送信バッファ確認。
	##バッファにデータがない
	##	→終了
	##バッファにデータがある
	##	→現在のカウンタを確認。
	##		現在のカウント値がタイマー値を超えている
	##			→送信バッファからデータを取り出し、データ送信
	##			→タイマー値をリセット
	##		超えていない
	##			→終了
	##
	#----------------------------------------------

}

#--------------------------------------------------
# ■送信キャンセル
# r = sendcancel( func )
#
sub sendcancel {
	print "\n(sendcancel)\n";				# ????
	my $self = shift;
#	my $func = shift;
#
#	return -1 unless ( defined($func) == 1 && (ref($func) eq 'CODE') );		# 引数が未定義または引数がルーチンのエントリポイントでなければエラー
#
#	@{ $self->{'sbuf'}->[$fileno] } = ();		# 送信バッファをクリア
#	$self->{'sbuf_timer'}->[$fileno] = $time;	# 現在時刻をクリア
#	$self->$func;								# ルーチンのエントリポイントを呼び出す
#												# （ここではキャンセル処理に特化したもののみを行い、ユーティリティについては、
#												# よびだし先のルーチン内で、setHandlerを用いて定義する）
#	return 0;
	print "(/sendcancel)\n";				# ????
}

#--------------------------------------------------
# ■送信要求
# r = sendreq( data, func )
#
sub sendreq {
	print "\n(sendreq)\n";				# ????
	my $self = shift;
#	return -1 if ( length( $self->{'sbuf'}->[$fileno] ) > 0 );		# 送信バッファが空でなければエラー
#
#	my $data = shift;
#	$data = decode('cp932', $data);									# 入力データをutf8デコード（※デコード処理を用意した方がいい）
#	@{ $self->{'sbuf'}->[$fileno] } = split('', $data);				# 送信バッファにデータが入る。
#
#																	# 現在時刻を保存
#	my ($sec, $msec) = gettimeofday;
#	#printf "sec=(%d), msec=(%d)\n", $sec, $msec;
#	$time = ( $sec + $msec * 0.000001 )  + 0.011;
#	#printf "time=(%f)\n\n",$time;
#	$self->{'sbuf_timer'}->[$fileno] = $time;
#	return 0;
	print "(/sendreq)\n";				# ????
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;