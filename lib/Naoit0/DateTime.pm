package Naoit0::DateTime;
our $VERSION = "0001.20180131.1048";
our $VERSION = "0002.20180419.2217";	# 微調整

use utf8;
use strict;
use warnings;
use Data::Dumper;
use Encode qw( encode decode );
use Time::Local qw( timelocal );
	local $Data::Dumper::Sortkeys = 1;
	local $Data::Dumper::Indent = 1;
use Debug;

# ■コンストラクタ
# datetime = new DateTime( [@localtime] );				# localtime で得るリスト形式
# datetime = new DateTime('yyyy/mm/dd hh:mm:ss');		# フォーマット文字列
# datetime = new DateTime( 1724500151 );				# エポック秒

# ■ プロパティの更新
# r = DateTime->update()								# プロパティの値(年,月,日,時,分,秒)で更新
# r = DateTime->update( [@localtime] )					# localtime で得るリスト形式で更新
# r = DateTime->update('yyyy/mm/dd hh:mm:ss')			# フォーマット文字列形式で更新
# r = DateTime->update( 1234567890 )					# エポック秒で更新
# r : ( 0=失敗(エラー), 1=成功 )

# ■localtimeリスト形式からエポック秒に変換（timelocal）
# epoch = DateTime->tl( [@localtime] )

# ■エポック秒からlocaltimeリスト形式に変換（localtime）
# @localtime = DateTime->lt( epoch )

# ■時刻プロパティの値を設定（値の検証なし、設定後update()呼び出し必要）
# DateTime->setTime( 'hms', 4, 2, 2 );		# 時分秒を設定
# DateTime->setTime( 'hm', 4, 2 );			# 時分を設定

# ■日付プロパティの値を設定（値の検証なし、設定後update()呼び出し必要）
# DateTime->setDate( 'ymd', 2018, 1, 1 );	# 年月日を設定
# DateTime->setDate( 'md', 1, 1 );			# 月日を設定

# ■プロパティの値（エポック秒）を返す
# epoch = DateTime->epoch()

# ■日付プロパティの値をフォーマット文字列に当てはめて返す
# DateTime->d("_年_月_日", y4, m2 ,d2 );		# '2018年01月31日'

# ■プロパティの値（年）を返す
# year = DateTime->year( arg );
#
#	4y   : '2018'
#	2y   : '18'
#	jh1y : '30'
#	jh2y : '30'
#	js1y : '93'
#	js2y : '93'

# ■プロパティの値（月）を返す
# month = DateTime->month( arg );
#
#	1m  : '9'
#	2m  : '09'
#	e1m : 'Sep'
#	e2m : 'September'

# ■プロパティの値（日）を返す
# day = DateTime->day( arg );
#
#	1d : '9'
#	2d : '09'

# ■プロパティの値（曜日）を返す
# weekday = DateTime->weekday( arg );
#
#	'e1w' : 'Mon'
#	'e2w' : 'Monday'
#	'jw'  : '月'

# ■時刻プロパティの値をフォーマット文字列に当てはめて返す
# DateTime->t("_時_分_秒", 2h24, m2 ,s2 );		# '20時01分31秒'

# ■プロパティの値（時）を返す
# hour = DateTime->hour( arg );
#
#	1h24 : '8'
#	2h24 : '20'
#	1h12 : '8',
#	2h12 : '08'
#	eh   : 'PM'
#	jh   : '午後'

# ■プロパティの値（分）を返す
# minute = DateTime->minute( arg );
#
#	1m		: '9'
#	2m		: '09'

# ■プロパティの値（秒）を返す
# second = DateTime->second( arg );
#
#	1s		: '9'
#	2s		: '09'

#--------------------------------------------------------------------
# ■コンストラクタ
# datetime = new DateTime( [@localtime] );				# localtime で得るリスト形式
# datetime = new DateTime('yyyy/mm/dd hh:mm:ss');		# フォーマット文字列
# datetime = new DateTime( 1724500151 );				# エポック秒
#
sub new {
	#printf "\n** < new [ %s ] >\n\n", Debug::_caller(caller);
	my $class = shift;

	my $self = ();
		$self->{epoch}		= undef;
		$self->{second}		= undef;
		$self->{minute}		= undef;
		$self->{hour}		= undef;
		$self->{year}		= undef;
		$self->{month}		= undef;
		$self->{day}		= undef;
		$self->{weekday}	= undef;
	bless($self, $class);

	if ( @_ ) {				# 引数あり→引数でプロパティを初期化
		my $arg = shift;
		$self->update( $arg );
	}
	else {					# 引数無し→現在日時でプロパティを初期化
		$self->update( time );
	}
	return $self;
}

#--------------------------------------------------------------------
# ■ プロパティの更新
# r = DateTime->update()						# プロパティの設定値(年,月,日,時,分,秒)から、エポック秒と曜日のプロパティを更新
# r = DateTime->update( [@localtime] )			# localtime で得るリスト形式でプロパティを更新
# r = DateTime->update('yyyy/mm/dd hh:mm:ss')	# フォーマット文字列形式で更新
# r = DateTime->update( 1234567890 )			# エポック秒で更新
# r : ( -1=失敗(エラー), 1=成功 )
#
sub update {
	#printf "\n** < update [%s] >\n\n", Debug::_caller(caller);
	my $self = shift;
	my $arg = shift;

	# 引数なし→プロパティの値で更新
	unless ( defined($arg) ) {
		#print "\n\n** ( -- update -- )\n";

		# プロパティの値からエポック秒を取得
		my $epoch = $self->tl( [ $self->{second}, $self->{minute}, $self->{hour}, $self->{day}, $self->{month}, $self->{year} ] );
		#printf "*(upd)* epoch=(%d)\n", $epoch;

		# プロパティの値の検証
		# プロパティの値からエポック秒を取得し、そのエポック秒から取得した各要素の値と比較する。
		# 相違なければエポック秒は問題ないと判断できる。
		my %A;			# 作業用変数
		my $r = 0;		# 結果
		($A{second}, $A{minute}, $A{hour}, $A{day}, $A{month}, $A{year}, $A{weekday}) = $self->lt( $epoch );

		foreach my $k (sort(keys(%A))) {
			next if ($k eq 'weekday');											# weekday はチェック対象外（相違なければ最後に定義する）
			$r = (defined($self->{$k})) ? $r : ++$r;							# 値が設定されていない→エラー
			#printf "*** k=[%s],\tself=[%s],\tA=[%s]\n", $k, $self->{$k}, $A{$k};
			$r = ($self->{$k} eq $A{$k}) ? $r : ++$r;							# プロパティの値とサブルーチンの返り値が一致しない→エラー
		}
		#printf "** _r=(%d)\n", $r;

		# プロパティの値が無効（エラー）
		if ( $r ) {
			#print "** err\n";
			$self->{weekday} = '';												# 曜日のプロパティをヌルにする
			return 0;
		}

		# プロパティの値は有効
		else {
			#print "** noerr\n";
			#printf "** defined.epoch = (%d)\n", defined($self->{epoch});
			#printf "** defined.weekday = (%d)\n", defined($self->{weekday});

			$self->{epoch} = $epoch unless defined($self->{epoch});				# エポック秒が未定義なら定義
			$self->{weekday} = $A{weekday} unless defined($self->{weekday});	# 曜日のプロパティが未定義なら定義
			return 1;
		}

	}

	# 引数あり→プロパティの値を定義
	else {
		$self->_clear;

		# フォーマット文字列形式でプロパティを更新
		# エポック秒、曜日は未定義
		if ( $arg =~ /[\/:]+/) {
			#print "** ( -- format-string -- )\n";
			( $self->{year}, $self->{month}, $self->{day}, $self->{hour}, $self->{minute}, $self->{second} ) = $arg =~ /^(\d{4})\/(\d{2})\/(\d{2}) (\d{2})\:(\d{2})\:(\d{2})$/;
			$self->{hour} = $self->{hour} + 0;
			$self->{minute} = $self->{minute} + 0;
			$self->{second} = $self->{second} + 0;
			$self->{day} = $self->{day} + 0;
			$self->{month} = $self->{month} + 0;
			$self->{year} = $self->{year} + 0;
			$self->{year} = $self->{year} - 1900;
			--$self->{month};
			#print "\n\n\n";
			#print '[B1]'.Dumper($self);
			return $self->update();		# エポック秒を更新
		}

		# エポック秒からプロパティを更新;
		elsif ( $arg =~ /^\d+$/) {
			#print "** ( -- epoch -- )\n";
			$self->{epoch} = $arg;
			( $self->{second}, $self->{minute}, $self->{hour}, $self->{day}, $self->{month}, $self->{year}, $self->{weekday} ) = $self->lt( $arg );
			#print "\n\n\n";
			#print '[B2]'.Dumper($self);
			return 1;		# エポック秒から更新しているので更新不要
		}

		# localtimeリスト形式からプロパティを更新
		# エポック秒は未定義
		else {
			#print "** ( -- localtime-list -- )\n";
			( $self->{second}, $self->{minute}, $self->{hour}, $self->{day}, $self->{month}, $self->{year}, $self->{weekday} ) = @{ $arg };
			#print "\n\n\n";
			#print '[B3]'.Dumper($self);
			return $self->update();			# エポック秒を更新
		}

	}
}

#--------------------------------------------------------------------
# ■プロパティのクリア
# DateTime->_clear()
#
sub _clear {
	#printf "\n** < clear [%s] >\n\n", Debug::_caller(caller);
	my $self = shift;
	$self->{epoch}		= undef;
	$self->{second}		= undef;
	$self->{minute}		= undef;
	$self->{hour}		= undef;
	$self->{day}		= undef;
	$self->{month}		= undef;
	$self->{year}		= undef;
	$self->{weekday}	= undef;
}

#--------------------------------------------------------------------
# ■localtimeリスト形式からエポック秒に変換（timelocal）
# epoch = DateTime->tl( [@localtime] )
#
sub tl {
	#printf "\n** < tl [%s] >\n\n", Debug::_caller(caller);
	my $self = shift;
	my $ary = shift;
	return timelocal( @{ $ary } );
}

#--------------------------------------------------------------------
# ■エポック秒からlocaltimeリスト形式に変換（localtime）
# @localtime = DateTime->lt( epoch )
#
sub lt {
	#printf "\n** < lt [%s] >\n\n", Debug::_caller(caller);
	my $self = shift;
	my $epochTime = shift;
	return localtime( $epochTime );
}

#--------------------------------------------------------------------
# ■プロパティの値（エポック秒）を返す
# epoch = DateTime->epoch()
#
sub epoch {
	#printf "\n** < epoch [%s] >\n\n", Debug::_caller(caller);
	my $self = shift;
	return $self->{epoch};
}

#--------------------------------------------------------------------
# ■日付プロパティの値をフォーマット文字列に当てはめて返す
# DateTime->d("_年_月_日", y4, m2 ,d2 );		# '2018年01月31日'
#
sub d {
	#printf "\n** < d [%s] >\n\n", Debug::_caller(caller);
	my $self = shift;
	my $fmtText = shift;
	my @arg = @_;

	my @fmt = split('', $fmtText);
	my %_k = (
				'y' => 'year',
				'm' => 'month',
				'd' => 'day',
				'w' => 'weekday'
		);

	for my $i (0..$#fmt) {
		if ($fmt[$i] eq '_') {
			my $param = shift;
			my $func;
			foreach my $key (keys(%_k)) {

				if ($param =~ /$key/) {
					$func = $_k{$key};			# サブルーチン名
				}
			}
			#printf "i=[%d]\n", $i;
			#printf "fmt[i]=[%s]\n", $fmt[$i];
			#printf "func=[%s]\n", $func;
			#printf "param=[%s]\n", $param;

			if ($func) {
				$fmt[$i] = $self->$func($param);
			}
		}
		#print Dumper(@fmt)."\n";
	}
	return join('', @fmt);
}

#--------------------------------------------------------------------
# ■プロパティの値（年）を返す
# year = DateTime->year( arg );
#
#	4y		: '2018'
#	2y		: '18'
#	jh1y	: '30'
#	jh2y	: '30'
#	js1y	: '93'
#	js2y	: '93'
#
sub year {
	#printf "\n** < year [%s] >\n\n", Debug::_caller(caller);
	my $self = shift;
	my $arg = shift;
	my $y = $self->{year};		# 118	= 2018
	chop ($arg);

	my ($type, $col, $fG);
	my $ofs = 1900;
	if ( $arg =~ /(\w*)(\d{1})$/i ) { $type = $1; $col = $2 };			# 形とカラム数を得る
	#print "type=[$type], col=[$col]\n";
	$y = ($y + $ofs);						# 西暦4桁を基準にする

	if ($type =~ /jh/i) {					# 平成
		$fG++;
		$y = ($y - 1989) + 1;
	}
	elsif ($type =~ /js/i) {				# 昭和
		$fG++;
		$y = ($y - 1926) + 1;
	}

	if ($col eq '2') {					# 2桁
		if ($fG) {							# 元号
		$y = sprintf("%02d", $y);
		}
		else {								# 西暦
			$y =~ s/(.*)(\d{2})$/$2/;
		}
	}
	return $y;
}

#--------------------------------------------------------------------
# ■プロパティの値（月）を返す
# month = DateTime->month( arg );
#
#	1m	: '9'
#	2m	: '09'
#	e1m	: 'Sep'
#	e2m	: 'September'
#
sub month {
	#printf "\n** < month [%s] >\n\n", Debug::_caller(caller);
	my $self = shift;
	my $arg = shift;
	my $m = $self->{month};
	chop ($arg);

	my ($type, $col);
	if ( $arg =~ /(\w*)(\d{1})$/i ) { $type = $1; $col = $2 };			# 形とカラム数を得る
	# print "type=[$type], col=[$col]\n";

	if ($type =~ /e/i) {					# 英式
		my @list = (
			[ 'Jan', 'January' ],
			[ 'Feb', 'February' ],
			[ 'Mar', 'March' ],
			[ 'Apr', 'April' ],
			[ 'May', 'May' ],
			[ 'Jun', 'June' ],
			[ 'Jul', 'July' ],
			[ 'Aug', 'August' ],
			[ 'Sep', 'September' ],
			[ 'Oct', 'October' ],
			[ 'Nov', 'November' ],
			[ 'Dec', 'December' ]
		);

		if ($col == '1')
				{ $m = $list[ $m ]->[0] }				# 英式省略
		elsif ($col == '2')
				{ $m = $list[ $m ]->[1] };				# 英式標準
	}
	else {									# 数値
		$m = ($m + 1);
		if ($col == '2') {					# 英式省略
			$m = sprintf("%02d", $m);
		}
	}
	return $m;
}

#--------------------------------------------------------------------
# ■プロパティの値（日）を返す
# day = DateTime->day( arg );
#
#	1d	: '9'
#	2d	: '09'
#
sub day {
	#printf "\n** < day [%s] >\n\n", Debug::_caller(caller);
	my $self = shift;
	my $arg = shift;
	my $d = $self->{day}; 
	chop ($arg);

	my ($type, $col);
	if ( $arg =~ /(\w*)(\d{1})$/i ) { $type = $1; $col = $2 };			# 形とカラム数を得る
	# print "type=[$type], col=[$col]\n";

	if ($col == '2') {					# 2桁
		$d = sprintf("%02d", $d);
	}
	return $d;
}

#--------------------------------------------------------------------
# ■プロパティの値（曜日）を返す
# weekday = DateTime->weekday( arg );
#
#	'e1w'	: 'Mon'
#	'e2w'	: 'Monday'
#	'jw'	: '月'
#
sub weekday {
	#printf "\n** < weekday [%s] >\n\n", Debug::_caller(caller);
	my $self = shift;
	my $arg = shift;
	my $wd = $self->{weekday};
	my $r;
	chop ($arg);

	my ($type, $col);
	if ( $arg =~ /^(\w{1})(.*)/i ) { $type = $1; $col = $2 };			# 形とカラム数を得る
	# print "type=[$type], col=[$col]\n";

	if ($type =~ /j/i) {						# 和式
		$r = ${['日','月','火','水','木','金','土']}[ $wd ];
	}
	elsif ($type =~ /e/i) {
		my @list = (
			[ 'Sun', 'Sunday' ],
			[ 'Mon', 'Monday' ],
			[ 'Tue', 'Tuesday' ],
			[ 'Wed', 'Wednesday' ],
			[ 'Thu', 'Thursday' ],
			[ 'Fri', 'Friday' ],
			[ 'Sat', 'Saturday' ]
		);

		if ($col == '1')
				{ $r = $list[ $wd ]->[0] }				# 英式省略
		elsif ($col == '2')
				{ $r = $list[ $wd ]->[1] };				# 英式標準
	}
	return ($r) ? $r : '';
}

#--------------------------------------------------------------------
# ■時刻プロパティの値をフォーマット文字列に当てはめて返す
# DateTime->t("_時_分_秒", 2h24, m2 ,s2 );		# '20時01分31秒'
#
sub t {
	#printf "\n** < t [%s] >\n\n", Debug::_caller(caller);
	my $self = shift;
	my $fmtText = shift;
	my @arg = @_;

	my @fmt = split('', $fmtText);
	my %_k = (
				'h' => 'hour',
				'm' => 'minute',
				's' => 'second',
		);

	for my $i (0..$#fmt) {
		if ($fmt[$i] eq '_') {
			my $param = shift;
			my $func;
			foreach my $key (keys(%_k)) {

				if ($param =~ /$key/) {
					$func = $_k{$key};			# サブルーチン名
				}
			}
			#printf "i=[%d]\n", $i;
			#printf "fmt[i]=[%s]\n", $fmt[$i];
			#printf "func=[%s]\n", $func;
			#printf "param=[%s]\n", $param;

			if ($func) {
				$fmt[$i] = $self->$func($param);
			}
		}
		#print Dumper(@fmt)."\n";
	}
	return join('', @fmt);
}

#--------------------------------------------------------------------
# ■プロパティの値（時）を返す
# hour = DateTime->hour( arg );
#
#	1h24	: '8'
#	2h24	: '20'
#	1h12	: '8',
#	2h12	: '08'
#	eh		: 'PM'
#	jh		: '午後'
#
sub hour {
	#printf "\n** < hour [%s] >\n\n", Debug::_caller(caller);
	my $self = shift;
	my $arg = shift;
	my $h = $self->{hour};

	my ($arg1, $arg2) = split('h', $arg);
	# print "arg1=[$arg1], arg2=[$arg2]\n";

	if ( $arg2 =~ /[12|24]/ ) {
		if ($arg2 eq '12') {						# 12時制
			if ($h >= 12) { $h = ($h - 12) };
		}
		if ($arg1 eq '2') {							# 2桁
			$h = sprintf("%02d", $h);
		}
	}
	else {
		if ($arg1 =~ /e/i) {						# 米式
			$h = ($h < 12) ? 'AM' : 'PM';
		}
		elsif ($arg1 =~ /j/i) {						# 和式
			$h = ($h < 12) ? '午前' : '午後';
		}
	}
	return $h;
}

#--------------------------------------------------------------------
# ■プロパティの値（分）を返す
# minute = DateTime->minute( arg );
#
#	1m		: '9'
#	2m		: '09'
#
sub minute {
	#printf "\n** < minute [%s] >\n\n", Debug::_caller(caller);
	my $self = shift;
	my $arg = shift;
	my $m = $self->{minute};

	if ($arg =~ /2/) {							# 2桁
		$m = sprintf("%02d", $m);
	}
	return $m;
}

#--------------------------------------------------------------------
# ■プロパティの値（秒）を返す
# second = DateTime->second( arg );
#
#	1s		: '9'
#	2s		: '09'
#
sub second {
	#printf "\n** < second [%s] >\n\n", Debug::_caller(caller);
	my $self = shift;
	my $arg = shift;
	my $s = $self->{second};

	if ($arg =~ /2/) {							# 2桁
		$s = sprintf("%02d", $s);
	}
	return $s;
}

#--------------------------------------------------------------------
# ■時刻プロパティの値を設定（値の検証なし）
# DateTime->setTime( 'hms', 4, 2, 2 );		# 時分秒を設定
# DateTime->setTime( 'hm', 4, 2 );			# 時分を設定
#
sub setTime {
	#printf "\n** < setTime [%s] >\n\n", Debug::_caller(caller);
	my $self = shift;
	my $keyList = shift;
	my @arg = @_;

	my @keys = split('', $keyList);
	my %_k = (
				'h' => 'hour',
				'm' => 'minute',
				's' => 'second'
		);

	foreach my $k (@keys) {
		my $v = shift @arg;
		if (exists($self->{$_k{$k}})) {
			$self->{$_k{$k}} = $v;
		}
	}
}

#--------------------------------------------------------------------
# ■日付プロパティの値を設定（値の検証なし）
# DateTime->setDate( 'ymd', 2018, 1, 1 );	# 年月日を設定
# DateTime->setDate( 'md', 1, 1 );			# 月日を設定
#
sub setDate {
	#printf "\n** < setDate [%s] >\n\n", Debug::_caller(caller);
	my $self = shift;
	my $keyList = shift;
	my @arg = @_;

	my @keys = split('', $keyList);
	my %_k = (
				'y' => 'year',
				'm' => 'month',
				'd' => 'day'
		);

	foreach my $k (@keys) {
		my $v = shift @arg;
		if ($k eq 'y') {
			$v = $v - 1900;
		}
		elsif ($k eq 'm') {
			return -1 if ($v <= -1 && $v >= 12);
			$v = $v - 1;
		}
		if (exists($self->{$_k{$k}})) {
			$self->{$_k{$k}} = $v;
		}
	}
}

#--------------------------------------------------------------------
# ■どのくらい経過した？
# ( hour, minute, second ) = DateTime->elapse( nowEpoch, pastEpoch );
#
sub elapse {
	#printf "\n(elapse)\n";				# ????
	my $self = shift;
	my $nowEpoch = shift;					# 終点となる時刻
	my $pastEpoch = shift;					# 起点となる時刻
	my $el = $nowEpoch - $pastEpoch;		# 経過時間
		#printf "el=(%d)\n\n", $el;
	my $h = ($el - ($el % 3600)) / 3600;
		#printf "h=(%d)\n", $h;
	my $m = ($el % 3600 - (($el % 3600) % 60)) / 60;
		#printf "m=(%d)\n", $m;
	my $s = $el - (($h * 3600) + ($m * 60));
		#printf "s=(%d)\n", $s;
	return ( $h, $m, $s );		# n時間 n分 n秒
}

#--------------------------------------------------
# ■日付文字列をチェック
# r = validate( [:|-], dateTime );
#
# 取得した文字列で曜日が取得できたり、シリアル時間が取得できたらパスとする
#
#	セパレータ文字			:          ':'	= 日時文字列を「時刻」として評価
#							: '/'または'-'	= 日時文字列を「時刻」として評価
#	日付時刻文字列			: (日付)		= 'yy/mm/dd'
#							: (時刻)		= 'hh:mm:ss'
#	返り値					: undef=異常, 1=正常
#
sub validate {
	print "\n(validate)\n";
	my $self	= shift;
	my $sp		= shift;		# セパレータ
	my $dt		= shift;		# 日付または時間の文字列
	my $rcType	= shift;		# リターンコードタイプ
	my @rAry;					# 検証結果

	#printf "** dt=[%s]\n", $dt;

	my $dateTime = new Naoit0::DateTime();

	# 時刻の検証
	if ($sp =~ /-|\//) {
		push (@rAry, encode('cp932', 'フォーマットに従っていない')) unless ($dt =~ /^(\d{2}|\d{4})$sp\d{2}$sp\d{2}$/);
		#print Dumper(@rAry);
		if ($#rAry == -1) {
			my ($y, $m, $d) = map { (sprintf("%d", $_) + 0) } split(/$sp/, $dt);							# dt='17-12-31' or '2017-12-31'
			#printf "** y=(%d), m=(%d), d=(%d)\n",$y,$m,$d;
			if ($y <= 99)																					# 2桁指定の場合、年の値の範囲は1960-2059
				{ $y = ($y < 60 && $y >= 0) ? ($y+2000) : ($y+1900) };
			#printf "** y=(%d), m=(%d), d=(%d)\n",$y,$m,$d;

			push (@rAry, encode('cp932', '年の値が範囲外（1950～）')) if ($y < 1950);
			push (@rAry, encode('cp932', '月の値が範囲外（1～12）')) if ($m < 1 || $m > 12);

			if ("$m" =~ /^(?:(4|6|9|11))$/)
				{ push (@rAry, encode('cp932', '日の値が範囲外(1～30)')) if ($d < 1 || $d > 30) }			# 西向く士の月
			else
				{ push (@rAry, encode('cp932', '日の値が範囲外(1～31)')) if ($d < 1 || $d > 31) }			# サーティーワンアイスの月

			if ($m == 2) {
				if ( ($y % 4) == 0)							# うるう年
					{ push (@rAry, encode('cp932', '日の値が範囲外(1～29)')) if ($d < 1 || $d > 29) }
				else
					{ push (@rAry, encode('cp932', '日の値が範囲外(1～28)')) if ($d < 1 || $d > 28) }
			}

			if ($#rAry == -1) {
				$dateTime->setDate("ymd", $y, $m, $d);
				$dateTime->update();
				push (@rAry, encode('cp932', 'いずれかの値が不正のため曜日を取得できない')) if ( $dateTime->weekday('jw') eq '' );		# 曜日を取得できなかった
			}
		}
		return 1 if ( Naoit0::DateTime::_validateReturn($rcType, @rAry) );
		return;
	}

	# 時刻の検証
	elsif ($sp eq ':') {
		push (@rAry, encode('cp932', 'フォーマットに従っていない')) unless ($dt =~ /^\d{2}$sp\d{2}+$sp\d{2}$/);
		if ($#rAry == -1) {
			my ($h, $m, $s) = map { (sprintf("%d", $_) + 0) } split(/$sp/, $dt);		# dt='23:59:59'
			#printf "** h=(%d), m=(%d), s=(%d)\n",$h,$m,$s;
			push (@rAry, encode('cp932', '時の値が範囲外(1～23)'))  if ($h < 0 || $h > 23);
			push (@rAry, encode('cp932', '分の値が範囲外(1～59)'))  if ($m < 0 || $m > 59);
			push (@rAry, encode('cp932', '秒の値が範囲外(1～59)'))  if ($s < 0 || $s > 59);

			if ($#rAry == -1) {
				$dateTime->setTime("hms", $h, $m, $s);
				$dateTime->update();
				push (@rAry, encode('cp932', 'いずれかの値が不正のため曜日を取得できない')) if ( $dateTime->epoch() <= 0 )				# エポック秒を取得できなかった
			}
		}
		return 1 if ( Naoit0::DateTime::_validateReturn($rcType, @rAry) );
		return;
	}

}

#--------------------------------------------------
# ■処理結果を返す
#
sub _validateReturn {
	my $rcType = shift;
	my @rAry = @_;

	if ( defined($rcType) ) {
		if ($rcType == 1) { return @rAry }					# エラーメッセージリストを返す
		elsif ($rcType == 2) { return ($#rAry + 1) };		# エラーカウントを返す
	}
	if ($#rAry > -1) { return 0 }
	else { return 1 };
}

#------------------------------------------------------
# ●現在時刻を返す
# r = now();
#
sub now {
	my $self = shift;
	return $self->d("_-_-_", "4y", "2m", "2d").' '.$self->t("_:_:_", "2h24", "2m", "2s");
}

#=========================================
#package Test;
#use utf8;
#use Encode qw( encode decode );
#use Data::Dumper;
#
#binmode STDOUT, ':encoding(cp932)';
#
#sub main {
#
##---------------------------------------------------
##	# どのくらい経った？
##		my $dt3 = new Naoit0::DateTime();
##
##	## 時間の差を求める→時間,分,秒の配列で返す
##		my $el = (3600 * 118) + (60 * 7) + (32);			# 67532
##		my @elap1 = $dt3->elapse( 1517400355 + $el , 1517400355);
##		printf "el=[%s]\n", join(':', map { sprintf "%02d", $_ } @elap1 );											# '18:45:32'
##		printf "el=[%s]\n", join(':', map { sprintf "%02d", $_ } $dt3->elapse( 1517400355 + $el , 1517400355) );	# この書き方でもＯＫみたい			# '18:45:32'
#
##---------------------------------------------------
## 動作テスト
##---------------------------------------------------
#	# コンストラクタを呼び出す
#		#my $d = new Naoit0::DateTime();		# 引数なしでプロパティを生成
#		#print Dumper($d)."\n";
#
#	# 現在日時をエポック秒で返す
#		#my $nowEpoch = $dateTime->epoch;
#		#printf "*** nowEpoch=(%d)\n", $nowEpoch;
#
#	# 現在日時をフォーマット文字列で置換して返す
#		#printf "[%s]\n", $dateTime->t("_:_:_", "2h24", "2m", "2s");
#		#printf "[%s]\n", $dateTime->d('_/_/_', "2y", "2m", "2d");
#
#	## プロパティを見る(1)
#	#	my $epoch = 1524128590;
#	#	my $dt1 = new Naoit0::DateTime( $epoch );		# エポック秒でdt1を生成
#	#	print Dumper($dt1)."\n";
#    #
#	## プロパティを見る(2)
#	#	my @array = localtime( $dt1->epoch );			# dt1のエポック秒をlocaltimeリスト形式に変換
#	#	my $dt2 = new Naoit0::DateTime( [ @array ] );		# 変換したものでdt2を生成
#	#	print Dumper($dt2)."\n";
#    #
#	## プロパティを見る(3)
#	#	my $fmtStr = sprintf "%s %s", $dt2->d('_/_/_', "4y", "2m", "2d"), $dt2->t("_:_:_", "2h24", "2m", "2s");		# dt2のプロパティをフォーマット文字列にあてる
#	#	printf "fmtStr=[%s]\n", $fmtStr;		# '2018/04/19 18:03:10';
#	#	my $dt3 = new Naoit0::DateTime( $fmtStr );		# あてた文字列でdt3を生成
#	#	print Dumper($dt3)."\n";
#
#
##---------------------------------------------------
##	my $t3 = '2018/12/01 21:05:07';									# epoch = 1517400355;
##	my $dt3 = new Naoit0::DateTime($t3);
##		print $dt3->d("_年_月_日 (_)", '4y', '2m', '2d', 'jw');
##		print $dt3->t(" _時_分_秒", '2h24', '2m', '2s')."\n";			# '20時01分31秒'
##		print $dt3->t("(_) _時_分_秒", 'jh', '1h12', '1m', '1s')."\n";		# '(午後) 9時5分7秒'
##
##	$dt3->update(1517400355 - (((60 * 60) * 24) * 4));
##		print $dt3->d("_年_月_日 (_)", '4y', '2m', '2d', 'jw');
##		print $dt3->t(" _時_分_秒", '2h24', '2m', '2s')."\n";		# '20時01分31秒'
##
##
##	$dt3->update(1517400355);
##	printf "epoch=(%d)\n", $dt3->update();
##
##		print $dt3->d("_年_月_日 (_)", '4y', '2m', '2d', 'jw');
##		print $dt3->t(" _時_分_秒", '2h24', '2m', '2s')."\n";		# '20時01分31秒'
##
##
##	$dt3->setDate( 'ymd', 2018, 1, 1 );								# 2018年1月1日に設定
##	printf "epoch=(%d)\n", $dt3->update();
##		print $dt3->d("_年_月_日 (_)", '4y', '2m', '2d', 'jw');
##		print $dt3->t(" _時_分_秒", '2h24', '2m', '2s')."\n";		# '20時01分31秒'
##	# print Dumper($dt3);
##
#
##---------------------------------------------------
## 表示できる種類
##---------------------------------------------------
##	my $d = new Naoit0::DateTime( time );					#	引数なし: 現在の日時
##	print Dumper($d);
##
##	print "\n";
##	print "** year\n";
##	printf "4y=[%s], 2y=[%s]\n", $d->year('4y'), $d->year('2y');
##	printf "平成: jh1y=[%s], jh2y=[%s]\n", $d->year('jh1y'), $d->year('jh2y');
##	printf "昭和: js1y=[%s], js2y=[%s]\n",$d->year('js1y'),$d->year('js2y');
##
##	print "\n";
##	print "** month\n";
##	printf "1m=[%s], 2m=[%s]\n", $d->month('1m'), $d->month('2m');
##	printf "e1m=[%s], e2m=[%s]\n", $d->month('e1m'), $d->month('e2m');
##
##	print "\n";
##	print "** day\n";
##	printf "1d=[%s], 2d=[%s]\n", $d->day('1d'), $d->day('2d');
##
##	print "\n";
##	print "** weekday\n";
##	printf "e1w=[%s], e2w=[%s], jw=[%s]\n", $d->weekday('e1w'), $d->weekday('e2w'), $d->weekday('jw');
##
##	print "\n";
##	print "** hour\n";
##	printf "1h24=[%s], 1h12=[%s]\n", $d->hour('1h24'), $d->hour('1h12');
##	printf "2h24=[%s], 2h12=[%s]\n", $d->hour('2h24'), $d->hour('2h12');
##	printf "eh=[%s], jh=[%s]\n", $d->hour('eh'), $d->hour('jh');
##
##	print "\n";
##	print "** minute\n";
##	printf "1m=[%s], 2m=[%s]\n", $d->minute('1m'), $d->minute('2m');
##
##	print "\n";
##	print "** second\n";
##	printf "1s=[%s], 2s=[%s]\n", $d->second('1s'), $d->second('2s');
##
##	print "\n\n";
#
##---------------------------------------------------
## いろいろな表示方法
##
##	my $d = new Naoit0::DateTime( time );							#	引数なし: 現在の日時
##	print Dumper($d);
##
##	print $d->d("_/_/_", '4y', '2m', '2d')."\n";					# '2018/01/31'
##	print $d->d("_年_月_日 (_)", '4y', '2m', '2d', 'jw')."\n";		# '2018年01月31日 (日)'
##	print $d->d("_, _ _ _", 'e2w', 'e2m', '2d', '4y')."\n";			# 'Wednesday, March 21 2012' (アメリカ式表記)
##	print $d->d("_/_/_", '2m', '2d', '2y')."\n";					# '01/31/18'
##	print $d->d("_-_-_", 'e1m', '1d', '2y')."\n";					# 'Jan-31-18'
##	print $d->d("_ _ _", 'e1w', 'e1m', '2d');						# Sat Oct 30 11:40:37 2010
##	print $d->t(" _:_:_", '2h24', '2m', '2s');						#
##	print $d->d(" _", '4y')."\n";									#
#
#}
#main();

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;