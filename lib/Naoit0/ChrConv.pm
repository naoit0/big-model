package Naoit0::ChrConv;
our $VERSION = "----.20190124.0137";		# 再構築
our $VERSION = "0001.20190129.0400";		# 完成

use utf8;
use Encode;
use Data::Dumper;

#-----------------------------------------
# ■インスタンス
#
sub new {
	my $class = shift;
	my $self = {};
	bless($self, $class);
	$self->_init();
	return $self;
}

#-----------------------------------------
# ■属性の初期化
#
sub _init {
	my $self = shift;

	#-----------------------------------
	# データテーブル
	#-----------------------------------

	# ■制御コード
	my @ctrlcode_bycode = (
		"\x00 \x01 \x02 \x03 \x04 \x05 \x06 \x07 \x08 \x09 \x0a \x0b",
		"\x0c \x0d \x0e \x0f \x10 \x11 \x12 \x13 \x14 \x15 \x16 \x17",
		"\x18 \x19 \x1a \x1b \x1c \x1d \x1e \x1f",
		"\x7f",
	);

	# ■制御コード名
	my @ctrlcode_byname = (
		'NUL  SOH  STX  ETX  EOT  ENQ  ACK  BEL  BS   HT   LF   VT  ',
		'FF   CR   SO   SI   DLE  DC1  DC2  DC3  DC4  NAK  SYN  ETB ',
		'CAN  EM   SUB  ESC  FS   GS   RS   US  ',
		'DEL',
	);

	# ■アルファベット（大文字）【半角】
	my @alphabet_big_halfwidth = (
		'A B C D E F G H I J K L M',
		'N O P Q R S T U V W X Y Z',
	);

	# ■アルファベット（大文字）【全角】
	my @alphabet_big_fullwidth = (
		'Ａ Ｂ Ｃ Ｄ Ｅ Ｆ Ｇ Ｈ Ｉ Ｊ Ｋ Ｌ Ｍ',
		'Ｎ Ｏ Ｐ Ｑ Ｒ Ｓ Ｔ Ｕ Ｖ Ｗ Ｘ Ｙ Ｚ',
	);

	# ■アルファベット（小文字）【半角】
	my @alphabet_small_halfwidth = (
		'a b c d e f g h i j k l m',
		'n o p q r s t u v w x y z',
	);

	# ■アルファベット（小文字）【全角】
	my @alphabet_small_fullwidth = (
		'ａ ｂ ｃ ｄ ｅ ｆ ｇ ｈ ｉ ｊ ｋ ｌ ｍ',
		'ｎ ｏ ｐ ｑ ｒ ｓ ｔ ｕ ｖ ｗ ｘ ｙ ｚ',
	);

	# ■数字【半角】
	my @number_halfwidth = (
		'0 1 2 3 4 5 6 7 8 9',
	);

	# ■数字【全角】
	my @number_fullwidth = (
		'０ １ ２ ３ ４ ５ ６ ７ ８ ９',
	);

	# ■記号【半角】
	my @symbol_halfwidth = (
		'!', '"', '#', '$', '%', '&', "\'",
		'(', ')', '*', '+', ',', '-', '.',
		'/', ':', ';', '<', '=', '>', '?',
		'@', '[', "\\", ']', '^', '_', "\`",
		'{', '|', '}', '~', "\\",
		'~',												# ※'~'で代用(ＣＰ９３２未定義キャラクタ)
	);

	# ■記号【全角】
	my @symbol_fullwidth = (
		'！', '＂', '＃', '＄', '％', '＆', '＇',
		'（', '）', '＊', '＋', '，', '－', '．',
		'／', '：', '；', '＜', '＝', '＞', '？',
		'＠', '［', '＼', '］', '＾', '＿', '｀',
		'｛', '｜', '｝', '～', '￥',
		'〜', 
	);

	# ■カタカナ（濁音・半濁音）【半角】
	my @katakana1_halfwidth = (
		'ｶﾞ ｷﾞ ｸﾞ ｹﾞ ｺﾞ ｻﾞ ｼﾞ ｽﾞ ｾﾞ ｿﾞ',
		'ﾀﾞ ﾁﾞ ﾂﾞ ﾃﾞ ﾄﾞ ﾊﾞ ﾋﾞ ﾌﾞ ﾍﾞ ﾎﾞ',
		'ﾊﾟ ﾋﾟ ﾌﾟ ﾍﾟ ﾎﾟ',
		'ｳﾞ',
	);

	# ■カタカナ（濁音・半濁音）【全角】
	my @katakana1_fullwidth = (
		'ガ ギ グ ゲ ゴ ザ ジ ズ ゼ ゾ',
		'ダ ヂ ヅ デ ド バ ビ ブ ベ ボ',
		'パ ピ プ ペ ポ',
		'ヴ',
	);

	# ■カタカナ（濁音・半濁音）【全角２文字】
	my @katakana1_fullwidth2 = (
		'カ゛ キ゛ ク゛ ケ゛ コ゛ サ゛ シ゛ ス゛ セ゛ ソ゛',
		'タ゛ チ゛ ツ゛ テ゛ ト゛ ハ゛ ヒ゛ フ゛ ヘ゛ ホ゛',
		'ハ゜ ヒ゜ フ゜ ヘ゜ ホ゜',
		'ウ゛',
	);

	# ■カタカナ（一般文字）【半角】
	my @katakana2_halfwidth = (
		'ｱ ｲ ｳ ｴ ｵ ｶ ｷ ｸ ｹ ｺ',
		'ｻ ｼ ｽ ｾ ｿ ﾀ ﾁ ﾂ ﾃ ﾄ',
		'ﾅ ﾆ ﾇ ﾈ ﾉ ﾊ ﾋ ﾌ ﾍ ﾎ',
		'ﾏ ﾐ ﾑ ﾒ ﾓ ﾔ ﾕ ﾖ',
		'ﾗ ﾘ ﾙ ﾚ ﾛ ﾜ ｦ ﾝ',
		'ｧ ｨ ｩ ｪ ｫ ｯ ｬ ｭ ｮ ﾜ',
	);

	# ■カタカナ（一般文字）【半角】（ＣＰ９３２）
	my @katakana2_halfwidth_cp932 = (
		"\xb1 \xb2 \xb3 \xb4 \xb5 \xb6 \xb7 \xb8 \xb9 \xba",
		"\xbb \xbc \xbd \xbe \xbf \xc0 \xc1 \xc2 \xc3 \xc4",
		"\xc5 \xc6 \xc7 \xc8 \xc9 \xca \xcb \xcc \xcd \xce",
		"\xcf \xd0 \xd1 \xd2 \xd3 \xd4 \xd5 \xd6",
		"\xd7 \xd8 \xd9 \xda \xdb \xdc \xa6 \xdd",
		"\xa7 \xa8 \xa9 \xaa \xab \xaf \xac \xad \xae \xdc",
	);

	# ■カタカナ（一般文字）【全角】
	my @katakana2_fullwidth = (
		'ア イ ウ エ オ カ キ ク ケ コ',
		'サ シ ス セ ソ タ チ ツ テ ト',
		'ナ ニ ヌ ネ ノ ハ ヒ フ ヘ ホ',
		'マ ミ ム メ モ ヤ ユ ヨ',
		'ラ リ ル レ ロ ワ ヲ ン',
		'ァ ィ ゥ ェ ォ ッ ャ ュ ョ ヮ',
	);

	# ■カタカナ（濁音・半濁音記号）【半角】
	my @katakana3_halfwidth = (
		'ﾞ ﾟ',
	);

	# ■カタカナ（濁音・半濁音記号）【半角】（ＣＰ９３２）
	my @katakana3_halfwidth_cp932 = (
		"\xde \xdf",
	);

	# ■カタカナ（濁音・半濁音記号）【全角】
	my @katakana3_fullwidth = (
		'゛ ゜',
	);

	# ■カタカナ（記号）【半角】
	my @katakana4_halfwidth = (
		'｡ ｢ ｣ ､ ･ ｰ',
	);

	# ■カタカナ（記号）【半角】（ＣＰ９３２）
	my @katakana4_halfwidth_cp932 = (
		"\xa1 \xa2 \xa3 \xa4 \xa5 \xb0",
	);

	# ■カタカナ（記号）【全角】
	my @katakana4_fullwidth = (
		'。 「 」 、 ・ ー',
	);

	# ■カタカナ（旧文字）【半角】（※旧文字変換用）
	my @katakana5_halfwidth = (
		'ｲ ｴ',
	);

	# ■カタカナ（旧文字）【全角】（※旧文字変換用）
	my @katakana5_fullwidth = (
		'イ エ',
	);

	# ■カタカナ（旧文字）【全角】（※旧文字変換用）
	my @katakana5_old_fullwidth = (
		'ヰ ヱ',
	);

	# ■ひらがな（濁音・半濁音）【全角】
	my @hiragana1_fullwidth = (
		'が ぎ ぐ げ ご ざ じ ず ぜ ぞ',
		'だ ぢ づ で ど ば び ぶ べ ぼ',
		'ぱ ぴ ぷ ぺ ぽ',
	);

	# ■ひらがな（濁音・半濁音）【全角２文字】
	my @hiragana1_fullwidth2 = (
		'か゛ き゛ く゛ け゛ こ゛ さ゛ し゛ す゛ せ゛ そ゛',
		'た゛ ち゛ つ゛ て゛ と゛ は゛ ひ゛ ふ゛ へ゛ ほ゛',
		'は゜ ひ゜ ふ゜ へ゜ ほ゜',
		'う゛',
	);

	# ■ひらがな（一般文字）【全角】
	my @hiragana2_fullwidth = (
		'あ い う え お か き く け こ',
		'さ し す せ そ た ち つ て と',
		'な に ぬ ね の は ひ ふ へ ほ',
		'ま み む め も や ゆ よ',
		'ら り る れ ろ わ を ん',
		'ぁ ぃ ぅ ぇ ぉ っ ゃ ゅ ょ ゎ',
	);

	# ■ひらがな（旧文字）【全角】（※旧文字変換用）
	my @hiragana3_old_fullwidth = (
		'ゐ ゑ',  
	);

	# テーブルリスト
	$self->{tbl} = {
		'ctrlcode_bycode'			=> \@ctrlcode_bycode,					# '\x00'	# 制御コード
		'ctrlcode_byname'			=> \@ctrlcode_byname,					# 'NUL'		# 制御コード名

		'alphabet_big_halfwidth'	=> \@alphabet_big_halfwidth,			# 'A'		# アルファベット（大文字）【半角】
		'alphabet_big_fullwidth'	=> \@alphabet_big_fullwidth,			# 'Ａ'		# アルファベット（大文字）【全角】
		'alphabet_small_halfwidth'	=> \@alphabet_small_halfwidth,			# 'a'		# アルファベット（小文字）【半角】
		'alphabet_small_fullwidth'	=> \@alphabet_small_fullwidth,			# 'ａ'		# アルファベット（小文字）【全角】

		'number_halfwidth'			=> \@number_halfwidth,					# '0'		# 数字【半角】
		'number_fullwidth'			=> \@number_fullwidth,					# '０'		# 数字【全角】

		'symbol_halfwidth'			=> \@symbol_halfwidth,					# '!'		# 記号【半角】
		'symbol_fullwidth'			=> \@symbol_fullwidth,					# '！'		# 記号【全角】

		'katakana1_halfwidth'		=> \@katakana1_halfwidth,				# 'ｶﾞ'		# カタカナ（濁音・半濁音付）【半角】
		'katakana1_fullwidth'		=> \@katakana1_fullwidth,				# 'ガ'		# カタカナ（濁音・半濁音付）【全角】
		'katakana1_fullwidth2'		=> \@katakana1_fullwidth2,				# 'カ゛'	# カタカナ（濁音・半濁音付）【全角２文字】
		'katakana2_halfwidth'		=> \@katakana2_halfwidth,				# 'ｱ'		# カタカナ（一般文字）【半角】
		'katakana2_halfwidth_cp932'	=> \@katakana2_halfwidth_cp932,			# 'ｱ'		# カタカナ（一般文字）【半角】（ＣＰ９３２）
		'katakana2_fullwidth'		=> \@katakana2_fullwidth,				# 'ア'		# カタカナ（一般文字）【全角】
		'katakana3_halfwidth'		=> \@katakana3_halfwidth,				# 'ﾞ'		# カタカナ（濁音・半濁音記号）【半角】
		'katakana3_halfwidth_cp932'	=> \@katakana3_halfwidth_cp932,			# 'ﾞ'		# カタカナ（濁音・半濁音記号）【半角】（ＣＰ９３２）
		'katakana3_fullwidth'		=> \@katakana3_fullwidth,				# '゛'		# カタカナ（濁音・半濁音記号）【全角】
		'katakana4_halfwidth'		=> \@katakana4_halfwidth,				# '｡'		# カタカナ（記号）【半角】
		'katakana4_halfwidth_cp932'	=> \@katakana4_halfwidth_cp932,			# '｡'		# カタカナ（記号）【半角】（ＣＰ９３２）
		'katakana4_fullwidth'		=> \@katakana4_fullwidth,				# '。'		# カタカナ（記号）【全角】

		'katakana5_halfwidth'		=> \@katakana5_halfwidth,				# 'ｲ'		# カタカナ（旧文字）【半角】（※旧文字変換用）
		'katakana5_fullwidth'		=> \@katakana5_fullwidth,				# 'イ'		# カタカナ（旧文字）【全角】（※旧文字変換用）
		'katakana5_old_fullwidth'	=> \@katakana5_old_fullwidth,			# 'ヰ'		# カタカナ（旧文字）【全角】（※旧文字変換用）

		'hiragana1_fullwidth'		=> \@hiragana1_fullwidth,				# 'が'		# ひらがな（濁音・半濁音付文字）【全角】
		'hiragana1_fullwidth2'		=> \@hiragana1_fullwidth2,				# 'か゛'	# ひらがな（濁音・半濁音付文字）【全角２文字】
		'hiragana2_fullwidth'		=> \@hiragana2_fullwidth,				# 'あ'		# ひらがな（一般文字）【全角】

		'hiragana3_old_fullwidth'	=> \@hiragana3_old_fullwidth,			# 'ゐ'		# ひらがな（旧文字）【全角】（※旧文字変換用）
	};

	# テーブルデータの一括整形
	foreach my $key ( keys(%{ $self->{tbl} }) ) {
		#printf "key=(%s)\n", $key;											# ???? 
		 $self->reformTable( $self->{tbl}->{$key} );						# テーブルのデータを共通形式に整形
	}

	#



	#print map { sprintf("(%d:%s) [%s]\n", ord($_), sprintf("%02X", ord($_)), $_); } @ctrlcode_bycode;			# ????

	#foreach my $key ( keys(%{ $self->{tbl} }) ) {							# ???? 確認用
	#	my $a = $self->{tbl}->{$key};
	#	print "[$key]\n".encode('cp932', join('|', @{$a}))."\n";			# ????
	#	print "[$key]\n".Dumper(@{$a})."\n";								# ????
	#}

	# 拡張テーブル
	# ＣＰ９３２－ＵＴＦ８半角カタカナ置換用
	#my @katakana1_halfwidth_cp932 = @katakana1_halfwidth;						# 'ｶﾞ'		# カタカナ（濁音・半濁音付）【半角】
	#$self->reformTable( \@katakana1_halfwidth_cp932, 'cp932' );					# テーブルのデータをＣＰ９３２にエンコードして整形
	#$self->{tbl}->{'katakana1_halfwidth_cp932'} = \@katakana1_halfwidth_cp932;	# データテーブルリファレンスをテーブルリストに登録

	#my @katakana2_halfwidth_cp932 = @katakana2_halfwidth;						# 'ｱ'		# カタカナ（一般文字）【半角】
	#$self->reformTable( \@katakana2_halfwidth_cp932, 'cp932' );					
	#$self->{tbl}->{'katakana2_halfwidth_cp932'} = \@katakana2_halfwidth_cp932;	

	#my @katakana3_halfwidth_cp932 = @katakana3_halfwidth;						# 'ﾞ'		# カタカナ（濁音・半濁音記号）【半角】
	#$self->reformTable( \@katakana3_halfwidth_cp932, 'cp932' );					
	#$self->{tbl}->{'katakana3_halfwidth_cp932'} = \@katakana3_halfwidth_cp932;	

	#my @katakana4_halfwidth_cp932 = @katakana4_halfwidth;						# '｡'		# カタカナ（記号）【半角】
	#$self->reformTable( \@katakana4_halfwidth_cp932, 'cp932' );				  	
	#$self->{tbl}->{'katakana4_halfwidth_cp932'} = \@katakana4_halfwidth_cp932;	

	#print Dumper( $self->{tbl}->{'ctrlcode'} )."\n";							# ???? 取り込み後
}

#---------------------------------------------
# ■データテーブルのデータ整形
# reformTable( \@table_name, encode_type )
#
# テーブルのデータを配列型に変換する
#
sub reformTable {
	#print "\n(reformTable)\n";			# ???? 
	my $self = shift;
	my $t = shift;						# データテーブルのリファレンス
	#print Dumper(@{$t})."\n";			# ???? 整形前
	my $enctype = shift;				# エンコード形式

	my @tbl = @{$t};					# テーブルの複製
	@{$t} = ();							# テーブルの消去
	while (@tbl) {
		my @a;															# 作業配列
			$a[0] = shift @tbl;												# データを取り込む
			$a[0] = encode($enctype, $a[0]) if ( defined($enctype) );		# エンコード形式が定義されていたら、データをエンコードする
			$a[0] =~ s/\x20+/ /g;
		@a = split(/ /, $a[0]);
		while(@a) {
			my $x = shift(@a);
			push( @{$t}, $x );
		}
	}
	#print Dumper(@{$t})."\n";			# ???? 整形後
	#print "(/reformTable)\n";			# ???? 
}

#-------------------------------------------
# ■置換処理
# r = replace( str, \@to, \@from1, \@from2, \@from... );
#
#	   r: 置換後文字列
#	 str: 置換対象文字列
#	from: 評価テーブル
#			評価テーブルは複数指定が可能で、指定したいずれかのテーブルに該当している場合に置換テーブルの文字で置換が行われる。
#     to: 置換テーブル
#			置換テーブルは１つのみ。
#
sub replace {
	#print "\n(replace)\n";		# ???? 
	my $self = shift;
	my $str = shift;			# 置換対象文字列
	my $to = shift;				# 置換テーブル

	my @froms = @_;				# 評価テーブル

	#printf "from=(%s)\n", join(', ', @froms);						# ????
	#printf "to=(%s)\n", $to;										# ????

	for my $i (0..$#{ $self->{tbl}->{$to} }) {						# 置換テーブル
		my $b = $self->{tbl}->{$to}->[$i];
		#printf "%s.b=(%s)\n", $to, $b;								# ????

		foreach my $from (@froms) {									# 評価テーブルリスト
			my $a = $self->{tbl}->{$from}->[$i];
			#printf "%s.a=(%s)\n", $from, $a;						# ????
			next unless ( defined($b) && defined($a) );				# 評価文字と置換文字が両方定義されていなければ置換を行わない。
			$str =~ s/\Q$a\E/$b/ge;
			#print "+++\n", if ( $str =~ s/\Q$a\E/$b/ge );			# ????
		}
	}
	#print "(/replace)\n";		# ????
	return $str;
}

#-------------------------------------------
# ■ 文字列を文字コードに変換
# r = toHex( str , tocode )
#
#    str : 評価対象の文字列
# tocode : ( 1=制御文字に置換 )
#      r : [ 31, 32, 33, 34, 35 ]
#
# ※動作検証用。汎用性なし。
#
sub toHex {
	#print "(toHex)\n";				# ????
	my $self = shift;
	my $str = shift;
	my $tocode = shift;

	my $to = 'ctrlcode_byname';					# 置換テーブル
	my $from = 'ctrlcode_bycode';				# 評価テーブル

	my @r;
	my @strs = split(//, $str);					# 文字列を配列にする
		#print "[str]".Dumper(@strs)."\n";		# ????

	foreach my $chr ( @strs ) {
		my $_ord = ord($chr);									# 文字コード
		$_ord = $_ord + 14897472 if ($_ord > 255);				# 文字コードが255を超えたらUTF8のコードに変換
		#printf "chr.ord=(%d)\n", $_ord;						# ???? 文字コードの値

		my $_chr;												# 置換後
		if ( defined($tocode) ) {								# 制御文字指定で置換
			#print "(2)\n";											# ????
			$_chr = $self->replace( $chr, $to, $from );
		}
		#printf "**** chr=[%s], _chr=[%s]\n", $chr, $_chr;		# ???? オリジナルと置換後を比較

		if ($chr eq $_chr) {									# 置換失敗
			$chr = sprintf( "%X", $_ord );							# 制御コードに変換
		}
		else {													# 置換成功
			$chr = '<'.$_chr.'>';									# 制御文字に変換
		}
		#printf "**** chr=[%s]\n", $chr;						# ???? 置換結果
		push(@r, $chr);
	}
	#print "[r]".Dumper(@r)."\n";							# ????
	#print "(/toHex)\n";			# ????
	return @r;
}

#-------------------------------------------
# ■パラメータ解析
# parse_param(\$p, @arg);
#
sub parse_param {
	#print "\n(parse_param)\n";
	my $self = shift;
	my $param = shift;								# パラメータ変数のリファレンス
	my @arg = @_;									# パラメータ

	while ( my $p = shift(@arg) ) {
		my ($v1, $v2) = $p =~ /([+|-]*)(.*)/;
		#printf "v1=[%s] v2=[%s]\n", $v1, $v2;		# ????
		$v1 = ($v1 eq '-') ? 0 : 1;
		$$param->{$v2} = $v1;
	}
	#print "(/parse_param)\n";
}

#-------------------------------------------
# ■半角カタカナ（ＣＰ９３２）←→半角カタカナ（ＵＴＦ８）変換
# r = encode( encode_type, str );
#
#	          r: 置換後文字列
#	        str: 置換対象文字列
#	encode_type: エンコード形式( cp932, utf8 )
#
#	※半角カタカナ専用。
#	※置換対象文字列は、あらかじめ指定するエンコード形式と反対の文字コードでエンコードしておく。
#	  例えば、cp932にエンコードする場合は、置換対象文字列をutf8でエンコードしておく必要がある。
#	  Encodeモジュールでは、cp932からutf8へ変換するとデータが壊れる（ためにこのルーチンを作った）ので、
#	  eucをutf8に変換するなどの場合、eucからcp932まではEncodeモジュールでも正常に変換できるので、
#	  あらかじめcp932にエンコードしたのち、このルーチンで変換することでutf8に変換することができる。
#
sub conv_hkana {
	#print "\n(conv_hkana)\n";
	my $self = shift;
	my $enctype = shift;						# エンコード形式
	my $str = shift;							# 置換対象文字列

	my @jobs;									# 置換ジョブ

	if ($enctype =~ /cp932/i) {
		#push(@jobs, [ 'katakana1_halfwidth_cp932', 'katakana1_halfwidth' ]);
		push(@jobs, [ 'katakana2_halfwidth_cp932', 'katakana2_halfwidth' ]);
		push(@jobs, [ 'katakana3_halfwidth_cp932', 'katakana3_halfwidth' ]);
		push(@jobs, [ 'katakana4_halfwidth_cp932', 'katakana4_halfwidth' ]);
	}
	elsif ($enctype =~ /utf8/i) {
		#push(@jobs, [ 'katakana1_halfwidth', 'katakana1_halfwidth_cp932' ]);
		push(@jobs, [ 'katakana2_halfwidth', 'katakana2_halfwidth_cp932' ]);
		push(@jobs, [ 'katakana3_halfwidth', 'katakana3_halfwidth_cp932' ]);
		push(@jobs, [ 'katakana4_halfwidth', 'katakana4_halfwidth_cp932' ]);
	}

	# 置換ジョブ実行
	while (my @from = @{ shift(@jobs) }) {
		my $to = shift @from;
		#printf "to=[%s] from=[%s]\n", $to, join(',', @from);				# ???? 
		$str = $self->replace( $str, $to, @from );
		#print "==".encode('cp932', $str)."\n";								# ???? 
	}
	#print "(/conv_hkana)\n";									# ????
	return $str;
}

#-------------------------------------------
# ■半角→全角処理
# r = h2z( str, opt... );
#
#	   r: 置換後文字列
#	 str: 置換対象文字列
#	 arg: 置換オプション( '-(name)': 置換しない, '+(name)': 置換する )
#			alphabet			アルファベット（大文字・小文字）を置換			(デフォルト: 置換する)
#			number				数字を置換										(デフォルト: 置換する)
#			symbol				記号を置換										(デフォルト: 置換する)
#			katakana			カタカナを置換									(デフォルト: 置換する)
#			katakana_type2		カタカナを置換（全角カタカナ【２文字型】）		(デフォルト: 置換しない)
#
sub h2z {
	#print "\n(h2z)\n";							# ???? 
	my $self = shift;
	my $str = shift;							# 置換対象文字列
	my @arg = @_;								# 引数

	my @jobs;									# 置換ジョブ
												# 	$jobs[n] = [ to, from_1, from_2, from_n... ]

	my $p;										# デフォルトパラメータ(0:無効, 1:有効)
		$p->{'number'}				= 1;			# 数字
		$p->{'symbol'}				= 1;			# 記号
		$p->{'alphabet'}			= 1;			# アルファベット
		$p->{'alphabet_big'}		= 1;			# アルファベット（大文字）
		$p->{'alphabet_small'}		= 1;			# アルファベット（小文字）
		$p->{'katakana'}			= 1;			# カタカナ
		$p->{'katakana_type2'}		= 0;			# カタカナ（２文字型）

	#print "[A]\n".Dumper($p)."\n";				# ???? 変更前
	$self->parse_param( \$p, @arg );			# パラメータの上書き
	#print "[B]\n".Dumper($p)."\n";				# ???? 変更後

	if ( $p->{'number'} == 1 ) {														# 番号
		push(@jobs, [ 'number_fullwidth', 'number_halfwidth' ]);
	}
	if ( $p->{'symbol'} == 1 ) {														# 記号
		push(@jobs, [ 'symbol_fullwidth', 'symbol_halfwidth' ]);
	}
	unless ( $p->{'alphabet'} == 0 ) {													# アルファベット
		if ( $p->{'alphabet_big'} == 1 ) {													# アルファベット（大文字）
			push(@jobs, [ 'alphabet_big_fullwidth', 'alphabet_big_halfwidth' ]);
		}
		if ( $p->{'alphabet_small'} == 1 ) {												# アルファベット（小文字）
			push(@jobs, [ 'alphabet_small_fullwidth', 'alphabet_small_halfwidth' ]);
		}
	}
	if ( $p->{'katakana'} == 1 ) {														# カタカナ
		if ( $p->{'katakana_type2'} == 1 ) {												# カタカナ【２文字型】
			push(@jobs, [ 'katakana1_fullwidth2', 'katakana1_halfwidth' ]);						# カタカナ【２文字型】（濁音・半濁音付）
			push(@jobs, [ 'katakana2_fullwidth', 'katakana2_halfwidth' ]);						# カタカナ【２文字型】（一般文字）
			push(@jobs, [ 'katakana3_fullwidth', 'katakana3_halfwidth' ]);						# カタカナ【２文字型】（濁音・半濁音記号）
			push(@jobs, [ 'katakana4_fullwidth', 'katakana4_halfwidth' ]);						# カタカナ【２文字型】（記号）
		}
		else {																			# カタカナ
			push(@jobs, [ 'katakana1_fullwidth', 'katakana1_halfwidth' ]);						# カタカナ（濁音・半濁音付）
			push(@jobs, [ 'katakana2_fullwidth', 'katakana2_halfwidth' ]);						# カタカナ（一般文字）
			push(@jobs, [ 'katakana3_fullwidth', 'katakana3_halfwidth' ]);						# カタカナ（濁音・半濁音記号）
			push(@jobs, [ 'katakana4_fullwidth', 'katakana4_halfwidth' ]);						# カタカナ（記号）
		}

	}

	# 置換ジョブ実行
	while (my @from = @{ shift(@jobs) }) {
		my $to = shift @from;
		#printf "to=[%s] from=[%s]\n", $to, join(',', @from);				# ???? 
		$str = $self->replace( $str, $to, @from );
		#print "==".encode('cp932', $str)."\n";								# ???? 
	}

	#print "(/h2z)\n";									# ????
	return $str;
}

#-------------------------------------------
# ■全角→半角処理【※未完成】
# r = z2h( str, opt... );
#
#	   r: 置換後文字列
#	 str: 置換対象文字列
#	 arg: 置換オプション( '-(name)': 置換しない, '+(name)': 置換する )
#			alphabet			アルファベット（大文字・小文字）を置換			(デフォルト: 置換する)
#			number				数字を置換										(デフォルト: 置換する)
#			symbol				記号を置換										(デフォルト: 置換する)
#			katakana			カタカナを置換									(デフォルト: 置換する)
#			katakana_type2		カタカナを置換（全角カタカナ【２文字型】）		(デフォルト: 置換しない)
#
sub z2h {
	#print "\n(z2h)\n";							# ???? 
	my $self = shift;
	my $str = shift;							# 置換対象文字列
	my @arg = @_;								# 引数

	my @jobs;									# 置換ジョブ
												# 	$jobs[n] = [ to, from_1, from_2, from_n... ]

	my $p;										# デフォルトパラメータ(0:無効, 1:有効)
		$p->{'number'}				= 1;			# 数字
		$p->{'symbol'}				= 1;			# 記号
		$p->{'alphabet'}			= 1;			# アルファベット
		$p->{'alphabet_big'}		= 1;			# アルファベット（大文字）
		$p->{'alphabet_small'}		= 1;			# アルファベット（小文字）
		$p->{'katakana'}			= 1;			# カタカナ
		$p->{'hiragana'}			= 0;			# ひらがな→半角カタカナ
		$p->{'old'}					= 0;			# ひらがな→半角カタカナ

	#print "[A]\n".Dumper($p)."\n";				# ???? 変更前
	$self->parse_param( \$p, @arg );			# パラメータの上書き
	#print "[B]\n".Dumper($p)."\n";				# ???? 変更後

	# 番号
	if ( $p->{'number'} == 1 ) {
		push(@jobs, [ 'number_halfwidth', 'number_fullwidth' ]);
	}
	# 記号
	if ( $p->{'symbol'} == 1 ) {
		push(@jobs, [ 'symbol_halfwidth', 'symbol_fullwidth' ]);
	}
	# アルファベット（大文字）
	if ( $p->{'alphabet_big'} == 1 ) {
		push(@jobs, [ 'alphabet_big_halfwidth', 'alphabet_big_fullwidth' ]);
	}
	# アルファベット（小文字）
	if ( $p->{'alphabet_small'} == 1 ) {
		push(@jobs, [ 'alphabet_small_halfwidth', 'alphabet_small_fullwidth' ]);
	}
	# カタカナ
	if ( $p->{'katakana'} == 1 ) {
		push(@jobs, [ 'katakana1_halfwidth', 'katakana1_fullwidth', 'katakana1_fullwidth2' ]);
		push(@jobs, [ 'katakana2_halfwidth', 'katakana2_fullwidth' ]);
		push(@jobs, [ 'katakana3_halfwidth', 'katakana3_fullwidth' ]);
		push(@jobs, [ 'katakana4_halfwidth', 'katakana4_fullwidth' ]);
	}

	# ひらがな→半角カタカナ
	if ( $p->{'hiragana'} == 1 ) {
	#	unless ( $p->{'katakana'} == 1 ) {
	#		push(@jobs, [ 'katakana3_halfwidth', 'katakana3_fullwidth' ]);		# カタカナ（濁音・半濁音記号）
	#		push(@jobs, [ 'katakana4_halfwidth', 'katakana4_fullwidth' ]);		# カタカナ（記号）
	#	}
	#	push(@jobs, [ 'katakana2_halfwidth', 'hiragana2_fullwidth' ]);			# ひらがな（一般文字）
	#}
	## 旧文字→半角カタカナ
	#if ( $p->{'old'} == 1 ) {
	#	push(@jobs, [ 'katakana5_halfwidth', 'katakana5_old_fullwidth'  ]);		# カタカナ（旧文字）→半角カタカナ
	#	push(@jobs, [ 'katakana5_halfwidth', 'hiragana3_old_fullwidth'  ]);		# ひらがな（旧文字）→半角カタカナ
	}

	# 置換ジョブ実行
	while (my @from = @{ shift(@jobs) }) {
		my $to = shift @from;
		#printf "to=[%s] from=[%s]\n", $to, join(',', @from);				# ???? 
		$str = $self->replace( $str, $to, @from );
		#print "==".encode('cp932', $str)."\n";								# ???? 
	}

	#print "(/z2h)\n";									# ????
	return $str;
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#package Naoit0::ChrConv::Test;
#
##BEGIN { push(@INC, '..') };
#use utf8;
#use Encode;
#use Data::Dumper;
##use Naoit0::ChrConv;
#
#my $o = new Naoit0::ChrConv;
#
##	conv_hkana();			# 半角カタカナ（ＣＰ９３２）→半角カタカナ（ＵＴＦ８）変換
##	h2z();					# 半角→全角置換
##	z2h();					# 全角→半角置換【※未完成】
##	parse_param();			# パラメータ解析
##	hiragana();				# ひらがな変換
#
##---------------------------------
## ■半角カタカナ（ＣＰ９３２）←→半角カタカナ（ＵＴＦ８）変換
## conv_hkana()
##
#sub conv_hkana {
#
#	# 置換対象データ
#	my $r;
#	my @a;
#
#	# 【半角】
#	push( @a,  (@{ $o->{tbl}->{'katakana1_halfwidth'} }) );			# 'ｶﾞ'		# カタカナ（濁音・半濁音付）
#	push( @a, "\n" );
#	push( @a,  (@{ $o->{tbl}->{'katakana2_halfwidth'} }) );			# 'ｱ'		# カタカナ（一般文字）
#	push( @a, "\n" );
#	push( @a, (@{ $o->{tbl}->{'katakana3_halfwidth'} }) );			# 'ﾞ'		# カタカナ（濁音・半濁音記号）
#	push( @a, "\n" );
#	push( @a, (@{ $o->{tbl}->{'katakana4_halfwidth'} }) );			# '｡'		# カタカナ（記号）
#	$r = join( '|', @a);
#
#	## (0) 原本データ
#	#printf "(0)r=[%s]\n", $r;																	# (0) utf8
#    #
#	## (1) (0)をcp932でエンコード（置換元データ）
#	#my $r_cp932 = encode('cp932', $r);															# (1) cp932
#	#printf "(1)r=[%s]\n", $r_cp932;
#    #
#	#		# (2) encode(Encode)でutf8に変換	→ 変換失敗
#	#		my $r_utf8_encode = encode('utf8', $r_cp932);										# (2) utf8
#	#		printf "(2)r=[%s]\n", $r_utf8_encode;
#    #
#	#		# (3) conv_hkanaでutf8に変換		→ 変換成功
#	#		my $r_utf8_conv_hkana = $o->conv_hkana('utf8', $r_cp932);							# (3) utf8
#	#		printf "(3)r=[%s]\n", $r_utf8_conv_hkana;
#    #
#	#				# (4) (3)を逆変換
#	#				my $r_cp932_conv_hkana = $o->conv_hkana('cp932', $r_utf8_conv_hkana);		# (4) cp932
#	#				printf "(4)r=[%s]\n", $r_cp932_conv_hkana;
#
#	my $r_cp932 = encode('cp932', $r);								# cp932でエンコード（置換元データ）
#	printf "r=[%s]\n", $r_cp932;
#
#	my $r_utf8_conv_hkana = $o->conv_hkana('utf8', $r_cp932);		# utf8に変換
#	printf "r=[%s]\n", $r_utf8_conv_hkana;
#
#}
#
##---------------------------------
## ■半角→全角変換
## h2z()
##
#sub h2z {
#
#	# 置換対象データ
#	my $r;
#	my @a;
#
#	# 【半角】
#	push( @a, (@{ $o->{tbl}->{'alphabet_big_halfwidth'} }) );		# 'A'		# アルファベット（大文字）
#	push( @a, "\n" );
#	push( @a, (@{ $o->{tbl}->{'alphabet_small_halfwidth'} }) );		# 'a'		# アルファベット（小文字）
#	push( @a, "\n" );
#	push( @a, (@{ $o->{tbl}->{'number_halfwidth'} }) );				# '0'		# 数字
#	push( @a, "\n" );
#	push( @a, (@{ $o->{tbl}->{'symbol_halfwidth'} }) );				# '!'		# 記号
#	push( @a, "\n" );
#	push( @a, (@{ $o->{tbl}->{'katakana1_halfwidth'} }) );			# 'ｶﾞ'		# カタカナ（濁音・半濁音付）
#	push( @a, "\n" );
#	push( @a, (@{ $o->{tbl}->{'katakana2_halfwidth'} }) );			# 'ｱ'		# カタカナ（一般文字）
#	push( @a, "\n" );
#	push( @a, (@{ $o->{tbl}->{'katakana3_halfwidth'} }) );			# 'ﾞ'		# カタカナ（濁音・半濁音記号）
#	push( @a, "\n" );
#	push( @a, (@{ $o->{tbl}->{'katakana4_halfwidth'} }) );			# '｡'		# カタカナ（記号）
#	#push( @a, (@{ $o->{tbl}->{'katakana5_halfwidth'} }) );			# 'ｲ'		# カタカナ（旧文字）（※旧文字変換用）
#	$r = join( '|', @a);
#
#	# 置換処理
#	printf "\nr=[%s]\n", encode('cp932', $r);
#	$r = $o->h2z( $r );
#	printf "\nr=[%s]\n", encode('cp932', $r);
#	print "\n";
#}
#
##---------------------------------
## ■全角半角置換
## z2h()
##
#sub z2h {
#
#	# 置換対象データ
#	my $r;
#	my @a;
#
#	# 【全角】
#	push( @a, (@{ $o->{tbl}->{'alphabet_big_fullwidth'} }) );		# 'Ａ'		# アルファベット（大文字）
#	push( @a, "\n" );
#	push( @a, (@{ $o->{tbl}->{'alphabet_small_fullwidth'} }) );		# 'ａ'		# アルファベット（小文字）
#	push( @a, "\n" );
#	push( @a, (@{ $o->{tbl}->{'number_fullwidth'} }));				# '０'		# 数字
#	push( @a, "\n" );
#	push( @a, (@{ $o->{tbl}->{'symbol_fullwidth'} }));				# '！'		# 記号
#	push( @a,  (@{ $o->{tbl}->{'katakana1_fullwidth'} }) );			# 'ガ'		# カタカナ（濁音・半濁音付）
#	push( @a, "\n" );
#	push( @a,  (@{ $o->{tbl}->{'katakana1_fullwidth2'} }) );			# 'カ゛'	# カタカナ（濁音・半濁音付）【２文字型】
#	push( @a, "\n" );
#	push( @a,  (@{ $o->{tbl}->{'katakana2_fullwidth'} }) );			# 'ア'		# カタカナ（一般文字）
#	push( @a, "\n" );
#	push( @a, (@{ $o->{tbl}->{'katakana3_fullwidth'} }) );			# '゛'		# カタカナ（濁音・半濁音記号）
#	push( @a, "\n" );
#	push( @a, (@{ $o->{tbl}->{'katakana4_fullwidth'} }) );			# '。'		# カタカナ（記号）
#	push( @a, "\n" );
#	push( @a, (@{ $o->{tbl}->{'hiragana1_fullwidth'} }) );			# 'が'		# ひらがな（濁音・半濁音付文字）
#	push( @a, "\n" );
#	push( @a, (@{ $o->{tbl}->{'hiragana1_fullwidth2'} }) );			# 'か゛'	# ひらがな（濁音・半濁音付文字）【２文字型】
#	push( @a, "\n" );
#	push( @a, (@{ $o->{tbl}->{'hiragana2_fullwidth'} }) );			# 'あ'		# ひらがな（一般文字）
#	push( @a, "\n" );
#	#push( @a, (@{ $o->{tbl}->{'hiragana3_old_fullwidth'} }) );		# 'ゐ'		# ひらがな（旧文字）（※旧文字変換用）
#	#push( @a, (@{ $o->{tbl}->{'katakana5_fullwidth'} }) );			# 'イ'		# カタカナ（旧文字）（※旧文字変換用）
#	#push( @a, (@{ $o->{tbl}->{'katakana5_old_fullwidth'} }) );		# 'ヰ'		# カタカナ（旧文字）（※旧文字変換用）
#	$r = join( '|', @a);
#
#	# 置換処理
#	printf "r=[%s]\n", encode('cp932', $r);
#	$r = $o->z2h( $r , );
#	printf "r=[%s]\n", encode('cp932', $r);
#}
#
##---------------------------------
## ■パラメータ解析
## parse_param()
##
#sub parse_param {
#	my $p;					# 保存先
#
#	#my @opt = (				# パラメータ
#	#	'a',
#	#	'-symbol', 
#	#	'-zkana1', 
#	#	'-zkana2', 
#	#	'-alphabet', 
#	#	'+hira1', 
#	#	'+hira2', 
#	#);
#	#$o->parse_param(\$p, @opt);
#
#	$o->parse_param( \$p, 'a', '-symbol', '-zkana1', '-zkana2', '-alphabet', '+hira1', '+hira2' );
#
##	print "[p]".Dumper($p)."\n";
#
#	foreach my $k (sort(keys(%{$p}))) {
#		printf "[%s]=[%s]\n", $k, $p->{$k};
#	}
#}
#
##---------------------------------
## ■ひらがな変換
## hiragana()
##
## ※ 実験用→用途がないため終了
##
#sub hiragana {
#	my %p;							# デフォルトパラメータ
#		$p{'zkatakana'}		= 1;		# 全角カタカナを対象に含める
#		$p{'zkatakana2'}	= 0;		# 全角カタカナ（２文字型）を対象に含める
#		$p{'hkatakana'}		= 1;		# 半角カタカナを対象に含める
#
#		$p{'type2'}			= 0;		# ひらがな（２文字型）で出力する
#
#	# 置換対象データ
#	my $r;
#	my @a;
#
#	# 【全角】
#	#push( @a, (@{ $o->{tbl}->{'alphabet_big_fullwidth'} }) );		# 'Ａ'		# アルファベット（大文字）
#	#push( @a, (@{ $o->{tbl}->{'alphabet_small_fullwidth'} }) );		# 'ａ'		# アルファベット（小文字）
#	#push( @a, (@{ $o->{tbl}->{'number_fullwidth'} }));				# '０'		# 数字
#	#push( @a, (@{ $o->{tbl}->{'symbol_fullwidth'} }));				# '！'		# 記号
#	push( @a,  (@{ $o->{tbl}->{'katakana1_fullwidth'} }) );			# 'ガ'		# カタカナ（濁音・半濁音付）
#	push( @a, '$' );
#	push( @a,  (@{ $o->{tbl}->{'katakana1_fullwidth2'} }) );			# 'カ゛'	# カタカナ（濁音・半濁音付）【２文字型】
#	push( @a, '$' );
#	push( @a,  (@{ $o->{tbl}->{'katakana2_fullwidth'} }) );			# 'ア'		# カタカナ（一般文字）
#	push( @a, '$' );
#	#push( @a, (@{ $o->{tbl}->{'katakana3_fullwidth'} }) );			# '゛'		# カタカナ（濁音・半濁音記号）
#	#push( @a, (@{ $o->{tbl}->{'katakana4_fullwidth'} }) );			# '。'		# カタカナ（記号）
#	#push( @a, (@{ $o->{tbl}->{'hiragana1_fullwidth'} }) );			# 'が'		# ひらがな（濁音・半濁音付文字）
#	#push( @a, (@{ $o->{tbl}->{'hiragana1_fullwidth2'} }) );			# 'か゛'	# ひらがな（濁音・半濁音付文字）【２文字型】
#	#push( @a, (@{ $o->{tbl}->{'hiragana2_fullwidth'} }) );			# 'あ'		# ひらがな（一般文字）
#	#push( @a, (@{ $o->{tbl}->{'hiragana3_old_fullwidth'} }) );		# 'ゐ'		# ひらがな（旧文字）（※旧文字変換用）
#	#push( @a, (@{ $o->{tbl}->{'katakana5_fullwidth'} }) );			# 'イ'		# カタカナ（旧文字）（※旧文字変換用）
#	#push( @a, (@{ $o->{tbl}->{'katakana5_old_fullwidth'} }) );		# 'ヰ'		# カタカナ（旧文字）（※旧文字変換用）
#
#	# 【半角】
#	#push( @a, (@{ $o->{tbl}->{'alphabet_big_halfwidth'} }) );		# 'A'		# アルファベット（大文字）
#	#push( @a, (@{ $o->{tbl}->{'alphabet_small_halfwidth'} }) );		# 'a'		# アルファベット（小文字）
#	#push( @a, (@{ $o->{tbl}->{'number_halfwidth'} }) );				# '0'		# 数字
#	#push( @a, (@{ $o->{tbl}->{'symbol_halfwidth'} }) );				# '!'		# 記号
#	push( @a,  (@{ $o->{tbl}->{'katakana1_halfwidth'} }) );			# 'ｶﾞ'		# カタカナ（濁音・半濁音付）
#	push( @a, '$' );
#	push( @a,  (@{ $o->{tbl}->{'katakana2_halfwidth'} }) );			# 'ｱ'		# カタカナ（一般文字）
#	push( @a, '$' );
#	#push( @a, (@{ $o->{tbl}->{'katakana3_halfwidth'} }) );			# 'ﾞ'		# カタカナ（濁音・半濁音記号）
#	#push( @a, (@{ $o->{tbl}->{'katakana4_halfwidth'} }) );			# '｡'		# カタカナ（記号）
#    #push( @a, (@{ $o->{tbl}->{'katakana5_halfwidth'} }) );			# 'ｲ'		# カタカナ（旧文字）（※旧文字変換用）
#
#	$r = join( '|', @a);
#	my $r2 = $r;
#	$r2 =~ s/\$/\n/g;
#	printf "\nr=[%s]\n", encode('cp932', $r2);
#
#	# ■置換処理
#
#	my $to, @from;
#
#	# (1) 濁音
#	$to = ( $p{'type2'} == 1 ) ? 'hiragana1_fullwidth2' : 'hiragana1_fullwidth';				# 出力タイプを決定
#	if ( $p{'hkatakana'} == 1 ) {																# 置換対象が半角カタカナ
#		$r = $o->replace( $r, $to, 'katakana1_halfwidth' );
#	}
#
#	if ( $p{'zkatakana2'} == 1 ) {																# 置換対象が全角２文字カタカナ
#		$r = $o->replace( $r, $to, 'katakana1_fullwidth2' );
#	}
#
#	if ( $p{'zkatakana'} == 1 ) {																# 置換対象が全角カタカナ
#		$r = $o->replace( $r, $to, 'katakana1_fullwidth' );
#	}
#
#	$to = ( $p{'type2'} == 1 ) ? 'hiragana1_fullwidth2' : 'hiragana1_fullwidth';				# 出力タイプを決定
#
#	# (2) 一般文字
#	if ( $p{'hkatakana'} == 1 ) {																# 置換対象が半角カタカナ
#		$r = $o->replace( $r, 'hiragana2_fullwidth', 'katakana2_halfwidth' );
#	}
#	if ( $p{'zkatakana'} == 1 ) {																# 置換対象が全角カタカナ
#		$r = $o->replace( $r, 'hiragana2_fullwidth', 'katakana2_fullwidth' );
#	}
#
#	# (3) 記号
#	if ( $p{'hkatakana'} == 1 ) {																# 置換対象が半角カタカナなら
#		$r = $o->replace( $r, 'katakana3_fullwidth', 'katakana3_halfwidth' );						# 半角カタカナ（濁音・半濁音記号）を全角化
#		$r = $o->replace( $r, 'katakana4_fullwidth', 'katakana4_halfwidth' );						# 半角カタカナ（記号）を全角化
#	}
#
#
#		# 'ヰ'		# カタカナ（旧文字）【全角】（※旧文字変換用）
#		# 'ゐ'		# ひらがな（旧文字）【全角】（※旧文字変換用）
#
#	$r =~ s/\$/\n/g;
#	printf "\nr=[%s]\n", encode('cp932', $r);
#	print "\n";
#}

1;
