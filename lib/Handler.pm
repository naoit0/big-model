package Handler;
our $VERSION = '0001.20190116.1759';		# 再構築
#
# ハンドラ制御
#
use strict;
use warnings;
use utf8;
use Data::Dumper;
#use Debug;

#-------------------------------------------------
# ■コンストラクタ
# new()
#
sub new {
	my $class = shift;
	my $self = { @_ };
	bless($self, $class);
	$self->_init();
	return $self;
}

#-------------------------------------------------
# ■属性の初期化
# _init()
#
sub _init {
	my $self = shift;

	$self->{'func'} = undef;		# サブルーチン
}

#-------------------------------------------------
# ■ハンドラを登録
# r = Handler->set( func )
#	func	: サブルーチン( sub { ... } )
#	r		: 結果( 0: 成功, 1: 失敗 )
#
sub set {
	my $self = shift;
	my $func = shift;

	return 1 unless ( ref($func) eq 'CODE' );			# 引数がサブルーチンでなければエラー(1)
	$self->{'func'} = $func;
	return 0;
}

#-------------------------------------------------
# ■ハンドラを削除
# r = Handler->unset()
#
sub unset {
	my $self = shift;

	$self->_init();
}

#-------------------------------------------------
# ■ハンドラを実行して結果を返す
# r = Handler->call( param )
#	param	: 引数
#	r		: サブルーチンが返した結果または実行できなかった場合は未定義値
#
sub call {
	my $self = shift;

	return undef unless ( ref($self->{'func'}) eq 'CODE' );	# ハンドラが未定義ならエラー(undef)
	return &{ $self->{'func'} }(@_);
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
#================================================================
#package Test;
#use Data::Dumper;
##use Handler;
#
#my $h1 = new Handler;
#my $h2 = new Handler;
#
#my $f1 = sub { print "[1]\n"; print "@@".Dumper(@_); };
#
#$h1->set( $f1 );
#$h2->set( sub { print "[2]\n"; print "@@".Dumper(@_); } );
#
#$h1->call($h1);
#$h2->call($h2);
#
#$h1->unset();
#
#printf "r=(%d)\n", $h1->call($h1);
#printf "r=(%d)\n", $h2->call($h2);

1;