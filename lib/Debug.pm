package Debug;
our $VERSION = "----.--------.----";
#
# デバッグサブルーチン
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
	$Data::Dumper::Sortkeys = 1;			# ハッシュのキーをソートする
#	$Data::Dumper::Indent = 1;			# インデントを縮める
#	$Data::Dumper::Terse = 1;				# $VAR数字要らない
#use Naoit0::ChrConv;


#-------------------------------------------------
# ■コンストラクタ
#
sub new {
	my $class = shift;

	my $self = { @_ };
	bless($self, $class);
	return $self;
}


#------------------------------------------------------
# ■ 呼び出し元の情報を返す
# r = _caller();
#
sub _caller {
	#printf "\n\n** < %s::_caller [%s] >\n", __PACKAGE__, Debug::_caller(caller);
	my $self = shift;
	return sprintf("pkg=( %s ), line=( %s )", $_[0], $_[1]) if ($#_ == 1);
	return sprintf("pkg=( %s ), src=( %s:%s )", $_[0], $_[1], $_[2]) if ($#_ == 2);
}

#------------------------------------------------------
# ■ 文字列のダンプ
# dump( data )
#
sub dump {
	my $self = shift;
	my $data = shift;
	my @datas = split('', $data);

	print map { sprintf "| {%-2s} - [%s]\n", sprintf("%02x", ord($_)), $_ } @datas;
	}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;