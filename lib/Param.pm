package Param;
our $VERSION = '0001.20190117.1536';		# 再構築
our $VERSION = '---1.20190204.0255';		# とりあえず完了
#
# パラメータ管理
#

use strict;
use warnings;
use utf8;
use Data::Dumper;

#-----------------------------------
# ■コンストラクタ
# new()
#
sub new {
	my $class = shift;
	my $self = { @_ };
	bless($self, $class);
	return $self;
}

#-----------------------------------
# ■キーの問い合わせ
# r = query( name )
# 
sub query {
	#print "\n(query)\n";			# ????
	my $self = shift;
	my $name = shift;
	#print "(/query)\n";			# ????
	return $self->{$name};
}

#-----------------------------------
# ■キーの登録
# set( name => value, name => value, ... )
# 
sub set {
	#print "\n(set)\n";			# ????
	my $self = shift;

	while(@_) {
		my $name = shift;
		my $value = shift;
		$self->{$name} = $value if ( defined($name) );
	}
	#print "(/set)\n";			# ????
}

#-----------------------------------
# ■キーの削除
# unset( name, name, ... )
# 
sub unset {
	#print "\n(unset)\n";			# ????
	my $self = shift;
	while (@_) {
		my $name = shift;
		delete($self->{$name});
	}
	#print "(/unset)\n";			# ????
}

#-----------------------------------
# ■キーの確認
# r = defined( name )
# 
sub defined {
	#print "\n(defined)\n";				# ????
	my $self = shift;
	my $name = shift;
	#printf "name=[%s]\n", $name;		# ????
	#print "(/defined)\n";				# ????
	return exists($self->{$name}) ? 1 : 0;
}

#-----------------------------------
# ■キーリストの取得
# ( key1, key2, ... ) = keys()
# 
sub keys {
	#print "\n(keys)\n";				# ????
	my $self = shift;
	my $name = shift;
	my @keys = sort( keys(%{$self}) );
	#print Dumper(@keys)."\n";			# ????
	#print "(/keys)\n";				# ????
	return @keys;
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
##-------------------------------------------
#package Test;
#
##use Param;
#use Data::Dumper;
#
#my $p = new Param;
#
### 追加
##$p->set( 'key14', "aaa", 'key41', "bbb",  );
##print Dumper($p);
##
##$p->set( 'key1'=>"key1:abcde", 'key2'=>"ccc",  );
##print Dumper($p);
##
##$p->set([ 'key2'=>"aaa" ]);
##print Dumper($p);
##
##$p->set({ 'key3'=>"2" });
##print Dumper($p);
##
### キー名リスト
##my $keys = $p->keys();
##print Dumper($keys)."\n";
##
### キーの確認
##printf "key01=(%d)\n", $p->defined('key01');
##printf "key1=(%d)\n", $p->defined('key1');
##
### 問い合わせ
##my $q = $p->query('key1');
##printf "query=[%s]\n", $q;
##
### 削除
##$p->unset( 'key1' );
##print Dumper($p);
##
##$p->unset( 'key41', 'key2' );
##print Dumper($p);
##
##$p->unset([ 'key3', 'key14' ]);
##print Dumper($p);
#
#$p->set( 'key1', undef );
#print Dumper($p);
#
## キーの確認
#printf "key1=(%d)\n", $p->defined('key1');
#
#$p->unset('key1');
#print Dumper($p);
#
## キーの確認
#printf "key1=(%d)\n", $p->defined('key1');
#
1;