BEGIN { push(@INC, './lib', './'); our $DEBUG = 1; }
our $VERSION = "----.20190116.1133";	# 再整備
our $VERSION = "----.20190207.0613";	# テスト版完了
#
# BIG-Model ホストプログラム
# 本体
use strict 'vars';
use warnings;
use utf8;
use Encode qw( encode decode );
use FindBin;
use Data::Dumper;
use BBS;								# コアクラス
use Arg;								# 入力制御

my $bm = new BBS;
	$bm->{'db'} = ['dbi:mysql:dbname=bigmodel;host=127.0.0.1', 'bigmodel', ''];			# ＤＢパラメータ

printf "\n** execpath=[%s]\n", $FindBin::Bin;

#====================================================
# ■データベースのチェック
#====================================================
# ●テーブル定義ファイルからデータをハッシュ変数に取り込む
#print encode('cp932', "テーブル定義をファイルから取り込みます\n");		# ????
#my $in = IO::File->new("table.sql") or die "$!";
#my $p = 0;
#my $htables;
#
#while (<$in>) {
#	$p = 1 if ( ($p == 0 ) && $_ =~ /^CREATE TABLE/);		# テーブル定義文の先頭
#	$_ =~ /^CREATE TABLE `(.*)` \(/;
#	my $t = $1;
#	#printf "** t=[%s]\n", $t if defined($t);				# テーブル定義文中
#	#print "-> " if ($p == 1);								# テーブル定義文
#	$p = 0 if ( ($p == 1) && ($_ =~ /\;$/) );				# テーブル定義文の文末
#	#print "$_";
#	chop if ($p == 0 );										# 文末なら文末のＣＲを削除
#
#	if ( defined($t) ) {
#		$htables->{$t} .= $_;
#	}
#}
##print "[htables]\n".Dumper($htables)."[/htables]\n";
#
## ●定義されているテーブルを取得
##print encode('cp932', "データベースに定義されているテーブル定義を取得します\n");		# ????
#my $dbh = DBI->connect(@{ $bm->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
#
#my @tables;
#my $q = "SHOW TABLE STATUS FROM `bigmodel`";
##printf "**[q]------------\n%s\n-----------------\n", $q;		# ????
#my $sth = $dbh->prepare( $q );
#$sth->execute();
#if ( $sth->rows ) {
#	while ( my ($t) = $sth->fetchrow_array() ) {
#		push(@tables, $t);
#	}
#}
#$sth->finish();
##print "[tables]\n".Dumper(@tables)."[/tables]\n";
#
## ●定義されているテーブルのテーブル構造を比較
##my $dbh = DBI->connect(@{ $bm->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
#
#my $err = 0;
#foreach my $table ( sort(@tables) ) {
#	my $q = "SHOW CREATE TABLE `$table`";
#	#printf "**[q]------------\n%s\n-----------------\n", $q;		# ????
#	my $sth = $dbh->prepare( $q );
#	$sth->execute();
#
#	if ( $sth->rows ) {
#		while ( my ($t, $c) = $sth->fetchrow_array() ) {
#			$c .= ';';
#			print encode('cp932', sprintf "テーブル(%s)を精査中 ... ", $t);		# ????
#			printf "**[%s]\n", $t;
#			printf "**[%s]\n", $c;
#			printf "**[%s]\n", $htables->{$t};
#
#			if ( $htables->{$t} eq $c ) {
#				print encode('cp932', "一致しました\n");			# ????
#			}
#			elsif ( $htables->{$t} ne $c ) {
#				$err++;
#				print encode('cp932', "一致しませんでした\n");		# ????
#			}
#		}
#	}
#	$sth->finish();
#}
#$dbh->disconnect();
#
##printf "** err=(%d)\n", $err;
#if ($err) {
#	print encode('cp932', "データベースが壊れています。同梱のテーブル定義ファイルにてテーブルを作成してください\n");
#	exit;
#}


#====================================================
# ■ ユーティリティのロード
#====================================================
#use BIGModel::Test;							# 検証用ユーティリティ

use BIGModel::Generic;							# 汎用ライブラリ							*** 完了
	# BIGModel::Generic::Map						# マップテーブルアクセス				*** 完了
	# BIGModel::Generic::Path						# パス制御								*** 完了
	# BIGModel::Generic::Alias						# エイリアスシステム					*** 完了
	# BIGModel::Generic::Permit;					# アクセス権限							*** 完了
use BIGModel::Connect;							# 接続										*** 完了
use BIGModel::Hello;							# タイトル									*** 完了
use BIGModel::Login;							# ログイン									*** 完了
	# BIGModel::Login::Guest;						# ゲストログイン
use BIGModel::Entrance;							# ログオン処理								*** 編集中
use BIGModel::Menu;								# 汎用メニュー								*** 完了
#use BIGModel::BuiltinBoard;					# 電子掲示板								
use BIGModel::Chat;								# チャット機能								*** 編集中
	# BIGModel::Chat::Roomlist						# チャットルームの状況通知
	# BIGModel::Chat::Utility::Roomlist				# チャットルームの状況通知（本体）
use BIGModel::Logout;							# 終了										*** 完了
#use BIGModel::Lets;							# 参加アンケート							
use BIGModel::Mail;							# メールボックス							
use BIGModel::Telegram;							# 電報機能									
use BIGModel::UserSetting;						# 端末環境変更機能							
use BIGModel::Tasklist;							# アクセス状況通知機能						*** 調整中
use BIGModel::SysOp;							# シスオペコマンド							
#												# ゲーム									
#use BIGModel::Game::Hamlet;						# ハムレット							
#use BIGModel::Game::HitandBlow;					# ヒットアンドブロー					
#use BIGModel::Game::SpaceRunner;					# スペースランナー						
#
use BIGModel::Utility;							# その他機能								(*) auxedit未対応
	# BIGModel::Utility::Tasklist;					# アクセス状況通知機能（本体）			*** 完了
	# BIGModel::Utility::LoginInfo;					# ログイン情報							*** 完了
	# BIGModel::Utility::LogoutInfo;				# ログアウト情報						*** 完了
	# BIGModel::Utility::Notice;					# お知らせ								*** 完了
	# BIGModel::Utility::Auxedit;					# エディタユーティリティ				
	# BIGModel::Utility::CmdShell;					# コマンドシェル						

#====================================================
# ■ ハンドラ定義
#====================================================

#--------------------------------------------------
# ■onIdleハンドラ（受信・接続待機）
#
# 現状は退避データの取り込み用として機能する

sub onidle {
	#print "\n(handle.onidle)\n";								# ????
	my $self = shift;

	#print "[nodes]\n".Dumper( $self->{'nodes'} )."\n";			# ???? ノードリスト
	#printf "#nodes=(%d)\n", $#{ $self->{'nodes'} };			# ???? 接続ノード数(-1: なし)
	if ( $#{ $self->{'nodes'} } < 0 ) {							# ノードリストに何もない場合は処理を行わない
		#print "(/handle.onidle)\n";									# ????
		return 87654;
	}

	# ■巡回処理
	#print "*** start {\n";											# ???? 巡回処理開始
	foreach my $fileno ( @{ $self->{'nodes'} } ) {
		#printf "*** fileno=(%d)\n", $fileno;							# ???? 実行するクライアントのファイル番号

		# ●退避データの取り込み
		my $store_count = $self->param( $fileno, 'store_count' );		# 取り込みフラグを得る
		$store_count--;													# 取り込みフラグ－１
		$self->param( $fileno, 'store_count' => $store_count );
		#printf "*** store_count=(%d)\n", $store_count;					# ???? 取り込みフラグ

		if ( $store_count == 0 ) {										# 取り込みフラグ＝０（受信完了）
			#print "*** store\n";											# ????
			my $arg = $self->param( $fileno, '_Arg' );						# 入力制御
			$arg->store();													# 余っている受信データを蓄積バッファに取り込む
		}
		elsif ( $store_count < 0 ) {									# 取り込みフラグ＝－１（受信待機中）
			#print "*** waiting\n";											# ????
			$self->param( $fileno, 'store_count' => 0 );					# 取り込みフラグ＝０
		}
		#elsif ( $store_count > 0 ) {									# 取り込みフラグ＝１（受信継続中）
		#	#print "*** reciving\n";										# ????
		#}

		#my $arg = $self->param( $fileno, '_Arg' );
		#$arg->arg();													# 受信データ取り出し
	}
	#print "*** } end\n";												# ???? 巡回処理終了
	#print "(/handle.onidle)\n";										# ????
	return 87654;
}

#--------------------------------------------------
# ■onaccessハンドラ（データ受信）
#
sub onaccess {
	#print "\n(handle.onaccess)\n";											# ????
	my $self = shift;

	# ●応対中ソケットの情報
	my $from = $self->from;													# 対応中ソケットのオブジェクト
	my $fileno = $from->fileno;												# 対応中ソケットのファイル番号
	#printf "*** fileno=(%d)\n", $fileno;									# ???? 対応中ソケットのファイル番号

	# ●入力制御の準備
	my $arg = $self->from_param('_Arg');									# 入力制御

	# ●受信データを蓄積バッファに取り込む
	my $data = $self->recv();												# （受信データはヌルでない）
	#printf "*** data_length=(%d)\n", length($data);						# ???? 受信データのデータ長
	#printf "*** data=(%d)[%s]\n", length($data), $data;					# ???? データ

	my $store_length = $arg->store($data);
	#printf "*** store_length=(%d)\n", $store_length;						# ???? 蓄積バッファのデータ長
	#print "[store]\n".Dumper( $arg->{'buf_store'})."\n";					# ???? 蓄積バッファの内容

	$self->from_param( 'store_count' => 2 );								# 取り込みフラグ＝２
	#printf "*** store_count=(%d)\n", $self->from_param('store_count');		# ????

	#print "(/handle.onaccess)\n";											# ????
	return 60000;
}

#--------------------------------------------------
# ■onConnectハンドラ（新規ソケット接続）
#
sub onconnect {
	#print "\n(handle.onconnect)\n";		# ????
	my $self = shift;

	# ●応対中ソケットの情報
	my $from = $self->from;					# 対応中ソケットのオブジェクト
	my $fileno = $from->fileno;				# 対応中ソケットのファイル番号
	#printf "fileno=(%d)\n", $fileno;		# ???? 対応中ソケットのファイル番号を返す

	# ●アクセスログの保存・メッセージ表示
	#$self->BIGModel::Generic::accesslog( '', '0', '', sprintf "ソケットが接続されました" );
	print encode( 'cp932', sprintf "%s ++ ソケットが接続されました(%s:%s) ++\n", BIGModel::Generic::now(), $from->peerhost(), $from->peerport() );

	# ●パラメータオブジェクトの準備・初期パラメータ設定

		# (以下はconnectユーティリティから定義)
		$self->from_param( 'ID'        => '@@ZOMBIE@@' );			# ＩＤ
		#$self->from_param( 'PASSWD'    => undef );				# パスワード
		#$self->from_param( 'ULEV'      => undef );				# 
		#$self->from_param( 'LOGINTIME' => undef );


	# ●入力制御オブジェクトの準備・初期設定
	my $arg = new Arg( $from );									# 入力制御オブジェクトを準備
		$arg->mode( 'echo'=>1, 'line'=>0, 'mask'=>0 );			# 入力モードを設定
		$arg->param( 'line_size'=>80, 'maskchr'=>'*' );			# パラメータを設定
	$self->from_param( '_Arg', $arg );

	# ●onIdleハンドラの定義
	$self->sethandler( sub { $self->BIGModel::Connect::connect } );		# 最初に呼び出すユーティリティのハンドラを定義
	#print Dumper($self->{'_onIdle'})."\n";								# ????

	#print "(/handle.onconnect)\n";			# ?????
	return 9999;							# ???? リターンコードを返す
}

#--------------------------------------------------
# ■onDisconnectハンドラ（ソケット切断）
#
sub ondisconnect {
	#print "\n(ondisconnect)\n";						# ????
	my $self = shift;

	# ●応対中ソケットの情報
	my $from = $self->from;								# 対応中ソケットのオブジェクト
	my $fileno = $from->fileno;							# 対応中ソケットのファイル番号
	#printf "fileno=(%d)\n", $fileno;					# ???? 対応中ソケットのファイル番号を返す

	# ●アクセスログの保存・メッセージ表示
	#$self->BIGModel::Generic::accesslog( '', '0',  '', sprintf "ソケットが切断しました" );
	print encode( 'cp932', sprintf "%s ++ ソケットが切断しました (%s:%s) ++\n", BIGModel::Generic::now(), $from->peerhost(), $from->peerport() );

	#print "(/ondisconnect)\n";						# ????
	return 4444;
}

#-----------------------------------
# ハンドラの定義
#
$bm->setsyshandler( 'onIdle',       sub { onidle($bm) } );
$bm->setsyshandler( 'onAccess',     sub { onaccess($bm) } );
$bm->setsyshandler( 'onConnect',    sub { onconnect($bm) } );
$bm->setsyshandler( 'onDisconnect', sub { ondisconnect($bm) }, '@' );
$bm->start(8888);

#printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;