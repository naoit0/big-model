package BIGModel::Logout;
our $VERSION = "0004.20180220.0202";
our $VERSION = "0005.20180323.1644";
our $VERSION = "0006.20180608.2358";	# 新Arg対応 完了
our $VERSION = "0006.20180622.2158";	# msgs()対応
our $VERSION = "0001.20190212.0335";	# 改訂版 
our $VERSION = "0001.20190219.0652";	# 改訂版 完了
#
# BIG-Model ホストプログラム
# 終了
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Naoit0::DateTime;
use Data::Dumper;


my $subs = {

	'0' =>
	sub {
		print "\n** [[ Logout ]]\n\n";
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>0, 'line'=>0, 'mask'=>0 );
		$self->sethandler( sub { $self->BIGModel::Logout::logout(1) } );
	},

	'1' =>	# 終了してよいですか
	sub {
		my $self = shift;
		$self->BIGModel::Generic::sendmsgs('LOGOUT_YESNO');
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1 );
		$self->sethandler( sub { $self->BIGModel::Logout::logout(2) } );
	},

	'2' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ● [ETX][ESC]					【キャンセル】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$arg->flush();
			$self->send("\r\n");
			$self->BIGModel::Logout::finalize;
		}
		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = uc( $arg->status('line') );

			# ●[Y]						【ログアウト】
			if ($input =~ /^Y$/i) {
				$self->sethandler( sub { $self->BIGModel::Logout::logout(3) } );
			}
			# ●(その他)				【戻る】
			else {
				$self->BIGModel::Logout::finalize;
			}
		}
	},

	'3' =>	# ログアウト実行
	sub {
		my $self = shift;
		my $id = $self->from_param('ID');
		my $dt= new Naoit0::DateTime;
		my $date_ = $dt->d("_-_-_", '4y', '2m', '2d');
		my $time_ = $dt->t("_:_:_", '2h24', '2m', '2s');
		$self->from_param( 'LOGOUT_DATETIME' => $date_.' '.$time_ );					# ログアウト日時を保存(logoutinfoで使用)
		$self->BIGModel::Generic::accesslog( $id, '0', '', 'ログアウトしました' );
		$self->BIGModel::Logout::updateUsers();
		#$self->BIGModel::Telegram::flush;												# 蓄積している電報を廃棄
		$self->BIGModel::Utility::LogoutInfo::logoutinfo;
		$self->BIGModel::Logout::logoutmsg;
		$self->send(encode('cp932', "\r\nNO CARRIER\r\n"));
		$self->disconnect( $self->from );
	},

};

#======================================================
# ■初期処理
#
sub initalize {
}

#------------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;
	$self->BIGModel::Generic::modoru();			# 戻る
}

#------------------------------------------------------
# ■ユーザ情報の更新
#
sub updateUsers {
	my $self = shift;
	my $userid = $self->from_param('ID');
	my $userlv = $self->from_param('USERLEVEL');
	my $logindt = $self->from_param('LOGIN_DATETIME');

	return if ($userlv <= 1);
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	$dbh->do("UPDATE users SET lastlogin='$logindt' WHERE userid='$userid'");
	$dbh->disconnect;
}

#------------------------------------------------------
# ■メッセージの送出
#
sub logoutmsg {
	my $self = shift;
	my $msg =<<'EOT';
====================================
== アクセスありがとうございました ==
====================================
EOT
	$msg =~ s/\n/\r\n/g;
	$self->send(encode('cp932', $msg));
}

#------------------------------------------------------
# ■ 呼び出し
#
sub logout {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

#------------------------------------------------------
# ■ エントリポイントを返す
#
#sub start {
#	return \&BIGModel::Logout::logout;
#}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;