package BIGModel::Mail::Write;
our $VERSION = "0001.20180512.2359";	# 着手
our $VERSION = "0002.20180520.1552";	# AUXEDIT対応 着手
our $VERSION = "0003.20180522.2015";	# 完了	userlist対応予定
our $VERSION = "0004.20180609.0821";	# 新Arg対応
our $VERSION = "0004.20180623.0055";	# msgs()対応
our $VERSION = "0001.20190222.0502";	# 改訂版 着手
#
# BIG-Model ホストプログラム
# 電子メール（送信）
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
use BIGModel::Utility::Auxedit;
my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ Mail::Write ]]\n\n";
		my $self = shift;
		$self->BIGModel::Mail::Write::initalize;
		if ( $self->from_param('_SEND_TO') ) {										# 送信先ＩＤが指定されていれば、ＩＤ入力をスキップ
			$self->sethandler( sub { $self->BIGModel::Mail::Write::write(3) });
		}
		else {
			$self->sethandler( sub { $self->BIGModel::Mail::Write::write(1) });
		}
	},

	'1' =>	# 送り先のＩＤを入力してください
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );											# 入力モードを設定
			$arg->param( 'line_size'=>46, 'maskchr'=>'*' );											# パラメータを設定
		$self->BIGModel::Generic::sendmsgs('PROMPT_SENDER');											# 「送り先のＩＤを入力してください」
		$self->sethandler( sub { $self->BIGModel::Mail::Write::write(2) });
	},

	'2' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $a = $arg->arg(); $a = defined($a) ? $a : '';

		# ●[ETX][ESC]				【 終了 】
		if ( $a eq "\x03" || $a eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::Mail::Write::finalize;
		}
		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			# ●(ヌル)				【 終了 】
			if ( $input eq '' ) {
				$self->BIGModel::Mail::Write::finalize;
			}
			# ●[?]					【 ユーザリスト表示を呼び出す 】
			elsif ($input eq '?') {
				$self->from_param( '_RETURN' => sub { $self->BIGModel::Mail::Write::write(1) } );		# 戻り先を定義
				$self->sethandler( sub { $self->BIGModel::UserSetting::Userlist::userlist });
			}
			# ●(その他の文字列)	【 ユーザチェック 】
			else {
				$self->from_param( '_SEND_TO' => $input );
				$self->sethandler( sub { $self->BIGModel::Mail::Write::write(3) });
			}
		}
	},

	'3' =>	# ユーザーチェック
	sub {
		my $self = shift;
		my $r = $self->BIGModel::Mail::Write::userCheck( $self->from_param('_SEND_TO') );
		unless ( defined($r) ) {
			$self->BIGModel::Generic::sendmsgs('MAIL_NO_SENDER');
			$self->sethandler( sub { $self->BIGModel::Mail::Write::write(1) } );
		}
		else {
			my ($userid, $handlename, $userlv) = @{$r};
			$self->from_param( '_SEND_TO' => $userid );		# 入力したＩＤを保存しているＩＤに置き換える
			$self->send( encode('cp932', sprintf( qq("%s"\r\n), decode('utf8', $handlename) )) );
			$self->sethandler( sub { $self->BIGModel::Mail::Write::write(4) } );
		}
	},

	'4' =>	# この送り先でよいですか
	sub {
		my $self = shift;
		$self->BIGModel::Generic::sendmsgs('AUXEDIT_MAIL_YESNO.1');						# 「この送り先でよいですか」
		$self->sethandler( sub { $self->BIGModel::Mail::Write::write(5) } );
	},

	'5' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $a = $arg->arg(); $a = defined($a) ? $a : '';

		# ●[ETX][ESC]						【 終了 】
		if ( $a eq "\x03" || $a eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::Mail::Write::finalize;
		}
		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			printf "**input=[%s]\n", $input;				# ????
			# ●[Y]							【 送信先リストに追加 】
			if ($input =~ /^Y$/i) {
				print "**(y)\n";			# ????
				my $recipients = $self->from_param('_RECIPIENTS');
				$recipients->{ $self->from_param('_SEND_TO') } = 1;
				$self->from_param( '_RECIPIENTS' => $recipients );
				$self->sethandler( sub { $self->BIGModel::Mail::Write::write(6) });
			}
			# ●[N]							【 送信先の再入力 】
			elsif ($input =~ /^N$/i) {
				print "**(n)\n";			# ????
				$self->sethandler( sub { $self->BIGModel::Mail::Write::write(1) });
			}
			# ●(ヌル・その他の文字列)		【 プロンプト再表示 】
			else {
				$self->sethandler( sub { $self->BIGModel::Mail::Write::write(4) });
			}
		}
	},

	'6' =>	# 他にも送りますか
	sub {
		my $self = shift;
		$self->BIGModel::Generic::sendmsgs('AUXEDIT_MAIL_YESNO.2');						# 「他にも送りますか」
		$self->sethandler( sub { $self->BIGModel::Mail::Write::write(7) });
	},

	'7' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $a = $arg->arg(); $a = defined($a) ? $a : '';

		# ●[ETX][ESC]						【 終了 】
		if ( $a eq "\x03" || $a eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::Mail::Write::finalize;
		}
		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			# ●[Y]							【 次の送信先を入力 】
			if ($input =~ /^Y$/i) {
				$self->from_param( '_SEND_TO' => undef );
				$self->sethandler( sub { $self->BIGModel::Mail::Write::write(1) });
			}
			# ●(ヌル・その他の文字列)		【 エディタユーティリティを呼び出す 】
			else {
				$self->sethandler( sub { $self->BIGModel::Mail::Write::write(8) });
			}
		}
	},

	'8' =>	# エディタユーティリティ（ＡＵＸＥＤＩＴ）を呼び出す
	sub {
		my $self = shift;
		$self->from_param( '_RETURN' => sub { $self->BIGModel::Mail::Write::write(9) } );		# ＡＵＸＥＤＩＴ終了後の戻り先を定義
		$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit });
	},

	'9' =>	# 編集したコンテンツをメールボックスに移動する
	sub {
		my $self = shift;
		my $r = $self->from_param('AUXEDIT_RESULT');					# リターンコードを回収したら廃棄
		$self->from_param('-AUXEDIT_RESULT');
			$self->BIGModel::Mail::Write::sendMail if ( $r == 1 );		# ＡＵＸＥＤＩＴの処理結果が正常ならメールボックスの保存処理を実行
			$self->BIGModel::Mail::Write::finalize;
	},

};

#==================================================
# ■メールボックスに保存
# sendMail();
#
sub sendMail {
	#print "\n(sendmail)\n";						# ????
	my $self = shift;
	my @recipients = keys(%{ $self->from_param('_RECIPIENTS') });				# 受信者ＩＤリスト
	my $to;																		# 受信者ＩＤ（リストから取り出したもの）
	my $author = $self->from_param('ID');										# 投稿者（送信者ＩＤ＝自分のＩＤ）
	my $editThreadId = '#'.$author;												# 編集した文書のスレッドＩＤ
	my $editArticleId = $self->from_param('AUXEDIT_ARTICLEID');					# 編集した文書の記事ＩＤ
		$self->from_param('-AUXEDIT_ARTICLEID');
	my $editArticleNum = $self->from_param('AUXEDIT_ARTICLENUM');				# 編集スレッドの記事番号
		$self->from_param('-AUXEDIT_ARTICLENUM');
	#printf "** articleid=(%s:%d)\n", $editThreadId , $editArticleNum;			# ????
	
	my $users = ( $#recipients >= 1 ) ? 1 : 0;
	#printf "** recipients=(%d), users=(%d)\n", $#recipients, $users;			# ????

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	# 受信者リストから一つずつ処理する
	while ( my $to = shift( @recipients ) ) {
		#printf "** recipients=(%d), users=(%d)\n", $#recipients, $users;		# ????
		my $threadId   = '@'.$to;																# 送信先ＩＤの先頭に'@'を付けたものをスレッド名とする
		my $data       = $self->BIGModel::Generic::DB::Articles::getLatestArticle( $threadId );			# スレッドの最新レコードを得る(** 確認中)
		my $articleNum = defined($data) ? ($data->[1] + 1) : 1;									# 未定義なら１に置き換える
		printf "** threadid=[%s], articlenum=[%d]\n", $threadId, $articleNum;	# ????

		# articlesテーブル : レコードをコピー
		my $q =	"INSERT INTO `articles` ".
				"SELECT NULL AS id, '$threadId' AS threadid, ".
				"'$articleNum' AS articlenum, datetime, author, title, flag ".
				"FROM `articles` WHERE id = '$editArticleId'";
		#printf "** q=[%s]\n", encode('cp932', $q);								# ????
		$dbh->do( $q );

		# articlesテーブル : コピーしたレコードのＩＤを取得
		my $duplicateArticleId;
		$q =	"SELECT id FROM `articles` ".
				"WHERE threadid = '$threadId' AND articlenum = '$articleNum'";
		#printf "** q=[%s]\n", encode('cp932', $q);								# ????
		my $sth = $dbh->prepare("$q");
		$sth->execute;
		if ( $sth->rows ) {
			($duplicateArticleId) = $sth->fetchrow_array;
		}
		$sth->finish();
		#printf "** duplicateArticleId=(%d)\n", $duplicateArticleId;


		# contentsテーブル : # 編集したコンテンツのレコードをコピー
		$q =	"INSERT INTO `contents` ".
				"SELECT NULL AS id, '$threadId' AS threadid, '$duplicateArticleId' AS articleid, page, contents ".
				"FROM `contents` WHERE threadid='$editThreadId' AND articleid='$editArticleId'";
		#printf "** q=[%s]\n", encode('cp932', $q);				# ????
		$dbh->do( $q );

		# 送信完了メッセージを送出
		$self->send( encode('cp932', sprintf( qq("%s"\r\n), $to )) ) if ( $users == 1 );		# 一件のみなら表示しない
		$self->send( encode('cp932', "** 手紙を送信しました **\r\n") );

		# もし送信先ユーザがログイン中ならメール着信通知を送る(INCOMING_MAIL)
		my $tasks = $self->config('TASKS');		#	$TASKS->[$i] = $fileno : $self->{'nodes'}->[n] = 
		foreach my $task ( @{ $tasks } ) {
			if ( defined($task) ) {
				#printf "** to=[%s], task.id=[%s]\n", $to, $task->{ID};							# ????
				$task->{INCOMING_MAIL} = 1 if ( $task->{ID} eq $to );							# 着信フラグを立てる
			}
		}
	}

	# コピー元レコードを削除
	my $q =	"DELETE FROM `articles` WHERE threadid='$editThreadId' AND articlenum='$editArticleNum'";
	#printf "** q=[%s]\n", encode('cp932', $q);					# ????
	$dbh->do( $q );
	$q =	"DELETE FROM `contents` WHERE threadid='$editThreadId' AND articleid='$editArticleId'";
	#printf "** q=[%s]\n", encode('cp932', $q);					# ????
	$dbh->do( $q );
	$dbh->disconnect;
}

#--------------------------------------------------
# ■ ユーザチェック
# r = userCheck( $userid )
# 拒否ユーザとゲストユーザには送信できない
#
# r:		= [ userid, handlename, userlv];		# (該当ユーザあり)
# 			= undef;								# (該当ユーザなし)
#
sub userCheck {
	#print "\n(usercheck)\n";					# ????
	my $self = shift;
	my $to = shift;
	my $userid;
	my $handlename;
	my $userlv;

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	my $q = "SELECT userid, handlename, level FROM `users` WHERE userid = '$to' AND level > 1";
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	if ( $sth->rows ) {
		($userid, $handlename, $userlv) = $sth->fetchrow_array();
	}
	$sth->finish();
	$dbh->disconnect();

	#if ( defined($userid) ) {															# ????
	#	printf "** userid=[%s]\n", $userid;												# ????
	#	printf "** userlv=(%d)\n", $userlv;												# ????
	#	printf "** handlename=[%s]\n", encode('cp932', decode('utf8', $handlename));	# ????
	#}																					# ????
	#else {																				# ????
	#	print "** (no_user)\n";															# ????
	#}																					# ????

	#print "(/usercheck)\n";					# ????
	if ( defined($userlv) ) {
		return [ $userid, $handlename, $userlv ];
	}
	else {
		return undef;
	}
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;
	$self->from_param( 'LOCATION' => 'MAIL' );				# 現在地
	$self->from_param( '_RECIPIENTS' => undef );			# 送信先リスト（ユーザが入力した送信先ＩＤの保存場所）
	$self->from_param( '_SUBJECT' => '' );					# タイトル
	$self->from_param( '_CONTENTS' => '' );					# 本文
	#print Dumper($self->{'param'});						# ????
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;
	$self->from_param('-_RECIPIENTS');
	$self->from_param('-_SEND_TO');
	$self->from_param('-_SUBJECT');
	$self->from_param('-_CONTENTS');

	$self->BIGModel::Generic::modoru();						# 戻る
}

#--------------------------------------------------
# ■呼び出し
#
sub write {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;