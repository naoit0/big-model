package BIGModel::Mail::Write;
our $VERSION = "0001.20180512.2359";	# 着手
our $VERSION = "0002.20180520.1552";	# AUXEDIT対応 着手
our $VERSION = "0003.20180522.2015";	# 完了	userlist対応予定
our $VERSION = "0004.20180609.0821";	# 新Arg対応
our $VERSION = "0004.20180623.0055";	# msgs()対応
#
# BIG-Model ホストプログラム
# 電子メール（送信）
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ Mail::Write ]]\n\n";
		my $self = shift;
		$self->BIGModel::Mail::Write::initalize;
		if ( $self->comm->param('_SEND_TO') ) {																	# 送信先ＩＤが指定されていれば、ＩＤ入力をスキップ
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Write::write(3) });
		}
		else {
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Write::write(1) });
		}
	},

	'1' =>	# 送り先のＩＤを入力してください
	sub {
		my $self = shift;
		$self->BIGModel::Generic::prompt('PROMPT_SENDER', '-echo'=>1, '-line'=>1, '-size'=>46, '-mask'=>0 );		# 送り先のＩＤを入力してください
		$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Write::write(2) });
	},

	'2' =>	# 入力待ち
	sub {
		my $self = shift;
		my $comm = $self->comm;
		my $arg = $self->arg->arg();
		my $chr = $self->arg->status('-ctrlchr');
			$chr = ( defined($chr) ) ? $chr : '';

		if ( $chr eq "\x1b" || $chr eq "\x03" ) {			# [ETX][ESC]			終了
			$comm->send("\r\n");
			$self->BIGModel::Mail::Write::finalize;
		}
		elsif ( $chr eq "\x0d" ) {
			$comm->send("\r\n");
			if ($arg eq '') {								# (ヌル)				終了
				$self->BIGModel::Mail::Write::finalize;
			}
			elsif ($arg eq '?') {							# [?]					ユーザリスト表示ユーティリティを呼び出す
				$comm->param( 'USERLIST_RETURN' => sub { $self->BIGModel::Mail::Write::write(1) } );				# 戻り先を定義
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::UserSetting::Userlist::userlist });
			}
			else {											# (その他の文字列)		ユーザチェック
				$comm->param( '_SEND_TO' => $arg );
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Write::write(3) });
			}
		}
	},

	'3' =>	# ユーザーチェック
	sub {
		my $self = shift;
		my $comm = $self->comm;
		my $r = $self->BIGModel::Mail::Write::userCheck( $comm->param('_SEND_TO') );
		unless ( defined($r) ) {
			$self->BIGModel::Generic::msgs('MAIL_NO_SENDER');
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Write::write(1) });
		}
		else {
			my ($userid, $handlename, $userlv) = @{$r};
			$comm->param( '_SEND_TO' => $userid );		# 入力したＩＤを保存しているＩＤに置き換える
			$comm->send( encode('cp932', sprintf( qq("%s"\r\n), decode('utf8', $handlename) )) );
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Write::write(4) });
		}
	},

	'4' =>	# この送り先でよいですか
	sub {
		my $self = shift;
		$self->BIGModel::Generic::prompt('AUXEDIT_MAIL_YESNO.1', '-echo'=>1, '-line'=>1, '-size'=>46, '-mask'=>0 );		# この送り先でよいですか
		$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Write::write(5) });
	},

	'5' =>	# 入力待ち
	sub {
		my $self = shift;
		my $comm = $self->comm;
		my $arg = $self->arg->arg();
		my $chr = $self->arg->status('-ctrlchr');
			$chr = ( defined($chr) ) ? $chr : '';

		if ( $chr eq "\x1b" || $chr eq "\x03" ) {		# [ETX][ESC]					ユーティリティを終了
			$comm->send("\r\n");
			$self->BIGModel::Mail::Write::finalize;
		}
		elsif ( $chr eq "\x0d" ) {
			$comm->send("\r\n");
			if ($arg =~ /^Y$/i) {						# [Y]							送信先リストに追加
				my $recipients = $comm->param('_RECIPIENTS');
				$recipients->{ $comm->param('_SEND_TO') } = 1;
				$comm->param( '_RECIPIENTS' => $recipients );
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Write::write(6) });
			}
			elsif ($arg =~ /^N$/i) {					# [N]							送信先の再入力
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Write::write(1) });
			}
			else {										# (ヌル・その他の文字列)		プロンプト再表示
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Write::write(4) });
			}
		}
	},

	'6' =>	# 他にも送りますか
	sub {
		my $self = shift;
		$self->BIGModel::Generic::prompt('AUXEDIT_MAIL_YESNO.2', '-echo'=>1, '-line'=>1, '-size'=>46, '-mask'=>0 );		# 他にも送りますか
		$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Write::write(7) });
	},

	'7' =>	# 入力待ち
	sub {
		my $self = shift;
		my $comm = $self->comm;
		my $arg = $self->arg->arg();
		my $chr = $self->arg->status('-ctrlchr');
			$chr = ( defined($chr) ) ? $chr : '';

		if ( $chr eq "\x1b" || $chr eq "\x03" ) {		# [ETX][ESC]					ユーティリティを終了
			$comm->send("\r\n");
			$self->BIGModel::Mail::Write::finalize;
		}
		elsif ( $chr eq "\x0d" ) {
			$comm->send("\r\n");
			if ($arg =~ /^Y$/i) {						# [Y]							次の送信先を入力
				$comm->param( '_SEND_TO' => undef );
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Write::write(1) });
			}
			else {										# (ヌル・その他の文字列)		エディタユーティリティを呼び出す
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Write::write(8) });
			}
		}
	},

	'8' =>	# エディタユーティリティ（ＡＵＸＥＤＩＴ）を呼び出す
	sub {
		my $self = shift;
		$self->comm->param( 'AUXEDIT_RETURN' => sub { $self->BIGModel::Mail::Write::write(9) } );			# ＡＵＸＥＤＩＴ終了後の戻り先を定義
		$self->BIGModel::Utility::Auxedit::auxedit;
	},

	'9' =>	# 編集したコンテンツをメールボックスに移動する
	sub {
		my $self = shift;
		my $r = $self->comm->param('AUXEDIT_RESULT');					# リターンコードを回収したら廃棄
		$self->comm->param( 'AUXEDIT_RESULT' => undef );
			$self->BIGModel::Mail::Write::sendMail if ( $r == 1 );		# ＡＵＸＥＤＩＴの処理結果が正常ならメールボックスの保存処理を実行
			$self->BIGModel::Mail::Write::finalize;
	},

};

#==================================================
# ■メールボックスに保存
# sendMail();
#
sub sendMail {
	#print "\n** <sendmail>\n\n";
	my $self = shift;
	my $comm = $self->comm;
	my @recipients = keys(%{ $comm->param('_RECIPIENTS') });				# 受信者ＩＤリスト
	my $to;																	# 受信者ＩＤ（リストから取り出したもの）
	my $author = $comm->param('ID');										# 投稿者（送信者ＩＤ＝自分のＩＤ）
	my $editThreadId = '#'.$author;											# 編集した文書のスレッドＩＤ
	my $editArticleId = $comm->param('AUXEDIT_ARTICLEID');					# 編集した文書の記事ＩＤ
		$comm->param( 'AUXEDIT_ARTICLEID' => undef );
	my $editArticleNum = $comm->param('AUXEDIT_ARTICLENUM');				# 編集スレッドの記事番号
		$comm->param( 'AUXEDIT_ARTICLENUM' => undef );
	#printf "** articleid=(%s:%d)\n", $editThreadId , $editArticleNum;
	
	my $users = ( $#recipients >= 1 ) ? 1 : 0;
	#printf "** recipients=(%d), users=(%d)\n", $#recipients, $users;

	my $dbh = DBI->connect('dbi:mysql:dbname=bigmodel;host=127.0.0.1', 'bigmodel', '', {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	# 受信者リストから一つずつ処理する
	while ( my $to = shift( @recipients ) ) {
		#printf "** recipients=(%d), users=(%d)\n", $#recipients, $users;
		my $threadId   = '@'.$to;																# 送信先ＩＤの先頭に'@'を付けたものをスレッド名とする
		my $data       = $self->BIGModel::DB::Articles::getLatestArticle( $threadId );			# スレッドの最新レコードを得る(** 確認中)
		my $articleNum = defined($data) ? ($data->[1] + 1) : 1;									# 未定義なら１に置き換える
		printf "** threadid=[%s], articlenum=[%d]\n", $threadId, $articleNum;

		# articlesテーブル : レコードをコピー
		my $q =	"INSERT INTO `articles` ".
				"SELECT NULL AS id, '$threadId' AS threadid, '$articleNum' AS articlenum, datetime, author, title, flag ".
				"FROM `articles` ".
				"WHERE id = '$editArticleId'";
		#printf "** q=[%s]\n", encode('cp932', $q);
		$dbh->do( $q );

		# articlesテーブル : コピーしたレコードのＩＤを取得
		my $duplicateArticleId;
		$q =	"SELECT id FROM `articles` ".
				"WHERE threadid = '$threadId' AND articlenum = '$articleNum'";
		#printf "** q=[%s]\n", encode('cp932', $q);
		my $sth = $dbh->prepare("$q");
		$sth->execute;
		if ( $sth->rows ) {
			($duplicateArticleId) = $sth->fetchrow_array;
		}
		$sth->finish();
		#printf "** duplicateArticleId=(%d)\n", $duplicateArticleId;


		# contentsテーブル : # 編集したコンテンツのレコードをコピー
		$q =	"INSERT INTO `contents` ".
				"SELECT NULL AS id, '$threadId' AS threadid, '$duplicateArticleId' AS articleid, page, contents ".
				"FROM `contents` ".
				"WHERE threadid = '$editThreadId' AND articleid = '$editArticleId'";
		#printf "** q=[%s]\n", encode('cp932', $q);
		$dbh->do( $q );

		# 送信完了メッセージを送出
		$comm->send( encode('cp932', sprintf( qq("%s"\r\n), $to )) ) if ( $users == 1 );		# 一件のみなら表示しない
		$comm->send( encode('cp932', "** 手紙を送信しました **\r\n") );

		# もし送信先ユーザがログイン中ならメール着信通知を送る(INCOMING_MAIL)
		foreach my $task ( @{ $self->comm->{'param'} } ) {
			if ( defined($task) ) {
				#printf "** to=[%s], task.id=[%s]\n", $to, $task->{ID};
				$task->{INCOMING_MAIL} = 1 if ( $task->{ID} eq $to );		# 着信フラグを立てる
			}
		}
	}

	# コピー元レコードを削除
	my $q =	"DELETE FROM `articles` WHERE threadid = '$editThreadId' AND articlenum = '$editArticleNum'";
	#printf "** q=[%s]\n", encode('cp932', $q);
	$dbh->do( $q );
	$q =	"DELETE FROM `contents` WHERE threadid = '$editThreadId' AND articleid = '$editArticleId'";
	#printf "** q=[%s]\n", encode('cp932', $q);
	$dbh->do( $q );

	$dbh->disconnect;
}

#--------------------------------------------------
# ■ ユーザチェック
# r = userCheck( $userid )
# 拒否ユーザとゲストユーザには送信できない
#
# r:		= [ userid, handlename, userlv];		# (該当ユーザあり)
# 			= undef;								# (該当ユーザなし)
#
sub userCheck {
	#print "\n** <userCheck>\n\n";
	my $self = shift;
	my $to = shift;
	my $userid;
	my $handlename;
	my $userlv;

	my $dbh = DBI->connect('dbi:mysql:dbname=bigmodel;host=127.0.0.1', 'bigmodel', '', {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	my $q = "SELECT userid, handlename, level FROM `users` WHERE userid = '$to' AND level > 1";
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	if ( $sth->rows ) {
		($userid, $handlename, $userlv) = $sth->fetchrow_array();
	}
	$sth->finish();
	$dbh->disconnect();

	#if ( defined($userid) ) {
	#	printf "** userid=[%s]\n", $userid;
	#	printf "** userlv=(%d)\n", $userlv;
	#	printf "** handlename=[%s]\n", encode('cp932', decode('utf8', $handlename));
	#}
	#else {
	#	print "** (no_user)\n";
	#}

	if ( defined($userlv) ) {
		return [ $userid, $handlename, $userlv ];
	}
	else {
		return undef;
	}
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	#printf "\n\n** < %s::initalize [%s] >\n", __PACKAGE__, Debug::_caller(caller);
	my $self = shift;
	my $comm = $self->comm;
		$comm->param( 'LOCATION' => 'MAIL' );			# 現在地
		$comm->param( '_RECIPIENTS' => undef );			# 送信先リスト（ユーザが入力した送信先ＩＤの保存場所）
		$comm->param( '_SUBJECT' => '' );				# タイトル
		$comm->param( '_CONTENTS' => '' );				# 本文
	#print Dumper($self->{'param'});
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	#printf "\n\n** < %s::finalize [%s] >\n", __PACKAGE__, Debug::_caller(caller);
	my $self = shift;
	my $comm = $self->comm;
		$comm->param( '_RECIPIENTS' => undef );
		$comm->param( '_SEND_TO' => undef );
		$comm->param( '_SUBJECT' => undef );
		$comm->param( '_CONTENTS' => undef );

	$self->BIGModel::Generic::Path::back();			# 現在のパスがルートでなければひとつ手前のユーティリティを呼び出す
}

#--------------------------------------------------
# ■呼び出し
#
sub write {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	if ( ref($func) eq 'CODE') {
		&{ $func }($self);
	}
}

#------------------------------------------------------
# ■ エントリポイントを返す
#
sub start {
	#printf "\n\n** < %s::start [%s] >\n", __PACKAGE__, Debug::_caller(caller);
	return \&BIGModel::Mail::Write::write;
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;