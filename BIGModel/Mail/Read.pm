package BIGModel::Mail::Read;
our $VERSION = "0001.20180523.1419";	# 着手
our $VERSION = "0002.20180622.0205";	# 完了
our $VERSION = "0002.20180704.0134";	# フィルタモードが正常動作しないので微調整
#
# BIG-Model ホストプログラム
# 電子メール（受信）
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
use DBI;
use Naoit0::DateTime;

my $subs = {

	'0' =>	# ■初期設定
	sub {
		print "\n** [[ Mail::Read ]]\n\n";
		my $self = shift;
		$self->BIGModel::Mail::Read::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# ** アクセス権がありません **
			$self->BIGModel::Mail::finalize();
		}
		else {
			my $r = $self->BIGModel::Mail::Read::startup();							# メールボックスの状態を得る
			$self->from_param( '_INBOX' => $r );									# 得た値をパラメータに保存
			my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>0, 'line'=>0 );										# ローカルエコー、ライン入力を無効
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(21) } );		# メールレポート処理
		}
	},

	'1' =>	# スタート位置を取得
	sub {
		my $self = shift;
		my $inbox = $self->from_param('_INBOX'); $self->from_param('-_INBOX');		# パラメータの値を取得して廃棄
		if ( $inbox >= 1 ) {														# メールが保管されている
			$self->BIGModel::Generic::sendmsgs('SEEMAIL.1');
			if ( $inbox == 2 ) {													# 新着メールがない
				$self->BIGModel::Mail::Read::articles();							# 最新のリストを表示
				$self->BIGModel::Mail::Read::down();								# 閾値に移動
			}
			$self->BIGModel::Mail::Read::articles();								# 現在値を表示
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(2) } );
		}
		else {																		# メールが保管されていない
			$self->BIGModel::Generic::sendmsgs( 'MAIL_NO_NEWMAIL' );					# 手紙は届いていません
			$self->BIGModel::Mail::Read::finalize;
		}
	},

	'2' =>	# ■操作
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $a = $arg->arg(); $a = defined($a) ? $a : '';

		# ●[CTRL+C][ESC][E]						【 終了 】
		if ( $a =~ /[\x03|\x1b|E|e]/ ) {
			$self->BIGModel::Mail::Read::finalize;
		}

		# ●番号[RETURN]							【 番号指定 】
		elsif ( $a =~ /[\d|\#]/ ) {
			$self->from_param( '_INPUT' => $a ) if ($a =~ /\d/);					# 入力した数字を保存
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(5) } );
		}

		# ●"キーワード[RETURN]						【 タイトル検索 】
		elsif ( $a eq "\"" ) {
			$self->from_param( '_READ_FILTER_MODE' => 'title' );					# 検索モードはタイトル検索
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(7) } );
		}

		# ●'キーワード[RETURN]						【 全文検索 】
		elsif ( $a eq "\'" ) {
			$self->from_param( '_READ_FILTER_MODE' => 'full' );						# 検索モードは全文検索
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(7) } );
		}

		# ●[CTRL+E][↑]							【 旧方向へ進む 】
		elsif ( $a =~ /[\x05|\x1e]/ ) {
			#print "** (up)\n";
			$self->BIGModel::Mail::Read::up();										# 前方に移動
			$self->BIGModel::Mail::Read::articles();								# 項目を表示
		}

		# ●[J]
		elsif ( $a eq 'J' ) {
			#print "** (up)\n";
			$self->BIGModel::Mail::Read::up();										# 前方に移動
			$self->BIGModel::Mail::Read::articles();								# 項目を表示
		}

		# ●[SHIFT+K]
		elsif ( $a eq 'k' ) {
			#print "** (up)\n";
			$self->BIGModel::Mail::Read::up();										# 前方に移動
			$self->BIGModel::Mail::Read::articles();								# 項目を表示
		}

		# ●[CTRL+R][←]							【 旧方向へ読み進む 】
		elsif ( $a =~ /[\x12|\x1d]/ ) {
			#print "** (back)\n";
			$self->from_param( '_MOVE' => 'BACK' );									# 後方に移動
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(9) } );		# 現在地を読む
		}

		# ●[L]
		elsif ( $a eq 'L' ) {
			#print "** (back)\n";
			$self->from_param( '_MOVE' => 'BACK' );									# 後方に移動
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(9) } );		# 現在地を読む
		}

		# ●[SHIFT+H]
		elsif ( $a eq 'h' ) {
			#print "** (back)\n";
			$self->from_param( '_MOVE' => 'BACK' );									# 後方に移動
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(9) } );		# 現在地を読む
		}

		# ●[CTRL+D][RETURN]						【 現在位置を読む 】
		elsif ( $a =~ /[\x04|\x0d]/ ) {
			#print "** (cr)\n";
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(9) } );		# 現在地を読む
		}

		# ●[CTRL+F][→]							【 新方向へ読み進む 】
		elsif ( $a =~ /[\x06|\x1c]/ ) {
			#print "** (next)\n";
			$self->from_param( '_MOVE' => 'NEXT' );									# 移動方向は前方
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(9) } );		# 現在地を読む
		}

		# ●[H]
		elsif ( $a eq 'H' ) {
			#print "** (next)\n";
			$self->from_param( '_MOVE' => 'NEXT' );									# 移動方向は前方
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(9) } );		# 現在地を読む
		}

		# ●[SHIFT+L]
		elsif ( $a eq 'l' ) {
			#print "** (next)\n";
			$self->from_param( '_MOVE' => 'NEXT' );									# 移動方向は前方
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(9) } );		# 現在地を読む
		}

		#●[CTRL+X][↓][ ]							【 新方向へ進む 】
		elsif ( $a =~ /[\x18|\x1f|\x20]/ ) {
			#print "** (down)\n";
			$self->BIGModel::Mail::Read::down();									# 後方に移動
			$self->BIGModel::Mail::Read::articles();								# 項目を表示
		}

		# ●[K]
		elsif ( $a eq 'K' ) {
			#print "** (down)\n";
			$self->BIGModel::Mail::Read::down();									# 後方に移動
			$self->BIGModel::Mail::Read::articles();								# 項目を表示
		}
		# ●[SHIFT+J]
		elsif ( $a eq 'j' ) {
			#print "** (down)\n";
			$self->BIGModel::Mail::Read::down();									# 後方に移動
			$self->BIGModel::Mail::Read::articles();								# 項目を表示
		}

		# ●[*]										【 削除 】
		elsif ( $a eq '*' ) {
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(13) } );
		}

		# ●[W]										【 メールを書く・返信 】
		elsif ( $a =~ /W/i ) {
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(17) } );
		}

		# ●[.]
		elsif ( $a eq  '.' ) {
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(17) } );
		}

		# ●[SHIFT+R]								【 旧方向へ読み続ける 】
		elsif ( $a eq 'r' ) {
			$self->from_param( '_MOVE' => 'BACK' );
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(15) } );
		}

		# ●[R]										【 新方向へ読み続ける 】
		elsif ( $a eq 'R' ) {
			$self->from_param( '_MOVE' => 'NEXT' );
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(15) } );
		}

		# ●[T]										【 タイトル一覧（現在値より旧方向へ） 】
		elsif ( $a eq 'T' ) {
			$self->from_param( '_MOVE' => 'BACK' );									# 移動方向は後方
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(11) } );
		}

		# ●[SHIFT+T]								【 タイトル一覧（現在値より新方向へ） 】
		elsif ( $a eq 't' ) {
			$self->from_param( '_MOVE' => 'NEXT' );									# 移動方向は前方
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(11) } );
		}

		# ●[SHIFT+T]								【 電報 】
		elsif ( $a eq '!' ) {
			$self->from_param( '_RETURN' => sub { $self->BIGModel::Mail::Read::read(14) } );	# 戻り先を設定
			$self->sethandler( sub { $self->BIGModel::Telegram::telegram } );
		}

		# ●[?]										【 説明 】
		elsif ( $a eq "?" ) {
			$self->BIGModel::Generic::sendmsgs('SEEMAIL.2');
			$self->BIGModel::Mail::Read::articles();
		}

		# ●;ジャンプ先[RETURN]						【 シグ間ジャンプ 】
		elsif ( $a eq ';' ) {
			$self->from_param( '_INPUT' => $a );									# 入力した文字を保存
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(19) } );
		}
	},

#---------------------------------------------
# ■【 番号指定 】
#---------------------------------------------
	'5' =>	# 番号を入力してください
	sub {
		my $self = shift;
		my $input = $self->from_param('_INPUT'); $self->from_param('-_INPUT');			# 直前の入力文字を回収して廃棄
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );								# 入力モードを設定
			$arg->param( 'line_size'=>10, 'maskchr'=>'*', 'allow_chr'=>'[\d]' );		# パラメータを設定
		$self->BIGModel::Generic::sendmsgs('PROMPT_NUMBER');								# 「番号を入力してください」
		$arg->store( $arg );															# 直前の文字を蓄積バッファに入れる
		$self->sethandler( sub { $self->BIGModel::Mail::Read::read(6) } );
	},

	'6' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $a = $arg->arg(); $a = defined($a) ? $a : '';

		# ● [ETX][ESC]					【キャンセル】
		if ( $a eq "\x03" ) {
			$arg->flush();
			$self->send("\r\n");
			$arg->mode( 'echo'=>0, 'line'=>0 );			# 入力モードをリセット
			$arg->param( 'allow_chr'=>'' );				# 入力モードをリセット
			$self->BIGModel::Mail::Read::articles();
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(2) } );
		}
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			$arg->mode( 'echo'=>0, 'line'=>0 );			# 入力モードをリセット
			$arg->param( 'allow_chr'=>'' );				# 入力モードをリセット
			if ( $input =~ /\d+/ ) {
				$input =~ s/#//;
				$self->from_param( '_READ_MOVE_TO_NUMBER' => $input );
			}
			$self->BIGModel::Mail::Read::articles();
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(2) } );
		}
	},

#---------------------------------------------
# ■ 【 タイトル検索／全文検索 】
#---------------------------------------------
	'7' =>	# 検索文字列を入力してください
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		$arg->mode( 'echo'=>1, 'line'=>1 );								# ローカルエコー、ライン入力を有効
		$arg->param( 'size'=>60 );										# 入力バッファサイズを調整
		$self->BIGModel::Generic::sendmsgs('PROMPT_SEARCH_STRING');			# 検索文字列を入力してください
		$self->sethandler( sub { $self->BIGModel::Mail::Read::read(8) } );
	},

	'8' =>	# 入力待ち
	sub {
		my $self = shift;
		my $filter_mode = $self->from_param('_READ_FILTER_MODE');		# フィルタモード
		my $arg = $self->from_param('_Arg');
		my $a = $arg->arg(); $a = defined($a) ? $a : '';

		# ● [ETX][ESC]					【キャンセル】
		if ( $a eq "\x03" ) {
			$arg->flush();
			$self->send("\r\n");
			$self->from_param( '_READ_FILTER_MODE' => undef );						# フィルタモードをリセット
			$self->from_param( '_READ_FILTER_KEYWORD' => undef );					# フィルタキーワードをクリア
			$arg->mode( 'echo'=>0, 'line'=>0 );										# ローカルエコー、ライン入力を無効
			$self->BIGModel::Mail::Read::articles();
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(2) } );
		}
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			$arg->mode( 'echo'=>0, 'line'=>0 );								# ローカルエコー、ライン入力を無効
			# ● (ヌル)
			if ( $input ne '' ) {
				$self->from_param( '_READ_FILTER_KEYWORD' => $input );		# フィルタキーワード
			}
			# ● (その他)
			else {
				$self->from_param( '_READ_FILTER_MODE' => undef );			# フィルタモードをリセット
				$self->from_param( '_READ_FILTER_KEYWORD' => undef );		# フィルタキーワードをクリア
			}
			$self->BIGModel::Mail::Read::articles();
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(2) } );
		}
	},

#---------------------------------------------
# ■ 現在位置を読む
#---------------------------------------------
	'9' =>
	sub {
		my $self = shift;
		my $data = $self->from_param('_READ_MYPLACE_DATA');			# 現在値のデータ
		my $move = $self->from_param('_MOVE');						# 移動方向
		my $threshold = $self->from_param('_READ_THRESHOLD');		# 閾値フラグ（現在値が閾値を踏んだら１、踏まなければ０にする）
		my $eot = 0;												# 最終ページ（または取得失敗）フラグ
		my $exit = 0;												# 終了フラグ

		# 呼び出し直前の状態（確認用）
		#print "\n";														# ????
		#	printf "** move=[%s]", ( defined($move) ? $move : '-' );		# ????
		#	printf ", eot=(%d)", $eot;										# ????
		#	print "\n";														# ????
		#print "\n";														# ????

		# ■コンテンツを表示
		#-------------------------------------
		my $flag = defined( $data->[6] ) ? $data->[6] : '';
		unless ( $flag =~ /[\*|\+|\-]/ ) {													# 削除フラグが立っているなら
			if ( $self->BIGModel::Mail::Read::view() ) {									# (1) 続きのページがある
				$eot = 0;
				$self->sethandler( sub { $self->BIGModel::Mail::Read::read(10) } );			# キー入力
			}
			else {																			# (0) 終了または取得失敗
				$eot = 1;
				$exit = 1;
			}
		}
		else {																				# 削除フラグが立っていないなら
			$eot = 1;
			$threshold = 1;																	# 閾値とみなす
			$exit = 1;
		}

		#printf "*1* threshold=[%s]\n", $threshold;

		# ■コンテンツの出力が終わったら移動とリストを表示
		#-------------------------------------
		if ( $eot == 1 ) {
			my $r;
			$self->from_param( '_FILTER_END_RECORD' => undef );						# みなし閾値は毎回リセット
			$move = $self->from_param('_MOVE');										# 移動方向を得る

			if ( defined($move) ) {													# 移動方向が指定されている
				$self->from_param( '_MOVE' => undef );
				$self->BIGModel::Mail::Read::up() if ( $move =~ /BACK/i );
				$self->BIGModel::Mail::Read::down() if ( $move =~ /NEXT/i );
				if ( $threshold == 1 )												# 現在値を閾値から移動する場合
					{ $r = $self->BIGModel::Mail::Read::articles(); }				# リストにヘッダを付けないで送出する
				else																# 現在値を閾値でない場所から移動する場合
					{ $r = $self->BIGModel::Mail::Read::articles(1); }				# リストにヘッダを付けて送出する
			}
			else {																	# 移動方向が指定されていない
				if ( $threshold == 0 )												# 閾値フラグ＝０なら
					{ $r = $self->BIGModel::Mail::Read::articles(); }				# リストにヘッダを付けないで送出する
			}

			if ( defined($r) == 1 && $r == -1 ) {									# フィルタモードで現在値が該当レコードを外れた場合
				$self->from_param( '_FILTER_END_RECORD' => 1 );						# フラグをセット
			}
			else {
				$self->from_param( '_FILTER_END_RECORD' => 0 );						# フラグをリセット
			}
		}

		#printf "*2* threshold=[%s]\n", $threshold;

		# 終了後の状態（確認用）
		#print "\n";														# ????
		#	printf "** move=[%s]", ( defined($move) ? $move : '-' );		# ????
		#	printf ", eot=(%d)", $eot;										# ????
		#	print "\n";														# ????
		#print "\n";														# ????

		if ( $exit == 1 ) {
			my $return = $self->from_param('_RETURN'); $self->from_param('-_RETURN');		# パラメータを取得し廃棄
			if ( ref($return) eq 'CODE' ) {													# 戻り先が定義されていたら
				$self->sethandler( sub { $self->$return } );								# 指定先に戻る
			}
			else {
				$self->sethandler( sub { $self->BIGModel::Mail::Read::read(2) } );			# 戻る
			}
		}
	},

	'10' =>	# キー入力
	sub {
		my $self = shift;
		$self->sethandler( sub { $self->BIGModel::Mail::Read::read(9) } );			# ※プロンプトは無効中
		# my $arg = $self->from_param('_Arg');
		# my $a = $arg->arg(); $a = defined($a) ? $a : '';
		# # ● [ETX][ESC]					【キャンセル】
		# if ( $a eq "\x03" ) {
		# 	$self->from_param( '_MOVE' => undef );									# パラメータを廃棄
		# 	$self->sethandler( sub { $self->BIGModel::Mail::Read::read(2) } );		# メインに戻る
		# }
		# else {
		# 	$self->sethandler( sub { $self->BIGModel::Mail::Read::read(9) } );		# 次の処理に移る
		# }
	},

#---------------------------------------------
# ■ 【 タイトル一覧 】
#---------------------------------------------
	'11' =>
	sub {
		my $self = shift;
		my $move = $self->from_param('_MOVE');											# 移動方向
		#printf "** move=[%s]\n", $move;												# ????
		$self->BIGModel::Mail::Read::up() if ( $move =~ /BACK/i );						# 後退したら上がる
		$self->BIGModel::Mail::Read::down() if ( $move =~ /NEXT/i );					# 前進したら下がる
		my $r = $self->BIGModel::Mail::Read::articles();
		my $threshold = $self->from_param('_READ_THRESHOLD');							# 閾値フラグ（articles(()呼び出し後で現在値が更新されている）
		#printf "** r=(%d), threshold=(%d)\n", $r, $threshold;							# ????
		unless ( $r == -1 || $threshold == 1 ) {										# 現在値が閾値またはみなし閾値にいないなら
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(12) } );			# キー入力
		}
		else {
			$self->from_param('-_MOVE');												# パラメータを廃棄
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(2) } );			# 戻る
		}
	},

	'12' =>	# キー入力
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $a = $arg->arg(); $a = defined($a) ? $a : '';
		# ● [ETX][ESC]					【キャンセル】
		if ( $a eq "\x03" ) {
			$arg->flush();
			$self->from_param('-_MOVE');												# パラメータを廃棄
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(2) } );			# メインに戻る
		}
		else {
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(11) } );			# 次の処理に移る
		}
	},

#---------------------------------------------
# ■ 【 削除 】
#---------------------------------------------
	'13' =>	# 
	sub {
		my $self = shift;
		my $threshold = $self->from_param('_READ_THRESHOLD');								# 閾値フラグ
		#printf "** threshold=[%s]\n", $threshold;											# ???
		if ( $threshold == 0 ) {
			my $threadId = $self->from_param('THREADID');									# スレッドＩＤ
			my $data = $self->from_param('_READ_MYPLACE_DATA');								# 現在値のデータ
			my $num = $data->[0];															# 文書ＩＤ
			my $flag = defined($data->[6]) ? $data->[6] : '';								# 文書フラグ

			if ( $flag =~ /\*/ ) {															# 削除フラグ＝１なら
				$flag =~ s/\*//g;															# 削除フラグ＝０
			}
			else {																			# 削除フラグ＝０なら
				$flag .= '*';																# 削除フラグ＝１
			}
			$self->BIGModel::Generic::DB::Articles::updFlag( $threadId, $num, $flag );		# フラグを更新
			$self->BIGModel::Mail::Read::articles();										# リストを表示する
		}
		$self->sethandler( sub { $self->BIGModel::Mail::Read::read(2) } );					# メインに戻る
	},

#---------------------------------------------
# ■ 【 電報 】（戻り先）
#---------------------------------------------
	'14' =>
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>0, 'line'=>0 );									# ローカルエコー、ライン入力を無効
		$self->BIGModel::Mail::Read::articles();
		$self->sethandler( sub { $self->BIGModel::Mail::Read::read(2) } );		# メインに戻る
	},

#---------------------------------------------
# ■ 【 新方向へ読み続ける 】
#---------------------------------------------
	'15' =>	# 新方向へ読み進む	閾値またはにいるなら終了
	sub {
		my $self = shift;
		my $move = $self->from_param('_MOVE');			# 移動方向
		#printf "** move=[%s]\n", $move;				# ????
		if ( defined($move) ) {
			$self->from_param( '__MOVE' => $move );													# 読み進むを抜けるとパラメータが廃棄されるため保存しておく
			$self->BIGModel::Mail::Read::up() if ( $move =~ /BACK/i );
			$self->BIGModel::Mail::Read::down() if ( $move =~ /NEXT/i );
			$self->from_param( '_RETURN' => sub { $self->BIGModel::Mail::Read::read(16) } );		# 読み進む(9)を終了したら分岐・キー入力(16)に移動する
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(9) } );						# 読み進む(9)を読み出す
		}
		else {
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(2) } );						# メインに戻る
		}
	},

	'16' =>	# 分岐・キー入力
	sub {
		my $self = shift;
		my $threshold = $self->from_param('_READ_THRESHOLD');							# 閾値フラグ（articles(()呼び出し後で現在値が更新されている）
		my $end_record = $self->from_param('_FILTER_END_RECORD');						# みなし閾値フラグ（フィルタモードで該当レコードの外に出た）
		#printf "threshold=(%d)\n", $r, $threshold;										# ????
		if ( $end_record == 1 || $threshold == 1 ) {									# 現在値が閾値なら
			$self->from_param('-__MOVE');												# パラメータを廃棄
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(2) } );			# メインに戻る
		}
		else {																			# 現在値が閾値でなければ
			my $arg = $self->from_param('_Arg');
			my $a = $arg->arg(); $a = defined($a) ? $a : '';
			if ( $a eq "\x03" ) {
				$self->from_param('-__MOVE');											# パラメータを廃棄
				$self->sethandler( sub { $self->BIGModel::Mail::Read::read(2) } );		# メインに戻る
			}
			else {
				$self->from_param( '_MOVE' => $self->from_param('__MOVE') );			# パラメータを復旧
				$self->sethandler( sub { $self->BIGModel::Mail::Read::read(15) } );		# 次の処理に移る
			}
		}
	},

#---------------------------------------------
# ■ 【 メールを書く・返信 】
#---------------------------------------------
	'17' => # 返信かどうか？
	sub {
		my $self = shift;
		my $threshold = $self->from_param('_READ_THRESHOLD');				# 閾値フラグ
		#printf "** threshold=[%s]\n", $threshold;							# ????

		# 送信元があれば返信、なければ送信
		if ( $threshold == 0 ) {
			my $data = $self->from_param('_READ_MYPLACE_DATA');										# 現在値のデータ
			$self->from_param( '_SEND_TO' => $data->[4] );											# 返信するＩＤを指定
			$self->BIGModel::Generic::sendmsgs('MAIL_QUERY_SENDER');									# ** 送信先検索中 **
		}
		$self->from_param( '_RETURN' => sub { $self->BIGModel::Mail::Read::read(18) } );			# 戻り先を定義
		$self->sethandler( sub { $self->BIGModel::Mail::Write::write } );							# 「書く」に移動
	},

	'18' =>	# 「書く」からの戻り先
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		$arg->mode( 'echo'=>0, 'line'=>0, 'size'=>0 );								# 入力モードをリセット
		$self->BIGModel::Mail::Read::articles();									# リストを送出
		$self->sethandler( sub { $self->BIGModel::Mail::Read::read(2) } );			# メインに戻る
	},

#---------------------------------------------
# ■ 【 シグ間ジャンプ 】
#---------------------------------------------
	'19' =>
	sub {
		my $self = shift;
		my $input = $self->from_param('_INPUT'); $self->from_param('-_INPUT');			# 直前の入力文字を回収して廃棄
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );								# 入力モードを設定
			$arg->param( 'line_size'=>60 );												# パラメータを設定
		$self->BIGModel::Generic::sendmsgs('PROMPT_SIGJUMP');								# 「移動先を入力してください」
		$arg->store( $input );															# 直前の文字を蓄積バッファに入れる
		$self->sethandler( sub { $self->BIGModel::Mail::Read::read(20) } );
	},

	'20' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $a = $arg->arg(); $a = defined($a) ? $a : '';

		# ● [ETX][ESC]					【キャンセル】
		if ( $a eq "\x03" ) {
			$arg->flush();
			$self->send("\r\n");
			$arg->mode( 'echo'=>0, 'line'=>0 );											# 入力モードをリセット
			$self->BIGModel::Mail::Read::articles();									# 項目を表示
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(2) } );			# メインに戻る
		}
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			# ●(ヌル)					【キャンセル】
			if ( $input eq '' ) {
				$arg->mode( 'echo'=>0, 'line'=>0 );										# 入力モードをリセット
				$self->BIGModel::Mail::Read::articles();								# リストを送出
				$self->sethandler( sub { $self->BIGModel::Mail::Read::read(2) } );		# メインに戻る
			}
			else {
				$input =~ s/\;//;
				$self->BIGModel::Mail::Read::finalize();								# 終了処理
			}
		}
	},

#---------------------------------------------
# ■ メールレポートを表示
#---------------------------------------------
	'21' =>	# メイン処理
	sub {
		my $self = shift;

		my $next = $self->from_param('_MAILREPORT_NEXT'); $self->from_param('-_MAILREPORT_NEXT');	# メールレポート処理終了フラグ
		my $type = $self->from_param('_MAILREPORT_TYPE');											# メールレポート処理モード
		#printf "*1* type=(%d)\n", $type;															# ????
		unless( defined($type) )
			{ $type = 1; $self->from_param( '_MAILREPORT_TYPE' => $type ); }
		#printf "*2* type=(%d)\n", $type;															# ????

		if ( defined($next) ) {
			$self->BIGModel::Mail::Read::removeMailReport('READED') if ( $type == 1 );				# 開封済みメールレポートを削除
			$self->BIGModel::Mail::Read::removeMailReport('DELETED') if ( $type == 2 );				# 削除済みメールレポートを削除
			$type++;
			$self->from_param( '_MAILREPORT_TYPE' => $type );										# 次の処理モードを切り替える
		}

		# 処理するメールレポートスレッドを選択
		my $r;
		if ( $type == 1 ) {
			#print "** ( readed_mailreport )\n";											# ????
			$r = $self->BIGModel::Mail::Read::readedMailReport(10);							# 開封済みメール
		}
		if ( $type == 2 ) {
			#print "** ( deleted_mailreport )\n";											# ????
			$r = $self->BIGModel::Mail::Read::deletedMailReport(10);						# 削除済みメール
		}
		if ( $type == 3 ) {
			#print "** ( stored_mailreport )\n";											# ????
			$r = $self->BIGModel::Mail::Read::storedMailReport(10);							# 保管メール
		}

		if ( $type <= 3 ) {
			if ( $r == 1 ) {																# 続きのレコードがある
				$self->BIGModel::Generic::sendmsgs('PROMPT_MORE');								# ** 続きを表示します **
				$self->sethandler( sub { $self->BIGModel::Mail::Read::read(22) } );
				#print "** <22>\n";															# ????
			}
			else {																			# 全てのページを出力した
				$self->from_param( '_MAILREPORT_NEXT' => 1 );								# 次の処理に進む
				$self->from_param( '_NOSTOP' => undef );									# ページ表示抑止を有効にする
				$self->sethandler( sub { $self->BIGModel::Mail::Read::read(21) } );
			}
		}
		else {																				# 全てのメールレポートを終了した
			$self->from_param('-_MAILREPORT_TYPE');											# パラメータを廃棄
			$self->from_param('-_MAILREPORT_NEXT');
			#$self->from_param('_NOSTOP' => undef );
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(1) } );
		}
	},

	'22' =>	# 入力待ち
	sub {
		my $self = shift;
		my $nonstop = $self->from_param('_NOSTOP');
		my $arg = $self->from_param('_Arg');
		my $a = $arg->arg(); $a = defined($a) ? $a : '';
		if ( defined($nonstop) ) {														# ページ表示抑止が有効なら
			$self->send( "\x08\x20\x08" x 22 );											# プロンプトを消去
			$self->sethandler( sub { $self->BIGModel::Mail::Read::read(21) } );			# メイン処理に移動
		}
		else {																			# ページ表示抑止が無効なら
			# ●[ETX][ESC]				【 ページ表示を中断 】
			if ( $a eq "\x03" || $a eq "\x1b" ) {
				$self->send( "\x08\x20\x08" x 22 );
				$self->send("\r\n");
				$self->from_param( '_MAILREPORT_NEXT' => 1 );							# 次の処理に進む
				$self->from_param('-_MAILREPORT_DATA');									# 処理中のデータを廃棄
				$self->sethandler( sub { $self->BIGModel::Mail::Read::read(21) } );
			}
			# ●[CR]					【 次のページを表示 】
			elsif ( $a eq "\x0d" ) {
				$self->send( "\x08\x20\x08" x 22 );
				$self->from_param( '_NOSTOP' => undef );								# ページ表示抑止を無効
				$self->sethandler( sub { $self->BIGModel::Mail::Read::read(21) } );
			}
			# ●(空白)					【 全てのページを表示 】
			elsif ( $a eq "\x20" ) {
				$self->send( "\x08\x20\x08" x 22 );
				$self->from_param( '_NOSTOP' => 1 );									# ページ表示抑止を有効
				$self->sethandler( sub { $self->BIGModel::Mail::Read::read(21) } );
			}
		}
	},

};

#==================================================
# ■記事を表示
# r = view();
# r = 0: 出力終了（待機中）, 1:出力途中
#
sub view {
	my $self = shift;
	my $threadId = $self->from_param('THREADID');					# スレッドＩＤ
	my $num      = $self->from_param('_READ_MYPLACE_ID');			# 現在値（articles.id )
	my $pages    = $self->from_param('_VIEWPAGES');					# 閲覧中のページＩＤリスト
	my $data     = $self->from_param('_READ_MYPLACE_DATA');			# 現在値のデータ
	#printf "** threadId=[%s], num=(%d), pages.defined=(%d)\n", $threadId, $num, defined($pages);			# ????

	return if ( $num <= 0 );					# 現在地が無効なら終了

	unless ( defined($pages) ) {				# ページＩＤリストがなければ取得
		$pages = $self->BIGModel::Generic::DB::Contents::getPagesId( $threadId, $num ) if ( defined($threadId) && defined($num) );
		$self->send( "\r\n" );
	}
	#print Dumper($pages);						# ????
	return unless ( defined($pages) );			# それでもページＩＤリストがなければ終了

	my $id = shift @{ $pages };
	if ( defined($id) ) {
		my $body = $self->BIGModel::Generic::DB::Contents::getContents( $id );							# コンテンツを取得
		$data->[6] = '' unless ( defined($data->[6]) );
		#printf "** data[6]=[%s], data[6].length=(%d)\n", $data->[6], length($data->[6]);				# ????
		if ( $data->[6] !~ /\#/ && defined( $self->from_param('_VIEWED') ) == 0 ) {						# 閲覧フラグ＝０なら
			$data->[6] .= '#';																			# 閲覧フラグ＝１
			$self->BIGModel::Generic::DB::Articles::updFlag( $threadId, $num, $data->[6] );				# フラグを更新
			#$self->from_param( '_VIEWED' => 1 );														# 閲覧フラグを廃棄
		}

		$body = encode('cp932', decode('utf8', $body));
		#printf "** body=[%s], body.length=(%d)\n", $body, length($body);				# ????
		$body =~ s/\n/\r\n/g;

		if ( $body =~ /\x03/ ) {											# 最終ページ（終端がＥＴＸ）なら
			$body =~ s/\x03//;												# 終端文字を除去
			$self->send( $body ) unless ($body eq '');						# ヌルでなければコンテンツを送出
			$self->send( "\r\n" );
			$self->from_param( '_VIEWPAGES' => undef );						# 閲覧するページＩＤリストを廃棄
			$self->from_param( '_VIEWED' => undef );						# 閲覧フラグを廃棄
			return 0;
		}
		else {																# ページの続きがあれば
			$self->send( $body );											# コンテンツを送出
			$self->from_param( '_VIEWPAGES' => $pages );					# 閲覧するページＩＤリストを更新
			$self->from_param( '_VIEWED' => 1 );							# 閲覧フラグ＝１
			return 1;
		}
	}
}

#--------------------------------------------------
# ■レコードを送出
# list();
#
sub list {
	my $self = shift;
	my ( $id, $num, $r_date, $r_time, $author, $title, $flag ) = @_;
	$flag = ' ' unless( defined($flag) );
	$self->send( sprintf "%05d  %s %s %-8s ", $num, $r_date, $r_time, $author );
	if ( $flag =~ /([\*|\+|\-])/ ) {						# 削除フラグが立っている場合はタイトルを隠す
		$flag = "$1";
		$title = '';
	}
	elsif ( $flag =~ /([!])/ ) {							# 内部フラグは隠す
		$flag =~ s/$1//;
	}

	$self->send( sprintf "%s", ($flag ne '') ? $flag : ' ' );
	$self->send( sprintf "%s\r\n", encode( 'cp932', decode('utf8', $title) ) );
	return;
}

#--------------------------------------------------
# ■クエリを実行
# r = query( query );
#
sub query {
	print "\n(query)\n";						# ????
	my $self = shift;
	my $q = join('', @_);
	#print Dumper(@_);										# ????

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	#printf "** q=[\n%s\n]\n", encode('cp932', $q);			# ????
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	my @row = $sth->fetchrow_array() if ( $sth->rows );
	$sth->finish();
	$dbh->disconnect();

	#print "[row]\n".Dumper(@row)."\n";						# ????
	print "(/query)\n";							# ????
	return if ($#row < 0);
	return [ @row ];
}

#--------------------------------------------------
# ■旧方向へ移動する
# up();
#
sub up {
	#print "\n(up)\n";					# ????
	my $self = shift;
	$self->from_param( '_READ_MOVE_DIRECTION' => 'UP' );			# 旧方向に移動
	#print "(/up)\n";					# ????
}

#--------------------------------------------------
# ■新方向へ移動する
# down();
#
sub down {
	#print "\n(down)\n";				# ????
	my $self = shift;
	$self->from_param( '_READ_MOVE_DIRECTION' => 'DOWN' );			# 新方向に移動
	#print "(/down)\n";					# ????
}

#--------------------------------------------------
# ■項目を送出する
# articles( header );
#
sub articles {
	#print "\n(articles)\n";			# ????
	my $self = shift;
	my $header = shift;				# ヘッダ付加フラグ

	my $threadId		= $self->from_param('THREADID');								# スレッドＩＤ
	my $tonum			= $self->from_param('_READ_MOVE_TO_NUMBER');					# 番号指定
						  $self->from_param( '_READ_MOVE_TO_NUMBER' => undef );
	my $move			= $self->from_param('_READ_MOVE_DIRECTION');					# 移動方向('up':旧方向, 'down':新方向)
						  $self->from_param( '_READ_MOVE_DIRECTION' => undef );
		$move			= defined($move) ? $move : '';
	my $myplace			= $self->from_param('_READ_MYPLACE');							# 現在値(articles.articlenum)
	my $myplace_id		= $self->from_param('_READ_MYPLACE_ID');						# 現在値(articles.id)
	my $threshold		= $self->from_param('_READ_THRESHOLD');							# 閾値フラグ(現在値が閾値を踏んだら1、踏まなければ0にする)
	my $filter_mode		= $self->from_param('_READ_FILTER_MODE');						# フィルタモード('title':タイトル検索, 'full':全文検索, undef:検索なし)
	my $filter_keyword	= $self->from_param('_READ_FILTER_KEYWORD');					# フィルタキーワード
	my $data			= $self->from_param('_READ_MYPLACE_DATA');						# 現在値のデータ
	my $end_record;																		# フィルタモードで該当レコードがなかった場合のフラグ

	# 呼び出し直前の状態（確認用）
	#print "\n";																								# ????
	#	printf "*1* myplace=(%d), myplace.id=(%d)\n", $myplace, ( defined($myplace_id) ? $myplace_id : -1 );	# ????
	#	printf "*1* threshold=(%d)\n", $threshold;																# ????
	#	print "[DATA]".Dumper($data)."\n";																		# ????
	#print "\n";																								# ????

	# ■基本形のクエリから状態に応じクエリを生成する
	#-----------------------------------------------
	my @query;
		$query[0] = "SELECT ".
						"`articles`.`id` as id, ".
						"articlenum, ".
						"date_format(datetime, '%y-%m-%d') AS r_date, ".
						"date_format(datetime, '%H:%i:%s') AS r_time, ".
						"author, title, flag ".
					"FROM `articles` ";
		$query[1] = "";
		$query[2] = "WHERE `articles`.`threadid` = '$threadId' ";
		$query[3] = "";
		$query[4] = "";
		$query[5] = "";
		$query[6] = "";

	# ■移動なし
	#-----------------------------------------------
	if ( $move eq '' ) {

		# ■番号指定あり（閾値を踏むことはない）
		#-----------------------------------------------
		if ( defined($tonum) ) {
			#printf "** (tonum) : tonum=(%d)\n", ( defined($tonum) ? $tonum : 0 );									# ????
			$tonum = 1 if ($tonum < 1);																				# 指定番号が１以下は１に訂正
			$tonum++;
			$query[4] = "AND articlenum < $tonum ";																	# 閾値を超えた場合は最新レコードの番号を返す
			$query[6] = "ORDER BY articlenum DESC LIMIT 1 ";

			my $r = $self->BIGModel::Mail::Read::query(@query);														# クエリを実行
			if ( defined($r) ) {
	 			my ( $id, $num, $r_date, $r_time, $author, $title, $flag ) = @{ $r };
				$myplace = $num; $myplace_id = $id;																		# 現在値を更新
				$self->BIGModel::Generic::sendmsgs('MAIL_HEADER');															# ヘッダを送出
				$self->BIGModel::Mail::Read::list( $id, $myplace, $r_date, $r_time, $author, $title, $flag );			# レコードを送出
				$data = [ $id, $myplace, $r_date, $r_time, $author, $title, $flag ];
				$threshold = 0;																							# 閾値フラグ＝０
			}
		}

		# ■番号指定なし（閾値を踏む場合あり）
		#-----------------------------------------------
		else {
			#print "** (no_move)\n";																					# ????
			$self->BIGModel::Generic::sendmsgs('MAIL_HEADER');																# ヘッダを送出
			if ($myplace <= 0) {																						# 現在値が０以下
				$self->send( sprintf "%05d  v\r\n", $myplace );															# 下限の閾値マークを送出
				$threshold = 1;																							# 閾値フラグ＝１
			}
			else {																										# 現在値が０以上
				my $r = $self->BIGModel::Generic::DB::Articles::getArticle( $threadId, $myplace );						# 現在値のレコードを得る
				if ( defined($r) ) {																					# 現在値のレコードあり
					my ( $id, $r_date, $r_time, $author, $title, $flag ) = @{ $r };
					$self->BIGModel::Generic::sendmsgs('MAIL_HEADER') if ( defined($header) );								# ヘッダを送出※
					$self->BIGModel::Mail::Read::list( $id, $myplace, $r_date, $r_time, $author, $title, $flag );		# レコードを送出
					$data = [ $id, $myplace, $r_date, $r_time, $author, $title, $flag ];
					$threshold = 0;																						# 閾値フラグ＝０
				}
				else {																									# 現在値のレコードなし
					$self->BIGModel::Generic::sendmsgs('MAIL_HEADER') if ( defined($header) );								# ヘッダを送出※
					$self->send( sprintf "%05d  ^\r\n", $myplace );														# 上限の閾値マークを送出
					undef($data);
					$threshold = 1;																						# 閾値フラグ＝１
					$end_record = -1;
				}
			}
		}
	}

	# ■移動あり
	#-----------------------------------------------
	else {

		# ■フィルタ関連のクエリ
		#-----------------------------------------------
		if ( defined($filter_mode) ) {

			# ■タイトル検索
			#-----------------------------------------------
			if ( $filter_mode =~ /TITLE/i ) {
				#print "** (title_search)\n";									# ????
				$query[1] = "";
				$query[3] = "AND `title` LIKE '%$filter_keyword%' ";
				$query[5] = "";
			}

			# ■全文検索
			#-----------------------------------------------
			elsif ( $filter_mode =~ /FULL/i ) {
				#print "** (fulltext_search)\n";								# ????
				$query[1] = "INNER JOIN `contents` ON `articles`.`id` = `contents`.`articleid` ";
				$query[2] = "WHERE `articles`.`threadid` = '$threadId' ";
				$query[3] = "AND ( `title` LIKE '%$filter_keyword%' OR `contents` LIKE '%$filter_keyword%' ) ";
				$query[5] = "GROUP BY `articles`.`id` ";
			}
		}

		# ■フィルタ停止中
		#-----------------------------------------------
		else {
			$query[1] = "";
			$query[3] = "";
			$query[5] = "";
		}

		# ■上移動（－方向・旧方向）
		#-----------------------------------------------
		if ( $move =~ /UP/i ) {
			#print "** (up)\n";														# ????
			#printf "** myplace=(%d)\n", $myplace;									# ????

			unless ( $threshold == 1 && $myplace == 0 ) {																	# 現在値＝０かつ閾値フラグ＝１でない
				$query[4] = "AND articlenum < $myplace ";
				$query[6] = "ORDER BY articlenum DESC LIMIT 1 ";
				my $r = $self->BIGModel::Mail::Read::query(@query);															# クエリを実行

				# （フィルタモードが無効）
				#-----------------------------------------------
				unless ( defined($filter_mode) ) {
					if ( defined($r) ) {																					# 該当レコードあり
			 			my ( $id, $num, $r_date, $r_time, $author, $title, $flag ) = @{ $r };
						$myplace = $num; $myplace_id = $id;																	# 現在値を更新
						$self->BIGModel::Generic::sendmsgs('MAIL_HEADER') if ( defined($header) );								# ヘッダを送出※
						$self->BIGModel::Mail::Read::list( $id, $myplace, $r_date, $r_time, $author, $title, $flag );		# レコードを送出
						$data = [ $id, $myplace, $r_date, $r_time, $author, $title, $flag ];
						$threshold = 0;																						# 閾値フラグ＝０
					}
					else {																									# 該当レコードなし（閾値）
						if ( $threshold == 0 ) {																			# 閾値フラグ＝０なら
							#print "** (threshold)\n";																		# ????
							$myplace--;
							$self->BIGModel::Generic::sendmsgs('MAIL_HEADER') if ( defined($header) );							# ヘッダを送出※
							$self->send( sprintf "%05d  v\r\n", $myplace );													# 閾値マークを送出
							undef($data);
							$threshold = 1;																					# 閾値フラグ＝１
							$end_record = -1;
						}
					}
				}

				# （フィルタモードが有効）
				#-----------------------------------------------
				else {
					if ( defined($r) ) {																# 該当レコードあり
			 			my ( $id, $num, $r_date, $r_time, $author, $title, $flag ) = @{ $r };
						$myplace = $num; $myplace_id = $id;												# 現在値を更新

						my $title_ = encode('cp932', decode('utf8', $title));
						my $filter_keyword_ = encode('cp932', $filter_keyword);

						#printf "** filter_mode=[%s]\n", $filter_mode;									# ????
						#printf "** filter_keyword=[%s]\n", $filter_keyword_;							# ????
						#printf "** title=[%s]\n", $title_;												# ????

						if ( $filter_mode =~ /TITLE/i ) {												# タイトル検索なら
							$self->BIGModel::Generic::sendmsgs('SEARCH_INSPECTED');							# ** 見つかりました **
						}

						if ( $filter_mode =~ /FULL/i ) {												# 全文検索なら
							if ( $title_ =~ /$filter_keyword_/ ) {										# 検索文字列がタイトルにふくまれていれば
								#print "** (1)\n";														# ????
								$self->BIGModel::Generic::sendmsgs('SEARCH_INSPECTED');						# ** 見つかりました **
							}
							else {																		# 含まれていなければ
								#print "** (2)\n";														# ????
								$self->BIGModel::Generic::sendmsgs('SEARCH_INSPECTED_BYTEXT');				# ** テキスト中に見つかりました **
							}
						}

						$self->BIGModel::Generic::sendmsgs('MAIL_HEADER') if ( defined($header) );									# ヘッダを送出※
						$self->BIGModel::Mail::Read::list( $id, $myplace, $r_date, $r_time, $author, $title, $flag );			# レコードを送出
						$data = [ $id, $myplace, $r_date, $r_time, $author, $title, $flag ];
						$threshold = 0;																							# 閾値フラグ＝０
					}
					else {																										# 該当レコードなし
						my $r = $self->BIGModel::Generic::DB::Articles::getArticle( $threadId, $myplace );						# 現在値のレコードを得る
						if ( defined($r) ) {																					# 現在値のレコードあり
							my ( $id, $r_date, $r_time, $author, $title, $flag ) = @{ $r };
							$self->BIGModel::Generic::sendmsgs('MAIL_HEADER');														# ヘッダを送出
							$self->BIGModel::Mail::Read::list( $id, $myplace, $r_date, $r_time, $author, $title, $flag );		# レコードを送出
							$data = [ $id, $myplace, $r_date, $r_time, $author, $title, $flag ];
							$threshold = 0;																						# 閾値フラグ＝０
						}
						else {																									# 現在値のレコードなし
							#print "** (threshold)\n";																			# ????
							$self->BIGModel::Generic::sendmsgs('MAIL_HEADER');														# ヘッダを送出
							$self->send( sprintf "%05d  v\r\n", $myplace ) if ($myplace == 0);									# 現在値が０なら下限の閾値マークを送出
							$self->send( sprintf "%05d  ^\r\n", $myplace ) unless ($myplace == 0);								# 現在値が０以外なら上限の閾値マークを送出
							undef($data);
							$threshold = 1;																						# 閾値フラグ＝１
						}
						$end_record = -1;
					}
				}
			}
		}

		# ■下移動（＋方向・新方向）
		#-----------------------------------------------
		elsif ( $move =~ /DOWN/i ) {
			#print "** (down)\n";											# ????
			#printf "** myplace=(%d)\n", $myplace;							# ????

			unless ( $threshold == 1 && $myplace != 0 ) {																	# 現在値＝０以外かつ閾値フラグ＝１でない
				$query[4] = "AND articlenum > $myplace ";
				$query[6] = "ORDER BY articlenum ASC LIMIT 1 ";
				my $r = $self->BIGModel::Mail::Read::query(@query);															# クエリを実行（レコードが追加されている場合があるので確認のため）

				# （フィルタモードが無効）
				#-----------------------------------------------
				unless ( defined($filter_mode) ) {
					if ( defined($r) ) {																					# 該当レコードあり
			 			my ( $id, $num, $r_date, $r_time, $author, $title, $flag ) = @{ $r };
						$myplace = $num; $myplace_id = $id;																	# 現在値を更新
						$self->BIGModel::Generic::sendmsgs('MAIL_HEADER') if ( defined($header) );								# ヘッダを送出※
						$self->BIGModel::Mail::Read::list( $id, $myplace, $r_date, $r_time, $author, $title, $flag );		# レコードを送出
						$data = [ $id, $myplace, $r_date, $r_time, $author, $title, $flag ];
						$threshold = 0;																						# 閾値フラグ＝０
					}
					else {																									# 該当レコードなし（閾値）
						if ( $threshold == 0 ) {																			# 閾値フラグ＝０なら
							#print "** (threshold)\n";																		# ????
							$myplace++;
							$self->BIGModel::Generic::sendmsgs('MAIL_HEADER') if ( defined($header) );							# ヘッダを送出※
							$self->send( sprintf "%05d  ^\r\n", $myplace );													# 閾値マークを送出
							$data = undef;
							$threshold = 1;																					# 閾値フラグ＝１
							$end_record = -1;
						}
					}
				}

				# （フィルタモードが有効）
				#-----------------------------------------------
				else {
					if ( defined($r) ) {																# 該当レコードあり
			 			my ( $id, $num, $r_date, $r_time, $author, $title, $flag ) = @{ $r };
						$myplace = $num; $myplace_id = $id;												# 現在値を更新

						my $title_ = encode('cp932', decode('utf8', $title));
						my $filter_keyword_ = encode('cp932', $filter_keyword);

						#printf "** filter_mode=[%s]\n", $filter_mode;									# ????
						#printf "** filter_keyword=[%s]\n", $filter_keyword_;							# ????
						#printf "** title=[%s]\n", $title_;												# ????

						if ( $filter_mode =~ /TITLE/i ) {												# タイトル検索なら
							$self->BIGModel::Generic::sendmsgs('SEARCH_INSPECTED');							# ** 見つかりました **
						}

						if ( $filter_mode =~ /FULL/i ) {												# 全文検索なら
							if ( $title_ =~ /$filter_keyword_/ ) {										# 検索文字列がタイトルにふくまれていれば
								#print "** (1)\n";														# ????
								$self->BIGModel::Generic::sendmsgs('SEARCH_INSPECTED');						# ** 見つかりました **
							}
							else {																		# 含まれていなければ
								#print "** (2)\n";														# ????
								$self->BIGModel::Generic::sendmsgs('SEARCH_INSPECTED_BYTEXT');				# ** テキスト中に見つかりました **
							}
						}

						$self->BIGModel::Generic::sendmsgs('MAIL_HEADER') if ( defined($header) );									# ヘッダを送出※
						$self->BIGModel::Mail::Read::list( $id, $myplace, $r_date, $r_time, $author, $title, $flag );			# レコードを送出
						$data = [ $id, $myplace, $r_date, $r_time, $author, $title, $flag ];
						$threshold = 0;																							# 閾値フラグ＝０
					}
					else {																										# 該当レコードなし
						my $r = $self->BIGModel::Generic::DB::Articles::getArticle( $threadId, $myplace );						# 現在値のレコードを得る
						if ( defined($r) ) {																					# 現在値のレコードあり
							my ( $id, $r_date, $r_time, $author, $title, $flag ) = @{ $r };
							$self->BIGModel::Generic::sendmsgs('MAIL_HEADER');														# ヘッダを送出
							$self->BIGModel::Mail::Read::list( $id, $myplace, $r_date, $r_time, $author, $title, $flag );		# レコードを送出
							$data = [ $id, $myplace, $r_date, $r_time, $author, $title, $flag ];
							$threshold = 0;																						# 閾値フラグ＝０
						}
						else {																									# 現在値のレコードなし
							#print "** (threshold)\n";																			# ????
							$self->BIGModel::Generic::sendmsgs('MAIL_HEADER');														# ヘッダを送出
							$self->send( sprintf "%05d  v\r\n", $myplace ) if ($myplace == 0);									# 現在値が０なら下限の閾値マークを送出
							$self->send( sprintf "%05d  ^\r\n", $myplace ) unless ($myplace == 0);								# 現在値が０以外なら上限の閾値マークを送出
							$data = undef;
							$threshold = 1;																						# 閾値フラグ＝１
						}
						$end_record = -1;
					}
				}
			}
		}
	}

	$myplace_id = -1 if ( $threshold == 1 );						# 現在値が閾値なら保存しているＩＤを未定義にする

	# 更新後の状態（確認用）
	#print "\n";																								# ????
	#	printf "*2* myplace=(%d), myplace.id=(%d)\n", $myplace, ( defined($myplace_id) ? $myplace_id : -1 );	# ????
	#	printf "*2* threshold=(%d)\n", $threshold;																# ????
	#	print "[DATA]".Dumper($data)."\n";																		# ????
	#print "\n";																								# ????
	#print "----------------------------------\n";

	# パラメータを更新する
	$self->from_param( '_READ_MYPLACE' => $myplace );				# 現在値（articles.articlenum)
	$self->from_param( '_READ_MYPLACE_ID' => $myplace_id );			# 現在値（articles.id)
	$self->from_param( '_READ_THRESHOLD' => $threshold );			# 閾値フラグ（現在値が閾値を踏んだら１、踏まなければ０にする）
	$self->from_param( '_READ_MYPLACE_DATA' => $data );				# 現在値のデータ

	return $end_record if ( defined($end_record) );					# フィルタモードでレコードが見つからなかった
	return $threshold;
}

#--------------------------------------------------
# ■開封済みメールのメールリポートを表示
# r = readedMailReport( step );
#
sub readedMailReport {
	#print "\n(readedmailreport)\n";				# ????
	my $self = shift;
	my $step = shift;
	#print "(/readedmailreport)\n";					# ????
	return $self->BIGModel::Mail::Read::mailReport( 'READED', $step );
}

#--------------------------------------------------
#■削除済みメールのメールレポートを表示
# r = deletedmailreport( step );
#
sub deletedMailReport {
	#print "\n(deletedMailReport)\n";				# ????
	my $self = shift;
	my $step = shift;
	#print "(/deletedMailReport)\n";				# ????
	return $self->BIGModel::Mail::Read::mailReport( 'DELETED', $step );
}

#--------------------------------------------------
# ■保管メールのリストを表示
# r = storedMailReport( step );
#
sub storedMailReport {
	#print "\n** <storedMailReport>\n\n";
	my $self = shift;
	my $step = shift;
	return $self->BIGModel::Mail::Read::mailReport( '', $step );
}

#--------------------------------------------------
# ■メールレポートの表示
# r = mailReport( type, step );
#
#	type = 'readed'		: 開封済みメール		'@{id}.READED'	(メールレポートスレッド)
#		   'deleted'	: 削除済みメール		'@{id}.DELETED'	(メールレポートスレッド)
#		   (null)		: 保管メール			'@{id}'			(メールボックススレッド)
#	step = (num)		一度に表示する件数(ページャー機能が有効のとき)
#
#	依存パラメータ: ID
#					_MAILREPORT_DATA
#
#	r = ( 0:続きなし, 1:続きあり )
#
sub mailReport {
	#print "\n(mailReport)\n";															# ????
	my $self = shift;
	my $type = shift;																	# レポートの対象('readed':開封済みメール, 'deleted':削除済みメール)
		$type = uc($type) if ( $type ne '' );												# レポートの対象が未指定なら保管メール
	my $step = shift;																	# 一度に表示する件数
		$step = 10 unless( defined($step) && ($step =~ /\d/) );							# （デフォルトは１０件）
	my $me = $self->from_param('ID');													# 自分のユーザＩＤ
	my $threadId = ( $type ne '' ) ? '@'.$me.'.'.$type : '@'.$me;						# スレッドＩＤ(メールボックススレッドまたはメールレポートスレッド)
	my $data = $self->from_param('_MAILREPORT_DATA');									# 処理途中のデータ
	#printf "** me=[%s], type=[%s], threadId=[%s]\n", $me, $type, $threadId;			# ????
	#print "[data]\n".Dumper($data)."\n";												# ????
	my ( $total, $cnt, $lastid );

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my ( $q, $sth );

	unless ( defined($data) ) {													# 処理途中のデータがなければ初期データを取得する
		$q = "SELECT count(id) FROM `articles` WHERE threadid = '$threadId'";
		$sth = $dbh->prepare( $q );
		$sth->execute();
		($total) = $sth->fetchrow_array() if ( $sth->rows );					# スレッドのレコード総数を得る
		$sth->finish();
		$cnt = 1;																# カウンタ＝１
		$lastid = -1;															# 最後に出力したレコードＩＤ＝無効値
	}
	else {
		( $total, $cnt, $lastid ) = @{ $data };
	}
	#printf "*(0)* total=(%d), cnt=(%d), lastid=(%d), \n", $total, $cnt, $lastid;		# ????

	# 出力するクエリを生成
	my @query = (
					"SELECT id, ",
						"date_format(datetime, '%y-%m-%d') AS r_date, ",
						"date_format(datetime, '%H:%i:%s') AS r_time, ",
						"author, title, flag",
					" FROM `articles` WHERE threadid = '$threadId'",
					"",
					" ORDER BY id ASC LIMIT $step"
				);
	$query[5] = " AND id > $lastid" unless ( $lastid == -1 );		# 最後に出力したレコードＩＤがあればクエリに盛り込む
	$q = join('', @query);
	#printf "** q=[\n%s\n]\n\n", $q;								# ????
	$sth = $dbh->prepare( $q );
	$sth->execute();

	#printf "*(1)* total=(%d), cnt=(%d), sth.rows=(%d), \n", $total, $cnt, $sth->rows;		# ????
	my $exit = ( $cnt + $sth->rows > $total ) ? 1 : 0;				# カウンタ＋該当レコード数が総レコード数に達したら終了フラグ＝１

	if ( $sth->rows ) {
		while ( my ($id, $r_date, $r_time, $reader, $title, $flag) = $sth->fetchrow_array() ) {
			$lastid = $id;
			$reader = $reader."\x20" x 8;
			$title = encode('cp932', decode('utf8', $title));
			$flag = ' ' unless (defined($flag));
			$flag =~ s/[!]//;										# 内部フラグは隠す
			if ( $cnt == 1 ) {										# １件目ならヘッダを付ける
				#printf "** type=[%s]\n", $type;					# ????
				$self->BIGModel::Generic::sendmsgs('MAILREPORT_READED') if ( $type eq 'READED' );		# ** 手紙が読まれました **
				$self->BIGModel::Generic::sendmsgs('MAILREPORT_DELETED') if ( $type eq 'DELETED' );		# ** 手紙が削除されました **
				$self->BIGModel::Generic::sendmsgs('MAILREPORT_NEWMAIL') if ( $type eq '' );			# ** 手紙が届いております **
				$self->BIGModel::Generic::sendmsgs('MAIL_HEADER');
			}
			$self->send( sprintf "%05d  %s %s %-.8s %s%s\r\n", $cnt, $r_date, $r_time, $reader, $flag, $title );
			$cnt++;
		}
		$self->from_param( '_MAILREPORT_DATA' => [ $total, $cnt, $lastid ] );		# 最終レコードのデータを保存
	}
	else {
		#print "** (no_record)\n";								# ????
	}
	$sth->finish();
	$dbh->disconnect;
	#printf "*(2)* total=(%d), cnt=(%d), lastid=(%d), \n", $total, $cnt, $lastid;		# ????
	#printf "*(2)* exit=(%d)\n", $exit;													# ????
	#print "\n\n";																		# ????

	$self->send( sprintf "%05d  ^\r\n", $cnt ) if ( $type eq '' && $total >= 1 && $cnt > $total );			# 保管メールの一覧の最後は閾値マーク

	if ( $exit == 1 ) {																# 続きがなければ未定義を返す
		$self->from_param( '_MAILREPORT_DATA' => undef );
		return 0;
	}
	return 1;																		# 続きがあれば１を返す
}

#--------------------------------------------------
# ■メールレポートの削除
# removeMailReport( type );
#
#	type = 'READED'			# 開封済みのメールレポート
#	type = 'DELETED'		# 削除済みのメールレポート
#
#	依存パラメータ: ID
#
sub removeMailReport {
	#print "\n(removeMailReport)\n";							# ????
	my $self = shift;
	my $type = shift;
		$type = uc($type) if ( $type ne '' );
	my $me = $self->from_param('ID');							# 自分のユーザＩＤ
	my $threadId = '@'.$me.'.'.$type if ( $type ne '' );		# スレッドＩＤ(メールレポートスレッド)
	#printf "** threadId=[%s]\n", $threadId;					# ????

	return unless ( defined($threadId) );						# スレッドＩＤが未定義なら戻る

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	my $q = "DELETE FROM `articles` WHERE threadid = '$threadId'";
	#printf "** q=[\n%s\n]\n\n", $q;							# ????
	$dbh->do( $q );
	$dbh->disconnect;
}

#--------------------------------------------------
# ■メールボックスの更新とメールレポート処理
# メール受信を抜けるときに次の処理をする
#	(1) 開封済みフラグ付きのメール（レポート済みフラグ付きのメールは処理しない）
#			→(1A)送信元へ開封済みをレポート、(1B)レポート済みフラグを付ける
#	(2) 削除済みフラグ付きのメール
#			→(2A)送信元へ削除済みをレポート、(2B)メール（アーティクルとコンテンツ）を削除する
#	(3) 文書番号を振り直す
#
sub refresh {
	#print "\n(refresh)\n";								# ????
	my $self = shift;
	my $from = $self->from_param('ID');					# ユーザＩＤ(自分)
	my $threadId = $self->from_param('THREADID');		# スレッドＩＤ(メールボックススレッド)

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	my $dbh2 = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh2->do("SET NAMES utf8;");

	# フラグ : ( '#'=開封済み, '*'=削除, '!'=レポート済み )

	# (1)開封済みフラグ付きのレコードを得る（レポート済みフラグ付きのレコードは除外する）
	#-------------------------------------------
	my $q =	"SELECT author, title FROM `articles` ".
			"WHERE threadid = '$threadId' AND ( flag LIKE '%#%' ) AND ( flag NOT LIKE '%!%' )";
	#printf "*(1)* q=[\n%s\n]\n\n", $q;					# ????
	my $sth = $dbh->prepare( $q );
	$sth->execute();

	# (1A)差出人のメールレポートスレッドに書き込む
	#-------------------------------------------
	while ( my @row = $sth->fetchrow_array ) {
		my $threadId = '@'.$row[0];
		my $title = $row[1];
		my $q = "INSERT INTO `articles` (threadid, articlenum, author, title) ".
				"VALUES ('$threadId.READED', '0', '$from', '$title')";
		#printf "*(1A)* q=[\n%s\n]\n\n", $q;			# ????
		$dbh2->do( $q );
	}
	$sth->finish;

	# (1B)メールレポートしたレコードのフラグを更新（開封済みフラグにレポート済みフラグを追加）する
	# '#' → '#!'
	#-------------------------------------------
	$q = "UPDATE `articles` SET flag = REPLACE(flag, '#', '#!' ) ".
		 "WHERE threadid = '$threadId' AND ( flag LIKE '%#%' ) AND ( flag NOT LIKE '%\!%' )";
	#printf "*(1B)* q=[\n%s\n]\n\n", $q;
	$dbh->do( $q );

	# (2)削除済みフラグ付きのレコードを得る
	#-------------------------------------------
	$q = "SELECT id, author, title FROM `articles` ".
		 "WHERE threadid = '$threadId' AND ( flag LIKE '%*%' )";
	#printf "*(2)* q=[\n%s\n]\n\n", $q;								# ????
	$sth = $dbh->prepare( $q );
	$sth->execute();

	# (2A,2B)差出人のメールレポートスレッドに書き込んで、メールのコンテンツを削除する
	#-------------------------------------------
	while ( my @row = $sth->fetchrow_array ) {
		#print "** (delete)\n";													# ????
		#printf "** id=(%d), author=[%s]\n", $row[0], $row[1];					# ????
		#printf "** title=[%s]\n", encode('cp932', decode('utf8', $row[2]));	# ????
		my $articleId = $row[0];
		my $threadId = '@'.$row[1];		# 送信先ＩＤ
		my $title = $row[2];
		my $q = "INSERT INTO `articles` (threadid , articlenum, author, title) ".
				"VALUES ('$threadId.DELETED', '0', '$from', '$title')";
		#printf "*(2A)* q=[\n%s\n]\n\n", $q;			# ????
		$dbh2->do( $q );
		$threadId = '@'.$from;			# 送信元ＩＤ（自分）
		$q = "DELETE FROM `contents` WHERE threadid = '$threadId' AND articleid = $articleId";
		#printf "*(2B-2)* q=[\n%s\n]\n\n", $q;			# ????
		$dbh2->do( $q );
	}
	$dbh2->disconnect;
	$sth->finish();

	# (2B)削除ずみフラグ付きのレコードを削除する
	#-------------------------------------------
	$q = "DELETE FROM `articles` ".
		 "WHERE threadid = '$threadId' AND ( flag LIKE '%*%' )";
	#printf "*(2B-2)* q=[\n%s\n]\n\n", $q;					# ????
	$dbh->do( $q );

	# (3)文書番号を振り直す
	#-------------------------------------------
	$q = "SET \@i:=0";
	#printf "*(3-1)* q=[\n%s\n]\n\n", $q;					# ????
	$dbh->do( $q );
	$q = "UPDATE `articles` SET articlenum = (\@i:=\@i+1) WHERE threadid = '$threadId';";
	#printf "*(3-2)* q=[\n%s\n]\n\n", $q;					# ????
	$dbh->do( $q );
	$dbh->disconnect;
}

#--------------------------------------------------
# ■スタートアップ
# startup();
#
# パラメータの初期化
# メールボックスの状態（保管メールや新着メールの有無）の取得
# 現在値の決定
#
#	依存パラメータ: ID
#					THREADID
#					_READ_FILTER_MODE
#					_READ_FILTER_KEYWORD
#					_READ_THRESHOLD
#					_READ_MYPLACE
#					_READ_MYPLACE_ID
#
sub startup {
	#print "\n(startup)\n";					# ????
	my $self = shift;

	my $recipient = $self->from_param('ID');						# メールレポートを送るユーザ（＝メールの受信者＝自分）
	my $threadId = '@'.$recipient;									# メールボックスのスレッドＩＤ
	$self->from_param( 'THREADID' => $threadId );
	#printf "** threadid=(%s)\n", $threadId;						# ????

	$self->from_param( '_READ_FILTER_MODE' => undef );				# フィルタのパラメータをリセット
	$self->from_param( '_READ_FILTER_KEYWORD' => undef );
	$self->from_param( '_READ_THRESHOLD' => 0 );					# 閾値フラグ
	my $r;

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	# (1)未開封メールの最も古いレコードを得る
	my $q = "SELECT id, articlenum FROM `articles` ".
			"WHERE threadid = '$threadId' AND ( ( flag IS NULL ) OR ( flag NOT LIKE '%#%' ) ) ORDER BY id ASC LIMIT 1";
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	my ( $myplace_id, $myplace ) = $sth->fetchrow_array() if ( $sth->rows );
	$sth->finish();
	#printf "** myplace=(%d), myplace_id=(%d)\n", $myplace, $myplace_id if ( defined($myplace) );			# ????

	if ( defined($myplace) ) {															# ●該当レコードあり（新着メールあり）
		$self->from_param( '_READ_MYPLACE' => $myplace );								# 現在値（文書番号）
		$self->from_param( '_READ_MYPLACE_ID' => $myplace_id );							# 現在値（レコード番号）
		$r = 1;
	}
	else {																				# ●該当レコードなし
		# (2)メールボックスの最も新しいレコードを得る
		my $q = "SELECT id, articlenum FROM `articles` WHERE threadid = '$threadId' ORDER BY articlenum DESC LIMIT 1";
		my $sth = $dbh->prepare( $q );
		$sth->execute();
		my ( $max_id, $max ) = $sth->fetchrow_array() if ( $sth->rows );
		$sth->finish();
		$dbh->disconnect();

		if ( defined($max) ) {															# ●該当レコードあり（保存メールあり）
			#printf "** max=(%d), max_id=(%d)\n", $max, $max_id if ( defined($max) );	# ????
			$self->from_param( '_READ_MYPLACE' => $max );								# 現在値（文書番号）
			$self->from_param( '_READ_MYPLACE_ID' => $max_id );							# 現在値（レコード番号）
			$r=2;
		}
		else {																			# ●該当レコードなし（保存メールなし）
			$r=0;
		}
	}
	#print "(/startup)\n";					# ????
	return $r;
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;
	$self->from_param( 'LOCATION' => 'MAIL' );		# 現在地
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;
	$self->BIGModel::Mail::Read::refresh();

	# ●パラメータの廃棄
	$self->from_param('-THREADID');
	$self->from_param('-_READ_MYPLACE');# 
	$self->from_param('-_READ_MYPLACE_ID');
	$self->from_param('-_READ_MYPLACE_DATA');
	$self->from_param('-_READ_THRESHOLD');

	$self->BIGModel::Generic::modoru();				# 戻る
}

#--------------------------------------------------
# ■呼び出し
#
sub read {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;