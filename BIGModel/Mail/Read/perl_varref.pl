use Data::Dumper;

my $param;											# パラメータ
#	$param{'CAPSULE'} = undef;						# カプセルデータ
#	$param->{'CAPSULE'}->{'query'} = undef;			# カプセルデータ
#print "[param]\n".Dumper($param)."[/param]\n\n";


my $data = $param->{'CAPSULE'};							# カプセル→作業変数
print "[data]\n".Dumper($data)."[/data]\n\n";


	my $query = \$data->{'query'};							# 変数(query)に作業変数(data)を関連付ける
		# $$query = undef;									# (←上の行が実行されると自動的に実行されている)
	print "[2data]\n".Dumper($data)."[/2data]\n\n";

	$$query = "aaaaa";										# 変数(query)から作業変数(data)にセット
	printf "**[%s]\n", $$query;								# 作業変数(data)の内容
	print "[data]\n".Dumper($data)."[/data]\n\n";			# 作業変数(data)の内容


$param->{'CAPSULE'} = $data;							# 作業変数→カプセル
print "[param]\n".Dumper($param)."[/param]\n\n";		# カプセルの内容
#