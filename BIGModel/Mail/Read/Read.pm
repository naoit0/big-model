package BIGModel::Mail::Read;
our $VERSION = "0001.20180523.1419";	# 着手
our $VERSION = "0002.20180622.0205";	# 完了
our $VERSION = "0002.20180704.0134";	# フィルタモードが正常動作しないので微調整
#
# BIG-Model ホストプログラム
# 電子メール（受信）
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
use DBI;
use Naoit0::DateTime;
use Debug;

my $subs = {

	'0' =>	# ■初期設定
	sub {
		print "\n** [[ Mail::Read ]]\n\n";
		my $self = shift;
		$self->BIGModel::Mail::Read::initalize;
		my $r = $self->BIGModel::Mail::Read::startup();													# メールボックスの状態を得る
		$self->comm->param( '_INBOX' => $r );															# 得た値をパラメータに保存
		$self->arg->mode( '-echo' => 0, '-line' => 0 );													# ローカルエコーを無効
		$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(21) });		# メールレポート処理
	},

	'1' =>	# スタート位置を取得
	sub {
		my $self = shift;
		$self->arg->mode( '-echo'=>0, '-line'=>0 );
		my $inbox = $self->comm->param('_INBOX'); $self->comm->param( '_INBOX' => undef );				# パラメータの値を取得して廃棄
		if ( $inbox >= 1 ) {																			# メールが保管されている
			$self->BIGModel::Generic::msgs('SEEMAIL.1');
			if ( $inbox == 2 ) {																			# 新着メールがない
				$self->BIGModel::Mail::Read::articles();														# 最新のリストを表示
				$self->BIGModel::Mail::Read::down();															# 閾値に移動
			}
			$self->BIGModel::Mail::Read::articles();															# 現在値を表示
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(2) });
		}
		else {																							# メールが保管されていない
			$self->BIGModel::Generic::msgs( 'MAIL_NO_NEWMAIL' );											# 手紙は届いていません
			$self->BIGModel::Mail::Read::finalize;
		}
	},

	'2' =>	# ■操作
	sub {
		#printf "\n** < 2 > (move)\n\n";
		
		my $self = shift;
		my $comm = $self->comm;
		my $arg = $self->arg->arg();					# 入力値を得る
		my $chr = $self->arg->status('-ctrlchr');		# 入力した制御文字
			$arg = ( defined($arg) ) ? $arg : '';
			$chr = ( defined($chr) ) ? $chr : $arg;

		# ■【 終了 】

		if ( $chr =~ /[\x03|\x1b|E|e]/ ) {				# [CTRL+C][ESC][E]
			$self->BIGModel::Mail::Read::finalize;
		}

		# ■【 番号指定 】

		#elsif ( $chr =~ /[\d|\#]/ ) {					# 番号[RETURN]
		#	$self->comm->param( '_ARG' => $chr );
		#	$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(5) });
		#}

		# ■【 タイトル検索 】

		elsif ( $chr eq "\"" ) {						# "キーワード[RETURN]
			$self->comm->param( '_READ_FILTER_MODE' => 'title' );
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(7) });
		}

		# ■【 全文検索 】

		elsif ( $chr eq "\'" ) {						# 'キーワード[RETURN]
			$self->comm->param( '_READ_FILTER_MODE' => 'full' );
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(7) });
		}

		# ■【 旧方向へ進む 】

		elsif ( $chr =~ /[\x05|\x1e]/ ) {				# [CTRL+E][↑]
			#print "** (up)\n";
			$self->BIGModel::Mail::Read::up();
			$self->BIGModel::Mail::Read::articles();
		}
		elsif ( $chr eq 'J' ) {							# [J]
			#print "** (up)\n";
			$self->BIGModel::Mail::Read::up();
			$self->BIGModel::Mail::Read::articles();
		}
		elsif ( $chr eq 'k' ) {							# [SHIFT+K]
			#print "** (up)\n";
			$self->BIGModel::Mail::Read::up();
			$self->BIGModel::Mail::Read::articles();
		}

		# ■【 旧方向へ読み進む 】

		elsif ( $chr =~ /[\x12|\x1d]/ ) {				# [CTRL+R][←]
			#print "** (back)\n";
			$self->comm->param( '_MOVE' => 'BACK' );
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(9) });
		}
		elsif ( $chr eq 'L' ) {							# [L]
			#print "** (back)\n";
			$self->comm->param( '_MOVE' => 'BACK' );
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(9) });
		}
		elsif ( $chr eq 'h' ) {							# [SHIFT+H]
			#print "** (back)\n";
			$self->comm->param( '_MOVE' => 'BACK' );
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(9) });
		}

		# ■【 現在位置を読む 】

		elsif ( $chr =~ /[\x04|\x0d]/ ) {				# [CTRL+D][RETURN]
			#print "** (cr)\n";
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(9) });
		}

		# ■【 新方向へ読み進む 】

		elsif ( $chr =~ /[\x06|\x1c]/ ) {				# [CTRL+F][→]
			#print "** (next)\n";
			$self->comm->param( '_MOVE' => 'NEXT' );
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(9) });
		}
		elsif ( $chr eq 'H' ) {							# [H]
			#print "** (next)\n";
			$self->comm->param( '_MOVE' => 'NEXT' );
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(9) });
		}
		elsif ( $chr eq 'l' ) {							# [SHIFT+L]
			#print "** (next)\n";
			$self->comm->param( '_MOVE' => 'NEXT' );
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(9) });
		}

		#■【 新方向へ進む 】

		elsif ( $chr =~ /[\x18|\x1f|\x20]/ ) {			# [CTRL+X][↓][ ]
			#print "** (down)\n";
			$self->BIGModel::Mail::Read::down();
			$self->BIGModel::Mail::Read::articles();
		}
		elsif ( $chr eq 'K' ) {							# [K]
			#print "** (down)\n";
			$self->BIGModel::Mail::Read::down();
			$self->BIGModel::Mail::Read::articles();
		}
		elsif ( $chr eq 'j' ) {							# [SHIFT+J]
			#print "** (down)\n";
			$self->BIGModel::Mail::Read::down();
			$self->BIGModel::Mail::Read::articles();
		}

		# ■【 削除 】
		
		elsif ( $chr eq '*' ) {							# [*]
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(13) });
		}

		# ■【 メールを書く・返信 】
		
		elsif ( $chr =~ /W/i ) {						# [W]
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(17) });
		}
		elsif ( $chr eq  '.' ) {						# [.]
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(17) });
		}

		# ■【 旧方向へ読み続ける 】

		elsif ( $chr eq 'r' ) {							# [SHIFT+R]
			$self->comm->param( '_MOVE' => 'BACK' );
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(15) });
		}

		# ■【 新方向へ読み続ける 】

		elsif ( $chr eq 'R' ) {							# [R]
			$self->comm->param( '_MOVE' => 'NEXT' );
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(15) });
		}

		# ■【 タイトル一覧（現在値より旧方向へ） 】

		elsif ( $chr eq 'T' ) {							# [T]
			$self->comm->param( '_MOVE' => 'BACK' );
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(11) });
		}

		# ■【 タイトル一覧（現在値より新方向へ） 】

		elsif ( $chr eq 't' ) {							# [SHIFT+T]
			$self->comm->param( '_MOVE' => 'NEXT' );
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(11) });
		}

		# ■【 電報 】

		elsif ( $chr eq '!' ) {							# [SHIFT+T]
			$self->comm->param( 'TELEGRAM_RETURN' => sub { $self->BIGModel::Mail::Read::read(14) } );
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Telegram::telegram } );
		}

		# ■【 説明 】

		elsif ( $chr eq "?" ) {							# [?]
			$self->BIGModel::Generic::msgs('SEEMAIL.2');
			$self->BIGModel::Mail::Read::articles();
		}

		# ■【 シグ間ジャンプ 】

		elsif ( $chr eq ';' ) {							# ;ジャンプ先[RETURN]
			$self->comm->param( '_ARG' => $chr );
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(17) });
		}
	},

#---------------------------------------------
# ■ 番号指定
#---------------------------------------------
#	'5' =>	# 番号指定
#	sub {
#		my $self = shift;
#		my $arg = $self->comm->param('_ARG'); $self->comm->param( '_ARG' => undef );
#		$self->BIGModel::Generic::prompt( 'PROMPT_NUMBER', '-echo'=>1, '-line'=>1, '-size'=>20, '-mask'=>0 );		# 番号を入力してください
#		$self->arg->store_unshift( $arg );																			# 入力途中の入力バッファを蓄積バッファに入れて
#		$self->arg->arg();																							# 入力バッファに入れ直す
#		$self->arg->mode( '-acceptChr'=>'[\d]' );																	# ここから数字入力のみ許可
#		$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(6) });
#	},
#
#	'6' =>	# 入力待ち
#	sub {
#		my $self = shift;
#		my $arg = $self->arg->arg();					# 入力値を得る
#		my $chr = $self->arg->status('-ctrlchr');		# 入力した制御文字
#			$chr = ( defined($chr) ) ? $chr : '';
#
#		if ( $chr eq "\x03" ) {
#			$self->arg->mode( '-echo'=>0, '-line'=>0, '-size'=>0, '-acceptChr'=>'' );		# 入力モードをリセット
#			$self->BIGModel::Mail::Read::articles();
#			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(2) });
#		}
#		elsif ( $chr eq "\x0d" ) {
#			$self->comm->send("\r\n");
#			$self->arg->mode( '-echo'=>0, '-line'=>0, '-size'=>0, '-acceptChr'=>'' );		# 入力モードをリセット
#			if ( $arg =~ /\d+/ ) {
#				$arg =~ s/#//;
#				$self->comm->param( '_READ_MOVE_TO_NUMBER' => $arg );
#			}
#			$self->BIGModel::Mail::Read::articles();
#			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(2) });
#		}
#	},

#---------------------------------------------
# ■ タイトル検索／全文検索
#---------------------------------------------
	'7' =>	# 検索文字列を入力してください
	sub {
		my $self = shift;
		$self->BIGModel::Generic::prompt( 'PROMPT_SEARCH_STRING', '-echo'=>1, '-line'=>1, '-size'=>60, '-mask'=>0 );		# 検索文字列を入力してください
		$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(8) });
	},

	'8' =>	# 入力待ち
	sub {
		my $self = shift;
		my $comm = $self->comm;
		my $filter_mode = $comm->param('_READ_FILTER_MODE');		# フィルタモード
		my $arg = $self->arg->arg();								# 入力値を得る
		my $chr = $self->arg->status('-ctrlchr');					# 入力した制御文字
			$chr = ( defined($chr) ) ? $chr : '';

		if ( $chr eq "\x03" ) {
			$comm->send("\r\n");
			$self->arg->mode( '-echo'=>0, '-line'=>0, '-size'=>0 );		# 入力モードをリセット
			$comm->param( '_READ_FILTER_MODE' => undef );				# フィルタモードをリセット
			$comm->param( '_READ_FILTER_KEYWORD' => undef );			# フィルタキーワードをクリア
			$self->BIGModel::Mail::Read::articles();
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(2) });
		}
		elsif ( $chr eq "\x0d" ) {
			$comm->send("\r\n");
			$self->arg->mode( '-echo'=>0, '-line'=>0, '-size'=>0 );		# 入力モードをリセット
			if ( $arg ne '' ) {
				$comm->param( '_READ_FILTER_KEYWORD' => $arg );			# フィルタキーワード
			}
			else {
				$comm->param( '_READ_FILTER_MODE' => undef );			# フィルタモードをリセット
				$comm->param( '_READ_FILTER_KEYWORD' => undef );		# フィルタキーワードをクリア
			}
			$self->BIGModel::Mail::Read::articles();
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(2) });
		}
	},

#---------------------------------------------
# ■ 現在位置を読む
#---------------------------------------------
	'9' =>
	sub {
		#printf "\n** < 9 > (read)\n\n";
		my $self = shift;
		my $data = $self->comm->param('_READ_MYPLACE_DATA');		# 現在値のデータ
		my $move = $self->comm->param('_MOVE');						# 移動方向
		my $threshold = $self->comm->param('_READ_THRESHOLD');		# 閾値フラグ（現在値が閾値を踏んだら１、踏まなければ０にする）
		my $eot = 0;												# 最終ページ（または取得失敗）フラグ
		my $exit = 0;												# 終了フラグ

		# 呼び出し直前の状態（確認用）
		#print "\n";
		#	printf "** move=[%s]", ( defined($move) ? $move : '-' );
		#	printf ", eot=(%d)", $eot;
		#	print "\n";
		#print "\n";

		# ■コンテンツを表示
		#-------------------------------------
		my $flag = defined($data->[6]) ? $data->[6] : '';
		unless ( $flag =~ /[\*|\+|\-]/ ) {																		# 削除フラグが立っているなら
			if ( $self->BIGModel::Mail::Read::view() ) {															# (1) 続きのページがある
				$eot = 0;
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(10) });				# キー入力
			}
			else {																									# (0) 終了または取得失敗
				$eot = 1;
				$exit = 1;
			}
		}
		else {																									# 削除フラグが立っていないなら
			$eot = 1;
			$threshold = 1;																							# 閾値とみなす
			$exit = 1;
		}

		#printf "*1* threshold=[%s]\n", $threshold;

		# ■コンテンツの出力が終わったら移動とリストを表示
		#-------------------------------------
		if ( $eot == 1 ) {
			my $r;
			$self->comm->param( '_FILTER_END_RECORD' => undef );					# みなし閾値は毎回リセット
			$move = $self->comm->param('_MOVE');									# 移動方向を得る

			if ( defined($move) ) {													# 移動方向が指定されている
				$self->comm->param( '_MOVE' => undef );
				$self->BIGModel::Mail::Read::up() if ( $move =~ /BACK/i );
				$self->BIGModel::Mail::Read::down() if ( $move =~ /NEXT/i );
				if ( $threshold == 1 )													# 現在値を閾値から移動する場合
					{ $r = $self->BIGModel::Mail::Read::articles(); }						# リストにヘッダを付けないで送出する
				else																	# 現在値を閾値でない場所から移動する場合
					{ $r = $self->BIGModel::Mail::Read::articles(1); }						# リストにヘッダを付けて送出する
			}
			else {																	# 移動方向が指定されていない
				if ( $threshold == 0 )													# 閾値フラグ＝０なら
					{ $r = $self->BIGModel::Mail::Read::articles(); }						# リストにヘッダを付けないで送出する
			}

			if ( defined($r) == 1 && $r == -1 ) {									# フィルタモードで現在値が該当レコードを外れた場合
				$self->comm->param( '_FILTER_END_RECORD' => 1 );						# フラグをセット
			}
			else {
				$self->comm->param( '_FILTER_END_RECORD' => 0 );						# フラグをリセット
			}
		}

		#printf "*2* threshold=[%s]\n", $threshold;

		# 終了後の状態（確認用）
		#print "\n";
		#	printf "** move=[%s]", ( defined($move) ? $move : '-' );
		#	printf ", eot=(%d)", $eot;
		#	print "\n";
		#print "\n";
		#print "------------------------------------------\n";

		if ( $exit == 1 ) {
			my $return = $self->comm->param('_RETURN'); $self->comm->param( '_RETURN' => undef );			# パラメータを取得し廃棄
			if ( ref($return) eq 'CODE' ) {
				$self->handler('onIdle')->add( 'onIdle1', $return );										# 指定先に戻る
			}
			else {
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(2) });		# 戻る
			}
		}
	},

	'10' =>	# キー入力
	sub {
		#printf "\n** < 10 >\n\n";
		my $self = shift;
		$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(9) });		# ※プロンプトは無効中

		#my $comm = $self->comm;
		#my $arg = $self->arg->arg();								# 入力値を得る
		#my $chr = $self->arg->status('-ctrlchr');					# 入力した制御文字
		#	$chr = ( defined($chr) ) ? $chr : '';
        #
		#if ( $chr eq "\x03" ) {
		#	$self->comm->param( '_MOVE' => undef );															# パラメータを廃棄
		#	$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(2) });			# メインに戻る
		#}
		#else {
		#	$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(9) });			# 次の処理に移る
		#}
	},

#---------------------------------------------
# ■ タイトル一覧
#---------------------------------------------
	'11' =>
	sub {
		#printf "\n** < 11 > (title)\n\n";
		my $self = shift;
		my $move = $self->comm->param('_MOVE');						# 移動方向
		#printf "** move=[%s]\n", $move;
		$self->BIGModel::Mail::Read::up() if ( $move =~ /BACK/i );
		$self->BIGModel::Mail::Read::down() if ( $move =~ /NEXT/i );
		my $r = $self->BIGModel::Mail::Read::articles();
		my $threshold = $self->comm->param('_READ_THRESHOLD');		# 閾値フラグ（articles(()呼び出し後で現在値が更新されている）
		#printf "** r=(%d), threshold=(%d)\n", $r, $threshold;
		unless ( $r == -1 || $threshold == 1 ) {															# 現在値が閾値またはみなし閾値にいないなら
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(12) });			# キー入力
		}
		else {
			$self->comm->param( '_MOVE' => undef );																# パラメータを廃棄
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(2) });				# 戻る
		}
	},

	'12' =>	# キー入力
	sub {
		my $self = shift;
		my $comm = $self->comm;
		my $arg = $self->arg->arg();								# 入力値を得る
		my $chr = $self->arg->status('-ctrlchr');					# 入力した制御文字
			$chr = ( defined($chr) ) ? $chr : '';
        
		if ( $chr eq "\x03" ) {
			$self->comm->param( '_MOVE' => undef );															# パラメータを廃棄
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(2) });			# メインに戻る
		}
		else {
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(11) });			# 次の処理に移る
		}
	},

#---------------------------------------------
# ■ 削除
#---------------------------------------------
	'13' =>	# 
	sub {
		#print "\n** < 13 > (delete)\n\n";
		my $self = shift;
		my $threshold = $self->comm->param('_READ_THRESHOLD');											# 閾値フラグ
		#printf "** threshold=[%s]\n", $threshold;

		if ( $threshold == 0 ) {
			my $threadId = $self->comm->param('THREADID');													# スレッドＩＤ
			my $data = $self->comm->param('_READ_MYPLACE_DATA');											# 現在値のデータ
			my $num = $data->[0];																			# 文書ＩＤ
			my $flag = defined($data->[6]) ? $data->[6] : '';												# 文書フラグ

			if ( $flag =~ /\*/ ) {																			# 削除フラグ＝１なら
				$flag =~ s/\*//g;																				# 削除フラグ＝０
			}
			else {																							# 削除フラグ＝０なら
				$flag .= '*';																					# 削除フラグ＝１
			}
			$self->BIGModel::DB::Articles::updFlag( $threadId, $num, $flag );								# フラグを更新
			$self->BIGModel::Mail::Read::articles();														# リストを表示する
		}
		$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(2) });			# メインに戻る
	},

#---------------------------------------------
# ■ 電報（終了後）
#---------------------------------------------
	'14' =>
	sub {
		print "\n** < 14 >\n\n";
		my $self = shift;
		$self->arg->mode( '-echo' => 0, '-line' => 0 );													# ローカルエコーを無効
		$self->BIGModel::Mail::Read::articles();
		$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(2) });			# メインに戻る
	},

#---------------------------------------------
# ■ 進行方向へ読み続ける
#---------------------------------------------
	'15' =>	# 新方向へ読み進む	閾値またはにいるなら終了
	sub {
		#print "\n** < 15 > (move_read)\n\n";
		my $self = shift;
		my $move = $self->comm->param('_MOVE');						# 移動方向
		#printf "** move=[%s]\n", $move;
		if ( defined($move) ) {
			$self->comm->param( '__MOVE' => $move );															# 読み進むを抜けるとパラメータが廃棄されるため保存しておく
			$self->BIGModel::Mail::Read::up() if ( $move =~ /BACK/i );
			$self->BIGModel::Mail::Read::down() if ( $move =~ /NEXT/i );
			$self->comm->param( '_RETURN' => sub { $self->BIGModel::Mail::Read::read(16) } );					# 読み進む(9)を終了したら分岐・キー入力(16)に移動する
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(9) });				# 読み進む(9)を読み出す
		}
		else {
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(2) });					# メインに戻る
		}
	},

	'16' =>	# 分岐・キー入力
	sub {
		#printf "\n** < 16 >\n\n";
		my $self = shift;
		my $comm = $self->comm;

		my $threshold = $comm->param('_READ_THRESHOLD');													# 閾値フラグ（articles(()呼び出し後で現在値が更新されている）
		my $end_record = $comm->param('_FILTER_END_RECORD');												# みなし閾値フラグ（フィルタモードで該当レコードの外に出た）
		#printf "threshold=(%d)\n", $r, $threshold;
		if ( $end_record == 1 || $threshold == 1 ) {														# 現在値が閾値なら
			$comm->param( '__MOVE' => undef );																	# パラメータを廃棄
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(2) });				# メインに戻る
		}
		else {																								# 現在値が閾値でなければ
			my $arg = $self->arg->arg();																		# 入力値を得る
			my $chr = $self->arg->status('-ctrlchr');															# 入力した制御文字
				$chr = ( defined($chr) ) ? $chr : '';
	        
			if ( $chr eq "\x03" ) {
				$comm->param( '__MOVE' => undef );																# パラメータを廃棄
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(2) });				# メインに戻る
			}
			else {
				$comm->param( '_MOVE' => $comm->param('__MOVE') );												# パラメータを復旧
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(15) });			# 次の処理に移る
			}
		}
	},

#---------------------------------------------
# ■ 書く
#---------------------------------------------
	'17' => # 返信かどうか？
	sub {
		#printf "\n** < 17 > (write)\n\n";
		my $self = shift;
		my $threshold = $self->comm->param('_READ_THRESHOLD');						# 閾値フラグ
		#printf "** threshold=[%s]\n", $threshold;

		# 送信元があれば返信、なければ送信
		if ( $threshold == 0 ) {
			my $data = $self->comm->param('_READ_MYPLACE_DATA');											# 現在値のデータ
			$self->comm->param( '_SEND_TO' => $data->[4] );													# 返信するＩＤを指定
			$self->BIGModel::Generic::msgs('MAIL_QUERY_SENDER');											# ** 送信先検索中 **
		}
		$self->comm->param( 'WRITE_RETURN' => sub { $self->BIGModel::Mail::Read::read(18) } );			# 戻り先を定義
		$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Write::write });			# 「書く」に移動
	},

	'18' =>	# 「書く」から戻る
	sub {
		my $self = shift;
		$self->arg->mode( '-echo'=>0, '-line'=>0, '-size'=>0 );												# 入力モードをリセット
		$self->BIGModel::Mail::Read::articles();															# リストを送出
		$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(2) });				# メインに戻る
	},

#---------------------------------------------
# ■ シグ間ジャンプ
#---------------------------------------------
	'19' =>
	sub {
		printf "\n** < 19 > (jump)\n\n";
		my $self = shift;
		my $arg = $self->comm->param('_ARG'); $self->comm->param( '_ARG' => undef );
		$self->BIGModel::Generic::prompt('PROMPT_SIGJUMP', '-echo'=>1, '-line'=>1, '-size'=>60, '-mask'=>0 );		# 移動先を入力してください
		$self->arg->store_unshift( $arg );				# 入力途中の入力バッファを蓄積バッファに入れて
		$self->arg->arg();								# 入力バッファに入れ直す
		$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(20) });
	},

	'20' =>	# 入力待ち
	sub {
		my $self = shift;
		my $comm = $self->comm;
		my $arg = $self->arg->arg();					# 入力値を得る
		my $chr = $self->arg->status('-ctrlchr');		# 入力した制御文字
			$chr = ( defined($chr) ) ? $chr : '';

		if ( $chr eq "\x03" ) {							# [ETX]			キャンセル
			$comm->send("\r\n");
			$self->arg->mode( '-echo'=>0, '-line'=>0, '-size'=>0 );												# 入力モードをリセット
			$self->BIGModel::Mail::Read::articles();															# リストを送出
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(2) });				# メインに戻る
		}
		elsif ( $chr eq "\x0d" ) {
			$comm->send("\r\n");
			if ( $arg eq '' ) {							# (ヌル)		キャンセル
				$self->arg->mode( '-echo'=>0, '-line'=>0, '-size'=>0 );											# 入力モードをリセット
				$self->BIGModel::Mail::Read::articles();														# リストを送出
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(2) });			# メインに戻る
			}
			else {
				$arg =~ s/\;//;
				$self->BIGModel::Mail::Read::finalize();														# 終了処理
			}
		}
	},

#---------------------------------------------
# ■ メールレポートを表示
#---------------------------------------------
	'21' =>	# メイン処理
	sub {
		#printf "\n** < 21 > (mailreport)\n\n";
		my $self = shift;
		my $comm = $self->comm;

		my $next = $comm->param('_MAILREPORT_NEXT'); $comm->param( '_MAILREPORT_NEXT' => undef );	# メールレポート処理終了フラグ
		my $type = $comm->param('_MAILREPORT_TYPE');												# メールレポート処理モード
		#printf "*1* type=(%d)\n", $type;
		unless( defined($type) ) { $type = 1; $comm->param( '_MAILREPORT_TYPE' => $type ); }
		#printf "*2* type=(%d)\n", $type;

		if ( defined($next) ) {
			$self->BIGModel::Mail::Read::removeMailReport('READED') if ( $type == 1 );			# 開封済みメールレポートを削除
			$self->BIGModel::Mail::Read::removeMailReport('DELETED') if ( $type == 2 );			# 削除済みメールレポートを削除
			$type++;
			$comm->param( '_MAILREPORT_TYPE' => $type );											# 次の処理モードを切り替える
		}

		# 処理するメールレポートスレッドを選択
		my $r;
		if ( $type == 1 ) {
			#print "** ( readed_mailreport )\n";
			$r = $self->BIGModel::Mail::Read::readedMailReport(10);				# 開封済みメール
		}
		if ( $type == 2 ) {
			#print "** ( deleted_mailreport )\n";
			$r = $self->BIGModel::Mail::Read::deletedMailReport(10);			# 削除済みメール
		}
		if ( $type == 3 ) {
			#print "** ( stored_mailreport )\n";
			$r = $self->BIGModel::Mail::Read::storedMailReport(10);				# 保管メール
		}

		if ( $type <= 3 ) {
			if ( $r == 1 ) {																					# 続きのレコードがある
				$self->BIGModel::Generic::msgs('PROMPT_MORE');													# ** 続きを表示します **
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(22) });
				#print "** <22>\n";
			}
			else {																								# 全てのページを出力した
				$comm->param( '_MAILREPORT_NEXT' => 1 );														# 次の処理に進む
				$comm->param( '_NOSTOP' => undef );																# ページ表示抑止を有効にする
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(21) });
			}
		}
		else {																								# 全てのメールレポートを終了した
			$comm->param( '_MAILREPORT_TYPE' => undef );														# パラメータを廃棄
			$comm->param( '_MAILREPORT_NEXT' => undef );														#
			#$comm->param( '_NOSTOP' => undef );																#
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(1) });
		}
	},

	'22' =>	# 入力待ち
	sub {
		my $self = shift;
		my $comm = $self->comm;
		my $nonstop = $comm->param('_NOSTOP');
		my $arg = $self->arg->arg();					# 入力値を得る
		my $chr = $self->arg->status('-ctrlchr');		# 入力した制御文字
			$chr = ( defined($chr) ) ? $chr : '';
			$arg = ( defined($arg) ) ? $arg : '';

		if ( defined($nonstop) ) {																				# ページ表示抑止が有効なら
			$comm->send( "\x08\x20\x08" x 22 );																		# プロンプトを消去
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(21) });				# メイン処理に移動
		}
		else {																									# ページ表示抑止が無効なら
			if ( $chr eq "\x03" || $chr eq "\x1b" ) {				# [ETX][ESC]		ページ表示を中断
				$comm->send( "\x08\x20\x08" x 22 );
				$comm->send("\r\n");
				$comm->param( '_MAILREPORT_NEXT' => 1 );																# 次の処理に進む
				$comm->param( '_MAILREPORT_DATA' => undef );															# 処理中のデータを廃棄
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(21) });
			}
			elsif ( $chr eq "\x0d" ) {								# [CR]				次のページを表示
				$comm->send( "\x08\x20\x08" x 22 );
				$comm->param( '_NOSTOP' => undef );																		# ページ表示抑止を無効
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(21) });
			}
			elsif ( $arg eq "\x20" ) {								# [SPACE]			全てのページを表示
				$comm->send( "\x08\x20\x08" x 22 );
				$comm->param( '_NOSTOP' => 1 );																			# ページ表示抑止を有効
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::Mail::Read::read(21) });
			}
		}
	},

};

#==================================================
# ■記事を表示
# r = view();
# r = 0: 出力終了（待機中）, 1:出力途中
#
sub view {
	printf  "\n** <view>\n\n";
	my $self = shift;
	my $threadId = $self->comm->param('THREADID');			# スレッドＩＤ
	my $num = $self->comm->param('_READ_MYPLACE_ID');		# 現在値（articles.id )
	my $pages = $self->comm->param('_VIEWPAGES');			# 閲覧中のページＩＤリスト
	my $data = $self->comm->param('_READ_MYPLACE_DATA');	# 現在値のデータ
	#printf "** threadId=[%s], num=(%d), pages.defined=(%d)\n", $threadId, $num, defined($pages);

	return if ( $num <= 0 );					# 現在地が無効なら終了

	unless ( defined($pages) ) {				# ページＩＤリストがなければ取得
		$pages = $self->BIGModel::DB::Contents::getPagesId( $threadId, $num ) if ( defined($threadId) && defined($num) );
		$self->comm->send( "\r\n" );
	}
	#print Dumper($pages);
	return unless ( defined($pages) );			# それでもページＩＤリストがなければ終了

	my $id = shift @{ $pages };
	if ( defined($id) ) {
		my $body = $self->BIGModel::DB::Contents::getContents( $id );										# コンテンツを取得
		$data->[6] = '' unless ( defined($data->[6]) );
		#printf "** data[6]=[%s], data[6].length=(%d)\n", $data->[6], length($data->[6]);
		if ( $data->[6] !~ /\#/ && defined( $self->comm->param('_VIEWED') ) == 0 ) {						# 閲覧フラグ＝０なら
			$data->[6] .= '#';																					# 閲覧フラグ＝１
			$self->BIGModel::DB::Articles::updFlag( $threadId, $num, $data->[6] );								# フラグを更新
#			$self->comm->param( '_VIEWED' => 1 );																# 閲覧フラグを廃棄
		}

		$body = encode('cp932', decode('utf8', $body));
		#printf "** body=[%s], body.length=(%d)\n", $body, length($body);
		$body =~ s/\n/\r\n/g;

		if ( $body =~ /\x03/ ) {																			# 最終ページ（終端がＥＴＸ）なら
			$body =~ s/\x03//;																					# 終端文字を除去
			$self->comm->send( $body ) unless ($body eq '');													# ヌルでなければコンテンツを送出
			$self->comm->send( "\r\n" );
			$self->comm->param( '_VIEWPAGES' => undef );														# 閲覧するページＩＤリストを廃棄
			$self->comm->param( '_VIEWED' => undef );															# 閲覧フラグを廃棄
			return 0;
		}
		else {																								# ページの続きがあれば
			$self->comm->send( $body );																			# コンテンツを送出
			$self->comm->param( '_VIEWPAGES' => $pages );														# 閲覧するページＩＤリストを更新
			$self->comm->param( '_VIEWED' => 1 );																# 閲覧フラグ＝１
			return 1;
		}
	}
}

#--------------------------------------------------
# ■レコードを送出
# list();
#
sub list {
	#print "\n** <list>\n\n";
	my $self = shift;
	my ( $id, $num, $r_date, $r_time, $author, $title, $flag ) = @_;
	$flag = ' ' unless( defined($flag) );
	$self->comm->send( sprintf "%05d  %s %s %-8s ", $num, $r_date, $r_time, $author );
	if ( $flag =~ /([\*|\+|\-])/ ) {		# 削除フラグが立っている場合はタイトルを隠す
		$flag = "$1";
		$title = '';
	}
	elsif ( $flag =~ /([!])/ ) {			# 内部フラグは隠す
		$flag =~ s/$1//;
	}

	$self->comm->send( sprintf "%s", ($flag ne '') ? $flag : ' ' );
	$self->comm->send( sprintf "%s\r\n", encode( 'cp932', decode('utf8', $title) ) );
	return;
}

#--------------------------------------------------
# ■クエリを実行
# r = query( query );
#
sub query {
	print "\n** <query>\n\n";
	my $self = shift;
	my $q = join('', @_);
	#print Dumper(@_);

	my $dbh = DBI->connect('dbi:mysql:dbname=bigmodel;host=127.0.0.1', 'bigmodel', '', {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	#printf "** q=[\n%s\n]\n", encode('cp932', $q);
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	my @row = $sth->fetchrow_array() if ( $sth->rows );
	$sth->finish();
	$dbh->disconnect();

	#print "[row]\n".Dumper(@row)."\n";
	return if ($#row < 0);
	return [ @row ];
}

#--------------------------------------------------
# ■旧方向へ移動する
# up();
#
sub up {
	#print "\n** <up>\n\n";
	my $self = shift;
	my $comm = $self->comm;
	$comm->param( '_READ_MOVE_DIRECTION' => 'UP' );				# 旧方向に移動
}

#--------------------------------------------------
# ■新方向へ移動する
# down();
#
sub down {
	#print "\n** <down>\n\n";
	my $self = shift;
	my $comm = $self->comm;
	$comm->param( '_READ_MOVE_DIRECTION' => 'DOWN' );			# 新方向に移動
}

#--------------------------------------------------
# ■項目を送出する
# articles( header );
#
sub articles {
	#print "\n** <articles>\n\n";
	my $self = shift;
	my $header = shift;				# ヘッダ付加フラグ
	my $comm = $self->comm;

	my $threadId		= $comm->param('THREADID');								# スレッドＩＤ
	my $tonum			= $comm->param('_READ_MOVE_TO_NUMBER');					# 番号指定
						  $comm->param( '_READ_MOVE_TO_NUMBER' => undef );
	my $move			= $comm->param('_READ_MOVE_DIRECTION');					# 移動方向('up':旧方向, 'down':新方向)
						  $comm->param( '_READ_MOVE_DIRECTION' => undef );
		$move			= defined($move) ? $move : '';
	my $myplace			= $comm->param('_READ_MYPLACE');						# 現在値（articles.articlenum)
	my $myplace_id		= $comm->param('_READ_MYPLACE_ID');						# 現在値（articles.id)
	my $threshold		= $comm->param('_READ_THRESHOLD');						# 閾値フラグ（現在値が閾値を踏んだら１、踏まなければ０にする）
	my $filter_mode		= $comm->param('_READ_FILTER_MODE');					# フィルタモード('title':タイトル検索, 'full':全文検索, undef:検索なし)
	my $filter_keyword	= $comm->param('_READ_FILTER_KEYWORD');					# フィルタキーワード
	my $data			= $comm->param('_READ_MYPLACE_DATA');					# 現在値のデータ
	my $end_record;																# フィルタモードで該当レコードがなかった場合のフラグ


	# 呼び出し直前の状態（確認用）
	#print "\n";
	#	printf "*1* myplace=(%d), myplace.id=(%d)\n", $myplace, ( defined($myplace_id) ? $myplace_id : -1 );
	#	printf "*1* threshold=(%d)\n", $threshold;
	#	print "[DATA]".Dumper($data)."\n";
	#print "\n";


	# ■基本形のクエリから状態に応じクエリを生成する
	#-----------------------------------------------
	my @query;
		$query[0] = "SELECT ".
						"`articles`.`id` as id, ".
						"articlenum, ".
						"date_format(datetime, '%y-%m-%d') AS r_date, ".
						"date_format(datetime, '%H:%i:%s') AS r_time, ".
						"author, title, flag ".
					"FROM `articles` ";
		$query[1] = "";
		$query[2] = "WHERE `articles`.`threadid` = '$threadId' ";
		$query[3] = "";
		$query[4] = "";
		$query[5] = "";
		$query[6] = "";

	# ■移動なし
	#-----------------------------------------------
	if ( $move eq '' ) {

		# ■番号指定あり（閾値を踏むことはない）
		#-----------------------------------------------
		if ( defined($tonum) ) {
			#printf "** (tonum) : tonum=(%d)\n", ( defined($tonum) ? $tonum : 0 );
			$tonum = 1 if ($tonum < 1);																				# 指定番号が１以下は１に訂正
			$tonum++;
			$query[4] = "AND articlenum < $tonum ";																	# 閾値を超えた場合は最新レコードの番号を返す
			$query[6] = "ORDER BY articlenum DESC LIMIT 1 ";

			my $r = $self->BIGModel::Mail::Read::query(@query);														# クエリを実行
			if ( defined($r) ) {
	 			my ( $id, $num, $r_date, $r_time, $author, $title, $flag ) = @{ $r };
				$myplace = $num; $myplace_id = $id;																		# 現在値を更新
				$self->BIGModel::Generic::msgs('MAIL_HEADER');															# ヘッダを送出
				$self->BIGModel::Mail::Read::list( $id, $myplace, $r_date, $r_time, $author, $title, $flag );			# レコードを送出
				$data = [ $id, $myplace, $r_date, $r_time, $author, $title, $flag ];
				$threshold = 0;																							# 閾値フラグ＝０
			}
		}

		# ■番号指定なし（閾値を踏む場合あり）
		#-----------------------------------------------
		else {
			#print "** (no_move)\n";
			$self->BIGModel::Generic::msgs('MAIL_HEADER');															# ヘッダを送出
			if ($myplace <= 0) {																					# 現在値が０以下
				$comm->send( sprintf "%05d  v\r\n", $myplace );															# 下限の閾値マークを送出
				$threshold = 1;																							# 閾値フラグ＝１
			}
			else {																									# 現在値が０以上
				my $r = $self->BIGModel::DB::Articles::getArticle( $threadId, $myplace );							# 現在値のレコードを得る
				if ( defined($r) ) {																					# 現在値のレコードあり
					my ( $id, $r_date, $r_time, $author, $title, $flag ) = @{ $r };
					$self->BIGModel::Generic::msgs('MAIL_HEADER') if ( defined($header) );									# ヘッダを送出※
					$self->BIGModel::Mail::Read::list( $id, $myplace, $r_date, $r_time, $author, $title, $flag );			# レコードを送出
					$data = [ $id, $myplace, $r_date, $r_time, $author, $title, $flag ];
					$threshold = 0;																							# 閾値フラグ＝０
				}
				else {																									# 現在値のレコードなし
					$self->BIGModel::Generic::msgs('MAIL_HEADER') if ( defined($header) );									# ヘッダを送出※
					$comm->send( sprintf "%05d  ^\r\n", $myplace );															# 上限の閾値マークを送出
					undef($data);
					$threshold = 1;																							# 閾値フラグ＝１
					$end_record = -1;
				}
			}
		}
	}

	# ■移動あり
	#-----------------------------------------------
	else {

		# ■フィルタ関連のクエリ
		#-----------------------------------------------
		if ( defined($filter_mode) ) {

			# ■タイトル検索
			#-----------------------------------------------
			if ( $filter_mode =~ /TITLE/i ) {
				#print "** (title_search)\n";
				$query[1] = "";
				$query[3] = "AND `title` LIKE '%$filter_keyword%' ";
				$query[5] = "";
			}

			# ■全文検索
			#-----------------------------------------------
			elsif ( $filter_mode =~ /FULL/i ) {
				#print "** (fulltext_search)\n";
				$query[1] = "INNER JOIN `contents` ON `articles`.`id` = `contents`.`articleid` ";
				$query[2] = "WHERE `articles`.`threadid` = '$threadId' ";
				$query[3] = "AND ( `title` LIKE '%$filter_keyword%' OR `contents` LIKE '%$filter_keyword%' ) ";
				$query[5] = "GROUP BY `articles`.`id` ";
			}
		}

		# ■フィルタ停止中
		#-----------------------------------------------
		else {
			$query[1] = "";
			$query[3] = "";
			$query[5] = "";
		}

		# ■上移動（－方向・旧方向）
		#-----------------------------------------------
		if ( $move =~ /UP/i ) {
			#print "** (up)\n";
			#printf "** myplace=(%d)\n", $myplace;

			unless ( $threshold == 1 && $myplace == 0 ) {															# 現在値＝０かつ閾値フラグ＝１でない
				$query[4] = "AND articlenum < $myplace ";
				$query[6] = "ORDER BY articlenum DESC LIMIT 1 ";
				my $r = $self->BIGModel::Mail::Read::query(@query);														# クエリを実行

				# （フィルタモードが無効）
				#-----------------------------------------------
				unless ( defined($filter_mode) ) {
					if ( defined($r) ) {																					# 該当レコードあり
			 			my ( $id, $num, $r_date, $r_time, $author, $title, $flag ) = @{ $r };
						$myplace = $num; $myplace_id = $id;																		# 現在値を更新
						$self->BIGModel::Generic::msgs('MAIL_HEADER') if ( defined($header) );									# ヘッダを送出※
						$self->BIGModel::Mail::Read::list( $id, $myplace, $r_date, $r_time, $author, $title, $flag );			# レコードを送出
						$data = [ $id, $myplace, $r_date, $r_time, $author, $title, $flag ];
						$threshold = 0;																							# 閾値フラグ＝０
					}
					else {																									# 該当レコードなし（閾値）
						if ( $threshold == 0 ) {																				# 閾値フラグ＝０なら
							#print "** (threshold)\n";
							$myplace--;
							$self->BIGModel::Generic::msgs('MAIL_HEADER') if ( defined($header) );									# ヘッダを送出※
							$comm->send( sprintf "%05d  v\r\n", $myplace );															# 閾値マークを送出
							undef($data);
							$threshold = 1;																							# 閾値フラグ＝１
							$end_record = -1;
						}
					}
				}

				# （フィルタモードが有効）
				#-----------------------------------------------
				else {
					if ( defined($r) ) {																					# 該当レコードあり
			 			my ( $id, $num, $r_date, $r_time, $author, $title, $flag ) = @{ $r };
						$myplace = $num; $myplace_id = $id;																		# 現在値を更新

						my $title_ = encode('cp932', decode('utf8', $title));
						my $filter_keyword_ = encode('cp932', $filter_keyword);
						#printf "** filter_mode=[%s]\n", $filter_mode;
						#printf "** filter_keyword=[%s]\n", $filter_keyword_;
						#printf "** title=[%s]\n", $title_;

						if ( $filter_mode =~ /TITLE/i ) {																		# タイトル検索なら
							$self->BIGModel::Generic::msgs('SEARCH_INSPECTED');												# ** 見つかりました **
						}

						if ( $filter_mode =~ /FULL/i ) {																		# 全文検索なら
							if ( $title_ =~ /$filter_keyword_/ ) {																	# 検索文字列がタイトルにふくまれていれば
								#print "** (1)\n";
								$self->BIGModel::Generic::msgs('SEARCH_INSPECTED');												# ** 見つかりました **
							}
							else {																									# 含まれていなければ
								#print "** (2)\n";
								$self->BIGModel::Generic::msgs('SEARCH_INSPECTED_BYTEXT');											# ** テキスト中に見つかりました **
							}
						}

						$self->BIGModel::Generic::msgs('MAIL_HEADER') if ( defined($header) );									# ヘッダを送出※
						$self->BIGModel::Mail::Read::list( $id, $myplace, $r_date, $r_time, $author, $title, $flag );			# レコードを送出
						$data = [ $id, $myplace, $r_date, $r_time, $author, $title, $flag ];
						$threshold = 0;																							# 閾値フラグ＝０
					}
					else {																									# 該当レコードなし
						my $r = $self->BIGModel::DB::Articles::getArticle( $threadId, $myplace );							# 現在値のレコードを得る
						if ( defined($r) ) {																					# 現在値のレコードあり
							my ( $id, $r_date, $r_time, $author, $title, $flag ) = @{ $r };
							$self->BIGModel::Generic::msgs('MAIL_HEADER');															# ヘッダを送出
							$self->BIGModel::Mail::Read::list( $id, $myplace, $r_date, $r_time, $author, $title, $flag );			# レコードを送出
							$data = [ $id, $myplace, $r_date, $r_time, $author, $title, $flag ];
							$threshold = 0;																							# 閾値フラグ＝０
						}
						else {																									# 現在値のレコードなし
							#print "** (threshold)\n";
							$self->BIGModel::Generic::msgs('MAIL_HEADER');															# ヘッダを送出
							$comm->send( sprintf "%05d  v\r\n", $myplace ) if ($myplace == 0);										# 現在値が０なら下限の閾値マークを送出
							$comm->send( sprintf "%05d  ^\r\n", $myplace ) unless ($myplace == 0);									# 現在値が０以外なら上限の閾値マークを送出
							undef($data);
							$threshold = 1;																							# 閾値フラグ＝１
						}
						$end_record = -1;
					}
				}
			}
		}

		# ■下移動（＋方向・新方向）
		#-----------------------------------------------
		elsif ( $move =~ /DOWN/i ) {
			#print "** (down)\n";
			#printf "** myplace=(%d)\n", $myplace;

			unless ( $threshold == 1 && $myplace != 0 ) {															# 現在値＝０以外かつ閾値フラグ＝１でない
				$query[4] = "AND articlenum > $myplace ";
				$query[6] = "ORDER BY articlenum ASC LIMIT 1 ";
				my $r = $self->BIGModel::Mail::Read::query(@query);															# クエリを実行（レコードが追加されている場合があるので確認のため）

				# （フィルタモードが無効）
				#-----------------------------------------------
				unless ( defined($filter_mode) ) {
					if ( defined($r) ) {																					# 該当レコードあり
			 			my ( $id, $num, $r_date, $r_time, $author, $title, $flag ) = @{ $r };
						$myplace = $num; $myplace_id = $id;																		# 現在値を更新
						$self->BIGModel::Generic::msgs('MAIL_HEADER') if ( defined($header) );									# ヘッダを送出※
						$self->BIGModel::Mail::Read::list( $id, $myplace, $r_date, $r_time, $author, $title, $flag );			# レコードを送出
						$data = [ $id, $myplace, $r_date, $r_time, $author, $title, $flag ];
						$threshold = 0;																							# 閾値フラグ＝０
					}
					else {																									# 該当レコードなし（閾値）
						if ( $threshold == 0 ) {																				# 閾値フラグ＝０なら
							#print "** (threshold)\n";
							$myplace++;
							$self->BIGModel::Generic::msgs('MAIL_HEADER') if ( defined($header) );									# ヘッダを送出※
							$comm->send( sprintf "%05d  ^\r\n", $myplace );															# 閾値マークを送出
							undef($data);
							$threshold = 1;																							# 閾値フラグ＝１
							$end_record = -1;
						}
					}
				}

				# （フィルタモードが有効）
				#-----------------------------------------------
				else {
					if ( defined($r) ) {																					# 該当レコードあり
			 			my ( $id, $num, $r_date, $r_time, $author, $title, $flag ) = @{ $r };
						$myplace = $num; $myplace_id = $id;																		# 現在値を更新

						my $title_ = encode('cp932', decode('utf8', $title));
						my $filter_keyword_ = encode('cp932', $filter_keyword);
						#printf "** filter_mode=[%s]\n", $filter_mode;
						#printf "** filter_keyword=[%s]\n", $filter_keyword_;
						#printf "** title=[%s]\n", $title_;

						if ( $filter_mode =~ /TITLE/i ) {																		# タイトル検索なら
							$self->BIGModel::Generic::msgs('SEARCH_INSPECTED');												# ** 見つかりました **
						}

						if ( $filter_mode =~ /FULL/i ) {																		# 全文検索なら
							if ( $title_ =~ /$filter_keyword_/ ) {																	# 検索文字列がタイトルにふくまれていれば
								#print "** (1)\n";
								$self->BIGModel::Generic::msgs('SEARCH_INSPECTED');												# ** 見つかりました **
							}
							else {																									# 含まれていなければ
								#print "** (2)\n";
								$self->BIGModel::Generic::msgs('SEARCH_INSPECTED_BYTEXT');											# ** テキスト中に見つかりました **
							}
						}

						$self->BIGModel::Generic::msgs('MAIL_HEADER') if ( defined($header) );									# ヘッダを送出※
						$self->BIGModel::Mail::Read::list( $id, $myplace, $r_date, $r_time, $author, $title, $flag );			# レコードを送出
						$data = [ $id, $myplace, $r_date, $r_time, $author, $title, $flag ];
						$threshold = 0;																							# 閾値フラグ＝０
					}
					else {																									# 該当レコードなし
						my $r = $self->BIGModel::DB::Articles::getArticle( $threadId, $myplace );							# 現在値のレコードを得る
						if ( defined($r) ) {																					# 現在値のレコードあり
							my ( $id, $r_date, $r_time, $author, $title, $flag ) = @{ $r };
							$self->BIGModel::Generic::msgs('MAIL_HEADER');															# ヘッダを送出
							$self->BIGModel::Mail::Read::list( $id, $myplace, $r_date, $r_time, $author, $title, $flag );			# レコードを送出
							$data = [ $id, $myplace, $r_date, $r_time, $author, $title, $flag ];
							$threshold = 0;																							# 閾値フラグ＝０
						}
						else {																									# 現在値のレコードなし
							#print "** (threshold)\n";
							$self->BIGModel::Generic::msgs('MAIL_HEADER');															# ヘッダを送出
							$comm->send( sprintf "%05d  v\r\n", $myplace ) if ($myplace == 0);										# 現在値が０なら下限の閾値マークを送出
							$comm->send( sprintf "%05d  ^\r\n", $myplace ) unless ($myplace == 0);									# 現在値が０以外なら上限の閾値マークを送出
							undef($data);
							$threshold = 1;																							# 閾値フラグ＝１
						}
						$end_record = -1;
					}
				}
			}
		}
	}

	$myplace_id = -1 if ( $threshold == 1 );		# 現在値が閾値なら保存しているＩＤを未定義にする

	# 更新後の状態（確認用）
	#print "\n";
	#	printf "*2* myplace=(%d), myplace.id=(%d)\n", $myplace, ( defined($myplace_id) ? $myplace_id : -1 );
	#	printf "*2* threshold=(%d)\n", $threshold;
	#	print "[DATA]".Dumper($data)."\n";
	#print "\n";
	#print "----------------------------------\n";

	# パラメータを更新する
	$comm->param( '_READ_MYPLACE' => $myplace );				# 現在値（articles.articlenum)
	$comm->param( '_READ_MYPLACE_ID' => $myplace_id );			# 現在値（articles.id)
	$comm->param( '_READ_THRESHOLD' => $threshold );			# 閾値フラグ（現在値が閾値を踏んだら１、踏まなければ０にする）
	$comm->param( '_READ_MYPLACE_DATA' => $data );				# 現在値のデータ

	return $end_record if ( defined($end_record) );				# フィルタモードでレコードが見つからなかった
	return $threshold;
}

#--------------------------------------------------
# ■開封済みメールのメールリポートを表示
# r = readedMailReport( step );
#
sub readedMailReport {
	#print "\n** <readedMailReport>\n\n";
	my $self = shift;
	my $step = shift;
	return $self->BIGModel::Mail::Read::mailReport( 'READED', $step );
}

#--------------------------------------------------
#■削除済みメールのメールレポートを表示
# r = deletedMailReport( step );
#
sub deletedMailReport {
	#print "\n** <deletedMailReport>\n\n";
	my $self = shift;
	my $step = shift;
	return $self->BIGModel::Mail::Read::mailReport( 'DELETED', $step );
}

#--------------------------------------------------
# ■保管メールのリストを表示
# r = storedMailReport( step );
#
sub storedMailReport {
	#print "\n** <storedMailReport>\n\n";
	my $self = shift;
	my $step = shift;
	return $self->BIGModel::Mail::Read::mailReport( '', $step );
}

#--------------------------------------------------
# ■メールレポートの表示
# r = mailReport( type, step );
#
#	type = 'readed'		: 開封済みメール		'@{id}.READED'	(メールレポートスレッド)
#		   'deleted'	: 削除済みメール		'@{id}.DELETED'	(メールレポートスレッド)
#		   (null)		: 保管メール			'@{id}'			(メールボックススレッド)
#	step = (num)		一度に表示する件数(ページャー機能が有効のとき)
#
#	依存パラメータ: ID
#					_MAILREPORT_DATA
#
#	r = ( 0:続きなし, 1:続きあり )
#
sub mailReport {
	#print "\n** <mailReport>\n\n";
	my $self = shift;
	my $type = shift;															# レポートの対象('readed':開封済みメール, 'deleted':削除済みメール)
		$type = uc($type) if ( $type ne '' );										# レポートの対象が未指定なら保管メール
	my $step = shift;															# 一度に表示する件数
		$step = 10 unless( defined($step) && ($step =~ /\d/) );					#		（デフォルトは１０件）
	my $me = $self->comm->param('ID');											# 自分のユーザＩＤ
	my $threadId = ( $type ne '' ) ? '@'.$me.'.'.$type : '@'.$me;				# スレッドＩＤ(メールボックススレッドまたはメールレポートスレッド)
	my $data = $self->comm->param('_MAILREPORT_DATA');							# 処理途中のデータ
	#printf "** me=[%s], type=[%s], threadId=[%s]\n", $me, $type, $threadId;
	#print "[data]\n".Dumper($data)."\n";
	my ( $total, $cnt, $lastid );

	my $dbh = DBI->connect('dbi:mysql:dbname=bigmodel;host=127.0.0.1', 'bigmodel', '', {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my ( $q, $sth );

	unless ( defined($data) ) {													# 処理途中のデータがなければ初期データを取得する
		$q = "SELECT count(id) FROM `articles` WHERE threadid = '$threadId'";
		$sth = $dbh->prepare( $q );
		$sth->execute();
		($total) = $sth->fetchrow_array() if ( $sth->rows );						# スレッドのレコード総数を得る
		$sth->finish();
		$cnt = 1;																	# カウンタ＝１
		$lastid = -1;																# 最後に出力したレコードＩＤ＝無効値
	}
	else {
		( $total, $cnt, $lastid ) = @{ $data };
	}
	#printf "*(0)* total=(%d), cnt=(%d), lastid=(%d), \n", $total, $cnt, $lastid;

	# 出力するクエリを生成
	my @query = (
					"SELECT id, ",
						"date_format(datetime, '%y-%m-%d') AS r_date, ",
						"date_format(datetime, '%H:%i:%s') AS r_time, ",
						"author, title, flag",
					" FROM `articles` WHERE threadid = '$threadId'",
					"",
					" ORDER BY id ASC LIMIT $step"
				);
	$query[5] = " AND id > $lastid" unless ( $lastid == -1 );					# 最後に出力したレコードＩＤがあればクエリに盛り込む
	$q = join('', @query);
	#printf "** q=[\n%s\n]\n\n", $q;
	$sth = $dbh->prepare( $q );
	$sth->execute();

	#printf "*(1)* total=(%d), cnt=(%d), sth.rows=(%d), \n", $total, $cnt, $sth->rows;
	my $exit = ( $cnt + $sth->rows > $total ) ? 1 : 0;							# カウンタ＋該当レコード数が総レコード数に達したら終了フラグ＝１

	if ( $sth->rows ) {
		while ( my ($id, $r_date, $r_time, $reader, $title, $flag) = $sth->fetchrow_array() ) {
			$lastid = $id;
			$reader = $reader."\x20" x 8;
			$title = encode('cp932', decode('utf8', $title));
			$flag = ' ' unless (defined($flag));
			$flag =~ s/[!]//;																						# 内部フラグは隠す
			if ( $cnt == 1 ) {																						# １件目ならヘッダを付ける
				#printf "** type=[%s]\n", $type;
				$self->BIGModel::Generic::msgs('MAILREPORT_READED') if ( $type eq 'READED' );		# ** 手紙が読まれました **
				$self->BIGModel::Generic::msgs('MAILREPORT_DELETED') if ( $type eq 'DELETED' );		# ** 手紙が削除されました **
				$self->BIGModel::Generic::msgs('MAILREPORT_NEWMAIL') if ( $type eq '' );			# ** 手紙が届いております **
				$self->BIGModel::Generic::msgs('MAIL_HEADER');
			}
			$self->comm->send( sprintf "%05d  %s %s %-.8s %s%s\r\n", $cnt, $r_date, $r_time, $reader, $flag, $title );
			$cnt++;
		}
		$self->comm->param( '_MAILREPORT_DATA' => [ $total, $cnt, $lastid ] );								# 最終レコードのデータを保存
	}
	else {
		#print "** (no_record)\n";
	}
	$sth->finish();
	$dbh->disconnect;
	#printf "*(2)* total=(%d), cnt=(%d), lastid=(%d), \n", $total, $cnt, $lastid;
	#printf "*(2)* exit=(%d)\n", $exit;
	#print "\n\n";

	$self->comm->send( sprintf "%05d  ^\r\n", $cnt ) if ( $type eq '' && $total >= 1 && $cnt > $total );			# 保管メールの一覧の最後は閾値マーク

	if ( $exit == 1 ) {													# 続きがなければ未定義を返す
		$self->comm->param( '_MAILREPORT_DATA' => undef );
		return 0;
	}
	return 1;															# 続きがあれば１を返す
}

#--------------------------------------------------
# ■メールレポートの削除
# removeMailReport( type );
#
#	type = 'READED'			# 開封済みのメールレポート
#	type = 'DELETED'		# 削除済みのメールレポート
#
#	依存パラメータ: ID
#
sub removeMailReport {
	#print "\n** <removeMailReport>\n\n";
	my $self = shift;
	my $type = shift;
		$type = uc($type) if ( $type ne '' );
	my $me = $self->comm->param('ID');							# 自分のユーザＩＤ
	my $threadId = '@'.$me.'.'.$type if ( $type ne '' );		# スレッドＩＤ(メールレポートスレッド)
	#printf "** threadId=[%s]\n", $threadId;

	return unless ( defined($threadId) );						# スレッドＩＤが未定義なら戻る

	my $dbh = DBI->connect('dbi:mysql:dbname=bigmodel;host=127.0.0.1', 'bigmodel', '', {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	my $q = "DELETE FROM `articles` WHERE threadid = '$threadId'";
	#printf "** q=[\n%s\n]\n\n", $q;
	$dbh->do( $q );
	$dbh->disconnect;
}

#--------------------------------------------------
# ■メールボックスの更新とメールレポート処理
# メール受信を抜けるときに次の処理をする
#	(1) 開封済みフラグ付きのメール（レポート済みフラグ付きのメールは処理しない）
#			→(1A)送信元へ開封済みをレポート、(1B)レポート済みフラグを付ける
#	(2) 削除済みフラグ付きのメール
#			→(2A)送信元へ削除済みをレポート、(2B)メール（アーティクルとコンテンツ）を削除する
#	(3) 文書番号を振り直す
#
sub refresh {
	#print "\n** <refresh>\n\n";
	my $self = shift;
	my $comm = $self->comm;
	my $from = $comm->param('ID');					# ユーザＩＤ(自分)
	my $threadId = $comm->param('THREADID');		# スレッドＩＤ(メールボックススレッド)

	my $dbh = DBI->connect('dbi:mysql:dbname=bigmodel;host=127.0.0.1', 'bigmodel', '', {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	my $dbh2 = DBI->connect('dbi:mysql:dbname=bigmodel;host=127.0.0.1', 'bigmodel', '', {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh2->do("SET NAMES utf8;");

	# フラグ : ( '#'=開封済み, '*'=削除, '!'=レポート済み )

	# (1)開封済みフラグ付きのレコードを得る（レポート済みフラグ付きのレコードは除外する）
	#-------------------------------------------
	my $q =	"SELECT author, title FROM `articles` ".
			"WHERE threadid = '$threadId' AND ( flag LIKE '%#%' ) AND ( flag NOT LIKE '%!%' )";
	#printf "*(1)* q=[\n%s\n]\n\n", $q;
	my $sth = $dbh->prepare( $q );
	$sth->execute();

	# (1A)差出人のメールレポートスレッドに書き込む
	#-------------------------------------------
	while ( my @row = $sth->fetchrow_array ) {
		my $threadId = '@'.$row[0];
		my $title = $row[1];
		my $q = "INSERT INTO `articles` (threadid, articlenum, author, title) VALUES ('$threadId.READED', '0', '$from', '$title')";
		#printf "*(1A)* q=[\n%s\n]\n\n", $q;
		$dbh2->do( $q );
	}
	$sth->finish;

	# (1B)メールレポートしたレコードのフラグを更新（開封済みフラグにレポート済みフラグを追加）する
	# '#' → '#!'
	#-------------------------------------------
	$q = "UPDATE `articles` SET flag = REPLACE(flag, '#', '#!' ) ".
		 "WHERE threadid = '$threadId' AND ( flag LIKE '%#%' ) AND ( flag NOT LIKE '%\!%' )";
	#printf "*(1B)* q=[\n%s\n]\n\n", $q;
	$dbh->do( $q );

	# (2)削除済みフラグ付きのレコードを得る
	#-------------------------------------------
	$q = "SELECT id, author, title FROM `articles` ".
		 "WHERE threadid = '$threadId' AND ( flag LIKE '%*%' )";
	#printf "*(2)* q=[\n%s\n]\n\n", $q;
	$sth = $dbh->prepare( $q );
	$sth->execute();

	# (2A,2B)差出人のメールレポートスレッドに書き込んで、メールのコンテンツを削除する
	#-------------------------------------------
	while ( my @row = $sth->fetchrow_array ) {
		#printf "** (delete) id=(%d), author=[%s], title=[%s]\n", $row[0], $row[1], encode('cp932', decode('utf8', $row[2]));
		my $articleId = $row[0];
		my $threadId = '@'.$row[1];		# 送信先ＩＤ
		my $title = $row[2];
		my $q = "INSERT INTO `articles` (threadid , articlenum, author, title) VALUES ('$threadId.DELETED', '0', '$from', '$title')";
		#printf "*(2A)* q=[\n%s\n]\n\n", $q;
		$dbh2->do( $q );
		$threadId = '@'.$from;			# 送信元ＩＤ（自分）
		$q = "DELETE FROM `contents` WHERE threadid = '$threadId' AND articleid = $articleId";
		#printf "*(2B-2)* q=[\n%s\n]\n\n", $q;
		$dbh2->do( $q );
	}
	$dbh2->disconnect;
	$sth->finish();

	# (2B)削除ずみフラグ付きのレコードを削除する
	#-------------------------------------------
	$q = "DELETE FROM `articles` ".
		 "WHERE threadid = '$threadId' AND ( flag LIKE '%*%' )";
	#printf "*(2B-2)* q=[\n%s\n]\n\n", $q;
	$dbh->do( $q );

	# (3)文書番号を振り直す
	#-------------------------------------------
	$q = "SET \@i:=0";
	#printf "*(3-1)* q=[\n%s\n]\n\n", $q;
	$dbh->do( $q );
	$q = "UPDATE `articles` SET articlenum = (\@i:=\@i+1) WHERE threadid = '$threadId';";
	#printf "*(3-2)* q=[\n%s\n]\n\n", $q;
	$dbh->do( $q );
	$dbh->disconnect;
}

#--------------------------------------------------
# ■スタートアップ
# startup();
#
# パラメータの初期化
# メールボックスの状態（保管メールや新着メールの有無）の取得
# 現在値の決定
#
#	依存パラメータ: ID
#					THREADID
#					_READ_FILTER_MODE
#					_READ_FILTER_KEYWORD
#					_READ_THRESHOLD
#					_READ_MYPLACE
#					_READ_MYPLACE_ID
#
sub startup {
	#print "\n** <startup>\n\n";
	my $self = shift;
	my $comm = $self->comm;

	my $recipient = $self->comm->param('ID');				# メールレポートを送るユーザ（＝メールの受信者＝自分）
	my $threadId = '@'.$recipient;							# メールボックスのスレッドＩＤ
	$comm->param( 'THREADID' => $threadId );
	#printf "** threadid=(%s)\n", $threadId;

	$comm->param( '_READ_FILTER_MODE' => undef );			# フィルタのパラメータをリセット
	$comm->param( '_READ_FILTER_KEYWORD' => undef );
	$comm->param( '_READ_THRESHOLD' => 0 );					# 閾値フラグ
	my $r;

	my $dbh = DBI->connect('dbi:mysql:dbname=bigmodel;host=127.0.0.1', 'bigmodel', '', {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	# (1)未開封メールの最も古いレコードを得る
	my $q = "SELECT id, articlenum FROM `articles` ".
			"WHERE threadid = '$threadId' AND ( ( flag IS NULL ) OR ( flag NOT LIKE '%#%' ) ) ORDER BY id ASC LIMIT 1";
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	my ( $myplace_id, $myplace ) = $sth->fetchrow_array() if ( $sth->rows );
	$sth->finish();
	#printf "** myplace=(%d), myplace_id=(%d)\n", $myplace, $myplace_id if ( defined($myplace) );

	if ( defined($myplace) ) {										# 該当レコードあり（新着メールあり）
		$comm->param( '_READ_MYPLACE' => $myplace );					# 現在値（文書番号）
		$comm->param( '_READ_MYPLACE_ID' => $myplace_id );				# 現在値（レコード番号）
		$r = 1;
	}
	else {															# 該当レコードなし
		# (2)メールボックスの最も新しいレコードを得る
		my $q = "SELECT id, articlenum FROM `articles` WHERE threadid = '$threadId' ORDER BY articlenum DESC LIMIT 1";
		my $sth = $dbh->prepare( $q );
		$sth->execute();
		my ( $max_id, $max ) = $sth->fetchrow_array() if ( $sth->rows );
		$sth->finish();
		$dbh->disconnect();

		if ( defined($max) ) {											# 該当レコードあり（保存メールあり）
			#printf "** max=(%d), max_id=(%d)\n", $max, $max_id if ( defined($max) );
			$comm->param( '_READ_MYPLACE' => $max );						# 現在値（文書番号）
			$comm->param( '_READ_MYPLACE_ID' => $max_id );					# 現在値（レコード番号）
			$r=2;
		}
		else {															# 該当レコードなし（保存メールなし）
			$r=0;
		}
	}
	return $r;
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	#printf "\n\n** < %s::initalize [%s] >\n", __PACKAGE__, Debug::_caller(caller);
	my $self = shift;
	my $comm = $self->comm;
		$comm->param( 'LOCATION' => 'MAIL' );		# 現在地
	#print Dumper($self->{'param'});
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	#printf "\n\n** < %s::finalize [%s] >\n", __PACKAGE__, Debug::_caller(caller);
	my $self = shift;
	$self->BIGModel::Mail::Read::refresh();

	$self->comm->param( 'THREADID' => undef );		#
	$self->comm->param( '_READ_MYPLACE' => undef );			# パラメータの廃棄
	$self->comm->param( '_READ_MYPLACE_ID' => undef );		#
	$self->comm->param( '_READ_MYPLACE_DATA' => undef );	#
	$self->comm->param( '_READ_THRESHOLD' => undef );		#

	$self->BIGModel::Generic::Path::back();					# 現在のパスがルートでなければひとつ手前のユーティリティを呼び出す
}

#--------------------------------------------------
# ■呼び出し
#
sub read {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	if ( ref($func) eq 'CODE') {
		&{ $func }($self);
	}
}

#------------------------------------------------------
# ■ エントリポイントを返す
#
sub start {
	#printf "\n\n** < %s::start [%s] >\n", __PACKAGE__, Debug::_caller(caller);
	return \&BIGModel::Mail::Read::read;
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;