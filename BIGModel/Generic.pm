package BIGModel::Generic;
our $VERSION = "0001.20190211.1953";	# 改訂版
our $VERSION = "0001.20190218.2207";	# 改訂版 完成
#
# BIG-Model ホストプログラム
# 汎用処理
#
use strict;
no strict 'refs';
use warnings;
use utf8;
use Encode qw( encode decode );
use DBI;
use Data::Dumper;
use Debug;
use Naoit0::DateTime;
use BIGModel::Generic::Path;			# パス
use BIGModel::Generic::Alias;			# エイリアス
use BIGModel::Generic::Permit;			# アクセス権限
use BIGModel::Generic::DB;				# ＤＢアクセス
#
# isutyload( util )						# ユーティリティがロードされている？
# modoru()								# 戻る
# msgs( id )							# 定型メッセージを送出
# accesslog()							# アクセスログに記録
# validate( [:|-], dateTime )			# 曜日が取得できたり、シリアル時間が取得できたらパスとする
# now()									# 現在時刻を得る

#------------------------------------------------------
# ●前のパスに戻る
# modoru();
#
# パスリストの順にユーティリティが呼び出せるかどうかを調べる。
# ユーティリティが呼べるパスが見つかったら、そのパスに移動し、ユーティリティを呼び出す。
# パスリストでユーティリティが呼べるパスが見つからなかったら、
# トップレベルに移動し、トップレベルのユーティリティを呼び出す。
# ※ユーティリティのfinalize()から呼び出す
#
sub modoru {
	printf "\n(modoru [%s])\n", join(':', caller);			# ????
	my $self = shift;
	my $userid = $self->from_param('ID');							# ユーザＩＤ
	my $userlv = $self->from_param('USERLEVEL');					# ユーザレベル
	my $p = $self->from_param('_Path');
	my @paths = $p->paths();										# パスリスト（履歴を書き換えないようにするためコピーを使用する）
	shift(@paths);													# パスリスト（コピー）からカレントパスを外す
	print "[paths-1]\n".Dumper(@paths)."[/paths-1]\n";				# ???? パスリスト（コピー）

	my $func;
	print "** (parse1)\n";											# ????
	while (@paths) {												# パスリスト（コピー）を巡回する
		my $path = shift(@paths);
		printf "** path=[%s]\n",$path;								# ???? 取り出したパス

		my $util = $self->BIGModel::Generic::DB::Map::getutil_bypath( $path );		# (1) パスのユーティリティを得る
		my ($pkg, $start) = @{$util} if ( defined($util) );
		if ( defined($pkg) && defined($start) ) {
			printf "** pkg=[%s], start=[%s]\n", $pkg, $start;					# ????
			if ( $self->BIGModel::Generic::Permit::allow() ) {					# (2) パスのアクセス権限(ALLOW, DISALLOW)チェック
				if ( $self->BIGModel::Generic::isutyload($pkg) ) {				# (3) ユーティリティのロードチェック
					print "** (successed)\n";						# ????
					$p->go($path);												# パスを移動する
					$func = 'BIGModel::'.$pkg.'::'.$start;						# 呼び出すユーティリティ
					$self->sethandler( sub { $self->$func } );					# 移動先をハンドラ定義
					@paths = ();												# パスをクリアしてループを抜ける
				}
				else {
					print "** (util_not_loaded)\n";				# ????
				}
			}
			else {
				print "** (disallow)\n";							# ????
			}
		}
		else {
			print "** (util_not_found)\n";							# ????
		}
	}

	unless ( defined($func) ) {													# 呼び出すユーティリティが見つからなかったら
		print "** (parse2)\n";										# ????
		my $path = ';';
		my $util = $self->BIGModel::Generic::DB::Map::getutil_bypath($path);		# (1) トップレベルのユーティリティを得る
		my ($pkg, $start) = @{$util} if ( defined($util) );
		my $errmsg;
		if ( defined($pkg) && defined($start) ) {								# ユーティリティを得られた
			printf "** pkg=[%s], start=[%s]\n", $pkg, $start;		# ????
			if ( $self->BIGModel::Generic::Permit::allow() ) {					# (2) パスのアクセス権限(ALLOW, DISALLOW)チェック
				if ( $self->BIGModel::Generic::isutyload($pkg) ) {				# (3) ユーティリティのロードチェック
					print "** (successed)\n";						# ????
					$p->go($path);												# パスを移動する
					$func = 'BIGModel::'.$pkg.'::'.$start;						# 呼び出すユーティリティ
					$self->sethandler( sub { $self->$func } );					# 移動先をハンドラ定義
				}
				else {
					$errmsg = sprintf("ユーティリティ(%s)がロードされていません", $pkg);
				}
			}
			else {
				$errmsg = sprintf("パス(%s)へのアクセス権限がありません", $path);
			}
		}
		else {
			$errmsg = sprintf("ユーティリティ(%s)がありません", $pkg);
		}
		if (defined($errmsg) ) {
			$self->send( encode('cp932', sprintf("\r\n** 異常終了しました (%s)\r\n",$errmsg)) );
			$self->disconnect($self->from);
		}
	}
	print "[paths-2]\n".Dumper( $p->paths() )."[/paths-2]\n";		# ????
	print "(/modoru)\n";											# ????
}

#------------------------------------------------------
# ●ユーティリティがロードされている？
# isutyload( pkg );
#
# バージョン番号が得られたらロード済みとする
#
sub isutyload {
	printf "\n(isutyload [%s])\n", join(':', caller);			# ????
	my $self = shift;
	my $pkg = shift;
	my $ver = ${ 'BIGModel::'.$pkg.'::VERSION' };
	if ( defined($ver) ) {									# バージョン番号が得られた
		printf "** pkg=[%s], var=[%s]\n", $pkg, $ver;		# ????
		print "(/isutyload)\n";			# ????
		return 1;
	}
	print "(/isutyload)\n";			# ????
	return;
}

#------------------------------------------------------
# ●定型メッセージを送信(ＣＰ９３２専用)
# r = sendmsgs( id, str, ... );
#
sub sendmsgs {
	#print "\n(sendmsgs)\n";		# ????
	my $self = shift;
	my $msgid = shift;

	#print "(/sendmsgs)\n";			# ????
	return unless defined($msgid);
	$self->send( encode('cp932', $self->BIGModel::Generic::msgs($msgid, @_)) );
	return;
}

#------------------------------------------------------
# ●定型メッセージを返す
# msg = msgs( id, str, ... );
#
sub msgs {
	#print "\n(msgs)\n";								# ????
	my $self = shift;
	my $msgid = shift;									# メッセージＩＤ

	return unless defined($msgid);
	my @arg = (@_, '');
	my $userlv = $self->from_param('USERLEVEL');		# ユーザレベル
		$userlv = defined( $userlv ) ? $userlv : 0;
	my $menumode = $self->from_param('MENUMODE');		# メニューモード
	  $menumode = defined( $menumode ) ? $menumode : 0;
	#printf "** msgid=(%s)\n", $msgid;										# ????
	#printf "** userlv=(%d), menumode=(%d)\n", $userlv, $menumode;			# ????

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	my $q = "SELECT body FROM `msgs` WHERE name='$msgid' AND user<=$userlv AND mode<=$menumode ".
			"ORDER BY user DESC, mode DESC LIMIT 1";
	#printf "**[q]------------\n%s\n-----------------\n", $q;				# ????
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	my $r;
	if ( $sth->rows ) {
		#printf "** rows=(%d)\n", $sth->rows;		# ????
		my ($body) = $sth->fetchrow_array;
		$body = decode('utf8', $body);				# 文字列をデコード
		#printf "*1* body=(%s)\n", $body;			# ????
		no warnings qw( redundant missing );		# 'Redundant argument' と 'Missing argument' エラーが出るので無効化
		$body = sprintf($body, @arg);				# 引数の文字列を埋め込む
		$body =~ s/\n/\r\n/g;
		#printf "*2* body=(%s)\n", $body;			# ????
		$r = $body;
	}
	$sth->finish();
	$dbh->disconnect();

	#print "(/msgs)\n";						# ????
	return $r;
}

#------------------------------------------------------
# ●アクセスログを記録する
# accesslog( userid, code, value, comment );
#
sub accesslog {
	#print "\n(accesslog)\n";				# ????
	my $self = shift;
	my $userid  = shift;
	my $code    = shift;
	my $value   = shift;
	my $comment = shift;

	my $from = $self->from;
	my $remoteaddr = $from->peerhost();
	my $remoteport = $from->peerport();
	#printf "** remorthost=(%s:%d), userid=(%s)\n", $remoteaddr, $remoteport, $userid;		# ????
	#printf "** code=(%d), value=[%s]\n", $code, $value;									# ????
	#printf "** comment=[ %s ]\n", encode('cp932', $comment);								# ????

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $sth = $dbh->prepare("INSERT INTO accesslog (remoteaddr, remoteport, userid, code, value, comment) VALUES (?, ?, ?, ?, ?, ?)");
	$sth->execute($remoteaddr, $remoteport, $userid, $code, $value, $comment);
	$sth->finish();
	$dbh->disconnect();
	#print "(/accesslog)\n";					# ????
	return;
}

#------------------------------------------------------
# ●現在時刻を返す
# r = now();
#
sub now {
	my $self = shift;
	my $dateTime = new Naoit0::DateTime();
	return $dateTime->now();
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;
