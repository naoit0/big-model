package BIGModel::Tasklist;
our $VERSION = "0001.20190213.1625";	# 改訂版
#
# BIG-Model ホストプログラム
# アクセス状況通知機能（ユーティリティ）
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
use BIGModel::Utility::Tasklist;		# アクセス状況通知機能(本体)

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n\n** [[ Tasklist ]]\n";
		my $self = shift;
		$self->BIGModel::Tasklist::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# ** アクセス権がありません **
			$self->BIGModel::Tasklist::finalize;
		}
		else {
			$self->BIGModel::Utility::tasklist();
			$self->BIGModel::Tasklist::finalize;
		}
	},

};

#==================================================
# ■初期処理
#
sub initalize {
	my $self = shift;
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;
	$self->BIGModel::Generic::modoru();			# 戻る
}

#--------------------------------------------------
# ■呼び出し
#
sub tasklist {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

#------------------------------------------------------
# ■ エントリポイントを返す
#
#sub start {
#	return \&BIGModel::Tasklist::tasklist;
#}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;