package BIGModel::Connect;
our $VERSION = "0001.20190211.2031";		# 改訂版
#
# BIG-Model ホストプログラム
# 接続開始処理
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
#use Data::Dumper;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ Connect ]]\n\n";
		my $self = shift;
		$self->BIGModel::Connect::initalize;
		$self->send("** Connect **\r\n");
		$self->sethandler( sub { $self->BIGModel::Connect::connect(1) } );
	},

	'1' =>	# 入力待ち（２秒間）
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $timeout = $self->from_param('TIMEOUT');

		if ( $timeout <= time ) {					# タイムアウトした
			$self->sethandler( sub { $self->BIGModel::Connect::connect(2) } );
		}
		else {										# タイムアウトしてない
			my $mode = $self->from_param('MENUMODE');
			unless ( defined($mode) ) {						# 数字キーでメニューモード選択すると変更ができない
				my $inkey = $arg->arg();						# 入力値を取得

				if ( defined($inkey) ) {						# 入力値があれば分岐へ
					# ● [Q]			【シスオペログイン】（オートログインテスト）
					if ( $inkey =~ /q/i ) {
						#print "** ( SYSOP )\n";				# ????
						$arg->store("SYSOP\x0d");
						$arg->store("sysop\x0d");
						$self->sethandler( sub { $self->BIGModel::Connect::connect(2) } );
					}
					# ● [W]			【サブユーザー１ログイン】（オートログインテスト）
					elsif ( $inkey =~ /w/i ) {
						#print "** ( BIG00001 )\n";				# ????
						$arg->store("BIG00001\x0d");
						$arg->store("pas00001\x0d");
						$self->sethandler( sub { $self->BIGModel::Connect::connect(2) } );
					}
					# ● [E]			【サブユーザー２ログイン】（オートログインテスト）
					elsif ( $inkey =~ /e/i ) {
						#print "** ( BIG00002 )\n";				# ????
						$arg->store("BIG00002\x0d");
						$arg->store("pas00002\x0d");
						$self->sethandler( sub { $self->BIGModel::Connect::connect(2) } );
					}
					# ● [R]			【サブユーザー２ログイン】（オートログインテスト）
					elsif ( $inkey =~ /r/i ) {
						#print "** ( GUEST )\n";				# ????
						$arg->store("guest\x0d");
						$arg->store( encode('cp932', "ゲストユーザ\x0d") );
						$self->sethandler( sub { $self->BIGModel::Connect::connect(2) } );
					}
					elsif ( $inkey eq '1' ) {						
					# ● [1]			【モード１（ＡＮＫ）】
						$self->from_param( 'MENUMODE' => 1 );
					}
					elsif ( $inkey eq '2' ) {						
					# ● [2]			【モード２（シフトＪＩＳ）】
						$self->from_param( 'MENUMODE' => 2 );
					}
					elsif ( $inkey eq '3' ) {						
					# ● [3]			【モード３（ＡＮＳＩ）】
						$self->from_param( 'MENUMODE' => 3 );
					}
					# ● [CR]			【ＣＲをカウント】（最大でおよそ３４回叩ける）
					elsif ( $inkey eq "\x0d" ) {					
						$self->from_param( 'CR_COUNT' => $self->from_param('CR_COUNT') + 1 );
					}
				}
			}
		}
	},

	'2' =>	# 終了処理
	sub {
		my $self = shift;
		$self->BIGModel::Connect::menumode;
		$self->BIGModel::Connect::finalize;
	},

};

#==================================================
# ■ＣＲを叩いた回数でメニューモードを設定する
# menumode()
#
sub menumode {
	my $self = shift;
	my $mode = $self->from_param('MENUMODE');				# メニューモード
	my $cnt = $self->from_param('CR_COUNT');				# ＣＲを叩いた回数

	unless ( defined($mode) ) {								# 数字キーで選択していなければＣＲを叩いた回数で選択
		   if ( $cnt == 1 ) {									# １回→モード１（ＡＮＫ）
			$self->from_param( 'MENUMODE' => 1 );
		}
		elsif ( $cnt == 2 ) {									# ２回→モード２（シフトＪＩＳ）
			$self->from_param( 'MENUMODE' => 2 );
		}
		else {													# それ以外→モード３（ＡＮＳＩ）
			$self->from_param( 'MENUMODE' => 3 );
		}
	}
	#printf "** cr=(%d), menumode=(%d)\n", $cnt, $self->from_param('MENUMODE');		# ???? メニューモードの状態
}

#--------------------------------------------------
# ■初期処理
# initalize()
#
sub initalize {
	my $self  = shift;
	my $arg = $self->from_param('_Arg');

	$arg->mode( 'echo'=>0, 'line'=>0, 'mask'=>0 );				# 入力制御の動作モードを変更
	$arg->param( 'line_size'=>80, 'maskchr'=>'*' );				# 入力制御のパラメータを設定

	$self->from_param( 'MENUMODE' => undef );					# メニューモード
	$self->from_param(  'TIMEOUT' => ( time + 2 ) );			# '** Connect **' を送出してから待機する時間
	$self->from_param( 'CR_COUNT' => 0 );						# CRを叩いた回数
}

#--------------------------------------------------
# ■終了処理
# finalize()
#
sub finalize {
	my $self  = shift;

	# パラメータを削除
	$self->from_param('-CR_COUNT');	
	$self->from_param('-TIMEOUT');
	$self->sethandler( sub { $self->BIGModel::Hello::hello } );
}

#--------------------------------------------------
# ■呼び出し
# connect()
#
sub connect {
	my $self = shift;
	my $id = shift;
		$id = defined($id) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

#--------------------------
# ●ローダテスト
##--------------------------------------------------
## ■ソケット切断処理
## disconnect()
##
#sub disconnect {
#	my $self = shift;
#	print "*** disconect (Connect)\n";
#	printf "** connect=(%d)\n", $self->config('Connect');
#	$self->config( 'Connect' => 2 );
#	printf "** connect=(%d)\n", $self->config('Connect');
#}
#
##--------------------------------------------------
## ■ローダ
## load()
##
#sub load {
#	my $self = shift;
#	print "**** loaded 2 ****\n";
#	$self->setsyshandler('ondisconnect', sub { BIGModel::Connect::disconnect($self) }, 'Connect');			# disconnect()をイベントにかませる
#	print "[onDisconnect]\n".Dumper($self->{'_onDisconnect'})."[/onDisconnect]\n";							# イベントハンドラにかませたルーチン一覧
#	$self->config( 'Connect' => 1 );
#	printf "** connect=(%d)\n", $self->config('Connect');
#}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;