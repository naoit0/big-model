package BIGModel::Generic::Alias;
our $VERSION = "0001.20190214.0104";	# 初版
#
# BIG-Model ホストプログラム
# エイリアスシステム
# 
# エイリアスシステムはメニューユーティリティに連動して機能します。
# ＢＢＳのロケーションにエイリアスを設定し、プロンプトが表示する場所で
# エイリアスコマンドを呼び出すことで直接移動できます。
# 
# 【設定方法】
# 
# 移動先のロケーションに移動し、表示するプロンプトで、エイリアスコマンド（＄）を
# 入力すると設定ができます。
# すでに設定済みのロケーションは、エイリアス名の変更または解除が選択できます。
# 
# 【呼出方法】
# 
# プロンプトが表示する場所で、＄と登録したエイリアス名を入力すると
# 登録したロケーションに移動します。
# 登録していないエイリアス名を入力した場合は、移動されることなくプロンプトのみが
# 表示されます。
# 
# 【注意事項】
# 
# 登録・更新の際、すでに登録済みのエイリアス名を使用することはできません。
# 
# 登録できるロケーションはメニューユーティリティのみのため、いまのところ
# 各ユーティリティには対応していません（各ユーティリティは順次対応予定）。
# 
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use DBI;
use Data::Dumper;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ Alias ]]\n\n";					# ????
		my $self = shift;
		$self->BIGModel::Generic::Alias::initalize;
		my $path = $self->from_param('_Path');
		my $name = $self->BIGModel::Generic::Alias::query( $path->path );					# エイリアス名問い合わせ

		if ( defined($name) ) {																# 変更・削除処理
			$self->from_param( '_ALIAS' => encode('cp932', decode('utf8', $name)) );
			$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias(1) } );
		}
		else {																				# 追加処理
			$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias(10) } );
		}
	},

#---------------------------------
# ■確認
#---------------------------------
	'1' =>	# イントロ
	sub {
		my $self = shift;
		$self->send("\r\n");
		$self->send( encode('cp932', "この場所はエイリアスが設定されています。\r\n") );
		$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias(2) } );
	},

	'2' =>	# プロンプト
	sub {
		my $self = shift;
		$self->send( encode('cp932', "選択してください ( [1] 変更 [2] 解除 ) : ") );
		$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias(3) } );
	},

	'3' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ● [ETX][ESC]				【キャンセル】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::Generic::Alias::finalize;
		}
		# ● （改行）
		elsif ( $arg->status('line_cr') == 1 ) {			
			$self->send("\r\n");
			my $input = $arg->status('line');
			# ● (ヌル)				【キャンセル】
			if ( $input eq '' ) {
				$self->BIGModel::Generic::Alias::finalize;
			}
			# ● [1]				【変更】
			elsif ( $input eq '1' ) {
				$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias(20) } );
			}
			# ● [2]				【解除】
			elsif ( $input eq '2' ) {
				$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias(30) } );
			}
			# ● （その他）			【再入力】
			else {
				$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias(2) } );
			}
		}
	},

#---------------------------------
# ■変更
#---------------------------------
	'20' =>	# プロンプト
	sub {
		my $self = shift;
		my $name = $self->from_param('_ALIAS');		# 問い合わせたエイリアス名
		my $arg = $self->from_param('_Arg');
		$self->send( encode('cp932', "変更するエイリアス名を入力してください : ") );
		$arg->store($name);							# 設定値を自動入力
		$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias(21) } );
	},

	'21' =>	# 入力待ち
	sub {
		my $self = shift;
		my $name = $self->from_param('_ALIAS');
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ● [ETX][ESC]				【キャンセル】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::Generic::Alias::finalize;
		}
		elsif ( $arg->status('line_cr') == 1 ) {			# （改行）
			$self->send("\r\n");
			my $input = $arg->status('line');
			# ● (ヌル)				【再入力】
			if ( $input eq '' ) {
				$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias(20) } );
			}

			# ● (その他)			【変更】
			else {
				if ( $name ne $input ) {
					$input = decode('cp932', $input);												# 入力値をデコード
					# ● [?]		【エラー】
					if ($input eq '?' ) {
						$self->send( encode('cp932', "** '?'はエイリアス名に登録できません **\r\n") );
						$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias(20) } );
					}
					# ● （その他）
					else {
						my $path = $self->from_param('_Path');											# パス
						my $r = $self->BIGModel::Generic::Alias::update( $path->path, $input );			# エイリアス更新処理
						if ( defined($r) ) {															# 更新完了
							$self->send( encode('cp932', "** エイリアス名を変更しました **\r\n") );
							$self->BIGModel::Generic::Alias::finalize;
						}
						else {																			# エラー
							$self->send( encode('cp932', "** すでに登録済みのエイリアス名です **\r\n") );
							$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias(20) } );
						}
					}
				}
				# ● （変更前と入力値が同名）
				else {
					$self->send( encode('cp932', "** エイリアス名の変更を中断しました **\r\n") );
					$self->BIGModel::Generic::Alias::finalize;
				}
			}
		}
	},

#---------------------------------
# ■解除
#---------------------------------
	'30' =>	# プロンプト
	sub {
		my $self = shift;
		$self->send( encode('cp932', "エイリアスを解除しますか ( Y または N ) : ") );
		$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias(31) } );
	},

	'31' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ● [ETX][ESC]				【キャンセル】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::Generic::Alias::finalize;
		}
		# ● （改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			my $name = $self->from_param('_ALIAS');
			$self->send("\r\n");
			my $input = $arg->status('line');
			# ● (ヌル)				【再入力】
			if ( $input eq '' ) {
				$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias(30) } );
			}
			# ● [Y]				【エイリアス変更】
			elsif ( $input =~ /^Y$/i ) {
				$self->BIGModel::Generic::Alias::remove( decode('cp932', $name) );					# エイリアス削除処理
				$self->send( encode('cp932', "** エイリアスを解除しました **\r\n") );
				$self->BIGModel::Generic::Alias::finalize;
			}
			# ● [N]				【キャンセル】
			elsif ( $input =~ /^N$/i ) {
				$self->send( encode('cp932', "** エイリアスの解除を中断しました **\r\n") );
				$self->BIGModel::Generic::Alias::finalize;
			}
			# ● （その他）			【再入力】
			else {
				$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias(30) } );
			}
		}
	},

#---------------------------------
# ■新規追加
#---------------------------------
	'10' =>	# イントロ
	sub {
		my $self = shift;
		my $msg =	"\r\n** この場所にエイリアスを設定します **\r\n";
		$self->send( encode('cp932', $msg) );
		$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias(11) } );
	},

	'11' =>	# プロンプト
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $retype = $self->from_param('_RETYPE'); $self->from_param('-_RETYPE');				# 再入力パラメータを回収し廃棄
		$self->send( encode('cp932', "エイリアス名を入力してください ( [?] ヘルプ ) : ") );
		$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );											# 入力制御の動作モードを変更
		$arg->param( 'line_size'=>64 );															# 入力制御のパラメータを設定
		$arg->store($retype) if ( defined($retype) );											# 文字列があれば再入力
		$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias(12) } );
	},

	'12' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ● [ETX][ESC]			【キャンセル】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::Generic::Alias::finalize;
		}
		# ● [?]				【キャンセル】
		elsif ( $inkey eq '?' ) {
			my $input = $arg->status('line');														# 入力値を得る
			$arg->flush();																			# 入力バッファクリア
			$self->send( "\x08\x20\x08" x (length($input)). "\r\n" );								# 後退文字を送出
			$input =~ s/\?$//;																		# '?'を削除
			$self->BIGModel::Generic::sendmsgs( 'HELP_ALIAS' );											# ヘルプを表示
			#printf "** line=[%s]\n", $input;														# ????
			$self->from_param( '_RETYPE' => $input );												# 再入力パラメータを保存
			$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias(11) } );
		}
		# ● （改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			# ● (ヌル)			【キャンセル】
			if ( $input eq '' ) {
				$self->send( encode('cp932', "** エイリアスの登録を中止しました **\r\n") );
				$self->BIGModel::Generic::Alias::finalize;
			}
			# ● (その他)
			else {
				$input = decode('cp932', $input);
				my $path = $self->from_param('_Path');
				my $r = $self->BIGModel::Generic::Alias::add( $input, $path->path );				# エイリアス登録処理
				# ● （新規登録）
				if ( defined($r) ) {																# 処理成功
					$self->send( encode('cp932', "** エイリアスを登録しました **\r\n") );
					$self->BIGModel::Generic::Alias::finalize;
				}
				# ● （他のエイリアス名として登録されている）
				else {																				# エラー
					$self->send( encode('cp932', "** すでに登録済みのエイリアス名です **\r\n") );
					$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias(11) } );
				}
			}
		}
	},

};

#======================================================
# ■ パスのエイリアス名を問い合わせる
# r = query( path );
#
# r = (エラー・登録なし: undef, 正常終了: エイリアス名)
#
sub query {
	#print "\n(query)\n";						# ????
	my $self = shift;
	my $path = shift;							# パス
	my $userid = $self->from_param('ID');		# ユーザＩＤ

	return unless ( defined($path) );			# 引数が未定義ならエラー
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $name;									# エイリアス名
	my $q = "SELECT name FROM `alias` WHERE userid='$userid' AND path='$path'";
	#printf "**[q]------------\n%s\n-----------------\n", $q;				# ????
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	if ( $sth->rows ) {
		( $name ) = $sth->fetchrow_array();
	}
	$sth->finish();
	$dbh->disconnect();
	#print "(/query)\n";						# ????
	return $name if defined($name);
	return;
}

#------------------------------------------------------
# ■ エイリアス名を登録
# r = add( name, path );
# 
# r = (エラー: undef, 正常終了: 1)
# 
sub add {
	#print "\n(add)\n";							# ????
	my $self = shift;
	my $name = shift;							# エイリアス名
	my $path = shift;							# パス
	my $userid = $self->from_param('ID');		# ユーザＩＤ

	return if ( (defined($name) == 0) || (defined($path) == 0) );		# 引数が未定義ならエラー

	my $r;
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	# 登録済みチェック
	my $q = "SELECT name FROM `alias` WHERE userid='$userid' AND name='$name'";
	#printf "**[q]------------\n%s\n-----------------\n", $q;			# ????
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	my $rows = $sth->rows;
	#printf "** rows=(%d)[%s](%d)\n", $rows, $rows, defined($rows);		# ????
	$sth->finish();

	# 登録がなければ登録
	unless ( $rows ) {
		my $q = "INSERT INTO `alias` (userid, name, path) VALUES ('$userid', '$name', '$path')";
		#printf "**[q]------------\n%s\n-----------------\n", $q;		# ????
		$dbh->do( $q );
		$r = 1;
	}
	$dbh->disconnect();
	#print "(/add)\n";					# ????
	return $r;
}

#------------------------------------------------------
# ■ エイリアス名を更新
# r = update( path, name );
# 
# r = (エラー: undef, 正常終了: 1)
# 
sub update {
	#print "\n(update)\n";					# ????
	my $self = shift;
	my $path = shift;							# パス
	my $name = shift;							# エイリアス名
	my $userid = $self->from_param('ID');		# ユーザＩＤ

	return if ( (defined($path) == 0) || (defined($name) == 0) );		# 引数が未定義ならエラー
	#printf "** path=[%s], name=[%s]\n", $path, $name;					# ????

	my $r;
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	# 登録済みチェック
	my $q = "SELECT name FROM `alias` WHERE userid='$userid' AND name='$name'";
	#printf "**[q]------------\n%s\n-----------------\n", $q;				# ????
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	my $rows = $sth->rows;
	#printf "** rows=(%d)[%s](%d)\n", $rows, $rows, defined($rows);		# ????
	$sth->finish();

	# 登録がなければ更新
	unless ( $rows ) {
		my $q = "UPDATE `alias` SET name='$name' WHERE userid='$userid' AND path='$path'";
		#printf "**[q]------------\n%s\n-----------------\n", $q;							# ????
		$dbh->do( $q );
		$r = 1;
	}
	$dbh->disconnect();
	#print "(/update)\n";					# ????
	return $r;
}

#------------------------------------------------------
# ■ エイリアス名を削除
# remove( name );
#
# r = (エラー: undef, 1: 正常終了)
# 
sub remove {
	#print "\n(remove)\n";					# ????
	my $self = shift;
	my $name = shift;							# エイリアス名
	my $userid = $self->from_param('ID');		# ユーザＩＤ

	return unless ( defined($name) );			# 引数が未定義ならエラー
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "DELETE FROM `alias` WHERE userid = '$userid' AND name = '$name'";
	#printf "**[q]------------\n%s\n-----------------\n", $q;							# ????
	$dbh->do( $q );
	$dbh->disconnect();
	#print "(/remove)\n";					# ????
	return 1;
}

#------------------------------------------------------
# ■ エイリアス名からパスを取得
# path = getpath_byalias( alias, userid )
#
# r = (エラー: undef, 正常終了: 1)
# 
sub getpath_byalias {
	#print "\n(getpath_byalias)\n";			# ????
	my $self = shift;
	my $alias = shift;
	my $userid = shift;

	return if ( (defined($alias) == 0) || (defined($userid) == 0) );		# 引数が未定義ならエラー
	my $dbh = DBI->connect( @{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "SELECT path FROM `alias` WHERE userid='$userid' AND name='$alias'";
	#printf "**[q]------------\n%s\n-----------------\n", $q;							# ????
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	#printf "** rows=(%d)\n", $sth->rows;												# ????
	my $path;
	if ( $sth->rows ) {
		($path) = $sth->fetchrow_array;
	}
	$sth->finish;
	$dbh->disconnect;

	#print "\n(/getpath_byalias)\n";			# ????
	return $path if ( defined($path) );
	return;
}

#------------------------------------------------------
# ■ 初期処理
#
sub initalize {
	my $self = shift;
}

#------------------------------------------------------
# ■ 終了処理
#
sub finalize {
	#print "\n(finalize)\n";					# ????
	my $self = shift;
	my $return = $self->from_param('_RETURN');		# 戻り先
	$self->from_param('-_RETURN');
	$self->from_param('-_ALIAS');
	$self->sethandler( sub { $self->$return } );
	#print "(/finalize)\n";					# ????
}

#------------------------------------------------------
# ■ 呼び出し
#
sub alias {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;
