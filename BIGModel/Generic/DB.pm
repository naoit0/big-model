package BIGModel::Generic::DB;
our $VERSION = "0001.20180624.1915";	# Users追加
#
# BIG-Model ホストプログラム
# ＤＢアクセス
#
use strict;
use warnings;
use utf8;
use BIGModel::Generic::DB::Articles;		# ＤＢアクセス(Articles)
use BIGModel::Generic::DB::Contents;		# ＤＢアクセス(Contents)
use BIGModel::Generic::DB::Map;				# ＤＢアクセス(Map)
use BIGModel::Generic::DB::Users;			# ＤＢアクセス(Users)

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;
