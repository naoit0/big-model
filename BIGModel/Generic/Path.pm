package BIGModel::Generic::Path;
our $VERSION = "0001.20180626.1233";	# 着手
our $VERSION = "0001.20190211.1859";	# 改訂版 着手
#
# BIG-Model ホストプログラム
# パス制御
#
# ※パスは自分の位置を明示化するためのもの。
# パスの構文チェックなどはユーティリティ側で個別に行う。
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;

# new( age )			コンストラクタ(age=保存する世代数)
# back()				パスリストをひとつ戻す
# up()					カレントパスのレベルを上げる
# go( path )			パスを移動する
# _update()				パスリストを更新する
# path( num )			パスを返す(num=0: カレントパス, 1: n世代前の移動履歴)
# paths( num )			パスリストを返す

#-------------------------------------------------
# ■コンストラクタ
# new( age )			# 保存する世代数
#
sub new {
	my $class = shift;
	my $age = shift;
		$age = defined( $age ) ? $age : 10;
	my $self = {};
		$self->{'age'} = $age;					# 保存する世代数
		$self->{'path'} = [];					# パスリスト
	bless($self, $class);
	return $self;
}

	# パスリスト
	# $self->{'path'}->[0] = path;	# カレントパス
	# $self->{'path'}->[1] = path;	# パス履歴（１つ前）
	# $self->{'path'}->[2] = path;	# パス履歴（２つ前）

#--------------------------------------------------
# ■パスリストを戻す
# path = back()
# 
sub back {
	#print "\n(back)\n";							# ????
	my $self = shift;
	return if ( $#{ $self->{'path'} } < 0 );		# 履歴リストが空なら戻る
	shift( @{ $self->{'path'} } );					# 履歴リストからカレントパスを取り除く
	#print "(/back)\n";								# ????
	return $self->{'path'}->[0];					# 現在のカレントパスを返す
}

#--------------------------------------------------
# ■カレントパスのレベルを上げる
# r = up()
# 
# r : 
#     undef : カレントパスが取得できない
#     path  : パス(レベル上昇後)
# 
sub up {
	#print "\n(up)\n";											# ????
	my $self = shift;
	return if ( $#{ $self->{'path'} } < 0 );					# 履歴リストが空なら戻る(undef)
	my $r;
	my $path = $self->{'path'}->[0];							# カレントパスを得る
	#printf "** curr_path=[%s]\n", $path;						# ????

	if ( ($path =~ s/\;/\;/g) <= 1 ) {							# カレントパスがレベル２未満
		$path = ';';
		#printf "*1* next_path=[%s]\n", $path;						# ????
		#print "(/up)\n";											# ????
		return $path;												# トップレベルを返す
	}

	else {
		my @next_path = split(';', $path);							# 
		pop(@next_path);											# カレントパスのレベルを下げる
		$path = join(';', @next_path);
		#printf "*2* next_path=[%s]\n", $path;						# ????
		#print "(/up)\n";											# ????
		return $path;												# パスを返す
	}
}

#--------------------------------------------------
# ■パスを移動する
# r = go( path )
# 
# r = 0:失敗, 1: 成功
# 
sub go {
	#print "\n(go)\n";													# ????
	my $self = shift;
	my $path = shift;
	#printf "** path=[%s]\n", $path;									# ????
	return if ( ( defined($path) == 0 ) || ( $path !~ /\;/ ) );			# 
	unshift( @{ $self->{'path'} }, $path );								# 履歴リストに追加
	$self->_update();
	#print "(/go)\n";													# ????
	return 1;
}

#--------------------------------------------------
# ■パスリストを更新する
# _update()
#
sub _update {
	#print "\n(_update)\n";											# ????
	my $self = shift;
	return if ( $#{ $self->{'path'} } < 0 );						# 履歴リストが空なら戻る
	my $path = shift;
	if ( $#{ $self->{'path'} } > $self->{'age'} ) {					# 世代数が6世代を超える
		splice( @{ $self->{'path'} }, -1, $self->{'age'});			# 古い世代を削除
	}
	#print "(/_update)\n";											# ????
	return;
}

#--------------------------------------------------
# ■パスを返す
# path = path( num )
#
#	num = (0: カレントパス, n: n世代前の移動履歴)
#
sub path {
	#print "\n(path)\n";							# ????
	my $self = shift;
	return if ( $#{ $self->{'path'} } < 0 );		# 履歴リストが空なら戻る
	my $num = shift;
		$num = '0' unless ( defined($num) );		# 引数が未定義→カレントパス
	#printf "** num=(%d)\n", $num;					# ????
	my $path = $self->{'path'}->[$num];
	#printf "** path=[%s]\n", $path;				# ????
	#print "(/path)\n";								# ????
	#return $path;
}

#--------------------------------------------------
# ■パスリストを返す
# ( path, ... ) = paths()
#
sub paths {
	#print "\n(paths)\n";							# ????
	my $self = shift;
	return if ( $#{ $self->{'path'} } < 0 );		# 履歴リストが空なら戻る
	#print "[paths]\n".Dumper( @{ $self->{'path'} } )."[/paths]\n";			# ????
	return @{ $self->{'path'} };
	#print "(/paths)\n";								# ????
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
#==============================================
#package BIGModel::Generic::Path::Test;
#use Data::Dumper;
#
#my $path = new BIGModel::Generic::Path(2);
#my $r;
#
#$r = $path->go('');
#printf "** r.defined=(%d)\n", defined($r);
#printf "** r.length=(%d)\n", length($r) if defined($r);
#printf "** r=[%s]\n", $r if defined($r);
#print "\n";
#
#$r = $path->go(';');
#printf "** r.defined=(%d)\n", defined($r);
#printf "** r.length=(%d)\n", length($r) if defined($r);
#printf "** r=[%s]\n", $r if defined($r);
#print "\n";
#
#$r = $path->go('A');
#printf "** r.defined=(%d)\n", defined($r);
#printf "** r.length=(%d)\n", length($r) if defined($r);
#printf "** r=[%s]\n", $r if defined($r);
#print "\n";
#
#exit;
#
#
#
#
#print "********\n";
#$r = $path->up();
#printf "** r.defined=(%d)\n", defined($r);
#printf "** r.length=(%d)\n", length($r) if defined($r);
#printf "** r=[%s]\n", $r if defined($r);
#print Dumper($path)."\n\n";
#
#$path->go(';');
#$path->go(';B');
#$path->go(';B;1');
#$path->go(';B;1;2');
#print Dumper($path)."\n";
#
#$r = $path->up();
#printf "** r.defined=(%d)\n", defined($r);
#printf "** r.length=(%d)\n", length($r) if defined($r);
#printf "** r=[%s]\n", $r if defined($r);
#print Dumper($path)."\n\n";
#
#$r = $path->up();
#printf "** r.defined=(%d)\n", defined($r);
#printf "** r.length=(%d)\n", length($r) if defined($r);
#printf "** r=[%s]\n", $r if defined($r);
#print Dumper($path)."\n\n";
#
#$r = $path->up();
#printf "** r.defined=(%d)\n", defined($r);
#printf "** r.length=(%d)\n", length($r) if defined($r);
#printf "** r=[%s]\n", $r if defined($r);
#print Dumper($path)."\n\n";
#
#$r = $path->up();
#printf "** r.defined=(%d)\n", defined($r);
#printf "** r.length=(%d)\n", length($r) if defined($r);
#printf "** r=[%s]\n", $r if defined($r);
#print Dumper($path)."\n\n";
#
#$r = $path->up();
#printf "** r.defined=(%d)\n", defined($r);
#printf "** r.length=(%d)\n", length($r) if defined($r);
#printf "** r=[%s]\n", $r if defined($r);
#print Dumper($path)."\n\n";
#
#
#print "#----------------\n";
#for my $i (0..9) {
#	$path->go("B;1;1;$i");
#	print Dumper($path)."\n";
#	printf "** curr=[%s]\n", $path->path(0);
#}
#
#print "#----------------\n";
#$path->back();
#printf "** curr=[%s]\n", $path->path(0);
#
#print "#----------------\n";
#$r = $path->up();
#printf "** r.defined=(%d)\n", defined($r);
#printf "** r.length=(%d)\n", length($r) if defined($r);
#printf "** r=[%s]\n", $r if defined($r);
#print Dumper($path)."\n\n";
#
#$r = $path->up();
#printf "** r.defined=(%d)\n", defined($r);
#printf "** r.length=(%d)\n", length($r) if defined($r);
#printf "** r=[%s]\n", $r if defined($r);
#print Dumper($path)."\n\n";
#
#$r = $path->up();
#printf "** r.defined=(%d)\n", defined($r);
#printf "** r.length=(%d)\n", length($r) if defined($r);
#printf "** r=[%s]\n", $r if defined($r);
#print Dumper($path)."\n\n";
#
#$r = $path->up();
#printf "** r.defined=(%d)\n", defined($r);
#printf "** r.length=(%d)\n", length($r) if defined($r);
#printf "** r=[%s]\n", $r if defined($r);
#print Dumper($path)."\n\n";
#
#$r = $path->up();
#printf "** r.defined=(%d)\n", defined($r);
#printf "** r.length=(%d)\n", length($r) if defined($r);
#printf "** r=[%s]\n", $r if defined($r);
#print Dumper($path)."\n\n";
#
1;