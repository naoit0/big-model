package BIGModel::Generic::Permit;
our $VERSION = "0001.20180329.0307";
our $VERSION = "0002.20180329.1733";		# シスオペ除外
our $VERSION = "0003.20180331.0253";		# disallow, member/nmemberまわりの不具合修正と機能改良
our $VERSION = "0003.20180703.0044";		# 微調整
our $VERSION = "0001.20190212.0313";		# 改訂版 着手
#
# BIG-Model ホストプログラム
# アクセス権限
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use DBI;
use Data::Dumper;
use Debug;

# r = allow( path, userid, userlv )			パスへのアクセス権限(DISALLOW)チェック
# r = member( path, userid, userlv )		ユーティリティの実行権限(MEMBER, NMEMBER)チェック
# r = sigop( path, userid, userlv )			シグオペ権限(SIGOP)のチェック


#--------------------------------------------------
# ●パスへのアクセス権限(ALLOW, DISALLOW)チェック
# r = allow( path, userid, userlv, usergrp )
#
# 	※ 引数はparam()から取得
#
# r = ( -1: エラー, 0: 拒否, 1: 許可 )
#
# 判定方法はトップレベルパスからカレントパスに向かって順番に判定条件をあててゆく。
# 条件に一致した場合はその時点で確定し判定終了となる。
#
# DISALLOWはパスへのアクセスが禁止され、対象ユーザからは不可視状態になる。
# 例えば ;B;1 をDISALLOW設定した場合、対象ユーザがそのパスにアクセスを試みても
# 無効とみなす（ユーザに対してエラーメッセージを表示しない）
#
#    ALLOW : 該当する場合→許可
# DISALLOW : 該当する場合→拒否
#
sub allow {
	printf "\n(allow [%s])\n", join(':', caller);						# ????
	my $self = shift;
	my $curr_path = shift;
		$curr_path = $self->from_param('_Path')->path() unless ( defined($curr_path) );		# カレントパス

	my $userid = $self->from_param('ID');					# ユーザＩＤ
	my $userlv = $self->from_param('USERLEVEL');			# ユーザレベル
		$userlv = 0 unless ( defined($userlv) );
	my $usergrp = $self->from_param('USERGROUP');			# ユーザグループ
		$usergrp = '' unless ( defined($usergrp) );			# ユーザグループが未定義ならヌル

	# ●引数が未定義（エラー）(-1)
	if ( (defined($curr_path) == 0) || (defined($userid) == 0) ) {
		print "** r=-1 (error)\n";			# ????
		print "(/allow)\n";					# ????
		return -1;
	}
	#printf "** current_path=[%s]\n", $curr_path;				# ???? カレントパス
	#printf "** userid=[%s], userlv=[%s]\n", $userid, $userlv;	# ???? ユーザＩＤ・ユーザレベル
	#printf "** usergrp=[%s]\n", $usergrp;						# ???? 所属グループ

	# ●ユーザレベルが３（シスオペ）なら許可(1)
	if ( $userlv == 3 ) {
		print "** r=1 (sysop)\n";			# ????
		print "(/allow)\n";					# ????
		return 1;
	}

	my @p = split(';', $curr_path);						# パス
	push(@p, '') if ($#p < 1);							# トップレベルのみならルートパスを追加
	#print "[p]\n".Dumper(@p)."[/p]\n";					# ????
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	# ●パスのレベルをトップレベルからカレントパスに向かって下げながら判定する
	my $r;
	my $path = '';
	while ( @p ) {
		my $p_ = shift(@p);
			$p_ = ( $path ne ';' ) ? ';'.$p_ : $p_;		# パスがトップレベルでなければセミロコンをつける
		$path = $path.$p_;								# パスを連結
		$path = ';' if ( $path eq '' );					# パスがヌルならセミコロンにする
		my $q = "SELECT permit, name, value FROM `permit` WHERE path='$path' AND ( permit='ALLOW' OR permit='DISALLOW' )";
		#printf "**[q]------------\n%s\n-----------------\n", $q;			# ????
		my $sth = $dbh->prepare( $q );
		$sth->execute();

		my $disallow = 0;				# 拒否判定フラグ
		my $allow = 0;					# 許可判定フラグ
		my $defined_disallow = 0;		# 拒否定義フラグ
		my $defined_allow = 0;			# 許可定義フラグ

		# ●設定あり
		if ( $sth->rows ) {
			while ( my ($permit, $name, $value) = $sth->fetchrow_array ) {
				$name = '' unless ( defined($name) );
				$value = '' unless ( defined($value) );
				printf "** path=[%s]\n", $path;				# ???? パス
				printf "** permit=[%s]\n", $permit;			# ???? 設定値の権限
				printf "** name=[%s]\n", $name;				# ???? 設定値名
				printf "** value=[%s]\n\n", $value;			# ???? 設定値

				# ●DISALLOW判定
				if ( $permit eq 'DISALLOW' ) {
					$defined_disallow = 1;								# DISALLOW定義あり
					if ( $name =~ /USER/i ) {							# ●ユーザID判定
						if ( compare( $userid, $value ) == 1 ) {
							print "** (restricted_user)\n";			# ????
							$disallow++;
						}
					}
					if ( $name =~ /GROUP/i ) {							# ●ユーザグループ判定
						if ( compare( $usergrp, $value ) == 1 ) {
							print "** (restricted_group)\n";			# ????
							$disallow++;
						}
					}
					if ( $name =~ /LEVEL/i ) {							# ●ユーザレベル判定
						if ( evaluate( $userlv, $value ) == 1 ) {
							print "** (restricted_level)\n";			# ????
							$disallow++;
						}
					}
				}

				# ●ALLOW判定
				elsif ( $permit eq 'ALLOW' ) {
					$defined_allow = 1;									# ALLOW定義あり
					if ( $name =~ /USER/i ) {							# ●ユーザID判定
						if ( compare( $userid, $value ) == 1 ) {
							print "** (allow_user)\n";					# ????
							$allow++;
						}
					}
					if ( $name =~ /GROUP/i ) {							# ●ユーザグループ判定
						if ( compare( $usergrp, $value ) == 1 ) {
							print "** (allow_group)\n";				# ????
							$allow++;
						}
					}
					if ( $name =~ /LEVEL/i ) {							# ●ユーザレベル判定
						if ( evaluate( $userlv, $value ) == 1 ) {
							print "** (allow_level)\n";				# ????
							$allow++;
						}
					}
				}
			}
		}

		# ●設定なし
		else {
			print "** (no_restrict)\n";								# ????
		}
		$sth->finish;

		# ●判定結果
		print "\n-----------------------\n";						# ????
		printf "** path=[%s]\n", $path;					# ???? パス
		printf "** disallow : defined=(%d), judge=(%d)\n", $defined_disallow, $disallow;		# ???? 拒否定義フラグ, 拒否判定フラグ
		printf "**    allow : defined=(%d), judge=(%d)\n",    $defined_allow,    $allow;		# ???? 許可定義フラグ, 許可判定フラグ
		print "\n-----------------------\n";						# ????

		#-------------------------------------------------------------------------------------
		# ●即確定パターン
		#-------------------------------------------------------------------------------------
		# (a)	r=1 (許可) : DISALLOW=-1, ALLOW=1  : （非メンバー定義なし、メンバー該当）
		# (b)	r=0 (拒否) : DISALLOW=1,  ALLOW=-1 : （非メンバー該当、メンバー定義なし）
		# (b)	r=0 (拒否) : DISALLOW=1,  ALLOW=0  : （非メンバー該当、メンバー非該当）
		# (b)	r=0 (拒否) : DISALLOW=1,  ALLOW=1  : （非メンバー該当、メンバー該当）
		# (c)	r=1 (許可) : DISALLOW=0,  ALLOW=1  : （非メンバー非該当、メンバー該当）
		#-------------------------------------------------------------------------------------

		if ( ($defined_disallow == 0) && ($allow >= 1) ) {			# 非メンバー定義なし、メンバー該当あり
			print "** (a)\n";											# ????
			$r = 1;														# (許可)
		}
		if ( $defined_disallow == 1 ) {								# 非メンバー定義あり
			if ( $disallow >= 1 ) {										# 非メンバー該当あり
				print "** (b)\n";											# ????
				$r = 0;														# (拒否)
			}
			if ( ($disallow == 0) && ($allow >= 1) ) {					# 非メンバーに該当なし、メンバーに該当あり
				print "** (c)\n";											# ????
				$r = 1;														# (許可)
			}
		}

		#-------------------------------------------------------------------------------------
		# ●最終判定パターン
		#-------------------------------------------------------------------------------------
		# (d)	r=1 (許可) : DISALLOW=-1, ALLOW=-1 : （非メンバー定義なし、メンバー定義なし）
		# (e)	r=0 (拒否) : DISALLOW=-1, ALLOW=0  : （非メンバー定義なし、メンバー非該当）
		# (f)	r=1 (許可) : DISALLOW=0,  ALLOW=-1 : （非メンバー非該当、メンバー定義なし）
		# (g)	r=1 (許可) : DISALLOW=0,  ALLOW=0  : （非メンバー非該当、メンバー非該当）
		#-------------------------------------------------------------------------------------

		#printf "** #p=(%d)\n", $#p;								# ????
		if ($#p == -1 ) {
			#print "** (last)\n";										# ????
			if ( $defined_disallow == 0 ) {								# 非メンバー定義なし
				if ( $defined_allow == 0 ) {								# メンバー定義なし
					print "** (d)\n";											# ????
					$r = 1;														# (許可)
				}
				if ( $defined_allow == 1 && $allow == 0 ) {				# メンバー該当なし
					print "** (e)\n";											# ????
					$r = 0;														# (拒否)
				}
			}
			if ( ($defined_disallow == 1) && ($disallow == 0) ) {			# 非メンバー該当なし
				if ( $defined_allow == 0 ) {								# メンバー定義なし
					print "** (f)\n";											# ????
					$r = 1;														# (許可)
				}
				if ( ($defined_allow == 1) && ($allow == 0) ) {			# 非メンバー該当なし、メンバー該当なし
					print "** (g)\n";											# ????
					$r = 1;														# (許可)
				}
			}
		}

		if ( defined($r) ) {						# 判定済みなら
			#printf "** path=[%s]\n", $path;				# ????
			#printf "** r=(%d)\n", $r;					# ????
			@p = ();									# パスをクリアして
			last;										# ループを抜ける
		}
	}
	$dbh->disconnect;

	#printf "** defined.r=(%d)\n", defined($r);			# ????
	$r = 1 unless ( defined($r) );						# 判定済みでなければ許可

	print "** r=0 (deny)\n"  if ($r == 0);			# ????
	print "** r=1 (allow)\n" if ($r == 1);			# ????
	print "(/allow)\n";							# ????
	return $r;
}

#--------------------------------------------------
# ●ユーティリティの実行権限(MEMBER, NMEMBER)チェック
# r = member( path, userid, userlv, usergrp )
#
# 	※ 引数はparam()から取得
#
# r = ( -1: エラー, 0: 拒否, 1: 許可 )
#
# パスに紐づけされているユーティリティの実行権限を判定。
# 判定方法はトップレベルパスからカレントパスに向かって順番に判定条件をあててゆく。
# 確定パターンに含まれる条件に一致した場合は、その時点で確定し判定終了となる。
# 最終判定パターンは、カレントパスまでに判定が持ち越された場合に判定を行い、最終判定を行う。
#
# 判定条件は次の通り(BIG-Model準拠)。
#
#	(-1: 権限定義なし, 0:非該当, 1:該当)
#
# @d		r=1 (許可) : NMEMBER=-1, MEMBER=-1 : （非メンバー定義なし、メンバー定義なし）	最終判定
# @e		r=0 (拒否) : NMEMBER=-1, MEMBER=0  : （非メンバー定義なし、メンバー非該当）		最終判定
# @a		r=1 (許可) : NMEMBER=-1, MEMBER=1  : （非メンバー定義なし、メンバー該当）		確定
# @f		r=1 (許可) : NMEMBER=0,  MEMBER=-1 : （非メンバー非該当、メンバー定義なし）		最終判定
# @b		r=0 (拒否) : NMEMBER=1,  MEMBER=-1 : （非メンバー該当、メンバー定義なし）		確定
# @g		r=1 (許可) : NMEMBER=0,  MEMBER=0  : （非メンバー非該当、メンバー非該当）		最終判定
# @c		r=1 (許可) : NMEMBER=0,  MEMBER=1  : （非メンバー非該当、メンバー該当）			確定
# @b		r=0 (拒否) : NMEMBER=1,  MEMBER=0  : （非メンバー該当、メンバー非該当）			確定
# @b		r=0 (拒否) : NMEMBER=1,  MEMBER=1  : （非メンバー該当、メンバー該当）			確定
#
# ●最終判定パターン
# 最終判定パターンはカレントパスに辿る過程で条件に一致してもカレントパスに辿るまで保留しておく。
#
# 	r=1 (許可) : NMEMBER=-1, MEMBER=-1 : （非メンバー定義なし、メンバー定義なし）
# 	r=0 (拒否) : NMEMBER=-1, MEMBER=0  : （非メンバー定義なし、メンバー非該当）
# 	r=1 (許可) : NMEMBER=0,  MEMBER=-1 : （非メンバー非該当、メンバー定義なし）
# 	r=1 (許可) : NMEMBER=0,  MEMBER=0  : （非メンバー非該当、メンバー非該当）

# ●即確定パターン
# 即確定パターンはカレントパスに辿る過程で、条件に一致した場合は即確定とする。
# 	r=1 (許可) : NMEMBER=-1, MEMBER=1  : （非メンバー定義なし、メンバー該当）
# 	r=1 (許可) : NMEMBER=0,  MEMBER=1  : （非メンバー非該当、メンバー該当）
# 	r=0 (拒否) : NMEMBER=1,  MEMBER=-1 : （非メンバー該当、メンバー定義なし）
# 	r=0 (拒否) : NMEMBER=1,  MEMBER=0  : （非メンバー該当、メンバー非該当）
# 	r=0 (拒否) : NMEMBER=1,  MEMBER=1  : （非メンバー該当、メンバー該当）
#
sub member {
	printf "\n(member [%s])\n", join(':', caller);			# ????
	my $self = shift;
	my $curr_path = shift;
		$curr_path = $self->from_param('_Path')->path() unless ( defined($curr_path) );		# カレントパス

	my $userid = $self->from_param('ID');					# ユーザＩＤ
	my $userlv = $self->from_param('USERLEVEL');			# ユーザレベル
		$userlv = 0 unless ( defined($userlv) );
	my $usergrp = $self->from_param('USERGROUP');			# ユーザグループ
		$usergrp = '' unless ( defined($usergrp) );			# ユーザグループが未定義ならヌル

	# ●引数が未定義（エラー）(-1)
	if ( (defined($curr_path) == 0) || (defined($userid) == 0) ) {
		print "** r=-1 (error)\n";			# ????
		print "(/member)\n";					# ????
		return -1;
	}
	#printf "** current_path=[%s]\n", $curr_path;				# ???? カレントパス
	#printf "** userid=[%s], userlv=[%s]\n", $userid, $userlv;	# ???? ユーザＩＤ・ユーザレベル
	#printf "** usergrp=[%s]\n", $usergrp;						# ???? 所属グループ

	# ●ユーザレベルが３（シスオペ）なら許可(1)
	if ( $userlv == 3 ) {
		print "** r=1 (sysop)\n";			# ????
		print "(/member)\n";					# ????
		return 1;
	}

	my @p = split(';', $curr_path);						# パス
	push(@p, '') if ($#p < 1);							# トップレベルのみならルートパスを追加
	#print "[p]\n".Dumper(@p)."[/p]\n";					# ????
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	# ●パスのレベルをトップレベルからカレントパスに向かって下げながら判定する
	my $r;
	my $path = '';
	while ( @p ) {
		my $p_ = shift(@p);
			$p_ = ( $path ne ';' ) ? ';'.$p_ : $p_;		# パスがトップレベルでなければセミロコンをつける
		$path = $path.$p_;								# パスを連結
		$path = ';' if ( $path eq '' );					# パスがヌルならセミコロンにする
		my $q = "SELECT permit, name, value FROM `permit` WHERE path='$path' AND ( permit='MEMBER' OR permit='NMEMBER' )";
		#printf "**[q]------------\n%s\n-----------------\n", $q;			# ????
		my $sth = $dbh->prepare( $q );
		$sth->execute();

		my $nmember = 0;				# 拒否判定フラグ
		my $member = 0;					# 許可判定フラグ
		my $defined_nmember = 0;		# 拒否定義フラグ
		my $defined_member = 0;			# 許可定義フラグ

		# ●設定あり
		if ( $sth->rows ) {
			while ( my ($permit, $name, $value) = $sth->fetchrow_array ) {
				$name = '' unless ( defined($name) );
				$value = '' unless ( defined($value) );
				printf "** path=[%s]\n", $path;				# ???? パス
				printf "** permit=[%s]\n", $permit;			# ???? 設定値の権限
				printf "** name=[%s]\n", $name;				# ???? 設定値名
				printf "** value=[%s]\n\n", $value;			# ???? 設定値

				# ●NMEMBER判定
				if ( $permit eq 'NMEMBER' ) {
					$defined_nmember = 1;								# NMEMBER定義あり
					if ( $name =~ /USER/i ) {							# ●ユーザID判定
						if ( compare( $userid, $value ) == 1 ) {
							print "** (restricted_user)\n";			# ????
							$nmember++;
						}
					}
					if ( $name =~ /GROUP/i ) {							# ●ユーザグループ判定
						if ( compare( $usergrp, $value ) == 1 ) {
							print "** (restricted_group)\n";			# ????
							$nmember++;
						}
					}
					if ( $name =~ /LEVEL/i ) {							# ●ユーザレベル判定
						if ( evaluate( $userlv, $value ) == 1 ) {
							print "** (restricted_level)\n";			# ????
							$nmember++;
						}
					}
				}

				# ●MEMBER判定
				elsif ( $permit eq 'MEMBER' ) {
					$defined_member = 1;									# MEMBER定義あり
					if ( $name =~ /USER/i ) {							# ●ユーザID判定
						if ( compare( $userid, $value ) == 1 ) {
							print "** (allow_user)\n";					# ????
							$member++;
						}
					}
					if ( $name =~ /GROUP/i ) {							# ●ユーザグループ判定
						if ( compare( $usergrp, $value ) == 1 ) {
							print "** (allow_group)\n";				# ????
							$member++;
						}
					}
					if ( $name =~ /LEVEL/i ) {							# ●ユーザレベル判定
						if ( evaluate( $userlv, $value ) == 1 ) {
							print "** (allow_level)\n";				# ????
							$member++;
						}
					}
				}
			}
		}

		# ●設定なし
		else {
			print "** (no_restrict)\n";								# ????
		}
		$sth->finish;

		# ●判定結果
		print "\n------------------------\n";						# ????
		printf "** path=[%s]\n", $path;					# ???? パス
		printf "** nmember : defined=(%d), judge=(%d)\n", $defined_nmember, $nmember;		# ???? 拒否定義フラグ, 拒否判定フラグ
		printf "**  member : defined=(%d), judge=(%d)\n",  $defined_member,  $member;		# ???? 許可定義フラグ, 許可判定フラグ
		print "\n------------------------\n";						# ????

		#-------------------------------------------------------------------------------------
		# ●即確定パターン
		#-------------------------------------------------------------------------------------
		# (a)	r=1 (許可) : NMEMBER=-1, MEMBER=1  : （非メンバー定義なし、メンバー該当）
		# (b)	r=0 (拒否) : NMEMBER=1,  MEMBER=-1 : （非メンバー該当、メンバー定義なし）
		# (b)	r=0 (拒否) : NMEMBER=1,  MEMBER=0  : （非メンバー該当、メンバー非該当）
		# (b)	r=0 (拒否) : NMEMBER=1,  MEMBER=1  : （非メンバー該当、メンバー該当）
		# (c)	r=1 (許可) : NMEMBER=0,  MEMBER=1  : （非メンバー非該当、メンバー該当）
		#-------------------------------------------------------------------------------------

		if ( ($defined_nmember == 0) && ($member >= 1) ) {			# 非メンバー定義なし、メンバー該当 (a)
			print "** (a)\n";											# ????
			$r = 1;														# (許可)
		}
		if ( $defined_nmember == 1 ) {													# 非メンバー定義あり
			if ( $nmember >= 1 ) {																		# 非メンバー該当 (b)
				print "** (b)\n";											# ????
				$r = 0;														# (拒否)
			}
			if ( ($nmember == 0) && ($member >= 1) ) {								# 非メンバー非該当、メンバー該当 (c)
				print "** (c)\n";										# ????
				$r = 1;													# (許可)
			}
		}

		#-------------------------------------------------------------------------------------
		# ●最終判定パターン
		#-------------------------------------------------------------------------------------
		# (d)	r=1 (許可) : NMEMBER=-1, MEMBER=-1 : （非メンバー定義なし、メンバー定義なし）
		# (e)	r=0 (拒否) : NMEMBER=-1, MEMBER=0  : （非メンバー定義なし、メンバー非該当）
		# (f)	r=1 (許可) : NMEMBER=0,  MEMBER=-1 : （非メンバー非該当、メンバー定義なし）
		# (g)	r=1 (許可) : NMEMBER=0,  MEMBER=0  : （非メンバー非該当、メンバー非該当）
		#-------------------------------------------------------------------------------------

		#printf "** #p=(%d)\n", $#p;								# ???? 残っているパス
		if ( $#p == -1 ) {
			#print "** (last)\n";										# ???? カレントパスに到達
			if ( $defined_nmember == 0 ) {							# 非メンバー定義なし
				if ( $defined_member == 0 ) {								# 非メンバー定義なし、メンバー定義なし (d)
					print "** (d)\n";											# ????
					$r = 1;														# (許可)
				}
				if ( $defined_member == 1 && $member == 0 ) {				# 非メンバー定義なし、メンバー非該当 (e)
					print "** (e)\n";											# ????
					$r = 0;														# (拒否)
				}
			}
			if ( ($defined_nmember == 1) && ($nmember == 0) ) {			# 非メンバー非該当
				if ( $defined_member == 0 ) {														# 非メンバー非該当、メンバー定義なし (f)
					print "** (f)\n";											# ????
					$r = 1;														# (許可)
				}
				if ( ($defined_member == 1) && ($member == 0) ) {			# 非メンバー非該当、メンバー非該当 (g)
					print "** (g)\n";											# ????
					$r = 1;														# (許可)
				}
			}
		}

		if ( defined($r) ) {						# 判定済みなら
			#printf "** path=[%s]\n", $path;				# ???? 確定したパス
			#printf "** r=(%d)\n", $r;					# ???? 確定結果
			@p = ();									# パスをクリアして
			last;										# ループを抜ける
		}
	}
	$dbh->disconnect;

	#printf "** defined.r=(%d)\n", defined($r);			# ????
	$r = 1 unless ( defined($r) );						# 判定済みでなければ許可

	print "** r=0 (deny)\n"   if ($r == 0);			# ????
	print "** r=1 (allow)\n"  if ($r == 1);			# ????
	print "(/member)\n";							# ????
	return $r;
}

#--------------------------------------------------
# シグオペ権限(SIGOP)のチェック
# r = sigop( path, userid, userlv, usergrp )
#
# 	※ 引数はparam()から取得
#
# r = ( -1: エラー, 0: 拒否, 1: 許可 )
#
# ※シグオペはユーザレベルの登録できない（ユーザレベルはシスオペ判定で使用）
# ※シグオペはＩＤまたはユーザグループで判定
#
sub sigop {
	print "\n(sigop)\n";								# ????
	my $self = shift;
	my $path = $self->from_param('_Path')->path();		# パス
	my $userid = $self->from_param('ID');				# ユーザＩＤ
	my $userlv = $self->from_param('USERLEVEL');		# ユーザレベル
		$userlv = '@GUEST' if ( $userlv eq '1' );		# ユーザレベルが１ならゲストユーザ
		$userlv = '@USER'  if ( $userlv eq '2' );		# ユーザレベルが２なら一般ユーザ
		$userlv = '@SYSOP' if ( $userlv eq '3' );		# ユーザレベルが３ならシスオペ
	my $usergrp = $self->from_param('GID');				# グループＩＤ
		$usergrp = '' if ( defined($usergrp) );			# ユーザグループが未定義ならヌル

	# ●引数が未定義（エラー）(-1)
	if ( (defined($path) == 0) || (defined($userid) == 0) || (defined($userlv) == 0) ) {
		print "** r=-1 (error)\n";							# ????
		print "(/sigop)\n";								# ????
		return -1;
	}
	printf "** current_path=[%s]\n", $path;						# ????
	printf "** userid=[%s], userlv=[%s]\n", $userid, $userlv;	# ????
	printf "** usergrp=[%s]\n", $usergrp;						# ????

	# ●ユーザレベルが３（シスオペ）なら許可(1)
	if ( $userlv eq '@SYSOP' ) {
		print "** r=1 (sysop)\n";							# ????
		print "(/sigop)\n";									# ????
		return 1;
	}

	my @p = split(';', $path);							# パス
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	# ●パスのレベルを上げながら判定する（ボトムアップ）
	my $sigop = 0;
	while ( @p ) {
		my $path = ';'.join(';', @p);
		$path = ';' if ($#p < 1);

		# ●ＩＤ判定
		my $q = "SELECT id FROM `permit` WHERE path='$path' AND type='SIGOP' AND value LIKE '%$userid%'";
		printf "**[q]------------\n%s\n-----------------\n", $q;			# ????
		my $sth = $dbh->prepare( $q );
		$sth->execute();

		if ( $sth->rows ) {												# ■ 設定あり
			printf "** r=1 (sigop_user) path=[%s]\n", $path;				# ????
			$sigop = 1;														# 許可
		}
		$sth->finish;

		# ●ユーザグループ判定
		if ($usergrp ne '' ) {
			my $q = "SELECT id FROM `permit` WHERE path='$path' AND type='SIGOP' AND groups LIKE '%$usergrp%'";
			printf "**[q]------------\n%s\n-----------------\n", $q;			# ????
			my $sth = $dbh->prepare( $q );
			$sth->execute();
			if ( $sth->rows ) {												# ■ 設定あり
				printf "** r=1 (sigop_group) path=[%s]\n", $path;				# ????
				$sigop = 1;														# 許可
			}
			$sth->finish;
		}

		if ($sigop == 0) {
			pop(@p);														# レベルを上げる
		}
		else {
			@p=();															# パスをクリア
			last;
		}
	}
	$dbh->disconnect;
	print "** r=0 (no_sigop)\n" if ($sigop == 0);			# ????
	print "(/sigop)\n";										# ????
	return $sigop;
}

#------------------------------------
# ●文字列評価
# compare( a, b )
#
# 比較文字が複数ある場合、いずれかの文字に該当したら１を返し、全てに該当しなければ０を返す（ＯＲ）。
#
sub compare {
	#print "\n(compare)\n";
	my $a = shift;				# ユーザ値
		$a = uc($a);				# 複数条件がある場合はカンマ（または空白または改行）で区切る。
		$a =~ s/^\x20+//;
		$a =~ s/\x0d+/,/g;			# CR区切り
		$a =~ s/\x20+/,/g;			# スペース区切り
		$a =~ s/\,+/,/g;
	my $b = shift;				# 設定値
		$b = uc($b);				# 複数条件がある場合はカンマ（または空白または改行）で区切る。
		$b =~ s/^\x20+//;
		$b =~ s/\x0d+/,/g;			# CR区切り
		$b =~ s/\x20+/,/g;			# スペース区切り
		$b =~ s/\,+/,/g;

	for my $a_ ( split(',', $a) ) {
		for my $b_ ( split(',', $b) ) {
			#printf "** a_=[%8s], b_=[%8s] ", $a_, $b_;		# ????
			if ($a_ eq $b_) {
				#print "**";								# ????
				#print "(/compare)\n";
				return 1;			# 該当した
			}
			#print "\n";			# ????
		}
	}
	#print "(/compare)\n";
	return 0;			# 全て該当しなかった
}

#--------------------------------------------------
# ●数値評価
#  r = evaluate( a, b )
#
# 条件が複数ある場合、全ての条件に該当したら１を返し、一つでも該当しなければ０を返す（ＡＮＤ）。
#
sub evaluate {
	#print "\n(evaluate)\n";
	my $a = shift;				# ユーザ値
	my $b = shift;				# 設定値( '<n': nより小さい, '>n': nより大きい, '=n'または'n': nに等しい )
								# 複数条件がある場合はカンマ（または空白または改行）で区切る。
		$b =~ s/^\x20+//;
		$b =~ s/\x0d+/,/g;		# CR区切り
		$b =~ s/\x20+/,/g;		# スペース区切り
		$b =~ s/\,+/,/g;

	for( split(',', $b) ) {
		my $r = 0;
		my ($b1, $b2);
			($b1, $b2) = $_ =~ /([^\d]*)(\d+)$/;
		#printf "** [%4s]:[%1s][%d] ", $_, $b1, $b2;		# ???? 設定値:演算子:数値

		if ($b1 eq '<') {			# ●より小さい
			if ($a < $b2) {
				#printf "**";			# ????
				$r++;
			}
		}
		elsif ($b1 eq '>') {		# ●より大きい
			if ($a > $b2) {
				#printf "**";			# ????
				$r++;
			}
		}
		else {						# ●等しい
			if ($a == $b2) {
				#printf "**";			# ????
				$r++;
			}
		}

		if ( $r == 0 ) {		# 該当しなかった
			#print "\n(/evaluate)\n";
			return 0;
		}
		#print "\n";
	}
	#print "(/evaluate)\n";
	return 1;							# すべて該当した
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;
