package BIGModel::Generic::DB::Articles;
our $VERSION = "0001.20180429.1927";	# 着手
#
# BIG-Model ホストプログラム
# ＤＢアクセス(Articles)
#
use strict;
use warnings;
use utf8;
use DBI;
use Encode qw( encode decode );
use Data::Dumper;
#
# ●指定した記事番号のレコードＩＤを返す
# BIGModel::Generic::DB::Articles::getArticleId( threadid, num );
#
# ●指定した記事番号のレコードを返す
# BIGModel::Generic::DB::Articles::getArticle( threadid, num );
#
# ●最も新しいレコードの記事番号を返す
# BIGModel::Generic::DB::Articles::getLatestArticleNum( threadid );
#
# ●最も新しいレコードを返す
# BIGModel::Generic::DB:::Articles::getLatestArticle( threadid );
#
# ●指定日付以降の直近のレコードを返す
# BIGModel::Generic::DB::Articles::getArticlebyDate( threadid, 'yyyy-mm-dd hh:mm:ss' );
#
# ●フラグを更新する
# BIGModel::Generic::DB::Articles::updFrag( threadid, num, flag );
#

#--------------------------------------------------
# ●指定した記事番号のレコードＩＤを返す
# BIGModel::DB::Articles::getArticleId( threadid, num )
# r = id
#
sub getArticleId {
	my $self = shift;
	my $threadId = shift;
	my $num = shift;

	return unless( defined($threadId) );							# スレッドＩＤが未定義なら終了
	return unless( defined($num) );									# 記事番号が未定義なら終了
	my $data = $self->BIGModel::DB::Articles::getArticle( $threadId, $num );
	printf "** id=(%d)\n", $data->[0] if ( defined($data) );		# ????
	return unless ( defined($data) );								# データが取得できなかった
	return $data->[0];												# データが取得できた
}

#--------------------------------------------------
# ●指定した記事番号のレコードを返す
# BIGModel::DB::Articles::getArticle( threadid, num )
# r = [ id, r_date, r_time, author, title, flag ]
#
sub getArticle {
	my $self = shift;
	my $threadId = shift;
	my $num = shift;

	return unless( defined($threadId) );		# スレッドＩＤが未定義なら終了
	return unless( defined($num) );				# 記事番号が未定義なら終了
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "SELECT id, ".
				"date_format(datetime, '%y-%m-%d') AS r_date, ".
				"date_format(datetime, '%H:%i:%s') AS r_time, ".
				"author, title, flag ".
			"FROM `articles` WHERE threadid = '$threadId' AND articlenum = '$num'";
	#printf "**[q]------------\n%s\n-----------------\n", $q;						# ????
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	my @row = $sth->fetchrow_array() if ( $sth->rows );
	$sth->finish;
	$dbh->disconnect;
	print "[row]\n".Dumper(@row)."[/row]\n" if ( $#row >= 0 );		# ????
	return if ( $#row < 0 );										# データが取得できなかった
	return [ @row ];												# データが取得できた
}

#--------------------------------------------------
# ●最も新しいレコードの記事番号を返す
# BIGModel::DB:::Articles::getLatestArticleNum( threadid )
# r = num
#
sub getLatestArticleNum {
	my $self = shift;
	my $threadId = shift;

	return unless( defined($threadId) );		# スレッドＩＤが未定義なら終了
	my $max;
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "SELECT max(articlenum) FROM `articles` WHERE threadid = '$threadId';";
	#printf "**[q]------------\n%s\n-----------------\n", $q;						# ????
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	if ( $sth->rows ) {							# 該当あり→そのレコードの記事番号を得る
		($max) = $sth->fetchrow_array;
	}
	else {										# 該当なし→未定義
		$max = undef;
	}
	$sth->finish;
	$dbh->disconnect;
	printf "** max=(%d)\n", if ( defined($max) );		# ????
	return unless ( defined($max) );					# データが取得できなかった
	return $max;										# データが取得できた
}

#--------------------------------------------------
# ●最も新しいレコードを返す
# BIGModel::DB:::Articles::getLatestArticle( threadid );
# r = [ id, num, r_date, r_time, author, title, flag ]
#
sub getLatestArticle {
	my $self = shift;
	my $threadId = shift;

	return unless ( defined($threadId) );		# 引数がなければ終了
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "SELECT ".
				"id, articlenum, ".
				"date_format(datetime, '%y-%m-%d') AS r_date, ".
				"date_format(datetime, '%H:%i:%s') AS r_time, ".
				"author, title, flag ".
			"FROM `articles` WHERE threadid = '$threadId' AND articlenum = (SELECT max(articlenum) FROM `articles` WHERE threadid = '$threadId' );";
	#printf "**[q]------------\n%s\n-----------------\n", $q;						# ????
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	my @row = $sth->fetchrow_array() if ( $sth->rows );
	$sth->finish;
	$dbh->disconnect;
	print "[row]\n".Dumper(@row)."[/row]\n" if ( $#row >= 0 );		# ????
	return if ( $#row < 0 );										# データが取得できなかった
	return [ @row ];												# データが取得できた
}

#--------------------------------------------------
# ●指定日付以降の直近のレコードを返す
# BIGModel::DB::Articles::getArticlebyDate( threadid, 'yyyy-mm-dd hh:mm:ss' )
# r = [ id, num, r_date, r_time, author, title, flag ]
#
sub getArticlebyDate {
	my $self = shift;
	my $threadId = shift;
	my $dateTime = shift;
	my $num;

	return unless ( defined($threadId) );		# 引数がなければ終了
	return unless ( defined($dateTime) );		# 引数がなければ終了
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "SELECT ".
				"id, articlenum, ".
				"date_format(datetime, '%y-%m-%d') AS r_date, ".
				"date_format(datetime, '%H:%i:%s') AS r_time, ".
				"author, title, flag ".
			"FROM `articles` WHERE threadid = '$threadId' AND datetime > '$dateTime' ORDER BY id ASC LIMIT 1";
	#printf "**[q]------------\n%s\n-----------------\n", $q;						# ????
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	my @row = $sth->fetchrow_array() if ( $sth->rows );
	$sth->finish;
	$dbh->disconnect;
	print "[row]\n".Dumper(@row)."[/row]\n" if ( $#row >= 0 );		# ????
	return if ( $#row < 0 );										# データが取得できなかった
	return [ @row ];												# データが取得できた
}

#--------------------------------------------------
# ●フラグを更新する
# BIGModel::DB::Articles::updFrag( threadid, num, flag );
#
sub updFlag {
	my $self = shift;
	my $threadId = shift;
	my $num = shift;
	my $flag = shift;
		$flag = ( $flag ne '' ) ? "'$flag'" : 'NULL';

	return unless ( defined($threadId) );		# 引数がなければ終了
	printf "** flag=[%s]\n", $flag;				# ????
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "UPDATE articles ".
			"SET flag = $flag ".
			"WHERE threadid = '$threadId' AND id = '$num'";
	#printf "**[q]------------\n%s\n-----------------\n", $q;						# ????
	$dbh->do( $q );
	$dbh->disconnect;
	return 1;
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;
