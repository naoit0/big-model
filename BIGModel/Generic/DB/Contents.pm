package BIGModel::Generic::DB::Contents;
our $VERSION = "0001.20180429.1927";	# 着手
#
# BIG-Model ホストプログラム
# ＤＢアクセス(Contents)
#
use strict;
use warnings;
use utf8;
use DBI;
use Encode qw( encode decode );
use Data::Dumper;
#
# ●コンテンツのレコードＩＤをページ番号順に得る
# BIGModel::Generic::DB::Contents::getPagesId( threadid, articleid );
#
# ●コンテンツ本体を得る
# BIGModel::Generic::DB::Contents::getContents( id );
#

#--------------------------------------------------
# ●コンテンツのレコードＩＤをページ番号順に得る
# BIGModel::DB::Contents::getPagesId( threadId, articleId )
# r : @pages
#
sub getPagesId {
	my $self = shift;
	my $threadId = shift;		# スレッドＩＤ
	my $articleId = shift;		# articlesのレコードＩＤ
	my @pages;					# レコードＩＤリスト(ページ番号順)

	return unless defined($articleId);		# 引数が未定義なら終了
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "SELECT page, id FROM `contents` WHERE threadid = '$threadId' AND articleid = '$articleId' ORDER BY page";
	#printf "**[q]------------\n%s\n-----------------\n", $q;						# ????
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	while ( my @row = $sth->fetchrow_array ) {
		$pages[ $row[0] - 1 ] = $row[1];
	}
	$sth->finish;
	$dbh->disconnect;
	return [ @pages ] if ( $#pages >= 0 );		# レコードがあればリストを返す
	return undef;								# レコードがなければ未定義を返す
}

#-----------------------------------------------
# ●コンテンツ本体を得る
# BIGModel::DB::Contents::getContents( id );
# r = body
#
sub getContents {
	my $self = shift;
	my $id = shift;				# contentsのレコードＩＤ
	my $body;					# コンテンツ本体

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	my $q = "SELECT contents FROM `contents` WHERE id='$id'";
	#printf "**[q]------------\n%s\n-----------------\n", $q;						# ????
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	#printf "** rows=(%d)\n", $sth->rows();			# ????
	($body) = $sth->fetchrow_array() if ( $sth->rows );
	$sth->finish;
	$dbh->disconnect;
	#printf "** body=[%s]\n", encode('cp932', $body);		# ????
	return $body if defined($body);							# 取得できた
	return;													# 取得できなかった
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;
