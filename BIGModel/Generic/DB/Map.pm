package BIGModel::Generic::DB::Map;
our $VERSION = "0001.20190218.2037";		# 初版（改訂版）
#
# BIG-Model ホストプログラム
# ＤＢアクセス(Map)
#
use strict;
use warnings;
use utf8;
use DBI;
use Encode qw( encode decode );
use Data::Dumper;
#
# ●シンボルからパスを取得
# BIGModel::Generic::DB::Map::getAccount( userid );
#
# ●パス名からユーティリティを取得
# BIGModel::Generic::DB::Map::getutil_bypath( path );

#--------------------------------------------------
# ●シンボルからパスを取得
# path = getpath_bysymbol( symbol )
# 
sub getpath_bysymbol {
	#print "\n(getpath_bysymbol)\n";			# ????
	my $self = shift;
	my $symbol = shift;

	return unless ( defined($symbol) );			# 引数が未定義ならエラー

	my $dbh = DBI->connect( @{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	my $q = "SELECT path FROM `map` WHERE symbol='$symbol'";
	#printf "**[q]------------\n%s\n-----------------\n", $q;							# ????
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	my $path;
	#printf "** rows=(%d)\n", $sth->rows;		# ????
	if ( $sth->rows ) {
		($path) = $sth->fetchrow_array;
	}
	$sth->finish;
	$dbh->disconnect;
 	if ( defined($path) ) {
		#printf "** path=[%s]\n", $path;		# ????
		#print "(/getpath_bysymbol)\n";			# ????
		return $path;
	}
	#print "(/getpath_bysymbol)\n";				# ????
	return;
}

#--------------------------------------------------
# ●パス名からユーティリティを取得
# ( pkg, start ) = getutil_bypath( path );
#
sub getutil_bypath {
	#printf "\n(getutil_bypath [%s])\n", join(':', caller);				# ????
	my $self = shift;
	my $path = shift;
	return unless ( defined($path) );				# 引数が未定義ならエラー
	#printf "** path=[%s]\n", $path;				# ????

	my $dbh = DBI->connect( @{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "SELECT utility, startpoint FROM `map` WHERE path='$path'";
	#printf "**[q]------------\n%s\n-----------------\n", $q;							# ????
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	my ($pkg, $start);
	#printf "** rows=(%d)\n", $sth->rows;		# ????
	if ( $sth->rows ) {
		($pkg, $start) = $sth->fetchrow_array;
	}
	$sth->finish;
	$dbh->disconnect;

 	if ( defined($pkg) && defined($start) ) {
		#printf "** pkg=[%s], start=[%s]\n", $pkg, $start;		# ????
		#print "(/getutil_bypath)\n";							# ????
		return [ $pkg, $start ];
	}
	#print "(/getutil_bypath)\n";				# ????
	return;
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
#==============================================
#my $self->{'db'} =
#	['dbi:mysql:dbname=bigmodel;host=127.0.0.1', 'bigmodel', '']; 		# ＤＢパラメータ
#
#my $symbol = ';B5-3';
#print "[getpath_bysymbol]\n".Dumper( BIGModel::Generic::DB::Map::getpath_bysymbol($self, $symbol) )."\n";
#
#my $path = ';C;R';
#print "[getutil_bypath]\n".Dumper( BIGModel::Generic::DB::Map::getutil_bypath($self, $path) )."\n";
#
#
1;