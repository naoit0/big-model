package BIGModel::Generic::DB::Users;
our $VERSION = "0001.20180429.1927";	# 着手
#
# BIG-Model ホストプログラム
# ＤＢアクセス(Users)
#
use strict;
use warnings;
use utf8;
use DBI;
use Encode qw( encode decode );
use Data::Dumper;
use Debug;
#
# ●指定ユーザのアカウントを得る
# BIGModel::Generic::DB::Users::getAccount( userid );

#--------------------------------------------------
# ●指定ユーザのアカウントを得る
# BIGModel::Generic::DB::Users::getAccount( userid, ( field_name1, field_name2, ... ) )
# r = [ field_data1, field_data2, ...  ]
#
sub getAccount {
	print "\n(getAccount)\n";						# ????
	my $self = shift;
	my $userid = shift;										# 取得するユーザＩＤ
	my $fields = $#_ >=0 ? join(', ', @_ ) : '*';			# 取得するデータのフィールド名(引数がなれけば全フィールドを取得)

	return unless defined($userid);							# 引数がなければ戻る
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "SELECT $fields FROM `users` WHERE userid = '$userid'";
	#printf "**[q]------------\n%s\n-----------------\n", $q;				# ????
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	my $r = $sth->rows();
	my @row = $sth->fetchrow_array() if ( $sth->rows );
	$sth->finish;
	$dbh->disconnect();
	print "[row]\n".Dumper(@row)."[/row]\n" if ( $#row >= 0 );		# ????
	print "(/getAccount)\n";							# ????
	return if ( $#row < 0 );										# 取得できなかった
	return [ @row ];												# 取得できた
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;
