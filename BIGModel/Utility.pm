package BIGModel::Utility;
our $VERSION = "0001.20190211.2107";		# 改訂版
#
# BIG-Model ホストプログラム
# その多機能
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
use BIGModel::Utility::LoginInfo;			# ログイン情報
use BIGModel::Utility::LogoutInfo;			# ログアウト情報
use BIGModel::Utility::Notice;				# お知らせ
use BIGModel::Utility::Tasklist;			# アクセス状況通知機能
use BIGModel::Utility::CmdShell;			# コマンドシェル
use BIGModel::Utility::Auxedit;				# エディタ
use BIGModel::Utility::IndexSelector;		# インデックスセレクタ（実験中）

#==================================================
sub logininfo {
	my $self = shift;
	return $self->BIGModel::Utility::LoginInfo::logininfo();
}

#--------------------------------------------------
sub logoutinfo {
	my $self = shift;
	return $self->BIGModel::Utility::LogoutInfo::logoutinfo();
}

#--------------------------------------------------
sub notice {
	my $self = shift;
	return $self->BIGModel::Utility::Notice::notice();
}

#--------------------------------------------------
sub tasklist {
	my $self = shift;
	return $self->BIGModel::Utility::Tasklist::tasklist();
}

#--------------------------------------------------
sub cmdshell {
	my $self = shift;
	return $self->BIGModel::Utility::CmdShell::cmdshell();
}

#--------------------------------------------------
sub auxedit {
	my $self = shift;
	return $self->BIGModel::Utility::Auxedit::auxedit();
}

#--------------------------------------------------
sub indexselector {
	my $self = shift;
	return $self->BIGModel::Utility::IndexSelector::indexselector();
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;