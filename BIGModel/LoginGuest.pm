package BIGModel::Login::Guest;
our $VERSION = "0001.20190211.2023";	# 改訂版
#
# BIG-Model ホストプログラム
# ゲストログイン
#

use strict;
no strict 'refs';
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
use DBI;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ Login::Guest ]]\n\n";
		my $self = shift;
		$self->sethandler( sub { $self->BIGModel::Login::Guest::login(1) } );
	},

	'1' =>	# ログイン直後に実行するユーティリティがあれば呼び出す（テスト）
	sub {
		my $self = shift;
	#	my $autoexec = $self->from_cookie('AUTOEXEC');		# AUTOEXEC = '{package}::{function}'
	#	#printf "** cookie:autoexec=[%s]\n", $autoexec if defined($autoexec);
    #
	#	if ( defined($autoexec) ) {
	#		my ($pkg, $func) = $autoexec =~ /(.*)::(.*)$/;		# パッケージとサブルーチンを分離
	#		$pkg = 'BIGModel::'.$pkg;
	#		$autoexec = $pkg.'::'.$func;
	#		my $ver = ${$pkg."::VERSION"};		# no strict 'refs' しないとエラーが起こる
    #
	#		if ( defined($ver) ) {		# ユーティリティがロードされていれば実行
	#			$self->sethandler( sub { $self->$autoexec } );
	#			#printf "** pkg=[%s] (%s), func=[%s]\n", $pkg, $ver, $func;
	#		}
	#		else {						# ユーティリティがロードされてなければ入口へ
	#			$self->sethandler( sub { $self->BIGModel::Login::Guest::login(2) } );
	#		}
	#	}
	#	else {
			$self->sethandler( sub { $self->BIGModel::Login::Guest::login(2) } );
	#	}
	},

	'2' =>	#
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );							# 入力制御の動作モードを変更
			$arg->param( 'line_size'=>60 );											# 入力制御のパラメータを設定
		$self->BIGModel::Generic::sendmsgs('LOGIN.3');									# 「プロフィールを入力してください」
		$self->sethandler( sub { $self->BIGModel::Login::Guest::login(3) } );
	},

	'3' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ● [ETX][ESC]				【キャンセル】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$arg->flush();
			$self->BIGModel::Login::Guest::finalize;
		}

		# ● （改行）				【保存】
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			$self->from_param( 'HANDLENAME' => decode('cp932', $input) ) if ( $input ne '' );		# 入力がなければ規定値を採用
			$self->BIGModel::Login::Guest::finalize;
		}
	},

};

#------------------------------------------------------
# ■ 終了処理
#
sub finalize {
	my $self = shift;
	my $userid = $self->from_param('ID');
	my $msg = sprintf( "ゲストログイン [%s]", $self->from_param('HANDLENAME') );
	$self->BIGModel::Generic::accesslog( $userid, '0', '', $msg );
	print encode('cp932', sprintf "%s ++ ゲストユーザ %s がログインしました ++\n", BIGModel::Generic::now(), $userid);
	$self->from_param( 'LOGIN_DATETIME' => BIGModel::Generic::now() );		# ログイン日時セット
	$self->sethandler( sub { $self->BIGModel::Entrance::entrance } );		# 入口にジャンプ
}

#------------------------------------------------------
# ■ 初期処理
#
sub initalize {
}

#------------------------------------------------------
# ■ 呼び出し
#
sub login {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;