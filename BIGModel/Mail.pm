package BIGModel::Mail;
our $VERSION = "----.20190121.2113";	# 再構築
#
# BIG-Model ホストプログラム
# 電子メール
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use DBI;
use BIGModel::Mail::Read;				# [R] メールボックス（受信）
use BIGModel::Mail::Write;				# [W] メールボックス（送信）
use Data::Dumper;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ Mail ]]\n\n";
		my $self = shift;
		$self->BIGModel::Mail::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# ** アクセス権がありません **
			$self->BIGModel::Mail::finalize();
		}
		else {
			$self->sethandler( sub { $self->BIGModel::Mail::mail(1) } );
		}
	},

	'1' =>	# 選択してください
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );					# 入力モードを設定
			$arg->param( 'line_size'=>46, 'maskchr'=>'*' );					# パラメータを設定
		$self->BIGModel::Generic::sendmsgs('MAIL');								# 「選択してください」
		$self->sethandler( sub { $self->BIGModel::Mail::mail(2) } );
	},

	'2' =>	# 分岐
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $a = $arg->arg(); $a = defined($a) ? $a : '';

		# ●[ETX][ESC]			【終了】
		if ( $a eq "\x03" || $a eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::Mail::finalize;
		}
		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(ヌル)			【戻る】
			if ( $input eq '' ) {
				$self->BIGModel::Mail::finalize;
			}
			# ●[R]				【受信】
			elsif ($input =~ /^R$/i) {
				$self->sethandler( sub { $self->BIGModel::Mail::Read::read } );
			}
			# ●[W]				【送信】
			elsif ($input =~ /^W$/i) {
				$self->sethandler( sub { $self->BIGModel::Mail::Write::write } );
			}
			# ●(その他)		【プロンプト再表示】
			else {
				$self->sethandler( sub { $self->BIGModel::Mail::mail(1) } );
			}
		}
	},

};

#==================================================
# ■新着メールをチェックする（プロンプトから呼び出す→通知ハンドラで呼び出す）
# incomingnotify()
#
sub incomingnotify {
	#print "\n(incomingnotify)\n";
	my $self = shift;
	my $incoming = $self->from_param('INCOMING_MAIL');

	if ( defined($incoming) ) {
		$self->send( encode('cp932', "** 手紙が届きました **\r\n" ) );
		$self->from_param( 'INCOMING_MAIL' => undef );
	}
	#print "(/incoming_notify)\n";
}

#--------------------------------------------------
# ■新着メールをチェックする（logininfoから呼び出す）
# checkmail( userid )
#
sub checkmail {
	print "\n(checkmail)\n";
	my $self = shift;
	my $userid  = $self->from_param('ID');

	return unless ( defined($userid) );

	my ( $new, $total ) = @{ $self->BIGModel::Mail::getmailcount() };
	#if ( $max > 0 ) {
	#	$self->send( encode('cp932', "** 手紙が届いております **\r\n" ) );
	#}
	if ( $new > 0 ) {
		$self->send( encode('cp932', sprintf "** 新しい手紙が届いております ( %d 通 ) **\r\n", $new ) );
	}
	print "(/checkmail)\n";
}

#--------------------------------------------------
# ■メールの件数チェック
# getmailcount( userid )
#
sub getmailcount {
	#print "\n(getmailcount)\n";							# ????
	my $self = shift;
	my $userid  = $self->from_param('ID');
	my $threadid = '@'.$userid;

	return unless ( defined($userid) );

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my ( $q, $sth );

	# ●新着メールの件数を得る
	$q = "SELECT count(*) FROM `articles` WHERE threadid='$threadid' AND flag IS NULL";
	#printf "**[q]------------\n%s\n-----------------\n", encode('cp932', $q);			# ????
	$sth = $dbh->prepare( $q );
	$sth->execute();
	my ($new) = $sth->fetchrow_array;
	$sth->finish;

	# ●メールの総数を得る
	$q = "SELECT count(*) FROM `articles` WHERE threadid='$threadid'";
	#printf "**[q]------------\n%s\n-----------------\n", encode('cp932', $q);			# ????
	$sth = $dbh->prepare( $q );
	$sth->execute();
	my ($total) = $sth->fetchrow_array;
	$sth->finish;
	$dbh->disconnect;

	#printf "** total=(%d), new=(%d)\n", $total, $new;						# ????
	#print "(/getmailcount)\n";								# ????
	return [ $new, $total ];
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;
	$self->from_param( 'LOCATION' => 'MAIL' );		# 現在地
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;

	$self->BIGModel::Generic::modoru();			# 戻る
}

#--------------------------------------------------
# ■ 呼び出し
#
sub mail {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;