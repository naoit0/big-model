package BIGModel::Hello;
our $VERSION = "0001.20190211.2037";		# 改訂版
#
# BIG-Model ホストプログラム
# タイトル
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
#use Data::Dumper;

my $subs = {

	'0' =>
	sub {
		print "\n** [[ Hello ]]\n\n";
		my $self = shift;
		$self->BIGModel::Hello::msg;
		$self->sethandler( sub { $self->BIGModel::Login::login });
	},

};

#==================================================
# ■ タイトル表示
# msg()
#
sub msg {
	my $self = shift;
	my $msg =<<'EOT';
===============================
 BIG-Model 3.0
 Sample System
===============================
 Guest ID = BIG00000
 SysOp ID = BIG00001 /PAS00001
 Users'ID = BIG00002 /PAS00002
===============================
EOT
	$msg =~ s/\n/\r\n/g;
	$self->send(encode('cp932', $msg));
}

#------------------------------------------------------
# ■ 呼び出し
# hello()
#
sub hello {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;