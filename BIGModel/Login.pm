# 【作業中】多重ログイン調整

package BIGModel::Login;
our $VERSION = "0001.20190211.1859";	# 改訂版
#
# BIG-Model ホストプログラム
# ログイン
#

use strict;
no strict 'refs';
use warnings;
use utf8;
use Encode qw( encode decode );
use DBI;
use Data::Dumper;
use Debug;
use BIGModel::LoginGuest;			# ゲストログイン

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ Login ]]\n\n";					# ????
		my $self = shift;
		$self->BIGModel::Login::initalize;
		$self->sethandler( sub { $self->BIGModel::Login::login(1) } );
	},

	'1' =>	# ＩＤを入力してください
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );					# 入力制御の動作モードを変更
			$arg->param( 'line_size'=>8 );									# 入力制御のパラメータを設定
		$self->BIGModel::Generic::sendmsgs('LOGIN.1');						# 「ＩＤを入力してください」
		$self->sethandler( sub { $self->BIGModel::Login::login(2) } );
	},

	'2' =>	# ＩＤ入力
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ● [ETX][ESC]					【キャンセル】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$arg->flush();
			$self->send("\r\n");
			$self->from_param( 'COUNT' => $self->from_param('COUNT') - 1 );				# 認証失敗とみなしてカウントする
			$self->sethandler( sub { $self->BIGModel::Login::login(1) } );				# ＩＤを再入力
		}
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			# ● (ヌル)					【プロンプトを再入力】
			if ( $input eq '' ) {
				$self->from_param( 'COUNT' => $self->from_param('COUNT') - 1 );			# 認証失敗とみなしてカウントする
				$self->sethandler( sub { $self->BIGModel::Login::login(1) } );			# ＩＤを再入力
			}
			# ● (その他)				【ユーザ情報を問い合わせる】
			else {
				$self->from_param( 'ID' => $input );									# 入力値をパラメータに保存
				$self->sethandler( sub { $self->BIGModel::Login::login(3) } );
			}
		}
	},

	'3' =>	# 分岐
	sub {
		my $self = shift;
		my $userid = $self->from_param('ID');
		#$self->send(sprintf "** userid=(%s)\r\n", $userid);							# ????
		my $userlv;
		my $userdata = $self->BIGModel::Generic::DB::Users::getAccount( $userid, 'userid', 'passwd', 'handlename', 'lastlogin', 'level', 'groups' );						# 入力したＩＤのユーザ情報を得る
		# [ userid, passwd, handlename, lastlogin, level, groups ]
		print "[userdata]\n".Dumper($userdata)."[/userdata]\n";			# ????

		# ユーザ情報が取得できなかったらログイン失敗だが、認証処理を行っているように装うため、
		# ダミーのユーザ情報を定義し、パスワードの入力を促す。
		unless ( defined($userdata) ) {
			$userlv = -1;									# ● -1
			$self->from_param( '_USERDATA' => [ undef, undef, undef, undef, -1, undef ] );
			#print "** (unknown_user) - ";												# ????
		}

		# ユーザ情報が取得できたらパラメータに保存
		else {
			$userlv = $userdata->[4];						# ● 0, 1, 2, 3
			$self->from_param( '_USERDATA' => $userdata );
			#print "** (known_user) - ";												# ????
		}
		#printf "userid=[%s], lv=(%d)\n", $userid, $userlv;								# ????

		# ● ゲスト → ゲストログインへ
		if ( $userlv == 1 ) {
			$self->from_param( 'LOGIN_DATETIME' => BIGModel::Generic::now() );			# ログイン日時セット
			$self->BIGModel::Login::prepare;
			$self->BIGModel::Login::setparam;
			$self->BIGModel::Login::finalize;
			$self->sethandler( sub { $self->BIGModel::Login::Guest::login } );
			#printf "** [ lv=(%d:guest) ]\n", $userlv;									# ????
		}
		# ● その他ユーザ → パスワードの入力を促す
		else {
			$self->sethandler( sub { $self->BIGModel::Login::login(4) } );
			#printf "** [ lv=(%d:unknown) ]\n", $userlv if ($userlv == -1);				# ????
			#printf "** [ lv=(%d:deny) ]\n", $userlv    if ($userlv == 0);				# ????
			#printf "** [ lv=(%d:normal) ]\n", $userlv  if ($userlv == 2);				# ????
			#printf "** [ lv=(%d:sysop) ]\n", $userlv   if ($userlv == 3);				# ????
		}
	},

	'4' =>	# パスワードを入力してください
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'mask'=>1 );											# 入力制御の動作モードを変更
			$arg->param( 'line_size'=>8, 'maskchr'=>'#');						# 入力制御のパラメータを設定
		$self->BIGModel::Generic::sendmsgs('LOGIN.2');							# 「パスワードを入力してください」
		$self->sethandler( sub { $self->BIGModel::Login::login(5) } );
	},

	'5' =>	# パスワード入力
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ● [ETX][ESC]					【キャンセル】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$arg->flush();
			$self->send("\r\n");
			$self->from_param( 'COUNT' => $self->from_param('COUNT') - 1 );				# 認証失敗とみなしてカウントする
			$self->sethandler( sub { $self->BIGModel::Login::login(1) } );				# ＩＤを再入力
		}
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			# ● (ヌル)					【プロンプトを再入力】
			if ( $input eq '' ) {
				$self->from_param( 'COUNT' => $self->from_param('COUNT') - 1 );			# 認証失敗とみなしてカウントする
				$self->sethandler( sub { $self->BIGModel::Login::login(1) } );
			}
			# ● (その他)				【ユーザ情報を問い合わせる】
			else {
				$self->from_param( 'PW' => $input );									# 入力値をパラメータに保存
				$self->sethandler( sub { $self->BIGModel::Login::login(6) } );
			}
		}
	},

	'6' =>	# 認証
	sub {
		my $self = shift;
		my $id = $self->from_param('ID');
		my $pw = $self->from_param('PW');
		my ($userid, $passwd, $handlename, $lastlogin, $userlv, $groups) = @{ $self->from_param('_USERDATA') };
		#printf "*** id=[%s], pw=[%s]\n", $id, $pw;					# ????

		if ($userlv <= 0) {
			$self->from_param( 'COUNT' => $self->from_param('COUNT') - 1 );
			# ● 失敗（ログイン拒否ユーザ）
			if ( $userlv == 0 ) {
				my $msg = sprintf("認証失敗(ログイン拒否ユーザ) [%s\;%s]", decode('cp932', $id), $pw);
				$self->BIGModel::Generic::accesslog( decode('cp932', $id), '0', '', $msg );
				print encode('cp932', BIGModel::Generic::now()." ++ ログインに失敗しました ++\n");
				#print "** (login failure: deny user)\n";			# ????
			}
			# ● 失敗（未登録ユーザ）
			else {
				my $msg = sprintf("認証失敗(登録のないユーザ) [%s\;%s]", decode('cp932', $id), $pw);
				$self->BIGModel::Generic::accesslog( '', '0', '', $msg );
				print encode('cp932', BIGModel::Generic::now()." ++ ログインに失敗しました ++\n");
				#print "** (login failure: unknown user)\n";		# ????
			}
			$self->sethandler( sub { $self->BIGModel::Login::login(1) } );
		}
		else {
			my $r = $self->BIGModel::Login::auth();				# パスワード照合
			#printf "** auth.r=(%d)\n", $r;						# ????
			# ● 失敗（多重ログイン）
			if ( $r == -1 ) {
				$self->BIGModel::Generic::accesslog( $id, '0', '', '多重ログインにより終了' );
				print encode('cp932', BIGModel::Generic::now()." ++ 二重にアクセスされました ++\n");
				$self->send("\r\nNO CARRIER\r\n");
				$self->BIGModel::Login::finalize;
				$self->disconnect( $self->from );
			}
			# ● 成功
			elsif ( $r == 1 ) {
				$self->BIGModel::Login::setparam;											# ユーザ情報を設定
				$self->BIGModel::Generic::accesslog( $id, '0', '', '認証成功' );
				print encode('cp932', sprintf("%s ++ %s がログインしました ++\n", BIGModel::Generic::now(), $id));
				$self->from_param( 'LOGIN_DATETIME' => BIGModel::Generic::now() );			# ログイン日時セット
				$self->BIGModel::Login::prepare;
				$self->BIGModel::Login::finalize;

				# （ログイン直後に実行するユーティリティがあれば呼び出す）
				## なければ入口にジャンプする
                #
				#my $autoexec = $self->from_cookie('AUTOEXEC');		# AUTOEXEC = '{package}::{function}'
				##printf "** cookie:autoexec=[%s]\n", $autoexec if defined($autoexec);						# ????
				#if ( defined($autoexec) ) {
				#	my ($pkg, $func) = $autoexec =~ /(.*)::(.*)$/;						# パッケージとサブルーチンを分離
				#	$pkg = 'BIGModel::'.$pkg;
				#	$autoexec = $pkg.'::'.$func;
				#	my $ver = ${$pkg."::VERSION"};										# no strict 'refs' しないとエラーが起こる
				#	#printf "** pkg=[%s] (%s), func=[%s]\n", $pkg, $ver, $func;								# ????
				#	if ( defined($ver) ) {												# ユーティリティがロードされていれば実行
				#		$self->sethandler( sub { $self->$autoexec } );
				#	}
				#	else {																# ユーティリティがロードされてなければ入口へ
				#		$self->sethandler( sub { $self->BIGModel::Entrance::entrance } );
				#	}
				#}
				#else {
					$self->sethandler( sub { $self->BIGModel::Entrance::entrance } );	# 入口
				#}
			}
			# ● 失敗（パスワードが一致しない）
			else {
				$self->from_param( 'COUNT' => $self->from_param('COUNT') - 1 );
				my $msg = sprintf("認証失敗(パスワード不一致) [%s\;%s]", decode('cp932', $id), $pw);
				$self->BIGModel::Generic::accesslog( decode('cp932', $id), '0', '', $msg );
				print encode('cp932', BIGModel::Generic::now()." ++ パスワードが不正です ++\n");
				$self->sethandler( sub { $self->BIGModel::Login::login(1) } );
			}
		}
	},

};

#======================================================
# ■パスワード照合
# r = auth()
#
# r = -1		# 失敗（多重ログインを試みた）
# r = 0			# 失敗（パスワードが一致しない）
# r = 1			# 成功
#
sub auth {
	my $self = shift;
	my $id = $self->from_param('ID');
	my $pw = $self->from_param('PW');
	my $passwd = $self->from_param('_USERDATA')->[1];
	#printf "** id=[%s]; pw=[%s] **\n", $id, $pw;				# ????
	#printf "** userdata.passwd=[%s]\n", $passwd;				# ????

	if ( $pw eq $passwd ) {
		## 多重ログインチェック
		#my $params = $comm->{'param'};
		##print "[params]".Dumper($params);
		#for my $i (4..$#{ $params }) {
		#	my $loggedUser = $params->[$i]->{'ID'};
		#	#printf "*** (%d:%s)\n", $i, $loggedUser;
		#	if ((defined($loggedUser)) && ($id eq $loggedUser) && ($i != $comm->fileno)) {
		#		#print "*(-1)* multiple login\n";
		#		return -1;																		# －１：失敗（多重ログイン）
		#	}
		#}
		#print "*(1)* password correct\n";
		return 1;																				# １：成功
	}
	else {
		#print "*(0)* password incorrect\n";
		return 0;																				# ０：失敗（パスワード不一致）
	}
}

#------------------------------------------------------
# ■ ユーザ情報をパラメータに定義する
# setparam()
#
sub setparam {
	#print "\n(setparam)\n";							# ????
	my $self = shift;
	my ($userid, $passwd, $handlename, $lastlogin, $userlv, $groups) = @{ $self->from_param('_USERDATA') };
	$self->from_param('-_USERDATA');

	$self->from_param( 'ID'         => $userid );							# ユーザーＩＤ（入力値ではなくＤＢに登録されたもの）
	$self->from_param( 'USERLEVEL'  => $userlv );							# ユーザーレベル
	$self->from_param( 'USERGROUP'  => $groups );							# ユーザーレベル
	$self->from_param( 'HANDLENAME' => decode('utf8', $handlename) );		# ハンドル名
	$self->from_param( 'LASTLOGIN'  => $lastlogin );						# 最終ログイン日時

	print "[params]\n";																	# ????
	map { printf "** '%s'=[%s]\n", $_, $self->from_param( $_ ); } $self->from_param();		# ???? パラメータの内容
	print "[/params]\n";																	# ????
	print "[cookies]\n";																	# ????
	map { printf "** '%s'=[%s]\n", $_, $self->from_cookie( $_ ); } $self->from_cookie();	# ???? クッキーの内容
	print "[/cookies]\n";																	# ????

	# ●クッキーからパラメータにコピー
	#print "[params]\n".Dumper( $self->from_param() )."[/params]\n";						# ????
	map { $self->from_param( $_ => $self->from_cookie( $_ ) ) } $self->from_cookie();
	#print "[params]\n".Dumper( $self->from_param() )."[/params]\n";						# ????
	#print "(/setparam)\n";							# ????

	# （※最後にパラメータからクッキーに保存する）
	# クッキーの名前リストを取得。パラメータからクッキーの名前リストに一致するものをクッキーへ保存。
}

#------------------------------------------------------
# ■ 準備処理
# prepare()
#
sub prepare {
	print "\n(prepare)\n";														# ????
	my $self = shift;

	# ●パス制御を準備
	my $path = new BIGModel::Generic::Path();		# オブジェクトを作成
	$path->go(';');									# ルートパスから開始
	$self->from_param( '_Path' => $path );			# オブジェクトをパラメータに保存

	# ●タスクリストに追加
	my $fileno = $self->from->fileno;
	my $tasks = $self->config('TASKS');
	#printf "** #tasks=(%d)\n", $#{$tasks};										# ????
	if ( $#{$tasks} < 0 ) {														# タスクリストが空
		$tasks->[1] = $fileno;
	}
	else {																		# タスクリストが空でない
		my $x = 0;
		for my $i (1..$#{$tasks}) {
			#printf "** i=(%d), defined=(%d)\n", $i, defined($tasks->[$i]);		# ????
			if ( defined($tasks->[$i]) == 0 ) {									# 空きタスクにファイル番号を追加する。
				$tasks->[$i] = $fileno;
				$x = 1;
				last;
			}
		}
		push( @{$tasks}, $fileno ) if ( $x == 0 );								# タスクに空きがなければ新たなタスクを追加する
	}
	#print "[tasks]\n".Dumper($tasks)."\n";										# ???? タスクリスト
	$self->config( 'TASKS' => $tasks );
	print "(/prepare)\n";														# ????
}

#------------------------------------------------------
# ■制限時間・認証回数チェック
# r = check()
#
# 制限時間を超えていないか、認証回数をこえていないかチェック
# 
sub check {
	my $self = shift;
	if ( defined($self->from_param('CHECK')) ) {
		if ( $self->from_param('TIMEOUT') < time ) {
			$self->BIGModel::Generic::accesslog( '', '0', '', '認証待ち時間制限により終了' );
			print encode('cp932', BIGModel::Generic::now()." ++ 制限時間です ++\n");
			$self->send("\r\n\r\nNO CARRIER\r\n");
			$self->disconnect( $self->from );
			return 0;
		}
		elsif ( $self->from_param('COUNT') <= 0 ) {
			$self->BIGModel::Generic::accesslog( '', '0', '', '認証回数制限により終了' );
			print encode('cp932', BIGModel::Generic::now()." ++ パスワードが不正です ++\n");
			$self->send("\r\nNO CARRIER\r\n");
			$self->disconnect( $self->from );
			return 0;
		}
	}
	return 1;
}

#------------------------------------------------------
# ■初期処理
# initalize()
# 
sub initalize {
	my $self = shift;
	$self->from_param( 'ID' => '' );						# ＩＤ
	$self->from_param( 'PW' => '' );						# パスワード
	$self->from_param( 'COUNT' => 3 );						# 再認証回数
	$self->from_param( 'TIMEOUT' => (time + 180) );			# 認証処理の制限時間
	$self->from_param( 'CHECK' => 1 );						# 制限時間・認証回数チェックを有効にする

	# ユーザ権限情報( BIGModel::Login::query() で取得する )
	#$self->from_param( '_USERDATA' => [ userid, passwd, handlename, lastlogin, userlv ] );		# ????
}

#------------------------------------------------------
# ■終了処理
# finalize()
# 
sub finalize {
	my $self = shift;

	# パラメータの削除
	$self->from_param('-PW');
	$self->from_param('-COUNT');
	$self->from_param('-TIMEOUT');
	$self->from_param('-_USERDATA');
	$self->from_param('-CHECK');

	#$self->BIGModel::Telegram::flush();				# ＤＢに残っている電報を廃棄する（これはTelegramに定義する）
}

#------------------------------------------------------
# ■呼び出し
# login( id )
# 
sub login {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
	$self->BIGModel::Login::check();		# 制限時間と認証回数をチェック
}

#------------------------------------------------------
# ■ソケット切断処理（ハンドラ）
# disconnect()
# 
sub disconnect {
	my $self = shift;
	my $fileno = $self->from->fileno;

	# タスクリストから削除
	my $tasks = $self->config('TASKS');
	for my $i (0..$#{$tasks}) {
		next unless defined($tasks->[$i] );							# 空きタスクなら次のタスクへ
		$tasks->[$i] = undef if ($tasks->[$i] == $fileno );			# 対象タスクを削除
	}
	$self->config( 'TASKS' => $tasks );
}

#------------------------------------------------------
# ■ローダ
# load()
# 
sub load {
	my $self = shift;
	$self->setsyshandler('ondisconnect', sub { BIGModel::Login::disconnect($self) }, 'Login');		# ソケット切断処理をハンドラに定義
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;