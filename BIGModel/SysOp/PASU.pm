package BIGModel::SysOp::PASU;
our $VERSION = "0001.20190313.0034";	# 改良版 完成
#
# BIG-Model ホストプログラム
# 会員抹消変更（シスオペコマンド）
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
use BIGModel::Generic::DB;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ SysOp::PASU ]]\n\n";
		my $self = shift;
		$self->BIGModel::SysOp::PASU::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# 「アクセス権がありません」
			$self->BIGModel::SysOp::PASU::finalize;
		}
		else {
			$self->sethandler( sub { $self->BIGModel::SysOp::PASU::pasu(1) } );
		}
	},

	'1' =>	# 会員の削除を行います
	sub {
		my $self = shift;
		$self->BIGModel::Generic::sendmsgs('PASU.1');		# 「会員の削除を行います」
		$self->sethandler( sub { $self->BIGModel::SysOp::PASU::pasu(2) } );
	},

	'2' =>	# 削除すべきＩＤを入力してください
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );				# 入力モードを設定
			$arg->param( 'line_size'=>60, 'maskchr'=>'*' );				# パラメータを設定
		$self->BIGModel::Generic::sendmsgs('PASU.2');					# 「削除すべきＩＤを入力してください」
		$self->sethandler( sub { $self->BIGModel::SysOp::PASU::pasu(3) } );
	},

	'3' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]				終了
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();														# 入力バッファをクリア
			$self->BIGModel::SysOp::PASU::finalize;
		}

		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(ヌル)				終了
			if ( $input eq '' ) {
				$self->BIGModel::SysOp::PASU::finalize;
			}

			# ●(それ以外)			ＩＤ照会
			else {
				my $data = $self->BIGModel::Generic::DB::Users::getAccount( $input, 'userid' );
				unless ( defined($data) ) {												# 登録なし
					$self->BIGModel::Generic::sendmsgs('PASU.3');							# 「すでに存在しません」
					$self->sethandler( sub { $self->BIGModel::SysOp::PASU::pasu(2) } );
				}
				else {																	# 登録あり
					$self->from_param( '_PASU_USERID' => $data->[0] );
					$self->send( encode('cp932', '"'.$data->[0].'"'."\r\n") );
					$self->sethandler( sub { $self->BIGModel::SysOp::PASU::pasu(4) } );
				}
			}
		}
	},

	'4' =>	# 削除してよいですか 
	sub {
		my $self = shift;
		$self->BIGModel::Generic::sendmsgs('PASU.4');							# 「削除してよいですか」
		$self->sethandler( sub { $self->BIGModel::SysOp::PASU::pasu(5) } );
	},

	'5' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			終了
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();														# 入力バッファをクリア
			$self->BIGModel::SysOp::PASU::finalize;
		}

		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●[Y]				削除
			if ( $input =~ /Y/i ) {
				my $id     = $self->from_param('ID');
				my $userid = $self->from_param('_PASU_USERID');
				$self->BIGModel::SysOp::PASU::deluser( $userid );
				$self->BIGModel::Generic::sendmsgs('PASU.5');					# 「削除しました」
				$self->BIGModel::Generic::accesslog( $id, '0', '', sprintf( "ユーザ削除 [%s]", $userid ) );
				$self->sethandler( sub { $self->BIGModel::SysOp::PASU::pasu(2) } );
			}

			# ●[N]				戻る
			elsif ( $input =~ /N/i ) {
				$self->sethandler( sub { $self->BIGModel::SysOp::PASU::pasu(2) } );
			}

			# ●(それ以外)		プロンプトを再表示
			else {
				$self->sethandler( sub { $self->BIGModel::SysOp::PASU::pasu(4) } );
			}
		}
	},

};

#==================================================
# ■削除
#
sub deluser {
	my $self = shift;
	my $userid = shift;

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "DELETE FROM `users` WHERE userid = '$userid'";
	#printf "**[q]------------\n%s\n-----------------\n", $q;		# ????
	my $r = $dbh->do($q);
	$dbh->disconnect();
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;
	$self->from_param( 'LOCATION' => 'SYSOP' );
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;
	$self->from_param('-_PASU_USERID');			# パラメータの廃棄
	$self->BIGModel::Generic::modoru();			# 戻る
}

#--------------------------------------------------
# ■呼び出し
sub pasu {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;