package BIGModel::SysOp::PASC;
our $VERSION = "0001.20190313.0034";	# 改良版 完成
#
# BIG-Model ホストプログラム
# パスワード変更（シスオペコマンド）(4.0)
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
use BIGModel::Generic::DB;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ SysOp::PASC ]]\n\n";
		my $self = shift;
		$self->BIGModel::SysOp::PASC::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# 「アクセス権がありません」
			$self->BIGModel::SysOp::PASC::finalize;
		}
		else {
			$self->sethandler( sub { $self->BIGModel::SysOp::PASC::pasc(1) } );
		}
	},

#----------------------------
	'1' =>	# パスワードを変更するＩＤを入力してください
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );				# 入力モードを設定
			$arg->param( 'line_size'=>60, 'maskchr'=>'*' );				# パラメータを設定
		$self->BIGModel::Generic::sendmsgs('PASC.1');					# 「パスワードを変更するＩＤを入力してください」
		$self->sethandler( sub { $self->BIGModel::SysOp::PASC::pasc(2) } );
	},

	'2' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]				終了
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();												# 入力バッファをクリア
			$self->BIGModel::SysOp::PASC::finalize;
		}

		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(ヌル)				終了
			if ( $input eq '' ) {
				$self->BIGModel::SysOp::PASC::finalize;
			}

			# ●(それ以外)			ＩＤ照会
			else {
				my $data = $self->BIGModel::Generic::DB::Users::getAccount( $input, 'userid' );
				unless ( defined($data) ) {													# 登録なし
					$self->BIGModel::Generic::sendmsgs('PASC.2');								# 「存在しません」
					$self->sethandler( sub { $self->BIGModel::SysOp::PASC::pasc(1) } );
				}
				else {																		# 登録あり
					$self->from_param( '_PASC_USERID' => $data->[0] );
					$self->send( encode('cp932', '"'.$data->[0].'"'."\r\n") );
					$self->sethandler( sub { $self->BIGModel::SysOp::PASC::pasc(3) } );
				}
			}
		}
	},

#----------------------------
	'3' =>	# パスワードを変更しますか
	sub {
		my $self = shift;
		$self->BIGModel::Generic::sendmsgs('PASC.3');							# 「パスワードを変更しますか」
		$self->sethandler( sub { $self->BIGModel::SysOp::PASC::pasc(4) } );
	},

	'4' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			終了
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();														# 入力バッファをクリア
			$self->BIGModel::SysOp::PASC::finalize;
		}

		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●[Y]				次へ
			if ( $input =~ /Y/i ) {
				$self->sethandler( sub { $self->BIGModel::SysOp::PASC::pasc(6) } );
			}

			# ●[N]				戻る
			elsif ( $input =~ /N/i ) {
				$self->sethandler( sub { $self->BIGModel::SysOp::PASC::pasc(2) } );
			}

			# ●(それ以外)		プロンプトを再表示
			else {
				$self->sethandler( sub { $self->BIGModel::SysOp::PASC::pasc(3) } );
			}
		}
	},

#----------------------------
	'6' =>	# 新しいパスワードを入力してください
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>1 );				# 入力モードを設定
			$arg->param( 'line_size'=>8, 'maskchr'=>'#' );				# パラメータを設定
		$self->BIGModel::Generic::sendmsgs('CHANGEPASSWORD.2');			# 「新しいパスワードを入力してください」
		$self->sethandler( sub { $self->BIGModel::SysOp::PASC::pasc(7) } );
	},

	'7' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			終了
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();												# 入力バッファをクリア
			$self->BIGModel::SysOp::REGS::finalize;
		}

		# ●(改行)
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(ヌル)				終了
			if ( $input eq '' ) {
				$self->BIGModel::SysOp::REGS::finalize;
			}

			# ●(その他)			次へ
			else {
				$self->from_param( '_PASC_PW' => $input );
				$self->sethandler( sub { $self->BIGModel::SysOp::PASC::pasc(8) } );
			}
		}
	},

#----------------------------
	'8' =>	# 確認のため同じパスワードを入力してください
	sub {
		my $self = shift;
		$self->BIGModel::Generic::sendmsgs('CHANGEPASSWORD.3');					# 「確認のため同じパスワードを入力してください」
		$self->sethandler( sub { $self->BIGModel::SysOp::PASC::pasc(9) } );
	},

	'9' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			終了
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();									# 入力バッファをクリア
			$self->BIGModel::SysOp::REGS::finalize;
		}

		# ●(改行)
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(ヌル)				終了
			if ( $input eq '') {
				$self->BIGModel::SysOp::REGS::finalize;
			}

			# ●(その他)			入力値を照合
			else {
				my $pw = $self->from_param('_PASC_PW');

				# ●入力値が不一致
				unless ( $pw eq $input ) {
					$self->BIGModel::Generic::sendmsgs('CHANGEPASSWORD.4');							# 「パスワードが違います」
					$self->sethandler( sub { $self->BIGModel::SysOp::PASC::pasc(6) } );				# パスワード入力(6)に戻る
				}

				# ●入力値が一致
				else {
					my $uid = $self->from_param('ID');
					my $userid = $self->from_param('_PASC_USERID');
					$self->BIGModel::SysOp::PASC::update( $userid, $input );							# パスワード更新
					$self->BIGModel::Generic::accesslog( $uid, '0', '', 'パスワードを変更' );		# 顛末をアクセスログに記録
					$self->BIGModel::Generic::sendmsgs('CHANGEPASSWORD.5');									# パスワードを変更しました
					$self->sethandler( sub { $self->BIGModel::SysOp::PASC::pasc(1) } );
				}
			}
		}
	},
};

#==================================================
# ■更新
# update( userid, passwd )
#
sub update {
	#print "\n(update)\n";							# ????
	my $self = shift;
	my $userid = shift;
	my $passwd = shift;
	#print encode( 'cp932', sprint("** userid=[%s], passwd=[%s]\n", $userid, $passwd) );		# ????

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "UPDATE `users` SET passwd='$passwd' WHERE userid = BINARY '$userid'";
	#printf "**[q]------------\n%s\n-----------------\n", $q;									# ????
	my $r = $dbh->do( $q );
	$dbh->disconnect();
	#print "(update)\n";							# ????
	return $r;
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;
	$self->from_param( 'LOCATION' => 'SYSOP' );
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;
	$self->from_param('-_PASC_USERID');			# パラメータの廃棄
	$self->BIGModel::Generic::modoru();			# 戻る
}

#--------------------------------------------------
# ■呼び出し
sub pasc {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;