package BIGModel::SysOp::REGS;
our $VERSION = "0001.20190313.0034";	# 改良版 完成
#
# BIG-Model ホストプログラム
# 会員登録（シスオペコマンド）

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
use BIGModel::Generic::DB;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ SysOp::REGS ]]\n\n";
		my $self = shift;
		$self->BIGModel::SysOp::REGS::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# 「アクセス権がありません」
			$self->BIGModel::SysOp::REGS::finalize;
		}
		else {
			$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(1) } );
		}
	},

	'1' =>	# 登録手続きを行います
	sub {
		my $self = shift;
		$self->BIGModel::Generic::sendmsgs('REGS.1');					# 「登録手続きを行います」
		$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(2) } );
	},

	'2' =>	# 登録すべきＩＤを入力してください
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );				# 入力モードを設定
			$arg->param( 'line_size'=>60, 'maskchr'=>'*' );				# パラメータを設定
		$self->BIGModel::Generic::sendmsgs('REGS.2');					# 「登録すべきＩＤを入力してください」
		$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(3) } );
	},

	'3' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			終了
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::SysOp::REGS::finalize;
		}

		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(ヌル)				終了
			if ( $input eq '' ) {
				$self->BIGModel::SysOp::REGS::finalize;
			}

			# ●(それ以外)			ＩＤを照会
			else {
				$input = uc($input);
				my $data = $self->BIGModel::Generic::DB::Users::getAccount( $input );			# ＩＤを照会
				if ( defined($data) ) {														# 登録あり
					$self->BIGModel::Generic::sendmsgs('REGS.3');								# 「すでに登録されています」
					$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(2) } );
				}
				else {																		# 登録なし
					$self->from_param( '_REGS_USERID' => $input );
					$self->send( sprintf("\"%s\"\r\n", encode('cp932', $input)) );
					$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(4) } );
				}
			}
		}
	},

	'4' =>	# 正しいですか
	sub {
		my $self = shift;
		$self->BIGModel::Generic::sendmsgs('YESNO_CORRECT');					# 「正しいですか」
		$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(5) } );
	},

	'5' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			終了
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();														# 入力バッファをクリア
			$self->BIGModel::SysOp::REGS::finalize;
		}

		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(ヌル)				終了
			if ( $input eq '' ) {
				$self->BIGModel::SysOp::REGS::finalize;
			}
			# ●[Y]					次へ
			elsif ( $input =~ /Y/i ) {
				$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(6) } );
			}
			# ●[N]					前に戻る
			elsif ( $input =~ /N/i ) {
				$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(2) } );
			}
			# ●(それ以外)			プロンプトを再表示
			else {
				$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(4) } );
			}
		}
	},

	'6' =>	# パスワードを入力してください
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>1 );				# 入力モードを設定
			$arg->param( 'line_size'=>8, 'maskchr'=>'#' );				# パラメータを設定
		$self->BIGModel::Generic::sendmsgs('REGS.4');					# 「パスワードを入力してください」
		$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(7) } );
	},

	'7' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			終了
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();														# 入力バッファをクリア
			$self->BIGModel::SysOp::REGS::finalize;
		}

		# ●(改行)
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(ヌル)				終了
			if ( $input eq '' ) {
				$self->BIGModel::SysOp::REGS::finalize;
			}

			# ●(その他)			次へ
			else {
				$self->from_param( '_REGS_PW' => $input );
				$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(8) } );
			}
		}
	},

	'8' =>	# 確認のため同じパスワードを入力してください
	sub {
		my $self = shift;
		$self->BIGModel::Generic::sendmsgs('CHANGEPASSWORD.3');					# 「確認のため同じパスワードを入力してください」
		$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(9) } );
	},

	'9' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			終了
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();									# 入力バッファをクリア
			$self->BIGModel::SysOp::REGS::finalize;
		}

		# ●(改行)
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(ヌル)				終了
			if ( $input eq '') {
				$self->BIGModel::SysOp::REGS::finalize;
			}

			# ●(その他)			入力値を照合
			else {
				my $pw = $self->from_param('_REGS_PW');
				unless ( $pw eq $input ) {														# 入力値が不一致
					$self->BIGModel::Generic::sendmsgs('CHANGEPASSWORD.4');							# 「パスワードが違います」
					$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(6) } );				# パスワード入力(6)に戻る

				}
				else {																			# 入力値が一致
					$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(10) } );			# 次へ
				}
			}
		}
	},

	'10' =>	# プロフィールを入力してください
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );							# 入力モードを設定
			$arg->param( 'line_size'=>60, 'maskchr'=>'*' );							# パラメータを設定
		$self->BIGModel::Generic::sendmsgs('REGS.5');								# 「プロフィールを入力してください」
		$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(11) } );		# 次へ
	},

	'11' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]				終了
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();														# 入力バッファをクリア
			$self->BIGModel::SysOp::REGS::finalize;
		}

		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			$self->from_param( '_REGS_HANDLENAME' => $input );							# 入力値をパラメータに保存
			$self->send( sprintf("\"%s\"\r\n", $input) );
			$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(12) } );		# 次へ
		}
	},

	'12' =>	# 正しいですか
	sub {
		my $self = shift;
		$self->BIGModel::Generic::sendmsgs('YESNO_CORRECT');							# 「正しいですか」
		$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(13) } );			# 次へ
	},

	'13' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]				終了
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();														# 入力バッファをクリア
			$self->BIGModel::SysOp::REGS::finalize;
		}

		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(ヌル)				終了
			if ( $input eq '' ) {
				$self->BIGModel::SysOp::REGS::finalize;
			}

			# ●[Y]					登録→最初に戻る
			elsif ( $input =~ /Y/i ) {
				my $id         = $self->from_param('ID');							# ログイン中のユーザＩＤ
				my $userid     = $self->from_param('_REGS_USERID');					# 登録するユーザＩＤ
				my $pw         = $self->from_param('_REGS_PW');						# パスワード
				my $handlename = $self->from_param('_REGS_HANDLENAME');				# ハンドルネーム
					$handlename = decode('cp932', $handlename);
				my $userlv     = 2;													# ユーザレベル（初回登録時は２（一般ユーザ））

				$self->BIGModel::SysOp::REGS::adduser( $userid, $pw, $handlename ,$userlv );		# 登録処理
				$self->BIGModel::Generic::sendmsgs('REGS.6');									# 「登録しました」
				$self->BIGModel::Generic::accesslog( $id, '0', '', sprintf( "ユーザ追加 [ %s (%s) ]", $userid, $handlename ) );		# 顛末をアクセスログに記録
				$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(2) } );					# 始めに戻る
			}

			# ●[N]					プロフィール入力に戻る
			elsif ( $input =~ /N/i ) {
				$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(10) } );
			}

			# ●(それ以外)			プロンプトを再表示
			else {
				$self->sethandler( sub { $self->BIGModel::SysOp::REGS::regs(12) } );
			}
		}
	},

};

#==================================================
# ■ユーザの追加
# adduser( userid, passwd, handlename ,userlv )
#
sub adduser {
	my $self = shift;
	my $userid = shift;
	my $passwd = shift;
	my $handlename = shift;
	my $userlv = shift;
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "INSERT INTO `users` ( userid, passwd, handlename, level ) VALUES ('$userid', '$passwd', '$handlename', '$userlv')";
	#printf "**[q]------------\n%s\n-----------------\n", $q;		# ????
	my $r = $dbh->do($q);
	$dbh->disconnect();
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;
	$self->from_param( 'LOCATION' => 'SYSOP' );			# 現在地
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;

	# パラメータの廃棄
	$self->from_param('-_REGS_USERID');
	$self->from_param('-_REGS_PW');
	$self->from_param('-_REGS_HANDLENAME');
	$self->BIGModel::Generic::modoru();			# 戻る
}

#--------------------------------------------------
# ■呼び出し
#
sub regs {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;