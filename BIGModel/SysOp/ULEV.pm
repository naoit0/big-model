package BIGModel::SysOp::ULEV;
our $VERSION = "0001.20190313.0034";	# 改良版 完成
#
# BIG-Model ホストプログラム
# ユーザレベル変更（シスオペコマンド）

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
use BIGModel::Generic::DB;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ SysOp::ULEV ]]\n\n";
		my $self = shift;
		$self->BIGModel::SysOp::ULEV::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');				# 「アクセス権がありません」
			$self->BIGModel::SysOp::ULEV::finalize;
		}
		else {
			$self->sethandler( sub { $self->BIGModel::SysOp::ULEV::ulev(1) } );
		}
	},

	'1' =>	# ユーザレベルを変更します
	sub {
		my $self = shift;
		$self->BIGModel::Generic::sendmsgs('ULEV.1');							# 「ユーザレベルを変更します」
		$self->sethandler( sub { $self->BIGModel::SysOp::ULEV::ulev(2) } );
	},

	'2' =>	# 変更すべきＩＤを入力してください
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );				# 入力モードを設定
			$arg->param( 'line_size'=>60, 'maskchr'=>'*' );				# パラメータを設定
		$self->BIGModel::Generic::sendmsgs('ULEV.2');					# 「変更すべきＩＤを入力してください」
		$self->sethandler( sub { $self->BIGModel::SysOp::ULEV::ulev(3) } );
	},

	'3' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			終了
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::SysOp::ULEV::finalize;
		}

		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(ヌル)				終了
			if ( $input eq '' ) {
				$self->BIGModel::SysOp::ULEV::finalize;
			}

			# ●(入力値)			ＩＤを照会
			else {
				my $data = $self->BIGModel::Generic::DB::Users::getAccount( $input, 'userid', 'userlv' );		# ユーザ照会
				unless ( defined($data) ) {																	# 登録なし
					$self->BIGModel::Generic::sendmsgs('ULEV.3');											# 「そのＩＤは登録されていません」
					$self->sethandler( sub { $self->BIGModel::SysOp::ULEV::ulev(2) } );
				}
				else {																			# 登録あり
					my ( $userid, $userlv ) = @{ $data };											# ユーザデータを取得
					$self->from_param( '_ULEV_USERID' => $userid );									# ユーザＩＤ
					$self->from_param( '_ULEV_USERLV' => $userlv );									# ユーザレベル
					$self->send( encode('cp932', '"'.$userid.'" ') );
					$self->send( encode('cp932', sprintf( "( 現在値: %d )\r\n", $userlv ) ) );
					$self->sethandler( sub { $self->BIGModel::SysOp::ULEV::ulev(4) } );
				}
			}
		}
	},

	'4' =>	# 変更するレベルを入力してください 
	sub {
		my $self = shift;
		$self->BIGModel::Generic::sendmsgs('ULEV.4');					# 「変更するレベルを入力してください」
		$self->sethandler( sub { $self->BIGModel::SysOp::ULEV::ulev(5) } );
	},

	'5' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			終了
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::SysOp::ULEV::finalize;
		}

		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(ヌル)				終了
			if ( $input eq '' ) {
				$self->BIGModel::SysOp::ULEV::finalize;
			}

			# ●(数値) 				変更				# （※有効範囲を制限するためのコードを追加する予定）
			elsif ( $input =~ /^\d*$/ ) {
				my $id = $self->from_param('ID');							# ログイン中のユーザＩＤ
				my $userid = $self->from_param('_ULEV_USERID');				# 変更対象のユーザＩＤ
				my $userlv = $self->from_param('_ULEV_USERLV');				# 変更するユーザレベル

				$self->BIGModel::SysOp::ULEV::update( $userid, $arg );			# 変更処理
				$self->BIGModel::Generic::sendmsgs('ULEV.3');					# 「変更しました」
				$self->BIGModel::Generic::accesslog( $id, '0', '', sprintf( "ユーザレベル変更 [%s] ( %d → %d )", $userid, $userlv, $arg ) );		# 顛末をアクセスログに記録
				$self->BIGModel::SysOp::ULEV::finalize;							# 終了
			}

			# ●(それ以外)			プロンプトを再表示
			else {
				$self->sethandler( sub { $self->BIGModel::SysOp::ULEV::ulev(4) } );
			}
		}
	},

};

#==================================================
# ■更新
#
sub update {
	my $self = shift;
	my $userid = shift;
	my $userlv = shift;
	my $dbh = DBI->connect('dbi:mysql:dbname=bigmodel;host=127.0.0.1','bigmodel','',{AutoCommit=>1,RaiseError=>1,PrintError=>0});
	my $q = "UPDATE `users` SET level = '$userlv' WHERE userid = '$userid'";
	#printf "\n** q=[\n%s\n]\n\n", $q;
	my $r = $dbh->do($q);
	$dbh->disconnect();
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;
	$self->from_param( 'LOCATION' => 'SYSOP' );			# 現在地
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;

	# パラメータの廃棄
	$self->from_param('-_ULEV_USERID');
	$self->from_param('-_ULEV_USERLV');
	$self->BIGModel::Generic::modoru();			# 戻る
}

#--------------------------------------------------
# ■呼び出し
#
sub ulev {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;