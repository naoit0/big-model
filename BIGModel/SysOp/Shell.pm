package BIGModel::SysOp::Shell;
our $VERSION = "0001.20190320.1648";	# 着手
#
# BIG-Model ホストプログラム
# コマンドシェル（シスオペコマンド）

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ SysOp::Shell ]]\n\n";
		my $self = shift;
		$self->BIGModel::SysOp::Shell::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');				# 「アクセス権がありません」
			$self->BIGModel::SysOp::Shell::finalize;
		}
		else {
			$self->sethandler( sub { $self->BIGModel::SysOp::Shell::tmap(1) } );
		}
	},

	'1' =>	# プロンプト表示
	sub {
		my $self = shift;
		my $return_path = $self->from_param('_Path')->path();
		$self->send( sprintf("[%s] > ", $path) );
			$self->sethandler( sub { $self->BIGModel::SysOp::Shell::tmap(2) } );
	},

	'2' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			終了
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::SysOp::ULEV::finalize;
		}

		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(ヌル)				終了
			if ( $input eq '' ) {
				$self->BIGModel::SysOp::ULEV::finalize;
			}

			# ●(入力値)			ＩＤを照会
			else {
				my $data = $self->BIGModel::Generic::DB::Users::getAccount( $input, 'userid', 'userlv' );		# ユーザ照会
				unless ( defined($data) ) {																		# 登録なし
					$self->BIGModel::Generic::sendmsgs('ULEV.3');													# 「そのＩＤは登録されていません」
					$self->sethandler( sub { $self->BIGModel::SysOp::TMAP::tmap(2) } );
				}
				else {																			# 登録あり
					my ( $userid, $userlv ) = @{ $data };											# ユーザデータを取得
					$self->from_param( '_ULEV_USERID' => $userid );									# ユーザＩＤ
					$self->from_param( '_ULEV_USERLV' => $userlv );									# ユーザレベル
					$self->send( encode('cp932', '"'.$userid.'" ') );
					$self->send( encode('cp932', sprintf( "( 現在値: %d )\r\n", $userlv ) ) );
					$self->sethandler( sub { $self->BIGModel::SysOp::TMAP::tmap(4) } );
				}
			}
		}
	},

};

#==================================================
# ■更新
#
sub update {
	my $self = shift;
	my $userid = shift;
	my $userlv = shift;
	my $dbh = DBI->connect('dbi:mysql:dbname=bigmodel;host=127.0.0.1','bigmodel','',{AutoCommit=>1,RaiseError=>1,PrintError=>0});
	my $q = "UPDATE `users` SET level = '$userlv' WHERE userid = '$userid'";
	#printf "\n** q=[\n%s\n]\n\n", $q;
	my $r = $dbh->do($q);
	$dbh->disconnect();
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;
	$self->from_param( 'LOCATION' => 'SYSOP' );			# 現在地
	$self->from_param( 'RETURN_PATH' => $self->from_param('_Path')->path() );	# 戻るときのパスを控えておく
	$self->from_param('_Path')->go(';');	# ルートパスに移動
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;

	# パラメータの廃棄
	$self->from_param( 'RETURN_PATH' => $self->from_param('_Path')->path() );	# 控えたパスを戻す
	#$self->BIGModel::Generic::modoru();			# 戻る
}

#--------------------------------------------------
# ■呼び出し
#
sub shell {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;