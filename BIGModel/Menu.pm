package BIGModel::Menu;
our $VERSION = "0001.20190212.0024";	# 改訂版 着手
our $VERSION = "0001.20190218.2202";	# 改訂版 完成
#
# BIG-Model ホストプログラム
# メニューユーティリティ
#
use strict;
no strict 'refs';
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
use DBI;

my $subs = {

#--------------------------------------------------
# ■ 初期設定・権限チェック(Permit)
#--------------------------------------------------
	'0' =>
	sub {
		print "\n\n** [[ Menu ]]\n";			# ????
		my $self = shift;
		$self->BIGModel::Menu::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# 「アクセス権がありません」
			$self->BIGModel::Menu::finalize;
		}
		else {
			$self->sethandler( sub { $self->BIGModel::Menu::menu(1) } );
		}
	},

#--------------------------------------------------
# ■ メニュー表示（メニュー抑止フラグが立っている場合はメニューを表示しない）
#--------------------------------------------------
	'1' =>
	sub {
		my $self = shift;
		my $nomenu = $self->from_param('_NOMENU');							# メニュー抑止フラグを回収
			$self->from_param('-_NOMENU');										# メニュー抑止フラグを廃棄
			$nomenu = defined( $nomenu ) ? $nomenu : 0;							# デフォルトは０
		unless ( $nomenu == 1 ) {											# メニュー抑止フラグが立っていないなら
			my $p = $self->from_param('_Path');
			printf "** path=[%s]\n", $p->path();							# ????
			$self->BIGModel::Generic::sendmsgs( 'MENU_'.$p->path() );			# 現在のパスのメニューを表示
		}
		$self->sethandler( sub { $self->BIGModel::Menu::menu(2) } );
	},

#--------------------------------------------------
# ■ プロンプトを表示
#--------------------------------------------------
	'2' =>
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );											# 入力モードを設定
			$arg->param( 'line_size'=>60, 'maskchr'=>'*' );											# パラメータを設定
		# （※ここに通知ハンドラを呼び出す）
		$self->BIGModel::Telegram::recieve() if ( $self->BIGModel::Telegram::check() );			# 電報の着信があれば表示
		#$self->BIGModel::Mail::notifyMail();														# メールの着信チェック
		$self->BIGModel::Generic::sendmsgs('PROMPT_CHOOSE');											# 「選択してください」
		print encode('cp932', "** ■■■■■■■■■■■■■■■■■■■■■■■■■■■■\n");		# ???? 入力待ち
		$self->sethandler( sub { $self->BIGModel::Menu::menu(3) } );
	},

#--------------------------------------------------
# ■ 【操作】入力待ち→入力分岐
#--------------------------------------------------
	'3' =>
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			【パスのレベルを上げる】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();														# 入力バッファをクリア
			$self->from_param( '_ACTION' => 'ETX' );								# ＥＴＸフラグ＝１
			$self->sethandler( sub { $self->BIGModel::Menu::menu(4) } );
		}

		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●[CR]			【パスのレベルを上げる】
			if ( $input eq '' ) {
				$self->from_param( '_ACTION' => 'CR' );							# ＣＲフラグ＝１
				$self->sethandler( sub { $self->BIGModel::Menu::menu(4) } );
			}

			# ●[~]				【コマンドプロンプト】
			elsif ( $input eq '~' ) {
				$self->sethandler( sub { $self->BIGModel::Utility::CmdShell::cmdshell } );			# コマンドシェルを呼び出す
			}

			# ●[$]				【エイリアス設定】
			elsif ( $input eq '$' ) {
				$self->from_param( '_RETURN' => sub { $self->BIGModel::Menu::menu(1) } );		# エイリアスからの戻り先を設定
				$self->sethandler( sub { $self->BIGModel::Generic::Alias::alias } );			# エイリアスを呼び出す
			}

			# ●[!]				【電報】
			elsif ( $input eq '!' ) {
				$self->from_param( '_NOMENU' => 1 );											# メニュー抑止フラグ＝１
				#$self->from_param( '_RETURN' => sub { $self->BIGModel::Menu::menu(1) } );		# 電報からの戻り先を設定
				$self->sethandler( sub { $self->BIGModel::Telegram::telegram } );
			}

			# ●[?]				【ヘルプ→プロンプトを表示】
			elsif ( $input eq '?' ) {
				my $p = $self->from_param('_Path');
				$self->BIGModel::Generic::sendmsgs( 'HELP_'.$p->path() );							# 現在のパスのヘルプを表示
				$self->sethandler( sub { $self->BIGModel::Menu::menu(1) } );
			}

			# ●(その他)		【(4)コマンド解析へ】
			else {
				$self->from_param( '_INPUT' => $input );
				$self->sethandler( sub { $self->BIGModel::Menu::menu(4) } );
			}
		}
	},

#--------------------------------------------------
# ■【判定・移動】判定処理
#--------------------------------------------------
	'4' =>
	sub {
		print "\n=============================\n";
		my $self = shift;

		my $action = $self->from_param('_ACTION'); $self->from_param('-_ACTION');		# 要求操作
		my $input = $self->from_param('_INPUT'); $self->from_param('-_INPUT');			# コマンド文字列
		my $p = $self->from_param('_Path');
		my $curr_path = $p->path();														# カレントパス
		my $next_path;																	# 移動先パス
		my $toplevelup = 0;																# トップレベルのパスから上昇した

		print "\n--[1]-------------------------\n";								# ????
		printf "** action=[%s]\n", $action if defined($action);					# ???? キー操作
		printf "** input=[%s]\n", $input if defined($input);					# ???? 入力文字列
		printf "** curr_path=[%s]\n", $curr_path if defined($curr_path);		# ???? カレントパス
		print "[paths]\n".Dumper( $p->paths() )."[/paths]\n";					# ???? 更新後のパスリスト
		print "------------------------------\n";								# ????

		# ●キー操作を調べる
		if ( defined($action) ) {
			my $r = $p->up();													# レベルを上げる
			if ( defined($r) ) {												# 返り値が定義された
				if ( ($r eq ';') && ($curr_path eq $r) ) {							# 次のパスがトップレベルで次のパスとカレントパスが同じ
					if ( $action eq 'ETX' ) {											# キャンセルが要求された
						#print "\n** (cancel - toplevel_up)\n";								# ????
						$self->from_param( '_NOMENU' => 1 );								# メニュー抑止フラグ＝１
					}
					#print "\n** (cr - toplevel_up)\n" if ( $action eq 'CR' );			# ????
					$next_path = ';';													# 次はトップレベルパス
					$toplevelup = 1;													# トップレベルのパスから上昇した
				}
				else {
					#print "\n** (cancel - up)\n" if ($action eq 'ETX');				# ????
					#print "\n** (cr - up)\n" if ($action eq 'CR');						# ????
					$next_path = $r;													# 次はカレントパスのレベルを上げたパス
				}
			}
			else {																# 返り値が未定義（履歴リストがない）※ルートを指定してスタートするので、ここは機能しない
				$next_path = ';';													# 次はトップレベルパス
			}
		}

		# ●入力文字列を調べる
		else {
			# ●＄マークで始まる（エイリアスコマンド）
			if ( $input =~ /^\$/ ) {
				#print "\n** (alias)\n";									# ???? 【エイリアス】		** pass
				$input =~ s/^\$//;
				my $userid = $self->from_param('ID');
				$next_path = $self->BIGModel::Generic::Alias::getpath_byalias( $input, $userid );			# エイリアス名からパスを取得
				unless ( defined($next_path) ) {															# パスが得られなかった（失敗）
					$self->from_param( '_NOMENU' => 1 );														# メニュー抑止フラグ＝１
					$next_path = $curr_path;																	# カレントパスに移動
				}
				#else {																							# ????
				#	print "** (alias - successed)\n";															# ????
				#}																								# ????
			}


			# ●入力値にセミコロンを含む
			elsif ( $input =~ /\;/ ) {
				#printf "** input=[%s]\n", $input;															# ????
				# ●セミコロンで始まる（シンボル・絶対パス）
				if ( $input =~ /^\;/ ) {
					$next_path = $self->BIGModel::Generic::DB::Map::getpath_bysymbol( $input );					# シンボル名からパスを取得
					unless ( defined($next_path) ) {															# パスを取得できたら
						#print "\n** (absorute_path)\n";					# ???? 【絶対パス】		** pass
						$next_path = $input;
					}
					#else {													# ????
					#	print "\n** (symbol)\n";							# ???? 【シンボル】
					#}														# ????
				}
				# ●セミコロンで始まらない（相対パス）
				else {
					#print "\n** (relative_path)\n";						# ???? 【相対パス】		** pass
					$next_path = $p->path().';'.$input;
				}
			}

			# ●入力値にセミコロンを含まない（メニュー選択）
			else {
				#print "\n** (menu_select)\n";								# ???? 【メニュー選択】		** pass
				my $path = $p->path();
				$input = ';'.$input if ($path ne ';');															# トップレベルなら';'をつける
				$next_path = $p->path().$input;
			}
		}

		print "\n--[2]-------------------------\n";					# ????
		printf "** curr_path=[%s]\n", $curr_path;					# ???? カレントパス
		printf "** next_path=[%s]\n", $next_path;					# ???? 移動先パス
		print "------------------------------\n";					# ????

		# ●最終決定
		my $r = $self->BIGModel::Menu::getutil( $next_path );				# パスからユーティリティを取得
		if ($r =~ /\d/) {
			if ( $r == -1 ) {													# (-1)ユーティリティが得られない
				$self->from_param( '_NOMENU' => 1 );								# メニュー抑止フラグ＝１
				print "** (utility_not_found)\n";									# ????
				$r = $self->BIGModel::Menu::getutil( $curr_path );					# カレントパスのユーティリティを取得
			}
			elsif ( $r == -2 ) {												# (-3)パスへのアクセス拒否
				print "** (disallow)\n";											# ????
				$self->from_param( '_NOMENU' => 1 );								# メニュー抑止フラグ＝１
				$r = $self->BIGModel::Menu::getutil( $curr_path );					# カレントパスのユーティリティを取得
			}
			elsif ( $r == -3 ) {													# (-2)ユーティリティがロードされていない
				print "** (utility_not_loaded)\n";									# ????
				$self->from_param( '_NOMENU' => 1 );								# メニュー抑止フラグ＝１
				$r = $self->BIGModel::Menu::getutil( $curr_path );					# カレントパスのユーティリティを取得
			}
			$p->go($curr_path);												# カレントパスに移動
		}
		else {
			print "** (successed)\n";										# ????
			$p->go($next_path) unless ( $toplevelup == 1 );					# 次のパスに移動
		}
		$self->sethandler( sub { $self->$r } );

		print "\n--[3]-------------------------\n";						# ????
		printf "*3* path=[%s]\n", $p->path();						# ???? 移動先のパス
		print "[paths]\n".Dumper( $p->paths() )."[/paths]\n";		# ???? 更新後のパスリスト
		print "------------------------------\n";						# ????
	},

};

#==================================================
# ★■ユーティリティを取得する（チェック機構付）
# r = getutil( path )
#
# (成功)
# 	ユーティリティのスタートポイント
# (エラー)
# 	-1 : ユーティリティが得られない（パスに紐づけされていない）
# 	-2 : パスへのアクセス拒否（Permitテーブルをチェック）
# 	-3 : ユーティリティがロードされていない
#
# ※内部でgetutil_bypath()を呼び出す
#
sub getutil {
	#printf "\n(getutil [%s])\n", join(':', caller);						# ????
	my $self = shift;
	my $path = shift;
	#printf "** path=[%s]\n", $path;

	my $r = 0;
	my $util = $self->BIGModel::Generic::DB::Map::getutil_bypath( $path );						# (1) パスのユーティリティを得る
	my ( $pkg, $start ) = @{$util} if ( defined($util) );
	if ( defined($pkg) && defined($start) ) {
		#printf "** pkg=[%s], start=[%s]\n", $pkg, $start;									# ????
		if ( $self->BIGModel::Generic::Permit::allow($path) ) {									# (2) パスのアクセス権限(DISALLOW)チェック
			if ( $self->BIGModel::Generic::isutyload($pkg) ) {								# (3) ユーティリティのロードチェック
				my $func = 'BIGModel::'.$pkg.'::'.$start;
				#printf "** (successed) func=[%s]\n", $func;
				#print "(/getutil)\n";														# ????
				return "$func";																# 呼び出すユーティリティ
			}
			else {
				$r = -3;
			}
		}
		else {
			$r = -2;
		}
	}
	else {
		$r = -1;
	}
	#print "** r=-1 (util_not_found)\n" if ($r == -1);		# ????
	#print "** r=-2 (disallow)\n"       if ($r == -2);		# ????
	#print "** r=-3 (util_not_load)\n"  if ($r == -3);		# ????
	#print "(/getutil)\n";									# ????
	return $r;
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;
	#my $argv = $self->from_param('_ARGV');				# ユーティリティの引数（ユーティリティを呼び出す前に定義する）
	#$self->from_param('-_ARGV');						#

	$self->from_param( 'LOCATION' => 'MENU' );
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;

	$self->BIGModel::Generic::modoru();				# 戻る
}

#--------------------------------------------------
# ■呼び出し
#
sub menu {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;
