package BIGModel::SysOp;
our $VERSION = "0001.20180624.1538";	# 着手
#
# BIG-Model ホストプログラム
# シスオペコマンド（パッケージローダ）
#

use strict;
no strict 'refs';
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;

#use BIGModel::SysOp::Shell;			# コマンドシェル（テスト）
use BIGModel::SysOp::REGS;			# 会員登録
use BIGModel::SysOp::PASU;			# 会員抹消
use BIGModel::SysOp::PASC;			# パスワード変更(4.0)
use BIGModel::SysOp::ULEV;			# ユーザレベル変更

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;