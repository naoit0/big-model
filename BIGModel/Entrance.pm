package BIGModel::Entrance;
our $VERSION = "0001.20190212.0007";		# 改訂版
#
# BIG-Model ホストプログラム
# 入口（ログイン後メッセージ）
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;



my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ Entrance ]]\n\n";
		my $self = shift;
		$self->BIGModel::Entrance::initalize;
		$self->sethandler( sub { $self->BIGModel::Entrance::entrance(1) } );
	},

	'1' =>
	sub {
		my $self = shift;
		$self->BIGModel::Utility::logininfo;		# ログイン情報
		$self->BIGModel::Utility::notice;			# お知らせ
		#$self->BIGModel::Mail::checkMail;			# 新着メールチェック
		$self->BIGModel::Entrance::finalize;
	},

};

#==================================================
# ■ 初期処理
#
sub initalize {
	my $self = shift;
}

#--------------------------------------------------
# ■ 終了処理
#
sub finalize {
my $self = shift;

	#$self->from_param('_Path')->go(';');			# トップレベルパスを設定(未定義でもmodoru()で強制的にトップレベルに移動する)
	my $r = $self->BIGModel::Generic::modoru();		# ひとつ前に戻る
}

#--------------------------------------------------
# ■ 呼び出し
#
sub entrance {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;