package BIGModel::Chat::Utility;
our $VERSION = "0001.20190213.0748";		# 改訂版 着手
#
# BIG-Model 
# チャット（ユーティリティ群）
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;

#==================================================
# ■ チャットルームの状況通知
#
sub roomlist {
	print "\n(roomlist)\n";					# ????
	my $self = shift;
	my $rooms = $self->config('CHATROOM');			# ルームリスト

	$self->send("\r\n");
	foreach my $roomname ( sort(keys(%{$rooms})) ) {

		my @members = @{ $rooms->{$roomname}->{'MEMBER'} };
		@members = map { $self->param( $_, 'ID') } @members;			# ファイル番号をユーザＩＤに置換
		my $msg = sprintf("%s  %s\r\n", $roomname, join(' ', @members));
		$self->send(encode('cp932', $msg));
	}
	$self->send("\r\n");
	print "(/roomlist)\n";					# ????
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;


# ※プライベートルームの場合は表示しないとか
