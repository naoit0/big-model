package BIGModel::Chat::Utility::Roomlist;
our $VERSION = "0002.20180130.1142";
#
# BIG-Model 
# チャットルームの状況通知（関数）
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;

#==================================================
# ■ チャットルームの状況通知
#
sub roomlist {
	my $self = shift;
	my $rooms = $self->comm->config('CHATROOM');
	my $params = $self->comm->{'param'};
	#print Dumper(%rooms)."\n";
	#print Dumper($params)."\n";
	$self->comm->send("\r\n");

	foreach my $room (sort(keys(%{$rooms}))) {
		my $msg = '';
		my @members = @{ $rooms->{$room}->{'MEMBER'} };
		if ($#members >= 0) {
			for my $i (0..$#members) {
				$members[$i] = $params->[$members[$i]]->{'ID'};
			}
		}
		$msg = sprintf("%s  %s\r\n", $room, join(' ', @members));
		$self->comm->send(encode('cp932', $msg));
	}
	$self->comm->send("\r\n");
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;