package BIGModel::Chat::Roomlist;
our $VERSION = "0001.20190213.0748";		# 改訂版 着手
#
# BIG-Model 
# チャットルームの状況通知
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use BIGModel::Chat::Utility;
	# BIGModel::Chat::Utility::roomlist()				# チャットルーム一覧(本体)
use Data::Dumper;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ Chat::Roomlist ]]\n\n";
		my $self = shift;
		$self->BIGModel::Chat::Roomlist::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# ** アクセス権がありません **
			$self->BIGModel::Chat::Roomlist::finalize;
		}
		else {
			$self->BIGModel::Chat::Utility::roomlist();
			$self->BIGModel::Chat::Roomlist::finalize;
		}
	},

};

#--------------------------------------------------
sub initalize {
	my $self = shift;
}

#--------------------------------------------------
sub finalize {
	my $self = shift;

	# 前のパスに移動
	my $return = $self->from_param('_RETURN'); $self->from_param('-_RETURN');
	if ( defined($return) ) {							# 戻り先があれば
		$self->sethandler( sub { $self->$return } );	# 戻り先を定義
	}
	else {
		$self->BIGModel::Generic::modoru();				# 前のパスに戻りユーティリティを呼び出す
	}
}

#--------------------------------------------------
# ■呼び出し
#
sub roomlist {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;