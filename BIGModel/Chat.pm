package BIGModel::Chat;
our $VERSION = "0005.20180220.0246";
our $VERSION = "0006.20180608.2205";	# 新Arg対応
our $VERSION = "0007.20180608.2317";	# 部屋の鍵対応
our $VERSION = "0007.20180703.0057";	# 微調整
our $VERSION = "0001.20190213.0743";	# 改訂版 着手
#
# BIG-Model ホストプログラム
# チャット
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
use BIGModel::Chat::Roomlist;			# チャットルーム一覧（インターフェース）

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ Chat ]]\n\n";
		my $self = shift;
		$self->BIGModel::Chat::initalize();
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# ** アクセス権がありません **
			$self->BIGModel::Chat::finalize();
		}
		else {
			if ( defined($self->BIGModel::Chat::startup) == 0 ) {				# スタートアップ
				$self->BIGModel::Chat::finalize();
			}
			else {
				$self->sethandler( sub { $self->BIGModel::Chat::chat(1) });
			}
		}
	},

	'1' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			【ルームを退室】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->BIGModel::Chat::leavedmsg( $arg->status('line') );
			$self->BIGModel::Chat::leave;
			$self->BIGModel::Chat::finalize;
		}

		# ● [^R]							【入力行リプリント】
		#elsif ( $inkey eq "\x13" ) {
		#}

		elsif ( $arg->status('line_cr') == 1 ) {
			my $input = $arg->status('line');
			printf "** input=[%s]\n", encode('cp932', $input);				# ????
			# ● [^]						【退室】
			if ($input eq '^') {
				$self->BIGModel::Chat::leavedmsg( $input );
				$self->BIGModel::Chat::leave;
				$self->BIGModel::Chat::finalize;
			}
			# ● [!]						【電報】
			elsif ($input eq '!') {
				$self->from_param( '_RETURN' => sub { $self->BIGModel::Chat::chat(1) } );		# 電報からの戻り先を設定
				$self->sethandler( sub { $self->BIGModel::Telegram::telegram } );
			}
			# ● [!?]						【ヘルプ】
			elsif ($input eq '!?') {
				$self->send("\r\n");
				$self->BIGModel::Generic::sendmsgs('CHATROOM.1');
			}
			# ● [!E]						【入力文字を自分自身へエコーするかどうかを切り換える】（廃止）
			#elsif ($input =~ /^!E$/i) {
			#}
			# ● [!L]						【他のメンバーが入れないように鍵をかける(4.0)】
			elsif ($input =~ /^!L$/i) {
				$self->send("\r\n");
				$self->BIGModel::Chat::lock();
			}
			# ● [!U]						【他のメンバーが入れるように鍵を開ける(4.0)】
			elsif ($input =~ /^!U$/i) {
				$self->send("\r\n");
				$self->BIGModel::Chat::unlock();
			}
			# ● [!R]						【チャットルームの状況通知】
			elsif ($input =~ /^!R$/i) {
				$self->send("\r\n");
				$self->BIGModel::Chat::Utility::roomlist();
			}
			# ● [!W]						【現在のアクセス状況通知】
			elsif ($input =~ /^!W$/i) {
				$self->send("\r\n");
				$self->BIGModel::Utility::tasklist();
			}
			# ● [CR]						【送信】
			else {
				$self->BIGModel::Chat::broadcast( $input );
			}
		}
		#$self->sethandler( sub { $self->BIGModel::Chat::chat(1) });		# ループ
	},

};

#==================================================
# ★■発信
# broadcast( str, mode )
#
sub broadcast {
	print "\n(broadcast)\n";											# ????
	my $self = shift;
	my $from_msg = shift;											# 発信者のメッセージ
		$from_msg = ( defined($from_msg) ) ? $from_msg : '';
	my $mode = shift;												# 後退処理抑止フラグ(0: 無効, 1: 有効)
		$mode = ( defined($mode) ) ? $mode : 0;

	my $from_no = $self->from->fileno;								# 発信者のファイル番号
	my $from_id = $self->from_param('ID');							# 発信者のユーザＩＤ
	my $roomid = $self->from_param('CHATROOM');					# 発信者が入室しているルーム名
	my $rooms = $self->config('CHATROOM');							# ルームリスト
	my @members = @{ $rooms->{$roomid}->{'MEMBER'} };				# ルームメンバー（内容は接続ソケットのファイル番号）

	foreach my $to_no (@members) {												# 受信者のファイル番号
		my $to_arg = $self->param( $to_no, '_Arg' );								# 受信者の入力制御オブジェクト
		my $to_input = $to_arg->status('line');										# 受信者の入力中の文字列
		$to_input = ( defined($to_input) ) ? $to_input : '';
		printf "** from_no=(%d), to_no=(%d)\n", $from_no, $to_no;					# ???? 発信者と受信者のファイル番号
		printf "** from_msg=[%s]\n", $from_msg;										# ???? 発信者のメッセージ
		printf "** to_input=[%s], ", $to_input;										# ???? 受信者の入力中の文字列

		# (1) 入力途中の文字列の後退処理
		if ( $from_no == $to_no ) {													# 発信者と受信者が同一なら
			if ($from_msg ne '') {
				my $len = length($from_msg);												# 発言内容のデータ長を取得
				$self->send("\x08\x20\x08"x$len, $to_no) if ( $mode == 0 );					# 後退処理
			}
		}
		else {																		# 発信者と受信者が同一でないなら
			if ($to_input ne '') {														# 入力中なら後退処理
				my $len = length($to_input);												# 入力中のデータ長を取得
				$self->send("\x08\x20\x08"x$len, $to_no);									# 後退処理
			}
		}

		# (2) 受信メッセージの送出
		$self->send( encode('cp932', $from_id).": ".$from_msg."\r\n", $to_no);		# 発信元ＩＤ付きでメッセージを送出

		# (3) 入力途中の文字列を送出（他者）
		unless ( $from_no == $to_no ) {												# 発信者が他者なら
			$self->send($to_input, $to_no) if ($to_input ne '');						# 入力中の文字列を送出
		}
	}
	print "(/broadcast)\n";				# ????
}

#--------------------------------------------------
# ★■チャットルームに入室しました
# joinedmsg()
#
sub joinedmsg {
	print "\n(joinedroom)\n";				# ????
	my $self = shift;
	$self->BIGModel::Chat::broadcast( encode('cp932', "** チャットルームに入室しました **"), 1);
	print "(/joinedroom)\n";					# ????
}

#--------------------------------------------------
# ★■退室します
# leavedmsg( str )
#
sub leavedmsg {
	print "\n(leavedmsg)\n";				# ????
	my $self = shift;
	my $str = shift;								# 送信者のメッセージ
		$str = ( defined($str) ) ? $str : '';
	my $arg = $self->from_param('_Arg');
	my $input = $arg->status('line');				# 入力途中の文字列

	if ( defined($input) ) {															# 入力途中の文字列があれば
		my $len = length( encode('cp932', $input) );
		$self->send( "\x08\x20\x08" x $len ) if ( $input ne '' );							# 後退する
	}

	if ( $str eq '^' ) {																# 終了マークで退室した場合は
		$self->send( "\x08\x20\x08" );
		$self->send( encode('cp932', $self->from_param('ID').": ".$str."\r\n") );			# 発信元ＩＤ付きでメッセージを送出
	}
	$self->BIGModel::Chat::broadcast( encode('cp932', "** 退室します **"), 1);
	print "(/leavedmsg)\n";				# ????
}

#--------------------------------------------------
# ★■鍵をかける
# lock()
#
sub lock {
	print "\n(lock)\n";				# ????
	my $self = shift;
	my $roomid = $self->from_param('CHATROOM');				# 入室中のルーム名
	my $rooms = $self->config('CHATROOM');						# ルームリスト
	my $locked = $rooms->{$roomid}->{'LOCKED'};				# 入室中のルームの施錠フラグ

	if ( $locked == 1 ) {
		$self->send( encode('cp932', "** すでに鍵がかかっています **\r\n") );
	}
	else {
		$rooms->{$roomid}->{'LOCKED'} = 1;
		$self->config( 'CHATROOM', $rooms );						# ルームリストを更新
		$self->BIGModel::Chat::broadcast( encode('cp932', "** 鍵をかけました **"), 1);
	}
	#print "[config.CHATROOM]\n".Dumper($rooms)."\n";		# ????
	print "(/lock)\n";				# ????
}

#--------------------------------------------------
# ★■鍵を開ける
# unlock()
#
sub unlock {
	print "\n(unlock)\n";				# ????
	my $self = shift;
	my $roomid = $self->from_param('CHATROOM');				# 入室中のルーム名
	my $rooms = $self->config('CHATROOM');						# ルームリスト
	my $locked = $rooms->{$roomid}->{'LOCKED'};				# 入室中のルームの施錠フラグ

	if ( $locked == 0 ) {
		$self->send( encode('cp932', "** すでに鍵は開いています **\r\n") );
	}
	else {
		$rooms->{$roomid}->{'LOCKED'} = 0;
		$self->config( 'CHATROOM', $rooms );						# ルームリストを更新
		$self->BIGModel::Chat::broadcast( encode('cp932', "** 鍵を開けました **"), 1);
	}
	#print "[config.CHATROOM]\n".Dumper($rooms)."\n";		# ????
	print "(/unlock)\n";				# ????
}

#--------------------------------------------------
# ★■ルームに入る
# joinroom( roomid )
#
sub joinroom {
		print "\n(joinroom)\n";									# ????
		my $self = shift;
	my $roomid = shift;										# 入室するルーム名

	my $rooms = $self->config('CHATROOM');						# ルームリスト
	print "[config.CHATROOM]\n".Dumper($rooms)."\n";			# ???? ルームリストの状況

	my $locked = $rooms->{$roomid}->{'LOCKED'};				# 入室先の部屋の施錠フラグ

	if ( defined($locked) && $locked == 1 ) {								# ■入室先の部屋に鍵がかかっている
		$self->send( encode('cp932', "** 鍵がかかっています **\r\n") );
		print "(/joinroom)\n";													# ????
		return;
	}
	else {																	# ■入室先の部屋に鍵がかかっていない
		$self->from_param( 'CHATROOM' => $roomid );							# 入室したルーム名を保存

		unless (defined($locked)) {												# 施錠フラグが未定義→新規ルーム開設
			$rooms->{$roomid}->{'LOCKED'} = 0;									# ルームの鍵を開けておく
		}
		push(@{ $rooms->{$roomid}->{'MEMBER'} }, $self->from->fileno);		# ルームメンバーにファイル番号を追加
		$self->config( 'CHATROOM' => $rooms );									# ルームリストを更新

		print "[config.CHATROOM]\n".Dumper($rooms)."\n";						# ???? ルームリストの状況
		$self->BIGModel::Generic::sendmsgs( 'CHATROOM.1' );							# チャットルームの看板を表示
		$self->BIGModel::Chat::joinedmsg;										# 同じルーム内に入室したことを知らせる

		my $arg = $self->from_param('_Arg');									# 入力制御を変更
		$arg->mode( 'echo'=>1, '-line'=>1, '-mask'=>0 );
		$arg->param( '-size'=>1024 );
		print "(/joinroom)\n";													# ????
		return $roomid;														# ルーム名を返す
	}
}

#--------------------------------------------------
# ★■ルームを出る
# leave()
#
sub leave {
	print "\n(leave)\n";				# ????
	my $self = shift;
	my $roomid = $self->from_param('CHATROOM');				# 入室中のルーム名
	my $rooms = $self->config('CHATROOM');						# ルームリスト
	my $from_no = $self->from->fileno;							# 退室者のファイル番号

	# ルームとルームメンバーを再構築する
	@{ $rooms->{$roomid}->{'MEMBER'} } = grep { $_ != $from_no } @{ $rooms->{$roomid}->{'MEMBER'} };

	# 入室者がいなければルーム名を削除
	delete( $rooms->{$roomid} ) if ($#{ $rooms->{$roomid}->{'MEMBER'} } < 0);

	#print "[config.CHATROOM]\n".Dumper($rooms)."\n";			# ????
	$self->config( 'CHATROOM', $rooms );						# ルームリストを更新
	$self->from_param('-CHATROOM');									# 退室者のパラメータを削除
	print "(/leave)\n";				# ????
}

#--------------------------------------------------
# ★■スタートアップ
# startup()
#
# 入室するルーム名の取得する
# ※入室するルーム名を任意に指定できるようにする予定
#
sub startup {
	print "\n(startup)\n";
	my $self = shift;
	my $roomid;
	$self->from_param( 'CHATROOM' => undef );		# 入室するルーム名

	# ルーム名を設定
	# シンボル名があればそれを使う。なければコマンドの文字を使う。
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my ($q, $sth);

	my $path = $self->from_param('_Path');
	my $p = $path->path();											# ???? 現在のパス
	$q = "SELECT symbol FROM `map` WHERE path='$p'";
	printf "q=[%s]\n", $q;											# ????
	$sth = $dbh->prepare( $q );
	$sth->execute();
	if ( $sth->rows ) {
		($roomid) = $sth->fetchrow_array;
		$roomid =~ s/\;// if defined($roomid);
	}
	else {
		$roomid = undef;
	}
	$sth->finish if defined($sth);
	$dbh->disconnect;
	printf "** defined.room=(%d)\n", defined($roomid);							# ????

	# シンボル名が未定義なら入ったコマンド文字を使う
	unless ( defined($roomid) ) {
		# （※パス制御を新設したため廃止）
		#my $p = $self->from_param('P_A_T_H');											# こうするとpopしたときparamの内容まで変わる
		#my @p = @{ $self->from_param('P_A_T_H') };										# こうすればpopしたときparamの内容は変わらない
		#$roomid = 'ROOM'.pop(@p);
	}
	printf "** roomid=[%s]\n", $roomid;											# ???? 入ったルーム名
	## print "2[PATH]".Dumper($self->from_param('P_A_T_H'));								# ???? （※パス制御を新設したため廃止）
	return $self->BIGModel::Chat::joinroom($roomid);								# 取得したルーム名に入室する
	print "(/startup)\n";
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;
	$self->from_param('-CHATROOM');				# パラメータを廃棄(入室したルーム名)
	$self->BIGModel::Generic::modoru();			# 戻る
}

#--------------------------------------------------
# ■呼び出し
#
sub chat {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

#------------------------------------------------------
# ■ ソケット切断処理【※編集中】
#
sub disconnect {
	my $self = shift;
	my $fileno = $self->from->fileno;
	my $chatroom = $self->config('CHATROOM');								# ???? パラメータを得る
		$chatroom = defined($chatroom) ? $chatroom : '<undef>';				# ???? パラメータが未定義なら<undef>に置換

	#printf "CHATROOM=[%s]\n", $chatroom;									# ???? パラメータを表示
	if ( defined($self->from_param('CHATROOM')) ) {							# チャット中なら退出
		$self->from_param('-CHATROOM');										# ???? チャット中フラグを削除
		#print encode('cp932', "** チャットルームからログオフしました **\n");
		#$self->BIGModel::Chat::leavedmsg("** ログオフしました **");
		#$self->BIGModel::Chat::leave;
	}
}

#------------------------------------------------------
# ■ ローダ
#
sub load {
	my $self = shift;
	$self->setsyshandler( 'onDisconnect', sub { disconnect($self) }, 'Chat' );		# ソケット切断処理をハンドラに定義
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);

#----------------------------------------
# 行中 [CTRL+C] 入力行キャンセル			
# [CTRL+R]      入力行リプリント			
# 行頭 [CTRL+C] チャットルームを退室		*** 完了
# ^[RETURN]     同上						*** 
# ![RETURN]     電報機能					*** 
# !?[RETURN]    コマンドヘルプ				*** 
# !E[RETURN]    エコーオフ/オン				
# !L[RETURN]    鍵をかける					*** 
# !R[RETURN]    チャットルームの状況		*** 
# !U[RETURN]    鍵を開ける					*** 
# !W[RETURN]    現在のアクセス状況			*** 
#----------------------------------------

# config('CHATROOM') => {
# 			'roomid' => {
#				'MEMBER' => [ fileno, ... ],
#				'LOCKED' => 1
#			},
# 			'roomid' => {
#				'MEMBER' => [ fileno, ... ],
#				'LOCKED' => 1
#			},
# }


1;