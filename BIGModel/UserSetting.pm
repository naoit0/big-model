package BIGModel::UserSetting;
our $VERSION = "0001.20180622.1611";	# 着手
#
# BIG-Model ホストプログラム
# 端末環境変更機能

use strict;
use warnings;
use utf8;
use BIGModel::UserSetting::ChangeMenuMode;			# [1] 漢字モード変更
use BIGModel::UserSetting::ChangePassword;			# [2] パスワード変更
use BIGModel::UserSetting::ChangeProfile;			# [3] プロフィール変更
#use BIGModel::UserSetting::Userlist;				# [4] 会員リスト
use BIGModel::UserSetting::ChangeLastLogin;			# [5] 最終ログイン日時仮設定
#use BIGModel::UserSetting::ListEdit;				# [6] 探索リスト登録
#use BIGModel::UserSetting::ListView;				# [7] 探索リスト読み出し
use BIGModel::UserSetting::TelegramMuteMode;		# [9] 電報受信設定

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;
