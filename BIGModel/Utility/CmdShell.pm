package BIGModel::Utility::CmdShell;
our $VERSION = "0001.20190221.1900";	# 着手
#
# BIG-Model ホストプログラム
# コマンドシェル
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ CmdShell ]]\n\n";					# ????
		my $self = shift;
		$self->BIGModel::Utility::CmdShell::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# ** アクセス権がありません **
			$self->BIGModel::Utility::CmdShell::finalize();
		}
		else {
			$self->send("\r\n");
			$self->sethandler( sub { $self->BIGModel::Utility::CmdShell::cmdshell(1) } );
		}
	},

	'1' =>	# プロンプト
	sub {
		my $self = shift;
		my $path = $self->from_param('_Path')->path();
		my $userid = $self->from_param('ID');
		my $userlv = $self->from_param('USERLEVEL');
		$self->send(sprintf "[%s](%s)@[%s] >",$userid, $userlv, $path );
		$self->sethandler( sub { $self->BIGModel::Utility::CmdShell::cmdshell(2) } );
	},

	'2' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ● [ETX][ESC]				【キャンセル】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::Utility::CmdShell::finalize;
		}
		# ● （改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			# ● (ヌル)				【キャンセル】
			if ( $input eq '' ) {
				$self->sethandler( sub { $self->BIGModel::Utility::CmdShell::cmdshell(1) } );
			}
			# ● （その他）			【再入力】
			else {
				#my $argv = $self->BIGModel::Utility::CmdShell::parse( $input );
				#$self->BIGModel::Utility::CmdShell::run( $argv );
				$self->BIGModel::Utility::CmdShell::run( $input );
				$self->send("\r\n");
				$self->sethandler( sub { $self->BIGModel::Utility::CmdShell::cmdshell(1) } );
			}
		}
	},

};

#======================================================
# ■ コマンド実行
# run()
#
sub run {
	my $self = shift;
	my $arg = shift;

	my $r = eval "return $arg";
	$r =~ s/\n/\r\n/g;
	$self->send( $r );
}

#------------------------------------------------------
# ■ 構文解析（とりあえずゴミ）
# parse()
#
#sub parse {
#	my $self = shift;
#	my $arg = shift;
#
#	if arg
#	my $argv;
#
#
#
#	return $argv;
#}

#------------------------------------------------------
# ■ 初期処理
#
sub initalize {
	my $self = shift;
}

#------------------------------------------------------
# ■ 終了処理
#
sub finalize {
	#print "\n(finalize)\n";					# ????
	my $self = shift;
	$self->BIGModel::Generic::modoru();		# 戻る
	#print "(/finalize)\n";						# ????
}

#------------------------------------------------------
# ■ 呼び出し
#
sub cmdshell {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;
