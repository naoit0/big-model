package BIGModel::Utility::Notice;
our $VERSION = "0001.20190211.2110";		# 改訂版
#
# BIG-Model ホストプログラム
# お知らせ
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
#use Data::Dumper;
#use Debug;

#==================================================
# ■お知らせを表示
#
sub notice {
	my $self = shift;
	my $msg =<<'EOT';
*************************************************
** 当システムはBIG-Modelのサンプルシステムです **
*************************************************
EOT
	$msg =~ s/\n/\r\n/g;
	$self->send(encode('cp932', $msg));
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;