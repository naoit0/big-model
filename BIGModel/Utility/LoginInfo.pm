package BIGModel::Utility::LoginInfo;
our $VERSION = "0001.20190211.2105";		# 改訂版
#
# BIG-Model ホストプログラム
# ログイン情報
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
#use Data::Dumper;
#use Debug;

#==================================================
# ■ログイン時にログオン情報を表示する
#
sub logininfo {
	my $self = shift;

	my $msg2;
	$msg2 = sprintf("| プロフィール %s\r\n", $self->from_param('HANDLENAME'));
	$msg2 .= sprintf("| 前回ログイン %s\r\n", $self->from_param('LASTLOGIN')) if ( $self->from_param('USERLEVEL') > 1);
	$msg2 .= sprintf("| 今回ログイン %s\r\n", $self->from_param('LOGIN_DATETIME'));

	my $msg =
		"\r\n".
		"+------------------------------------\r\n".
		$msg2.
		"+------------------------------------\r\n";
	$self->send(encode('cp932', $msg));
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;