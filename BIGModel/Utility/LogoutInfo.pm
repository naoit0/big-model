package BIGModel::Utility::LogoutInfo;
our $VERSION = "0001.20190211.2108";		# 改訂版
#
# BIG-Model ホストプログラム
# ログアウト情報
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Naoit0::DateTime;
#use Data::Dumper;
#use Debug;
#==================================================
# ■ログアウト時にログオン情報を表示する
# Utility::logoutinfo()
#
sub logoutinfo {
	my $self = shift;
	my $dateTime = new Naoit0::DateTime;

	my $logoutDT = $self->from_param('LOGOUT_DATETIME');
	$logoutDT =~ tr/-/\//;
	$dateTime->update($logoutDT);
	my $logoutEpoch = $dateTime->epoch;			# ログアウト日時のエポック秒を得る

	my $loginDT = $self->from_param('LOGIN_DATETIME');
	$loginDT =~ tr/-/\//;
	$dateTime->update($loginDT);
	my $loginEpoch = $dateTime->epoch;			# ログオン日時のエポック秒を得る

	#printf "** loginDT=[%s]\n", $loginDT;					# ????
	#printf "** logoutDT=[%s]\n", $logoutDT;				# ????
	#printf "** logoutEpoch=(%d)\n", $logoutEpoch;			# ????
	#printf "** loginEpoch=(%d)\n", $loginEpoch;			# ????

	my $elapseTime = join(':', map { sprintf "%02d", $_ } $dateTime->elapse( $logoutEpoch, $loginEpoch ) );		# 経過時間を計算

	my $msg2;
	$msg2 = sprintf("| 前回ログイン %s\r\n",		$self->from_param('LASTLOGIN')) if ( $self->from_param('USERLEVEL') > 1);
	$msg2 .= sprintf("| 今回ログイン %s\r\n",		$self->from_param('LOGIN_DATETIME'));
	$msg2 .= sprintf("| 現在         %s\r\n",		$self->from_param('LOGOUT_DATETIME'));
	$msg2 .= sprintf("| 経過時間                %s\r\n",	$elapseTime);

	my $msg =
		"\r\n".
		"+---------------------------------\r\n".
		$msg2.
		"+---------------------------------\r\n".
		"\r\n";

	$self->send(encode('cp932', "$msg"));
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;