package BIGModel::Utility::Auxedit;
our $VERSION = "0001.20180512.2359";		# 着手
our $VERSION = "0001.20180522.1943";		# 完了
our $VERSION = "0001.20180622.1512";		# msgs()対応 完了
our $VERSION = "0001.20180705.1949";		# 微調整 完了
our $VERSION = "0001.20190218.2202";		# 改訂版 着手
#
# BIG-Model ホストプログラム
# エディタユーティリティ
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ Auxedit ]]\n\n";
		my $self = shift;
		$self->BIGModel::Utility::Auxedit::initalize;
		$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(1) } );
	},

	'1' =>	# タイトルを入力してください
	sub {
		my $self = shift;
		my $loc = $self->from_param('LOCATION');
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );								# 入力モードを設定
			$arg->param( 'line_size'=>46, 'maskchr'=>'*' );								# パラメータを設定
		$self->BIGModel::Generic::sendmsgs('AUXEDIT_SIG') if ( $loc eq 'SIG' );				# 「提供に感謝します」
		$self->BIGModel::Generic::sendmsgs('AUXEDIT.2');									# 「タイトルを入力してください」(全角23文字まで入力可能)
		$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(2) } );
	},

	'2' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $a = $arg->arg(); $a = defined($a) ? $a : '';

		# ●[ETX][ESC]				【 終了 】
		if ( $a eq "\x03" || $a eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->from_param( 'AUXEDIT_RESULT' => 0 );
			$self->BIGModel::Utility::Auxedit::finalize;
		}
		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			# ●(ヌル)				【 終了 】
			if ( $input eq '' ) {
				$self->from_param( 'AUXEDIT_RESULT' => 0 );
				$self->BIGModel::Utility::Auxedit::finalize;
			}
			# ●(入力値)				【 編集 】
			else {
				$self->from_param( '_AUXEDIT_TITLE' => $input );
				$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(3) } );
			}
		}
	},

	'3' =>	# 内容を入力してください
	sub {
		my $self = shift;
		$self->BIGModel::Generic::sendmsgs('AUXEDIT.3');								# 「内容を入力してください」
		$self->BIGModel::Generic::sendmsgs('AUXEDIT');									# ミニヘルプを表示
		my $arg = $self->from_param('_Arg');
		$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );							# 入力モードを設定
		$arg->param( 'line_size'=>4016, 'maskchr'=>'*' );						# 入力モードを変更（１行当たりの文字数は２００８文字）
		$arg->param( 'allow_chrs'=>'\x1b', 'deny_chrs'=>'' );					# 入力禁止文字を設定
		$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(4) } );
	},

	'4' =>	# 編集中
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $a = $arg->arg(); $a = defined($a) ? $a : '';

		# ●[ETX]				【 編集終了 】
		if ( $a eq "\x03" ) {
			my $input = $arg->status('line');										# 入力バッファを得る
			$arg->flush();															# 入力バッファをクリア
			$self->send("\r\n");
			$self->from_param( '_AUXEDIT_EOT' => 1 );								# 編集終了フラグ＝１

			#printf "** input.length=(%d), str=[%s]\n", length($input), encode('cp932', $input);
			if ( length($input) > 0 ) {;											# 入力バッファが空でないなら
				$input .= "\n\x03";													# 入力バッファに改行とＥＴＸを追加する
				$self->send("\r\n");
			}
			else {																	# 入力バッファがヌルなら
				$input = "\x03";													# 入力バッファにＥＴＸを定義する
			}
			#printf "** input.length=(%d), str=[%s]\n", length($input), encode('cp932', $input);
			$self->BIGModel::Utility::Auxedit::addline( $input );							# 入力バッファを編集バッファに追加
			$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(5) } );
		}
		# ●(改行)
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			# ●[^]				【 編集終了 】
			if ($input eq '^') {
				$self->from_param( '_AUXEDIT_EOT' => 1 );							# 編集終了フラグ＝１
				$self->BIGModel::Utility::Auxedit::addline( "\x03" );				# ＥＴＸを編集バッファに追加
				$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(5) } );
			}
			# ●(入力値)		【 入力値を確定し改行 】
			else {
				$self->BIGModel::Utility::Auxedit::addline( $input."\n" );			# 入力値と改行を編集バッファに追加
			}
		}
	},

	'5' =>	# この内容でよいですか
	sub {
		my $self = shift;
		my $loc = $self->from_param('LOCATION');
		$self->BIGModel::Generic::sendmsgs( 'AUXEDIT_SIG_YESNO.3' ) if ( $loc eq 'SIG' );		# 「この内容を登録してよいですか」
		$self->BIGModel::Generic::sendmsgs( 'AUXEDIT_MAIL_YESNO.3' ) if ( $loc eq 'MAIL' );		# 「この内容でよいですか」
		my $arg = $self->from_param('_Arg');
		$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );										# 入力モードを設定
		$arg->param( 'line_size'=>10, 'maskchr'=>'*', 'allow_chrs'=>'', 'deny_chrs'=>'' );	
		$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(6) } );
	},

	'6' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $a = $arg->arg(); $a = defined($a) ? $a : '';

		# ●[ETX][ESC]			【 プロンプトを再表示 】
		if ( $a eq "\x03" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(5) } );
		}
		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			# ●[Y]				【 保存 】
			if ( $input =~ /^Y$/i ) {
				$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(10) } );
			}
			# ●[N]				【 キャンセル→終了 】
			elsif ( $input =~ /^N$/i ) {
				$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(7) } );
			}
			# ●[R]				【 編集内容を表示(4.0) 】
			elsif ( $input =~ /^R$/i ) {
				$arg->mode( 'echo' => 0 );			# 表示中はローカルエコーを無効にする
				$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(11) } );
			}
			# ●[W]				【 内容を破棄して新規作成(4.0) 】
			elsif ( $input =~ /^W$/i ) {
				$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(8) } );
			}
			# ●(それ以外)		【 プロンプトを再表示 】
			else {
				$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(5) } );
			}
		}
	},

	#--------------------------------------------
	# [N] キャンセル
	#--------------------------------------------
	'7' =>	# キャンセル
	sub {
		my $self = shift;
		my $loc = $self->from_param('LOCATION');
		$self->BIGModel::Utility::Auxedit::remove();										# 編集中のデータを削除
		$self->BIGModel::Generic::sendmsgs( 'AUXEDIT_SIG.2' ) if ( $loc eq 'SIG' );				# 「登録をとりやめました」
		$self->BIGModel::Generic::sendmsgs( 'AUXEDIT_MAIL.2' ) if ( $loc eq 'MAIL' );			# 「送信をとりやめました」
		$self->from_param( 'AUXEDIT_RESULT' => 0 );
		$self->BIGModel::Utility::Auxedit::finalize;
	},

	#--------------------------------------------
	# [W] 内容を破棄して新規作成(4.0)
	#--------------------------------------------
	'8' =>	# 内容を破棄して新たに作成しますか
	sub {
		my $self = shift;
		$self->BIGModel::Generic::sendmsgs('AUXEDIT_MAIL_YESNO.4');							# 「内容を破棄して新たに作成しますか」
		$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(9) } );
	},

	'9' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $a = $arg->arg(); $a = defined($a) ? $a : '';


		# ●[ETX][ESC]			【 プロンプトを再表示 】
		if ( $a eq "\x03" || $a eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(8) } );
		}
		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			# ●[Y]				【 編集内容を破棄し新規作成 】
			if ($input =~ /^Y$/i) {
				$self->BIGModel::Utility::Auxedit::remove();
				$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(3) } );
			}
			# ●[N]				【 キャンセル 】
			elsif ($input =~ /^N$/i) {
				$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(5) } );
			}
			# ●(それ以外)		【 プロンプトを再表示 】
			else {											# 		
				$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(8) } );
			}
		}
	},

	#--------------------------------------------
	# [Y] 保存
	#--------------------------------------------
	# ※ここでは編集内容をarticlesテーブルに保存するだけで、メールや掲示板への移動は戻り先側で処理する
	#
	'10' =>	# 保存
	sub {
		my $self = shift;
		$self->BIGModel::Utility::Auxedit::save();
		$self->from_param( 'AUXEDIT_RESULT' => 1 );		# 正常終了
		$self->BIGModel::Utility::Auxedit::finalize;
	},

	#--------------------------------------
	# [R] 内容を表示(4.0)
	#--------------------------------------
	'11' =>	# コンテンツを表示
	sub {
		my $self = shift;
		my $page = $self->from_param('_AUXEDIT_CURRENT_PAGE');								# 表示中のページ番号
		$self->send("\r\n") unless ( defined($page) );								# 先頭ページなら改行を送出する
		my $more = $self->BIGModel::Utility::Auxedit::preview();
		if ($more == 1) {
			$self->send("\r\n");														# 最終ページなら改行を送出する
			$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(5) } );
		}
		else {
			$self->BIGModel::Generic::sendmsgs( 'PROMPT_MORE' );								# 「続きを表示します」
			$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(12) } );
		}
	},

	'12' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $a = $arg->arg(); $a = defined($a) ? $a : '';

		# ●[ETX][ESC]			【 ページ表示を終了 】
		if ( $a eq "\x03" || $a eq "\x1b" ) {
			$arg->flush();
			$self->send( "\x08\x20\x08" x 22 );						# プロンプトを消去
			$self->from_param('-MORE');								# パラメータを廃棄
			$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(5) } );
		}
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send( "\x08\x20\x08" x 22 );					# プロンプトを消去
			$self->sethandler( sub { $self->BIGModel::Utility::Auxedit::auxedit(11) } );
		}
	},

};

#==================================================
# ■入力バッファを編集バッファに溜めていく
# addline( data );
# 編集バッファが規定量を超えたらcontentsテーブルに移動する
# 編集終了フラグが立っているなら全てのバッファをcontentsテーブルに移動する
# ※入力バッファサイズ arg->mode(size) を規定量の10％程度にしておく（レコードの肥大化防止のため）
#
sub addline {
	my $self = shift;
	my $data = shift;														# 入力データ
	my $contents    = $self->from_param('_AUXEDIT_CONTENTS');				# 編集バッファ
	my $threadId    = $self->from_param('_AUXEDIT_THREADID');				# 編集中のスレッドＩＤ
	my $articleNum  = $self->from_param('_AUXEDIT_ARTICLENUM');				# 編集中の文書番号
	my $pages       = $self->from_param('_AUXEDIT_PAGES');					# 編集中の文書の最終ページ番号
		$pages      = defined($pages) ? $pages : 0;
	my $eot         = $self->from_param('_AUXEDIT_EOT');					# 編集終了フラグ
						$self->from_param('-_AUXEDIT_EOT');
		$eot        = defined($eot) ? $eot : 0;
	my $pageSize    = 4096 - 80;											# １ページ当たりの規定量
	my $dataLen     = length($data);										# 入力バッファのデータ長
	my $contentsLen = length($contents);									# 編集バッファのデータ長
	#printf "** pages=(%d), eot=(%d)\n", $pages, $eot;											# ????
	#printf "** arg.length=(%d), contents.length=(%d)\n", length($data), length($contents);		# ????

	# (A)編集バッファが未定義か
	# (B)入力バッファのデータ長＋編集バッファのデータ長がページ１当たりの規定量に満たないか
	# (C)編集終了フラグが立っているなら
	# 入力バッファを編集バッファに移動し、入力バッファをクリアする
	if ( $contents eq '' || ($dataLen + $contentsLen) < $pageSize || $eot == 1 ) {
		$contents = $contents.$data;
		$data = '';
	}
	# true  : contents = (contents + arg ), arg = '';
	# false : contents = contents,          arg = arg;

	# (A)入力バッファのデータ長＋編集バッファのデータ長がページ１当たりの規定量以上か
	# (B)編集終了フラグが立っているなら
	# 編集バッファをＤＢに移動して、編集バッファクリアする
	if ( $pageSize <= ($dataLen + $contentsLen) || $eot == 1 ) {
		$pages++;												# ページ番号を＋１する
		my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
		$dbh->do("SET NAMES utf8;");

		$contents = decode('cp932', $contents);
		$contents =~ s/\\/\\\\/g;								# ＳＱＬエラー文字回避('\')
		$contents =~ s/\'/\\\'/g;								# ＳＱＬエラー文字回避(''')
		my $q =	"INSERT INTO `contents` (threadid, articleid, page, contents) ". 
				"VALUES ('$threadId', '$articleNum', '$pages', '$contents')";		# articleNumは後でarticlesテーブルに追加したときのidに置き換える
		#printf "*** q=[%s]\n", encode('cp932', $q);			# ????
		$dbh->do( $q );
		$dbh->disconnect;
		$contents = '';											# 編集バッファをクリアする
	}
	#printf "** pages=(%d)\n", $pages;							# ???? 書き込んだページ番号
	$contents = $contents . $data;								# 入力バッファを編集バッファに移動する
	$contents = undef if ($eot == 1);							# 編集終了フラグが立っているなら未定義にする
	$self->from_param( '_AUXEDIT_PAGES' => $pages );			# 最終ページ番号をパラメータに戻す
	$self->from_param( '_AUXEDIT_CONTENTS' => $contents );		# 残っている編集バッファをパラメータに戻す（最後は未定義なのでパラメータが廃棄される）
}

#--------------------------------------------------
# ■編集内容を再表示
# r = preview();
# r = 1			# 送出中
# r = 0			# 送出が終わった
#
sub preview {
	my $self = shift;
	my $threadId   = $self->from_param('_AUXEDIT_THREADID');				# 編集中のスレッドＩＤ
	my $articleNum = $self->from_param('_AUXEDIT_ARTICLENUM');				# 編集中の文書番号
	my $maxpages   = $self->from_param('_AUXEDIT_PAGES');					# 編集中の文書の最終ページ番号
	my $r;																	# 処理結果

	my $page       = $self->from_param('_AUXEDIT_CURRENT_PAGE');			# 表示中のページ番号
		$page      = defined($page) ? $page : 1;							# ページ番号が未定義なら１
	#printf "** page=(%d)\n", $page;										# ????

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	my $q = "SELECT contents FROM `contents` WHERE threadid = '$threadId' AND articleid = '$articleNum' AND page = '$page'";
	#printf "** q=[%s]\n", $q;												# ????
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	if ($sth->rows) {
		my ($contents) = $sth->fetchrow_array;
		unless ($contents eq "\n\x03") {
			$contents =~ s/\n/\r\n/g;
			$self->send( encode('cp932', decode('utf8', $contents)) );		# シフトＪＩＳにエンコードして送出
		}
	}
	$sth->finish();
	$dbh->disconnect();

	$page++;																# ページ番号を＋１する
	$page = ($page >= 1 && $page <= $maxpages) ? $page : undef;				# ページ番号が編集中のページ範囲外なら未定義にする
	$self->from_param( '_AUXEDIT_CURRENT_PAGE' => $page );					# ページ番号をパラメータに保存する（undefならパラメータを廃棄）
	return defined($page) ? 0 : 1;											# 送出が終わっているなら１にする
}

#--------------------------------------------------
# ■編集内容を保存
# save();
# articleテーブルを追加してコンテンツの編集を確定する
#
sub save {
	my $self = shift;
	my $threadId   = $self->from_param('_AUXEDIT_THREADID');				# 保存先のスレッドＩＤ
	my $articleNum = $self->from_param('_AUXEDIT_ARTICLENUM');				# 保存した文書番号
	my $title      = $self->from_param('_AUXEDIT_TITLE');					# タイトル
	my $author     = $self->from_param('ID');								# 投稿者（自分のＩＤ）

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	$title = decode('cp932', $title);
	$title =~ s/\\/\\\\/g;					# ＳＱＬエラー文字回避('\')
	$title =~ s/\'/\\\'/g;					# ＳＱＬエラー文字回避(''')

	# articlesテーブルにデータを追加する
	my $q = "INSERT INTO `articles` (threadid, articlenum, author, title) ".
			"VALUES ('$threadId', '$articleNum', '$author', '$title')";
	#printf "*** q=[%s]\n", encode('cp932', $q);					# ????
	$dbh->do( $q );

	#articlesテーブルに追加したレコード番号を得る
	my $articleId;
	$q = "SELECT id FROM `articles` WHERE threadid = '$threadId' AND articlenum = '$articleNum'";
	#printf "*** q=[%s]\n", encode('cp932', $q);					# ????
	my $sth = $dbh->prepare("$q");
	$sth->execute;
	if ($sth->rows) {
		($articleId) = $sth->fetchrow_array;
	}
	$sth->finish();
	#printf "** articleNum=(%d), articleId=(%d)\n", $articleNum, $articleId;					# ????

	#追加したcontentsテーブルのレコードを更新する（articleid:文書番号→articlesのレコード番号）
	$q = "UPDATE `contents` SET articleid = '$articleId' ".
		"WHERE threadid = '$threadId' AND articleid = '$articleNum'";
	#printf "*** q=[%s]\n", encode('cp932', $q);					# ????
	$dbh->do( $q );
	$dbh->disconnect;
	$self->from_param( 'AUXEDIT_ARTICLEID' => $articleId );			# articlesテーブルに追加したレコード番号（戻り先で参照する）
	$self->from_param( 'AUXEDIT_ARTICLENUM' => $articleNum );		# articlesテーブルに追加した投稿番号（戻り先で参照する）
}

#--------------------------------------------------
# ■編集中のコンテンツをcontentsテーブルから破棄
# remove();
#
sub remove {
	my $self = shift;
	my $threadId   = $self->from_param('_AUXEDIT_THREADID');
	my $articleNum = $self->from_param('_AUXEDIT_ARTICLENUM');

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	# contentsテーブルを初期化
	my $q = "DELETE FROM `contents` WHERE threadid = '$threadId' AND articleid = '$articleNum'";
	#printf "*** q=[%s]\n", $q;							# ????
	$dbh->do( $q );
	$dbh->disconnect();

	# パラメータを初期化
	$self->from_param( '_AUXEDIT_PAGES' => 0 );			# ページ番号
	$self->from_param( '_AUXEDIT_CONTENTS' => '' );		# 編集バッファ
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;

	# パラメータを定義
	my $threadId   = '#'.$self->from_param('ID');
	my $data       = $self->BIGModel::Generic::DB::Articles::getLatestArticle( $threadId );			# スレッドの最新レコードを得る(** 確認中)
	my $articleNum = defined($data) ? ($data->[1] + 1) : 1;									# 未定義なら１に置き換える
	printf "** threadid=[%s], articlenum=[%d]\n", $threadId, $articleNum;					# ????

	$self->from_param( '_AUXEDIT_THREADID' => $threadId );			# スレッドＩＤ
	$self->from_param( '_AUXEDIT_ARTICLENUM' => $articleNum );		# 文書番号
	$self->from_param( '_AUXEDIT_PAGES' => 0 );						# ページ番号
	$self->from_param( '_AUXEDIT_TITLE' => '' );					# タイトル
	$self->from_param( '_AUXEDIT_CONTENTS' => '' );					# 本文
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;
	$self->from_param( '_AUXEDIT_CONTENTS' => undef );				# 編集バッファ
	$self->from_param( '_AUXEDIT_TITLE' => undef );					# タイトル
	$self->from_param( '_AUXEDIT_PAGES' => undef );					# 編集中または閲覧中のページ番号
	$self->from_param( '_AUXEDIT_ARTICLENUM' => undef );			# 文書番号
	$self->from_param( '_AUXEDIT_THREADID' => undef );				# スレッドＩＤ
	$self->from_param( '_AUXEDIT_EOT' => undef );					# 編集終了フラグ

	my $return = $self->from_param('_RETURN');						# パラメータを回収
	$self->from_param('-_RETURN');									# パラメータを廃棄
	if ( ref($return) eq 'CODE' ) {									# 戻り先が定義されていれば
		$self->sethandler( sub { $self->$return } );				# そちらにジャンプする
	}
	else {															# 戻り先が定義されていなければ
		#$self->send( encode('cp932', "** ＡＵＸＥＤＩＴを終了します **\r\n") );
		$self->BIGModel::Generic::Path::back();						# 現在のパスがルートでなければひとつ手前のユーティリティを呼び出す
	}
}

#--------------------------------------------------
# ■呼び出し
#
sub auxedit {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;