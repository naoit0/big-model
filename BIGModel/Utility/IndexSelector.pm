package BIGModel::Utility::IndexSelector;
our $VERSION = "0001.20190223.1829";		# 着手
#
# BIG-Model ホストプログラム
# インデックスセレクタ（実験中）
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
local $Data::Dumper::Sortkeys = 1; # ハッシュのキーをソートする
local $Data::Dumper::Indent = 1; # インデントを縮める
use DBI;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ IndexSelector ]]\n\n";
		my $self = shift;
		$self->BIGModel::Utility::IndexSelector::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# ** アクセス権がありません **
			$self->BIGModel::Utility::IndexSelector::finalize;
		}
		else {
			# ●初期設定
			$self->from_param( 'THREADID' => ';B;2' );					# ++++ スレッドＩＤ
			my $threadid = $self->from_param('THREADID');
			$self->from_param( 'HERE' => 1 );							# ++++ 現在値の初期値
			my $here = $self->from_param('HERE');

			# ●インデックスセレクタの準備
			my $is = new BIGModel::Utility::IndexSelector( $threadid, $self->{'db'} );
			$is->BIGModel::Utility::IndexSelector::reload( $here );		# キャッシュデータを更新
			$self->from_param( '_IndexSelector' => $is );				# ++++ インデックスセレクタ

			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(1) } );
		}
	},

	'1' =>	# ヘッダ表示
	sub {
		my $self = shift;
		$self->BIGModel::Utility::IndexSelector::header();			# (3)ヘッダ表示
		$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(2) } );
	},

	'2' =>	# 初期設定
	sub {
		my $self = shift;
		$self->BIGModel::Utility::IndexSelector::title( $self->from_param('HERE') );			# (4)項目表示
		$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(3) } );
	},

	'3' =>	# 初期設定
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		$arg->mode( 'echo'=>0, 'line'=>0, 'mask'=>0 );
		$arg->param( 'line_size'=>0 );
		printf "** threshold=(%d)\n", $self->from_param('THRESHOLD');	# ????
		$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(4) } );
	},

	'4' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ● [CTRL+E][↑][K][j]					【 旧方向へ進む 】										■■■■■■■■■■■■■
		if ( $inkey =~ /[\x05|K|j]/ ) {
			print "** (back)\n";		# ????
			$self->BIGModel::Utility::IndexSelector::back();											# (2)戻る
			$self->BIGModel::Utility::IndexSelector::title( $self->from_param('HERE') );				# (4)項目表示
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(3) } );
		}

		# ● [CTRL+R][←][H][l]					【 旧方向へ読み進む 】
		elsif ( $inkey =~ /[\x12|H|l]/ ) {
			print "** (read_back)\n";		# ????
			$self->from_param( 'DIRECTION' => 'back' );						# 戻る
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(5) } );
		}

		# ● [CTRL+D][RETURN]					【 現在位置を読む 】									■■■■■■■■■■■■■
		elsif ( $inkey =~ /[\x04|\x0d]/ ) {
			print "** (read)\n";		# ????
			$self->from_param('-DIRECTION');
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(5) } );
		}

		# ● [CTRL+F][→][L][h][TAB]			【 新方向へ読み進む 】
		elsif ( $inkey =~ /[\x06|\x09|L|h]/ ) {
			print "** (read_forward)\n";		# ????
			$self->from_param( 'DIRECTION' => 'forward' );					# 進む
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(5) } );
		}

		# ● [CTRL+X][↓][J][k][ ]				【 新方向へ進む 】										■■■■■■■■■■■■■
		elsif ( $inkey =~ /[\x18|\x20|J|k]/ ) {
			print "** (forward)\n";		# ????
			$self->BIGModel::Utility::IndexSelector::forward();											# (2)読む
			$self->BIGModel::Utility::IndexSelector::title( $self->from_param('HERE') );				# (4)項目表示
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(3) } );
		}

		## ● [R]								【 新方向へ読み続ける 】
		#elsif ( $inkey eq 'R' ) {
		#	#$self->BIGModel::Utility::IndexSelector::read_down();
		#	$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(2) } );
		#}
        #
		## ● [SHIFT+R]							【 旧方向へ読み続ける 】
		#elsif ( $inkey eq 'r' ) {
		#	#$self->BIGModel::Utility::IndexSelector::read_up();
		#	$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(2) } );
		#}
        
		# ● [T]								【 現在位置から新方向へタイトル一覧 】
		elsif ( $inkey eq 'T' ) {
			$self->from_param( 'DIRECTION' => 'forward' );					# 進む
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(30) } );
		}
        
		# ● [SHIFT+T]							【 現在位置から旧方向へタイトル一覧 】
		elsif ( $inkey eq 't' ) {
			$self->from_param( 'DIRECTION' => 'back' );						# 戻る
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(30) } );
		}

		# ● [CTRL+C][ESC][E]					【 終了 】											■■■■■■■■■■■■■
		elsif ( $inkey =~ /[\x03|\x1b|E|e]/ ) {
			$self->send("\r\n");
			$self->BIGModel::Utility::IndexSelector::finalize;
		}

		# ● [?]								【 説明 】											■■■■■■■■■■■■■
		elsif ( $inkey eq '?' ) {
			$self->send("\r\n");
			my $p = $self->from_param('_Path');
			my $msgid = $p->path();
				$msgid = 'HELP_'.$msgid if ( defined($msgid) );
			#printf "** msgid=[%s]\n", $msgid;				# ???? ヘルプはパス名で取得する
			$self->BIGModel::Generic::sendmsgs( $msgid );
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(1) } );
		}

		# ●番号[RETURN]						【 番号指定 】										■■■■■■■■■■■■■
		elsif ( $inkey =~ /[\d|\#]/ ) {
			$self->from_param('_INPUT' => $inkey ) if ( $inkey =~ /\d/ );					# ++++ 入力値
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(10) });
		}
        
		# ●"キーワード[RETURN]					【 タイトル検索 】									■■■■■■■■■■■■■
		elsif ( $inkey eq "\"" ) {
			$self->from_param('FILTERMODE' => 'TITLE' );							# ++++ フィルタモード
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(20) });
		}
        
		# ●'キーワード[RETURN]					【 全文検索 】										■■■■■■■■■■■■■
		elsif ( $inkey eq "\'" ) {
			$self->from_param('FILTERMODE' => 'FULLTEXT' );							# ++++ フィルタモード
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(20) });
		}

		# ● [*]								【 削除 】
		#elsif ( $inkey eq '*' ) {
		#	$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector() } );
		#}

		# ● [W][U][.]							【 書く 】
		#elsif ( $inkey =~ /[W|w|U|u] || $inkey eq '.' ) {
		#	$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector() } );
		#}
	},

#---------------------------------------------
# ■ 読む
#---------------------------------------------
	'5' =>	# 初期設定
	sub {
		print "\n+++ (5)\n";						# ????
		my $self = shift;

		my $here = $self->from_param('HERE');									# 現在値
		my $is = $self->from_param('_IndexSelector');							# インデックスセレクタ
		my $r = $is->BIGModel::Utility::IndexSelector::contents( $here );		# アーティクルＩＤを得る

		if ($r == 1) {			# 準備完了(1)
			$self->send("\r\n");
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(6) } );
		}
		else {					# エラー(0, -1, -2)
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(8) } );
		}
		print "+++ (/5)\n";						# ????
	},

	'6' =>	# コンテンツ出力
	sub {
		print "\n+++ (6)\n";					# ????
		my $self = shift;
		my $is = $self->from_param('_IndexSelector');					# インデックスセレクタ
		my $r = $is->BIGModel::Utility::IndexSelector::contents();		# アーティクルＩＤを得る

		no warnings qw( numeric );		# numeric ～がうるさいので無効化
		if ($r == -3) {						# コンテンツ終り
			$self->send("\r\n");
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(8) } );		# 移動処理
		}
		else {
			my $contents = encode('cp932', decode('utf8', "$r"));
			$contents =~ s/\n/\r\n/g;
			$self->send( $contents );									# データを送出
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(7) } );		# コンテンツ出力
		}
		print "+++ (/6)\n";						# ????
	},

	'7' =>	# 入力待ち
	sub {
		print "\n+++ (7)\n";					# ????
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ● [ETX][ESC]					【キャンセル】
		if ( $inkey =~ /[\x03|\x1b]/ ) {
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(8) } );
		}
		else {
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(6) } );
		}
		print "+++ (/7)\n";						# ????
	},

	'8' =>	# 移動処理
	sub {
		print "\n+++ (8)\n";					# ????
		my $self = shift;
		my $direction = $self->from_param('DIRECTION'); $self->from_param('-DIRECTION');		# 移動方向

		if ( defined( $direction ) ) {
			$self->BIGModel::Utility::IndexSelector::forward() if ( $direction =~ /forward/i );
			$self->BIGModel::Utility::IndexSelector::back() if ( $direction =~ /back/i );
		}
		if ( $self->from_param('THRESHOLD') == 0 ) {
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(3) } );
		}
		else {
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(1) } );
		}
		print "+++ (/8)\n";						# ????
	},

#---------------------------------------------
# ■ 番号指定（フィルタに依存しない）				※確認済み
#---------------------------------------------
	'10' =>	# 入力設定・プロンプト
	sub {
		my $self = shift;
		my $inkey = $self->from_param('_INPUT'); $self->from_param('-_INPUT');
		my $arg = $self->from_param('_Arg');
		$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );
		$arg->param( 'line_size'=>10 );
		$arg->store( $inkey );											# 入力値を蓄積バッファへ(再入力)
		$arg->param( 'allow_chrs'=>'[\d]' );						# 数字入力のみ許可
		$self->BIGModel::Generic::sendmsgs('PROMPT_NUMBER');		# 「番号を入力してください」
		$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(11) } );
	},

	'11' =>	# 入力待ち		** pass
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ● [ETX][ESC]					【キャンセル】
		if ( $inkey =~ /[\x03|\x1b]/ ) {
			$self->send("\r\n");
			$arg->flush();
			$arg->mode( 'echo'=>0, 'line'=>0 );
			$arg->param( 'allow_chrs'=>'' );
			$self->BIGModel::Utility::IndexSelector::header();											# (3)ヘッダ表示
			$self->BIGModel::Utility::IndexSelector::title(  $self->from_param('HERE'), 1 );			# (4)項目表示（ＤＢダイレクト）
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(3) } );
		}
		# ● (改行)
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			$arg->mode( 'echo'=>0, 'line'=>0 );
			$arg->param( 'allow_chrs'=>'' );
			my $num = $self->BIGModel::Utility::IndexSelector::go( $input );							# (2)移動
			$self->from_param( 'HERE' => $num );
			$self->BIGModel::Utility::IndexSelector::header();											# (3)ヘッダ表示
			$self->BIGModel::Utility::IndexSelector::title( $self->from_param('HERE'), 1 );				# (4)項目表示（ＤＢダイレクト）
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(3) } );
		}
	},

#---------------------------------------------
# ■ タイトル検索／全文検索
#---------------------------------------------
	'20' =>	# 入力設定・プロンプト
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );
		$arg->param( 'line_size'=>60 );
		$self->BIGModel::Generic::sendmsgs('PROMPT_SEARCH_STRING');		# 「検索文字列を入力してください」
		# （フィルタモード有効中なら設定しているキーワードを再送出させるとか）
		my $keyword = $self->from_param('FILTER_KEYWORD');
		if ( defined($keyword) ) {
			$arg->store( $keyword );
		}
		$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(21) } );
	},

	'21' =>	# 入力待ち・パラメータ設定
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';
		my $input;

		# ● [ETX][ESC]				【 キャンセル 】
		if ( $inkey =~ /[\x03|\x1b]/ ) {
			$self->send("\r\n");
			$arg->flush();
			$input = '';								# 入力値はヌル（モードリセット）
		}
		# ● (改行・入力あり)
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			$input = $arg->status('line');
		}

		# ●入力があればそれぞれ処理
		if ( defined($input) ) {
			my $mode = $self->from_param('FILTERMODE');
			my $here = $self->from_param('HERE');
			print "** here=(%d)\n", $here;						# ????

			# ●入力値がヌルならパラメータを廃棄
			if ( $input eq '' ) {
				$self->from_param('-FILTERMODE');
				$self->from_param('-FILTER_KEYWORD');
				$mode = undef;
			}
			# ●入力値が定義されていたらパラメータに保存
			else {
				$self->from_param('FILTER_KEYWORD' => $input );
			}

			my $is = $self->from_param('_IndexSelector');
			$is->filtermode( $mode, $input );													# フィルタモードを設定
			$arg->mode( '-echo'=>0, '-line'=>0 );												# 入力モードを戻す
			$self->BIGModel::Utility::IndexSelector::header();									# (3)ヘッダ表示
			$self->from_param( 'THRESHOLD' => 0 );
			$self->BIGModel::Utility::IndexSelector::title( $here, 1 );		# (4)項目表示
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(3) } );
		}
		#else {
		#	$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(21) } );
		#}
	},

#---------------------------------------------
# ■ タイトル一覧
#---------------------------------------------
	'30' =>	# 初期設定
	sub {
		print "\n+++ (30)\n";						# ????
		my $self = shift;

		$self->BIGModel::Utility::IndexSelector::back()    if ( $self->from_param('DIRECTION') =~ /back/i );
		$self->BIGModel::Utility::IndexSelector::forward() if ( $self->from_param('DIRECTION') =~ /forward/i );
		my $here = $self->from_param('HERE');

		$self->BIGModel::Utility::IndexSelector::title( $here );									# (4)項目表示
		my $threshold = $self->from_param('THRESHOLD');
		if ( $threshold == 0 ) {
			printf "** threshold=(%d)\n", $threshold;
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(31) } );	# 移動処理
		}
		else {
			printf "** threshold=(%d)\n", $threshold;
			$self->from_param('-DIRECTION');
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(4) } );		# 移動処理
		}
		print "+++ (/30)\n";						# ????
	},

	'31' =>	# 入力待ち
	sub {
		print "\n+++ (31)\n";					# ????
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ● [ETX][ESC]					【キャンセル】
		if ( $inkey =~ /[\x03|\x1b]/ ) {
			$self->from_param('-DIRECTION');
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(4) } );
		}
		else {
			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(30) } );
		}
		print "+++ (/31)\n";						# ????
	},

};

#==================================================
# ■項目表示
# title( num, mode )
#
sub title {
	my $caller = join(';', caller);
	printf "\n(title; [%s])\n", $caller;					# ????
	my $self = shift;
	my $num = shift;												# 現在値
	my $mode = shift;												# 取得モード(0: キャッシュ, 1:ＤＢ)
	my $threshold	= $self->from_param('THRESHOLD');				# 閾値フラグ
	my $is			= $self->from_param('_IndexSelector');			# インデックスセレクタ
	my $max_num		= $is->{'max_num'};								# 総アーティクル数
	my $r = defined($mode) ? $is->article( $num ) : $is->article_bycache( $num );		# レコードデータ取得
	my $data;

	printf "** num=(%d), mode=(%d)\n", $num, defined($mode);			# ???? 現在値
	print "[r]\n".Dumper($r)."[/r]\n";							# ????

	if ( $r == 0 ) {											# ０以下
		$data = '00000  v';
		$threshold++;
	}
	elsif ($r == -1 ) {										# 最大値以上
		$data = sprintf( "%05d  ^", $max_num+1 );
		$threshold++;
	}
	else {													# 
		my $num		= $r->[1];		# アーティクル番号
		my $rdate	= $r->[2];		# 投稿日付
		my $rtime	= $r->[3];		# 投稿時刻
		my $author	= $r->[4];		# 投稿者
		my $title	= $r->[5];		# タイトル
		my $flag	= $r->[6];		# フラグ

		# 削除フラグが有効ならタイトルを削除
		#if ( $flag eq '*' ) {				# ユーザ削除フラグが有効
		#	$title = '';
		#);
		#elsif ( $flag eq '-' ) {			# サブオペ削除フラグが有効
		#	$title = '';
		#);
		#elsif ( $flag eq '+' ) {			# シスオペ削除フラグが有効
		#	#$title = '';
		#);

		$data = sprintf( "%05d  %s %s %s %s%s", $num, $rdate, $rtime, $author, $flag, $title );
		$threshold = 0;
	}

	if ( $threshold <= 1 ) {
		$self->send( $data."\r\n" );
	}
	$threshold = 1 if ( $threshold > 1 );

	$self->from_param( 'THRESHOLD' => $threshold );			# 閾値フラグ
	$self->from_param( 'HERE' => $num );
	print "(/title)\n";					# ????
}

#--------------------------------------------------
# ■戻る
#
sub back {
	my $caller = join(';', caller);
	printf "\n(back [%s])\n", $caller;						# ????
	my $self = shift;
	my $num = $self->from_param('HERE');				# 現在値

	# （フィルタモード無効時）
	$num--;
	if ( $num <= 0 ) {								# ０を下回る場合は０（閾値）
		$num = 0;
	}

	# （フィルタモード有効時）
	# 現在値より後ろの番号を得る、得られなければ終端


	$self->from_param( 'HERE' => $num );
	print "(/back)\n";						# ????
}

#--------------------------------------------------
# ■進む
#
sub forward {
	my $caller = join(';', caller);
	printf "\n(forward [%s])\n", $caller;						# ????
	my $self = shift;
	my $is = $self->from_param('_IndexSelector');		# インデックセレクタ
	my $max_num = $is->{'max_num'};						# 総アーティクル数
	my $num = $self->from_param('HERE');				# 現在値

	# （フィルタモード無効時）
	$num++;
	if ( $num > ( $max_num + 1 ) ) {					# 総アーティクル数＋１を超える場合は総アーティクル数＋１（閾値）
		$num = $max_num + 1;
	}

	# （フィルタモード有効時）

	$self->from_param( 'HERE' => $num );
	print "(/forward)\n";						# ????
}

#--------------------------------------------------
# ■ジャンプ
# num = go( num )
#
sub go {
	print "\n(go)\n";							# ????
	my $self = shift;
	my $num = shift;			# 移動先

	my $is = $self->from_param('_IndexSelector');				# インデックセレクタ
	my $max_num = $is->{'max_num'};								# 総アーティクル数
	#printf "** num=(%d), max_num=(%d)\n", $num, $max_num;		# ????

	# （フィルタモード無効時）
	if ( $num > $max_num ) {							# 総アーティクル数を超える場合は総アーティクル数
		$num = $max_num;
	}
	elsif ( $num <= 1 ) {								# １を下回る場合は１
		$num = 1;
	}

	# （フィルタモード有効時）

	$self->from_param( 'HERE' => $num );
	#printf "** num=(%d)\n", $num;								# ????
	#print "(/go)\n";											# ????
	return $num;
}

#--------------------------------------------------
# ■ヘッダ表示
#
sub header {
	my $self = shift;
	$self->BIGModel::Generic::sendmsgs('SIG_HEADER');
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;
	$self->BIGModel::Generic::modoru();								# 戻る
}

#--------------------------------------------------
# ■呼び出し
#
sub indexselector {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

#-------------------------------------------------
# ■■■ インデックスセレクタメソッド ■■■
#-------------------------------------------------
# ●総アーティクル数と最新アーティクルのレコードＩＤを取得し、
# 総アーティクル数を返す
# max_num = getmax();
#
sub getmax {
	print "\n(getmax)\n";						# ????
	my $self		= shift;
	my $threadid	= $self->{'threadid'};		# スレッドＩＤ
	my $max_num		= \$self->{'max_num'};		# 最新アーティクルの番号
	my $max_id		= \$self->{'max_id'};		# 最新アーティクルのレコードＩＤ

 	my $q = "SELECT max(id), max(num) FROM articles WHERE threadid='$threadid'";
	#printf "**[q]------------\n%s\n-----------------\n", $q;							# ????
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $sth = $dbh->prepare( $q );
	$sth->execute;
	my $sth_rows = $sth->rows;
	( $$max_id, $$max_num ) = $sth->fetchrow_array() if ( $sth->rows );
	$sth->finish;
	$dbh->disconnect;
	printf "** max_id=(%d), max_num=(%d), sth_rows=(%d)\n", $$max_id, $$max_num, $sth_rows;			# ????

	print "(/getmax)\n";						# ????
	return $$max_num;
}

#-------------------------------------------------
# ●キャッシュテーブルの更新
# reload( num );
#
sub reload {
	print "\n(reload)\n";				# ????
	my $self		= shift;
	my $num			= shift;							# 基準値
	my $threadid	= $self->{'threadid'};				#@ スレッドＩＤ
	my $max_num		= $self->{'max_num'};				#@ 最新アーティクルの番号（＝アーティクル総数）
	my $mode		= $self->{'filtermode'};			#@ フィルタモード
	my $kw			= $self->{'filter_keyword'};		#@ フィルタキーワード

	my $table		= \$self->{'table'};				#@ キャッシュテーブル
		$$table		= {};								# キャッシュテーブルをクリア
	my $rows		= \$self->{'table_rows'};			#@ キャッシュテーブルの最大レコード数
	my $expire		= \$self->{'table_expire'};			#@ キャッシュテーブルの有効期限
	my $forward		= \$self->{'rows_forward'};			# 前方検索の該当レコード数
	my $backward	= \$self->{'rows_backward'};		# 後方検索の該当レコード数

	my $table_max	= \$self->{'table_max'};			# キャッシュテーブルの上限の番号【※廃止予定】
	my $table_min	= \$self->{'table_min'};			# キャッシュテーブルの下限の番号【※廃止予定】

	printf "** num=(%d)\n", $num;		# ????

	# ●フィルタモードに応じてクエリを組み立てる
    
	my @qs;
	# (デフォルト)
	my $select = "SELECT articles.id, articles.num, ".
	             "date_format(articles.datetime, '%y-%m-%d') AS r_date, \n".
	             "date_format(articles.datetime, '%H:%i:%s') AS r_time, \n".
	             "articles.author, articles.title, flag FROM articles";
	my $where  = "WHERE threadid='$threadid'";
	  my $and  = "";
	my $group  = "";
	my $order  = "";
	my $limit  = "LIMIT $$rows";
    
	# ●フィルタモードが有効
	if ( defined($mode) ) {
		# (全文検索)
		if ( $mode =~ /fulltext/i ) {
			$where = "INNER JOIN contents ON articles.id = contents.articleid \n".
			         "WHERE articles.threadid='$threadid' ";
			  $and = "AND ( title LIKE '%$kw%' OR contents LIKE '%$kw%' )";
			$group = "GROUP BY articles.id";
		}
		# (タイトル検索)
		if ( $mode =~ /title/i ) {
			$and = "AND title LIKE '%$kw%'";
		}
    
		# 前方検索用
		$and = $and." AND num<=$num";
		$order = "ORDER BY num DESC";
		push(@qs, join("\n", ( $select, $where, $and, $group, $order, $limit )));
    
		# 後方検索用
		$and = $and." AND num>=$num";
		$order = "ORDER BY num ASC";
		push(@qs, join("\n", ( $select, $where, $and, $group, $order, $limit )));
	}
    
	# ●フィルタモードが無効
	else {
		my $span = int( $$rows / 2 );

		# 現在値の位置を補正
		if ( $num <= $span ) {							# 現在値がキャッシュテーブルの最大レコード数を下回る場合
			my $sp = $num;
			$num = 1;
		}
		elsif ( $num > ( $max_num - $span ) ) {			# 現在値が最新アーティクルの番号超えている場合
			my $sp = $num;
			$num = ( $max_num - $$rows ) + 1;
		}
		else {											# それ以外 ** pass
			my $sp = $num;
			$num = ( $num - $span ) + 1;
		}

		# 後方検索専用
		$and = $and." AND num>=$num";
		$order = "ORDER BY num ASC";
		push(@qs, undef);									# 前方検索はなし
		push(@qs, join("\n", ( $select, $where, $and, $group, $order, $limit )));
	}
    
	# （この時点で、フィルタモードが有効の場合は２つ、無効の場合は１つのクエリを生成）
	# フィルタモード有効: [ 前方検索用, 後方検索用 ]
	# フィルタモード無効: [ undef,      後方検索用 ]
	#print "[qs]\n".Dumper(@qs)."[/qs]\n";					# ????

	# ●クエリを実行し、結果を保存
	while (@qs) {
		shift(@qs) unless ( defined($mode) );			# フィルタモードが無効の場合は、２つ目がクエリなので要素を飛ばす
		my $q = shift(@qs);								# クエリを取り出す
		printf "**[q]------------\n%s\n-----------------\n", $q;							# ????
		my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
		$dbh->do("SET NAMES utf8;");
		my $sth = $dbh->prepare( $q );
		$sth->execute;
		my $sth_rows = $sth->rows;
		while ( my @r = $sth->fetchrow_array() ) {
			@r = map { $_ = '' unless(defined($_)); encode('cp932', decode('utf8', "$_")); } @r;
			# print join('|', @r)."\n";						# ????
			my $num = sprintf("%05d", $r[1]);
			$$table->{$num} = [ @r ];						# 該当レコードをキャッシュテーブルに保存
		}
		$sth->finish;
		$dbh->disconnect;

		if ( defined($mode) ) {					# フィルタモードが有効
			if ( $#qs == 0 ) {						# 前方検索での該当レコード数
				$$forward = $sth_rows;
			}
			else {									# 後方検索での該当レコード数
				$$backward = $sth_rows;
			}
		}
		else {									# フィルタモードが無効
			$$forward = undef;
			$$backward = undef;
		}
	}

	# ●キャッシュテーブルの上限と下限の番号を設定
	my @a = sort(keys(%{ $$table }));

	if ($#a >= 1) {
		my $min = shift(@a);
		my $max = pop(@a);
		$$table_max = $max + 0;		# 数値に変換
		$$table_min = $min + 0;		# 数値に変換
	}
	elsif ($#a == 0) {					# レコードが１件のみ
		my $max = pop(@a);
		my $min = $max;
		$$table_max = $max + 0;
		$$table_max = $max + 0;
	}
	else {								# レコードなし
		$$table_max = undef;
		$$table_min = undef;
	}
	#print "[table_min]\n".Dumper( $$table_min )."[/table_min]\n";		# ????
	#print "[table_max]\n".Dumper( $$table_max )."[/table_max]\n";		# ????

	# ●キャッシュの有効期限を設定
	$$expire = time + 20;

	print "(/reload)\n";					# ????
}

#-------------------------------------------------
# ●フィルタモードを設定
# filtermode( mode, keyword )
#
sub filtermode {
	my $caller = join(';', caller);
	printf "\n(filtermode [%s])\n", $caller;						# ????
	my $self = shift;
	my $mode = shift;									# フィルタモード(undef: 無効, 'title': タイトル検索, 'fulltext': 全文検索)
	my $keyword = shift;								# キーワード(ヌル(未定義): フィルタモード無効)
		$keyword = '' unless ( defined($keyword) );

	if ($keyword ne '' ) {							# キーワードがヌルでない(フィルタ有効)
		$self->{'filtermode'}		= $mode;		# フィルタモード
		$self->{'filter_keyword'}	= $keyword;		# フィルタキーワード
	}
	else {
		$self->{'filtermode'}		= undef;		# フィルタモードを無効
		$self->{'filter_keyword'}	= undef;		# フィルタキーワード
	}

	print "** (0:no_filter)\n" unless ( defined($mode) );							# ????
	print "** (1:fulltext)\n"  if ( defined($mode) && ($mode =~ /fulltext/i) );		# ????
	print "** (2:title)\n"     if ( defined($mode) && ($mode =~ /title/i) );		# ????
	printf "** keyword=[%s]\n", $keyword if ( defined($keyword) );					# ????
	print "(/filtermode)\n";					# ????
}

#--------------------------------------------------
# ●アーティクルのレコードＩＤを返す
# r = getaid( num );
#
# r: (データあり) : レコードＩＤ
#    (データなし) : -1
#
sub getaid {
	print "\n(getaid)\n";						# ????
	my $self = shift;
	my $num = shift;							# アーティクル番号
	my $aid;									# アーティクルのレコードＩＤ
	#printf "** num=(%d)\n", $num;				# ????

	my $r = $self->article_bycache( $num );
	if ( (defined($r)) && (ref($r) eq 'ARRAY') ) {		# データあり
		$aid = $r->[0];
	}
	else {
		$aid = -1;										# データなし
	}

	#printf "** aid=(%d)\n", $aid;				# ????
	print "(/getaid)\n";						# ????
	return $aid;
}

#-------------------------------------------------
# ●アーティクル本文を取得
# text = contents( num )
#
# num:      アーティクル番号
#
# (1回目)
# r: (エラー) :        0: アーティクル番号が０以下
#                     -1: アーティクル番号が総アーティクル数を超過
#                     -2: アーティクル本文が定義されていない
#      (成功) :        1: 準備完了

# (2回目～)
#      (成功) : contents: コンテンツ始まり～
#                     -3: コンテンツ終り
#
# 初回は初期設定を行い処理データを生成、２回目から処理データからコンテンツを取得、データがなくなれば処理データを廃棄
#
sub contents {
	my $caller = join(';', caller);
	printf "\n(contents [%s])\n", $caller;			# ????
	my $self = shift;
	my $data = \$self->{'contents'};				# 処理中データ
	my $r;

	# ●処理中データがない→初期設定
	unless ( defined($$data) ) {
		my $num = shift;							# アーティクル番号
		my $max_num = $self->{'max_num'};			# 最新のアーティクル番号（＝アーティクル総数）

		$r = 0 if ( $num <= 0 );					# アーティクル番号が０以下(0)
		$r = -1 if ( $num > $max_num );				# アーティクル番号が最新のアーティクル番号を超えている(-1)

		# ●現在がテーブル内→初期設定
		unless ( defined($r) ) {
			my $threadid	= $self->{'threadid'};			# スレッドＩＤ
			my $aid			= $self->getaid( $num );		# アーティクルＩＤを得る
			if ( $aid >= 0 ) {
				# ●ページ数を取得
				print "** (start)\n";						# ????
				my $q = "SELECT id FROM contents WHERE threadid='$threadid' AND articleid=$aid";
				printf "**[q]------------\n%s\n-----------------\n", $q;							# ????
				my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
				$dbh->do("SET NAMES utf8;");
				my $sth = $dbh->prepare( $q );
				$sth->execute;
				$$data->{'aid'}				= $aid;
				$$data->{'page_total'}		= $sth->rows;	# 総ページ数を保存
				$$data->{'page_current'}	= 1;			# カレントページ
				printf "** page_total=(%d)\n", $$data->{'page_total'};		# ????
				$sth->finish;
				$dbh->disconnect;
				$r = 1;					# 準備完了(1)
			}
			else {
				print "** (none)\n";						# ????
				$r = -2;				# アーティクル本文が定義されていない(-2)
			}
		}
	}

	# ●処理中データがある→コンテンツを受け取る
	else {
		if ( defined($$data) ) {
			my $threadid	= $self->{'threadid'};
			my $aid			= $$data->{'aid'};
			my $page		= $$data->{'page_current'};
			my $total		= $$data->{'page_total'};

			if ( $page > $total ) {
				delete( $self->{'contents'} );
				$r = -3;
			}
			elsif ( $page <= $total ) {
				print "** (get)\n";						# ????
				my $q = "SELECT contents FROM contents WHERE threadid='$threadid' AND articleid=$aid AND page=$page";
				printf "**[q]------------\n%s\n-----------------\n", $q;							# ????
				my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
				$dbh->do("SET NAMES utf8;");
				my $sth = $dbh->prepare( $q );
				$sth->execute;
				$r = $sth->fetchrow_array if ($sth->rows);		# データあり
				$sth->finish;
				$dbh->disconnect;
				$page++;
				$$data->{'page_current'} = $page;
			}
		}
	}

	printf "(/contents)\n";			# ????
	return $r;
}

#-------------------------------------------------
# ●キャッシュテーブルからアーティクル情報を取得
# r = article_bycache( num )
#
# num:      アーティクル番号
#
# r: (エラー) :   undef: データなし
#                     0: 現在値が０以下
#                    -1: 現在値が総アーティクル数を超過
#      (成功) : rowdata: アーティクル情報のレコード
#
# アーティクル情報はキャッシュテーブルから取り出す。
# アーティクル情報がキャッシュテーブルにない、または、キャッシュテーブルの有効期限が超過した場合は
# キャッシュテーブル更新後に取り出す。
#
sub article_bycache {
	my $caller = join(';', caller);
	printf "\n(article_bycache [%s])\n", $caller;			# ????
	my $self = shift;
	my $num  = shift;								# アーティクル番号

	my $max_num		= $self->{'max_num'};			# 最新のアーティクル番号（＝アーティクル総数）
	my $table		= $self->{'table'};				# キャッシュテーブル
	my $expire		= $self->{'table_expire'};		# キャッシュテーブルの有効期限
	my $table_max	= $self->{'table_max'};			# キャッシュテーブルの上限の番号
	my $table_min	= $self->{'table_min'};			# キャッシュテーブルの下限の番号
	my $r;

	printf "** num=(%d)\n", $num;												# ????
	printf "** table_min=(%d), table_max=(%d)\n", $table_min, $table_max;		# ????

	# 現在値が０以下なら０を返す
	$r = 0 if ( $num <= 0 );

	## 現在値が最大値を超えているなら－１を返す
	$r = -1 if ( $num > $max_num );

	# いずれかの条件に当てはまればキャッシュテーブルを更新
	$self->reload( $num ) if (time > $expire);									# (1)現在時刻が有効期限を超えている
	unless ( defined($r) ) {
		$self->reload( $num ) if ( (defined($table_max)) && ($num > $table_max) );	# (2)現在値がキャッシュテーブルの上限を超えている
		$self->reload( $num ) if ( (defined($table_min)) && ($num < $table_min) );	# (3)現在値がキャッシュテーブルの下限を下回る
	}
	# table_max, table_min が未定義の場合は該当レコードなし→有効期限のみ機能

	$num = sprintf("%05d", $num);							# アーティルクル番号を文字列に変換

	print "(/article_bycache)\n";									# ????
	return $r if ( defined($r) );							# 返り値が定義済み→返り値を返す
	return unless ( exists($self->{'table'}->{$num}) );		# キャッシュテーブルが未定義→未定義を返す
	return $self->{'table'}->{$num};						# キャッシュテーブルが定義済み→レコードを返す
}

#--------------------------------------------------
# ●ＤＢからアーティクル情報を取得
# r = article( num );
#
# num:      アーティクル番号
#
# r: (エラー) :   undef: データなし
#                     0: 現在値が０以下
#                    -1: 現在値が総アーティクル数を超過
#      (成功) : rowdata: アーティクル情報のレコード
#
#
sub article {
	my $caller = join(';', caller);
	printf "\n(article [%s])\n", $caller;			# ????
	my $self = shift;
	my $num  = shift;				# アーティクル番号

	my $threadid	= $self->{'threadid'};			# 最新のアーティクル番号（＝アーティクル総数）
	my $max_num		= $self->{'max_num'};			# 最新のアーティクル番号（＝アーティクル総数）
	my $r;

	printf "** num=(%d), max_num=(%d)\n", $num, $max_num;						# ????

	# 現在値が０以下なら０を返す
	$r = 0 if ( $num <= 0 );

	## 現在値が最大値を超えているなら－１を返す
	$r = -1 if ( $num > $max_num );
	unless ( defined($r) ) {
		my $select = "SELECT articles.id, articles.num, ".
		             "date_format(articles.datetime, '%y-%m-%d') AS r_date, \n".
		             "date_format(articles.datetime, '%H:%i:%s') AS r_time, \n".
		             "articles.author, articles.title, flag FROM articles";
		my $where  = "WHERE threadid='$threadid'";
		my $and    = "AND num=$num";
			my $q = join("\n", ( $select, $where, $and ));
		printf "**[q]------------\n%s\n-----------------\n", $q;							# ????
		my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
		$dbh->do("SET NAMES utf8;");
		my $sth = $dbh->prepare( $q );
		$sth->execute;
		my $sth_rows = $sth->rows;
		@{$r} = $sth->fetchrow_array() if ( $sth->rows );
			@{$r} = map { $_ = '' unless(defined($_)); encode('cp932', decode('utf8', "$_")); } @{$r};
		$sth->finish;
		$dbh->disconnect;
	}
	print "(/article)\n";						# ????
	return $r;
}

#--------------------------------------------------
# ■コンストラクタ
# new( threadid, db )
#
sub new {
	my $class = shift;
	my $threadid = shift;											# スレッドＩＤ
	my $db = shift;													# ＤＢＩアカウント( $BBS::self->{'db'} )
	return unless ( defined($threadid) && defined($db) );
	my $self = {};
		$self->{'threadid'} = $threadid;
		$self->{'db'} = $db;
	bless($self, $class);
	$self->_init();
	$self->getmax();
	return $self;
}

#-------------------------------------------------
# ■プロパティの初期化
# _init();
#
sub _init {
	my $self = shift;
	#$self->{'db'}				= $db;				# ＤＢアカウント
	#$self->{'threadid'}		= $threadid;		# スレッドＩＤ
	$self->{'max_num'}			= undef;			# 最新アーティクルの番号（＝アーティクル総数）
	$self->{'max_id'}			= undef;			# 最新アーティクルのレコードＩＤ【※廃止予定】
	$self->{'filter_keyword'}	= undef;			# フィルタキーワード
	$self->{'filtermode'}		= undef;			# フィルタモード
													# (undef: モード無効, 'fulltext': 全文検索, 'title': タイトル検索)
	$self->{'table'}			= {};				# キャッシュテーブル
	$self->{'table_rows'}		= 20;				# キャッシュテーブルの最大レコード数
	$self->{'table_expire'}		= undef;			# キャッシュテーブルの有効期限
	$self->{'table_max'}		= undef;			# キャッシュテーブルの上限の番号
	$self->{'table_min'}		= undef;			# キャッシュテーブルの下限の番号
	$self->{'rows_forward'}		= undef;			# 前方検索の該当レコード数
	$self->{'rows_backward'}	= undef;			# 後方検索の該当レコード数
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;