package BIGModel::Utility::IndexSelector;
our $VERSION = "0001.20190223.1829";		# 着手
#
# BIG-Model ホストプログラム
# インデックスセレクタ（実験中）
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
use DBI;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ IndexSelector ]]\n\n";
		my $self = shift;
		$self->BIGModel::Utility::IndexSelector::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# ** アクセス権がありません **
			$self->BIGModel::Utility::IndexSelector::finalize;
		}
		else {
			my $data;
			$self->from_param( '_THREADID' => ';B;2' );					# スレッドＩＤ
			$data = $self->from_param('_DBHELPER_DATA');				# カプセルデータ
			print "[data]\n".Dumper($data)."[/data]\n";					# ????

			$self->BIGModel::Utility::IndexSelector::dbhelper();

			$data = $self->from_param('_DBHELPER_DATA');				# カプセルデータ
			print "[data]\n".Dumper($data)."[/data]\n";					# ????

#			unless ( defined($self->from_param('SELECTOR_DATA')) ) {		# オブジェクトデータがなければ生成
			#	my $total = start();										# 初期設定
			#}

			$self->sethandler( sub { $self->BIGModel::Utility::IndexSelector::indexselector(1) } );
		}
	},

	'1' =>	#
	sub {
		my $self = shift;

		my $data = $self->from_param('_DBHELPER_DATA');				# カプセルデータ
		print "[data]\n".Dumper($data)."[/data]\n";					# ????

		exit;
		$self->BIGModel::Utility::IndexSelector::finalize;
	},

};


#==================================================
# ■ＤＢアクセスヘルパー
# dbhelper( @action )
#
sub dbhelper {
	print "\n(dbhelper)\n";							# ????
	my $self = shift;
	my $data;
	my $r;

	# ●カプセルデータがなければ、データを作成、アーティクル総数を得る
	printf "** defined=(%d)\n", defined( $self->from_param('_DBHELPER_DATA') );				# ????
	my $max_num;
	unless ( defined($self->from_param('_DBHELPER_DATA')) ) {
		$data = $self->from_param('_DBHELPER_DATA');
		$max_num = $self->BIGModel::Utility::IndexSelector::_init($data);
	}

	# ●アーティクル総数が０なら終了処理
	if ( $max_num <= 0 ) {
		$self->BIGModel::Utility::IndexSelector->_finish($data);
	}
	else {
		$data = $self->from_param('_DBHELPER_DATA');		# カプセルデータ
		print "[data]\n".Dumper($data)."[/data]\n";			# ????

		# ●カプセルデータを展開
		my $dbh			= $data->{'dbh'};				# ＤＢＩ（データベースハンドル）
		my $sth			= $data->{'sth'};				# ＤＢＩ（ステートメントハンドル）
		my $this_num	= $data->{'this_num'};			# 現在値のアーティクル番号
		my $this_id		= $data->{'this_id'};			# 現在値のレコードＩＤ
		#my $max_num		= $data->{'max_num'};		# 最大値のアーティクル番号
		my $max_id		= $data->{'max_id'};			# 最大値のレコードＩＤ
		my $threadid	= $data->{'threadid'};			# スレッド名
		my $refresh		= $data->{'refresh'};			# 更新期限
		my $table		= $data->{'table'};				# キャッシュテーブル
		my $query		= $data->{'query'};				# クエリ（配列）

		# ●アクションに応じて処理
		#while (@_) {
		#}
	}

	return $r;
}

#--------------------------------------------------
# ■カプセルデータの準備
# _init( \$data )
#
sub _init {
	print "\n(_init)\n";							# ????
	my $self = shift;
	my $data = shift;									# カプセルデータのリファレンス

	return if ( defined($data) );						# データが定義済みならエラー

	# ●カプセルデータ
	my ( $dbh, $sth );									# ＤＢＩ
	my ( $this_num, $this_id );							# 現在値
	my ( $max_num, $max_id );							# 最大値
	my $threadid = $self->from_param('_THREADID');		# スレッドＩＤ
															# (※カプセルデータ作成後はパラメータがセットされても意味をなさない)
	my $refresh;										# キャッシュ更新期限
	my $table;											# キャッシュテーブル
															#tables->{'_num_'}=[ field, field, field, ... ]
	my $query;											# クエリ（配列）

	# ●ＤＢＩの準備
	$dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	# ●基本クエリの設定
	$query->[0]  = "SELECT ";						# [0]	SELECT
	$query->[1]  = "";								# [1]			(取得データフィールド)
	$query->[2]  = "FROM articles WHERE ";			# [2]	FROM - WHERE
	$query->[3]  = "threadid='$threadid'";			# [3]			(抽出対象フィールド)
	#$query->[4] = "ORDER BY " ;					# [4]	ORDER BY
	#$query->[5] = "";								# [5]			(並べ替え条件)
	#$query->[6] = " LIMIT ";						# [6]	LIMIT
	#$query->[7] = "";								# [7]			(取得レコード数)

	# ●最終投稿ＩＤと投稿総数を得る
	$query->[1] = "max(id), max(num) ";

	# ●拡張クエリの設定
	$query->[1] = '*'     unless (defined($query->[1]));	# SELECT指定がなければ定義
	$query->[4] = "ORDER BY " if (defined($query->[5]));	# ORDER BY指定があれば定義
	$query->[6] = " LIMIT "   if (defined($query->[7]));	# LIMIT指定があれば定義

	# ●クエリの生成
	my $q = join('', @{$query});
	printf "**[q]------------\n%s\n-----------------\n", $q;							# ????
	$sth = $dbh->prepare( $q );
	$sth->execute;
	( $max_id, $max_num ) = $sth->fetchrow_array() if ( $sth->rows );
	my $r = $sth->rows;
	$sth->finish();
	printf "** max_id=(%d), max_num=(%d), r=(%d)\n", $max_id, $max_num, $r;			# ????

	# ●データをカプセルに収める
	$data->{'dbh'}		 = $dbh;						# ＤＢＩ（データベースハンドル）
	$data->{'sth'}		 = $sth;						# ＤＢＩ（ステートメントハンドル）
	$data->{'this_num'}	 = $this_num;					# 現在値のアーティクル番号
	$data->{'this_id'}	 = $this_id;					# 現在値のレコードＩＤ
	$data->{'max_num'}	 = $max_num;					# 最大値のアーティクル番号
	$data->{'max_id'}	 = $max_id;						# 最大値のレコードＩＤ
	$data->{'threadid'}	 = $threadid;					# スレッド名
	$data->{'refresh'}	 = $refresh;					# 更新期限
	$data->{'table'}	 = $table;						# キャッシュテーブル
	$data->{'query'}	 = $query;						# クエリ（配列）
#	$self->from_param( '_DBHELPER_DATA' => $data );		# カプセルデータ

	return $max_num;		# 最大値のアーティクル番号を返す
}

#--------------------------------------------------
# ■ヘルパーの終了処理
# _finish();
#
sub _finish {
	print "\n(_finish)\n";							# ????
	my $self = shift;
	my $data = shift;					# カプセルデータのリファレンス

	return if ( defined($data) );						# データが定義済みならエラー
	my $dbh = $data->{'dbh'};		# ＤＢＩ（データベースハンドル）
	$dbh->disconnect;
	return 1;
}

#--------------------------------------------------
# ■指定するアーティクル番号に移動してデータを取得
# go( num );
#
sub go {
	print "\n(go)\n";							# ????
	my $self = shift;
	my $num  = shift;										# 移動先のアーティクル番号
	my $data = $self->from_param('_DBHELPER_DATA');		# カプセルデータ

	# ●カプセルデータを展開
	my $dbh			= $data->{'dbh'};
	my $sth			= $data->{'sth'};
	my $this_num	= $data->{'this_num'};
	my $this_id		= $data->{'this_id'};
	my $max_num		= $data->{'max_num'};
	my $max_id		= $data->{'max_id'};
	my $threadid	= $data->{'threadid'};
	my $refresh		= $data->{'refresh'};
	my $table		= $data->{'table'};
	my $query		= $data->{'query'};

	# ●移動先が最大値かそれ以上なら、キャッシュを再読み込み
	#$self->_prefetch() if ( $go_num => $max_num );

	# ●キャッシュにデータがあるかチェック
	#my $recdata = $self->_querycache( $go_num );





	# ●クエリの定義
	$query->[1] = '*';
	$query->[3] .= " AND num=$num";

	# ●クエリの生成
	my $q = join('', @$query);
	print "[query]\n".Dumper($query)."[/query]\n";					# ????
	printf "**[q]------------\n%s\n-----------------\n", $q;							# ????
	$sth = $dbh->prepare( $q );
	$sth->execute;
	( $this_id, $this_num ) = $sth->fetchrow_array() if ( $sth->rows );
	my $r = $sth->rows;
	$sth->finish();
	$dbh->disconnect;
	printf "** max_id=(%d), max_num=(%d), r=(%d)\n", $max_id, $max_num, $r;			# ????



}

#--------------------------------------------------
# ■キャッシュに問い合わせる÷
# _querycache( num );		# num: アーティクル番号
#
sub _querycache {
	print "\n(_querycache)\n";							# ????
	my $self = shift;
	my $num = shift;
	my $data = $self->from_param('_DBHELPER_DATA');		# カプセルデータ

	my $refresh		= $data->{'refresh'};		# 更新期限
	my $table 		= $data->{'table'};		# キャッシュテーブル


	# ●キャッシュになければ、キャッシュを再構築
	$num = sprintf( "%05d", $num );
	unless ( exists($table->{$num}) ) {
		#$self->_reload( num );					# 問い合わせの番号の前後１０レコードを読み込む
	}



}


##--------------------------------------------------
## ■キャッシュを更新
## _querycache( num );		# num: アーティクル番号
##
#sub _cache {
#
#	my $dbh			= \$data->{'dbh'};			# ＤＢＩ（データベースハンドル）
#	my $sth			= \$data->{'sth'};			# ＤＢＩ（ステートメントハンドル）
#	my $max_num		= \$data->{'max_num'};		# 最大値のアーティクル番号
#	my $max_id		= \$data->{'max_id'};		# 最大値のレコードＩＤ
#	my $threadid	= \$data->{'threadid'};		# スレッド名
#	my $refresh		= \$data->{'refresh'};		# 更新期限
#	my $table		= \$data->{'table'};		# キャッシュテーブル
#	my $query		= \$data->{'query'};		# クエリ（配列）
#
#
#
#
#
#
#
#キャッシュテーブル
#
#
#1	
#2	
#3	スタート
#4	
#5	
#
#
## ■取り込む範囲の決定
##
## スタート位置によりキャッシュの構築範囲が変わる。
##
## ①先頭（現在地より後方１０個まで）
#if ( $this < 1 ) {
#	'WHERE a_num=1 LIMIT 10'
#}
#
## ②最後尾（現在地より前方１０個まで）
#elsif ( $this >= $max ) {						# maxは初期設定で取得した総レコード数
#	'WHERE a_num=$max-1 LIMIT 10'
#}
#
## ③中間（前後１０個まで）
#else {
#	'WHERE a_num=$this-5 LIMIT 10'
#}
#
## 取り込んだ情報をキャッシュテーブルに
#
#
#
#
#
#
#
#
#現在地より
#prefetch()で読み込む
#
#
#
## もし、現在地が最後尾に来たなら、総レコード数を取得し、更新しなおす
#
#if ($this >= $max ) {
#	"SELECT max(id), max(articlenum) FROM articles WHERE threadid=';B;2'"
#
#	$id;	レコード番号
#	$a_num;	アーティクル番号
#}
#

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;
	$self->BIGModel::Generic::modoru();								# 戻る
}

#--------------------------------------------------
# ■呼び出し
#
sub indexselector {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;