package BIGModel::Utility::Tasklist;
our $VERSION = "0001.20190213.0629";	# 改訂版 完了
#
# BIG-Model ホストプログラム
# アクセス状況通知機能（本体）
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;

#==================================================
# ■各タスクの状況を表示
# tasklist()
#
sub tasklist {
	#print "\n(tasklist)\n";						# ????
	my $self = shift;

	my $tasks = $self->config('TASKS');				# タスクリスト
	my $me = $self->from->fileno;					# 自分のファイル番号

	#print "[tasks]\n".Dumper($tasks)."\n";			# ????
	my $max = $#{ $tasks };							# 接続タスク数
	my $maxlen = length($max);						# 接続タスク数の桁数

	$self->send("\r\n");
	for my $i (0..$max) {
		my $fileno     = $tasks->[$i];							# タスクのファイル番号
		my $userid     = '';									# タスクのユーザＩＤ
		my $mark       = '';									# 応対中のタスクと同一の場合は*をつける
		my $menumode   = '';									# タスクのメニューモード
		my $handlename = '';									# タスクのハンドルネーム
		my $num = substr( $i.(' ' x $maxlen), 0, $maxlen );		# タスク番号

		if ( defined($fileno) ) {
			$mark       = ($fileno == $me) ? '*' : ' ';
			$userid     = sprintf("%-8s", $self->param( $fileno, 'ID' ));
			$menumode   = 'MODE'. $self->param( $fileno, 'MENUMODE' );
			$handlename = $self->param( $fileno, 'HANDLENAME' );
		}

		my $msg = sprintf "TASK%s %s%s %s %s\r\n", $num, $mark, $userid, $menumode, $handlename;
		$self->send(encode('cp932', $msg));
	}
	$self->send("\r\n");
	#print "(tasklist)\n";							# ????
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);

#----------------------------------------------------
#  TASK0  SYSOP    MODE3  お試し版ネット･シスオペ
#  TASK1 *BIG00001 MODE3  ＳｙｓＯｐ

#  TASK0  BIGMODEL MODE3 BIG-Modelサポートセンター
#  TASK1 *BIG99999 MODE1 オンラインマニュアル執筆用
#  TASK2 
#  TASK3 
#  TASK4 
#  TASK5 
#  TASK6 
#  TASK7 
#  TASK8  BIG24300 MODE2 ナツメ出版企画（株）山浦
#  TASK9 
#  TASK10
#  TASK11  BIG00000 MODE2 ゲストアクセス
#  TASK12
#  TASK13
#----------------------------------------------------

1;