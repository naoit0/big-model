#===========================================
# ■ 操作
#===========================================

# ●旧方向へ進む					#�A戻る						#�C項目表示
# ●旧方向へ読み進む	#�@読む		#�A戻る		#�Bヘッダ表示	#�C項目表示
# ●現在位置を読む		#�@読む					#�Bヘッダ表示	#�C項目表示
# ●新方向へ読み進む	#�@読む		#�A進む		#�Bヘッダ表示	#�C項目表示
# ●新方向へ進む					#�A進む						#�C項目表示







#--------------------------------
# �Bヘッダ表示
#
sub header {
	$self = shift;

	my $msgid;
	$msgid = 'MAIL_HEADER' if ( $self->from_param('LOCATION') eq 'MAIL');
	$msgid = 'SIG_HEADER'  if ( $self->from_param('LOCATION') eq 'SIG');
	$msgid = 'SIG_HEADER'  if ( $self->from_param('LOCATION') eq 'SIG');
	$self->BIGModel::Generic::sendmsgs( $msgid );
}

#-NUM-  -R.DATE- -R.TIME- -SENDER-  -CONTENTS-
#-NUM-  -R.DATE- -R.TIME- -SENDER-  -TELEGRAM-
#-NUM-  -R.DATE- -R.TIME- -SENDER-  -MAIL-
#-ROOMNAME- -USER- -TOPIC-


#--------------------------------
# �C項目表示
#
sub title {
	$self = shift;
	my $num = $self->from_param('HERE');									# 現在値
	$self->send('^') if ( $num == 0 );										# 現在地が０なら閾値マーク
	$self->send('v') if ( $num > $max_num + 1 );							# 現在地が上限値＋１なら閾値マーク

	# ＤＢに問い合わせ
	$title = $self->db_title( $threadid, $num );
		# (1)キャッシュの有効期限が過ぎていたら、キャッシュを更新
			#	"SELECT * FROM articles WHERE threadid='$threadid' AND num=$num"
		# (2)その番号がキャッシュにあれば、キャッシュデータを返す
		# キャッシュになければ、キャッシュを更新
		# キャッシュデータがなければ未定義を返す
}
#--------------------------------
# �@読む
# 現在値のアーティクルを読む
#
sub read {
	$self = shift;
}

#--------------------------------
# �A戻る
#
sub back {
	$self = shift;
	my $num = $self->from_param('HERE');								# 現在値
	$self->from_param( 'THRESHOLD' => 1 ) $if ( $num == 0 )				# 現在値が０なら閾値フラグを立てる
	$num--;
}

#--------------------------------
# �A進む
#
sub forward {
	$self = shift;
	my $num = $self->from_param('HERE');								# 現在値
	$self->from_param( 'THRESHOLD' => 1 ) $if ( $num == 0 )				# 現在値が０なら閾値フラグを立てる
	$num++;
}

