use strict;
use warnings;
use utf8;

use Encode;

use DBI;

my $threadid   = ';B;2';					# スレッドＩＤ


# ●フィルタモード(undef: フィルタモード無効, 'fulltext': 全文検索, 'title':タイトル検索)
#my $filtermode = undef;
my $filtermode = 'title';
#my $filtermode = 'fulltext';

# ●フィルタキーワード
my $kw         = 'ー';

# ●前方検索(undef: 無効, 1: 前方検索, 2: 後方検索)
#my $forward    = 1;		#	7 6
my $forward    = 2;			#	10 12 14 15 18
#my $forward    = undef;


my $span       = 5;						# LIMIT
my $here_num   = 8;						# 現在値


#========================================

# ●クエリ決定

#●共通（デフォルト）
my $select = "SELECT articles.id, articles.num, ".
             "date_format(articles.datetime, '%y-%m-%d') AS r_date, \n".
             "date_format(articles.datetime, '%H:%i:%s') AS r_time, \n".
             "articles.author, articles.title, flag FROM articles";					# SELECT
my $where  = "WHERE threadid='$threadid'";							# WHERE(フィルタモード無効・タイトル検索)
my $and    = "";													# AND
my $group  = "";													# GROUP BY
my $order  = "";													# ORDER BY
my $limit  = "LIMIT $span";											# LIMIT

if ( defined($filtermode) ) {
	if ( $filtermode =~ /fulltext/i ) {								# ●全文検索
		$where = "INNER JOIN contents ON articles.id = contents.articleid \n".
		         "WHERE articles.threadid='$threadid' ";
		$and   = "AND ( title LIKE '%$kw%' OR contents LIKE '%$kw%' )";
		$group = "GROUP BY articles.id";
	}

	if ( $filtermode =~ /title/i ) {								# ●タイトル検索
		$and = "AND title LIKE '%$kw%'";
	}
}

if ( defined($forward) ) {
	if ( $forward == 1 ) {									#●前方検索
		$and   = $and . " AND num<$here_num";
		$order = "ORDER BY num DESC";
	}
	else {													#●後方検索
		$and   = $and . " AND num>$here_num";
		$order = "ORDER BY num ASC";
	}
}


my $dbh = DBI->connect('dbi:mysql:dbname=bigmodel;host=127.0.0.1', 'bigmodel', '', {AutoCommit=>1,RaiseError=>1,PrintError=>0});
$dbh->do("SET NAMES utf8;");


my $q = join("\n", ( $select, $where, $and, $group, $order, $limit ));
print "------\n";
print encode('cp932', $q)."\n";
print "------\n";

	my $sth = $dbh->prepare( $q );
	$sth->execute;
	while ( my @r = $sth->fetchrow_array ) {
		no warnings;
		@r = map { $_='' unless(defined($_)); encode('cp932', decode('utf8', "{$_}")) } @r;
		print join('', @r)."\n";
	}
	$sth->finish;
	$dbh->disconnect;



#●総数を取得
#SELECT max(id), max(num) FROM articles WHERE threadid=';B;2'
#
#●生レコードを取得
#SELECT * FROM articles WHERE threadid=';B;2'
#
#●表示形式で取得
#SELECT id, num, 
#date_format(datetime, '%y-%m-%d') AS r_date, date_format(datetime, '%H:%i:%s') AS r_time, 
#author, title, flag 
#FROM articles WHERE threadid=';B;2'
#


