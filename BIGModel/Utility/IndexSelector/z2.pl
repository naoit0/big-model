# 総レコード数
my $max_num = 32;

# キャッシュの最大レコード数
my $cache_rows = 20;
my $rows = \$cache_rows;



# span値
my $span = int( $$rows / 2 );
printf "** span=(%d)\n", $span;

# 現在値をカウントアップ
for my $num (1..42) {
	#print "\n" unless ( $num % 10 );

				if ( $num <= $span ) {							# 現在値がキャッシュテーブルの最大レコード数を下回る場合
					my $sp = $num;
					printf "*[A]* sp=(%d)\t", $sp;
					$num = 1;
					printf "** num=(%d - %d)(%d)\t", $num, ( $num + $$rows ) - 1, (( $num + $$rows ) - 1);		# ???? 取得範囲
					print "\n";
				}

	elsif ( $num > ( $max_num - $span ) ) {			# 現在値が最新アーティクルの番号超えている場合
		my $sp = $num;
		printf "*[C]* sp=(%d)\t", $sp;

		$num = ( $max_num - $$rows ) + 1;
		printf "** num=(%d - %d)(%d)\t", $num, $max_num, ( $num + $$rows ) - 1;			# ????
		print "\n";
	}

				else {													# それ以外 ** pass
					my $sp = $num;
					printf "*[B]* sp=(%d)\t", $sp;
					$num = ( $num - $span ) + 1;
					printf "** num=(%d - %d)(%d)\t", $num, ( $num + $$rows ) - 1, (( $num + $$rows ) - 1);		# ????
					print "\n";
				}
}
