package BIGModel::Telegram;
our $VERSION = "0001.20180515.1134";	# 着手
our $VERSION = "0002.20180518.1125";	# 完了
our $VERSION = "0003.20180609.0353";	# 新Arg対応
our $VERSION = "0003.20180624.0534";	# 外部呼出し対応、微調整
our $VERSION = "0001.20190221.0844";	# 改訂版 着手
our $VERSION = "0001.20190221.2008";	# 改訂版 完了
#
# BIG-Model ホストプログラム
# 電報機能
#

use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
use Debug;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ Telegram ]]\n\n";
		my $self = shift;
		$self->BIGModel::Telegram::initalize;
		$self->sethandler( sub { $self->BIGModel::Telegram::telegram(1) });
	},

	'1' =>	# 送り先を選択
	sub {
		my $self = shift;
		my $tasks = $self->BIGModel::Telegram::taskList();	# 送信可能なタスクリスト
		if ( defined($tasks) ) {								# 接続中のタスクがある→送信するタスクを選択
			$self->BIGModel::Generic::sendmsgs('TELEGRAM.1A');		# 「送り先( 」
			foreach my $i (sort(keys(%{ $tasks }))) {
				$self->send(sprintf "[%d] %s ", $i, $tasks->{$i}->[1]);
			}
			$self->BIGModel::Generic::sendmsgs('TELEGRAM.1B');		# 「)
			my $arg = $self->from_param('_Arg');
				$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );					# 入力制御の動作モードを変更
				$arg->param( 'line_size'=>46 );									# 入力制御のパラメータを設定
			$self->sethandler( sub { $self->BIGModel::Telegram::telegram(2) });
		}
		else {												# 接続中のタスクがない→ユーティリティを終了
			$self->BIGModel::Telegram::finalize;
		}
	},

	'2' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ● [ETX][ESC]					【キャンセル】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$arg->flush();
			$self->send("\r\n");
			$self->BIGModel::Telegram::finalize;
		}
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			# ● (ヌル)					【キャンセル】
			if ($input eq '') {
				$self->BIGModel::Telegram::finalize;
			}
			# ● [?]					【タスクリストを表示】
			elsif ($input eq '?') {
				$self->BIGModel::Utility::Tasklist::tasklist;
				$self->sethandler( sub { $self->BIGModel::Telegram::telegram(1) });
			}
			# ● (番号)					【送信先を選択】
			elsif ($input =~ /^\d*$/) {
				$self->from_param( '_TELEGRAM_TO' => $input );
				$self->sethandler( sub { $self->BIGModel::Telegram::telegram(3) });
			}
			# ● (その他)				【プロンプト再表示】
			else {
				$self->sethandler( sub { $self->BIGModel::Telegram::telegram(1) });
			}
		}
	},

	'3' =>	# 入力値チェック
	sub {
		my $self = shift;
		my $tasks = $self->BIGModel::Telegram::taskList();		# 送信可能なタスクリスト
		my $to = $self->from_param('_TELEGRAM_TO');				# 送信先のタスク番号
			$to = sprintf("%04d", $to);

		#print "\n".Dumper($task);											# ????
		#printf "** to=(%d)\n", $to;										# ????
		#printf "** id=[%s]\n", $task->[$to] if defined($task->[$to]);		# ????
		#print "** noid\n" unless defined($task->[$to]);					# ????

		if ( defined($tasks->{$to}) ) {										# 入力値の送信先タスクが有効→電文入力
			$self->from_param('_TELEGRAM_TO' => $tasks->{$to} );			# タスク番号→タスク情報
			$self->sethandler( sub { $self->BIGModel::Telegram::telegram(4) });
		}
		else {																		# 入力値の送信先タスクが無効→送信先タスクの選択
			$self->sethandler( sub { $self->BIGModel::Telegram::telegram(1) });
		}
	},

	'4' =>	# 電文を入力してください
	sub {
		my $self = shift;
			my $arg = $self->from_param('_Arg');
			$self->BIGModel::Generic::sendmsgs('TELEGRAM.2');						# 「電文を入力してください」
			$self->sethandler( sub { $self->BIGModel::Telegram::telegram(5) });
	},

	'5' =>	# 分岐
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ● [ETX][ESC]					【キャンセル】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$arg->flush();
			$self->send("\r\n");
			$self->BIGModel::Telegram::finalize;
		}
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			# ● (ヌル)					【キャンセル】
			if ($input eq '') {
				$self->BIGModel::Telegram::finalize;
			}
			# ● (その他)				【電報を送信→終了】
			else {
				$self->from_param( '_TELEGRAM_MSG' => $input );
				$self->BIGModel::Telegram::send;
				$self->BIGModel::Telegram::finalize;
			}
		}
	},

};

#==================================================
# ■電報を送る
# send();
#
sub send {
	#print "\n(send)\n";					# ????
	my $self = shift;
	#print "[_TELEGRAM_TO]\n".Dumper( $self->from_param('_TELEGRAM_TO') )."[/_TELEGRAM_TO]\n";			# ????
	my ( $to, $toid ) = @ { $self->from_param('_TELEGRAM_TO') };		# 受信者のファイル番号
	my $fromid = $self->from_param('ID');								# 発信者のユーザＩＤ
	my $userlv = $self->from_param('USERLEVEL');						# 発信者のユーザレベル
	my $msg = decode('cp932', $self->from_param('_TELEGRAM_MSG'));		# 電文

	# ●発信者のユーザレベルがシスオペなら電報を保存する。
	if ( $userlv <= 2 ) {
		# ●受信者の電報受信制限が「全電報を拒否」なら、全ての電報を保存せず終了
		if ( $self->param($to, 'TELEGRAM_MUTE') == 3 ) {
			return;
		}
		# ●受信者の電報受信制限が「会員からのみ許可」で、発信者のユーザレベルがゲストなら電報を保存せず終了。
		elsif ( ($self->param($to, 'TELEGRAM_MUTE') >= 2) && ($userlv <= 1) ) {
			return;
		}
	}

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "INSERT INTO `telegram` (fileno, sender, msg) VALUES ('$to', '$fromid', '$msg')";
	#printf "**[q]------------\n%s\n-----------------\n", encode('cp932', $q);			# ????
	$dbh->do( $q );
	$dbh->disconnect();
	$self->param( $to, 'TELEGRAM' => 1 );					# 電報着信フラグ（check()で回収）
	$self->BIGModel::Generic::sendmsgs('TELEGRAM.3');			# ** 電報を送りました **
	#print "(/send)\n";					# ????
}

#--------------------------------------------------
# ■着信チェック
# check();
#
sub check {
	#print "\n(check)\n";								# ????
	my $self = shift;
	my $telegram = $self->from_param('TELEGRAM');		# 電報着信フラグ
	#print "(/check)\n";								# ????
	return 1 if ( defined($telegram) );					# 着信フラグが立っていれば１
	return;												# 着信フラグがたってなければ０
}

#--------------------------------------------------
# ■受信
# recieve();
#
sub recieve {
	#print "\n(recieve)\n";								# ????
	my $self = shift;
	my $telegram = $self->from_param('TELEGRAM');		# 電報着信フラグ

	if ( defined($telegram) ) {							# 電報着信フラグが立っているなら保存しているメッセージを送出
		my $fileno = $self->from->fileno;				# 応対中ソケットのファイル番号

		my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
		$dbh->do("SET NAMES utf8;");

		# 蓄積しているメッセージを送出
		my $num = 1;
		my $q = "SELECT date_format(datetime, '%y-%m-%d') AS d, date_format(datetime, '%H:%i:%s') AS t,".
				" sender, msg FROM `telegram` WHERE fileno = '$fileno' ORDER BY datetime";
		#printf "**[q]------------\n%s\n-----------------\n", encode('cp932', $q);			# ????
		my $sth = $dbh->prepare( $q );
		$sth->execute();
		if ($sth->rows) {
			$self->BIGModel::Generic::sendmsgs('TELEGRAM.4');				# 「電報が届きました」
			$self->BIGModel::Generic::sendmsgs('TELEGRAM_HEADER');			# 電報のヘッダを表示
			while ( my ($d, $t, $sender, $msg) = $sth->fetchrow_array ) {
				$self->send( encode('cp932', sprintf "%05d  %s %s %-8s  %s\r\n", $num, $d, $t, $sender, decode('utf8', $msg)) );
				$num++;
			}
			$self->BIGModel::Generic::sendmsgs('TELEGRAM.5');			# 「以上」
		}
		$sth->finish();

		# 送出したメッセージを削除
		$q = "DELETE FROM `telegram` WHERE fileno = '$fileno'";
		#printf "**[q]------------\n%s\n-----------------\n", encode('cp932', $q);			# ????
		$dbh->do( $q );

		$dbh->disconnect();
		$self->from_param( 'TELEGRAM' => undef );	# 電報着信フラグを回収
	}
	#print "(/recieve)\n";					# ????
}

#--------------------------------------------------
# ■送信可能なタスク一覧を返す
# tasklist = taskList();
#
# tasklist->{ taskno } = [ fileno, userid ]
#
sub taskList {
	#print "\n(taskList)\n";										# ????
	my $self = shift;
	my $tasks = $self->config('TASKS');								# タスクリスト
	#print "[tasks]\n".Dumper($tasks)."[/tasks]\n";					# ???? タスクリストの内容
	my $me = $self->from->fileno;									# 自分のファイル番号
	my $tasklist;

	# 各タスクの情報を得る
	for my $taskno (0..$#{ $tasks }) {
		my $fileno = $tasks->[$taskno];
		next unless ( defined($fileno) );
		my $userid = $self->param($fileno, 'ID');
		next if ( $fileno == $me );									# 自分は除外
		$taskno = sprintf("%04d", $taskno);
		$tasklist->{ $taskno } = [ $fileno, $userid ];
	}
	#print "\n";													# ????
	#print "[tasklist]\n".Dumper($tasklist)."[/tasklist]\n";		# ???? 送信可能なタスクリスト
	#print "(/taskList)\n";											# ????
	return $tasklist;
}

#--------------------------------------------------
# ■蓄積している未読メッセージの廃棄（ソケット切断時に呼び出す）
# flush();
#
sub flush {
	#print "\n(flush)\n";					# ????
	my $self = shift;
	my $fileno = $self->from->fileno;		# タスクのファイル番号

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	# 蓄積しているメッセージを削除
	my $q = "DELETE FROM `telegram` WHERE fileno = '$fileno'";
	#printf "**[q]------------\n%s\n-----------------\n", encode('cp932', $q);			# ????
	$dbh->do( $q );
	$dbh->disconnect();
	#print "(/flush)\n";					# ????
}

#--------------------------------------------------
# ■終了処理
# initalize()
#
sub initalize {
	my $self = shift;
}

#--------------------------------------------------
# ■終了処理
# finalize()
#
sub finalize {
	my $self = shift;
	$self->from_param('-_TELEGRAM_TO');
	$self->from_param('_TELEGRAM_MSG');

	# 戻り先が定義されていればそちらにジャンプする
	my $return =  $self->from_param('_RETURN');  $self->from_param('-_RETURN');
	if ( ref($return) eq 'CODE' ) {
		$self->sethandler( sub { $self->$return } );
	}
	else {
		$self->BIGModel::Generic::modoru();				# 戻る
	}
}

#--------------------------------------------------
# ■ 呼び出し
# telegram( id )
#
sub telegram {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

#------------------------------------------------------
# ■ソケット切断処理（ハンドラ）
# disconnect()
# 
sub disconnect {
	my $self = shift;
	$self->BIGModel::Telegram::flush();				# 電報の廃棄
}

#------------------------------------------------------
# ■ローダ
# load()
# 
sub load {
	my $self = shift;
	$self->setsyshandler('ondisconnect', sub { BIGModel::Telegram::disconnect($self) }, 'Telegram');		# ソケット切断処理をハンドラに定義
}


printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;