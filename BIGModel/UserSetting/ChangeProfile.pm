package BIGModel::UserSetting::ChangeProfile;
our $VERSION = "0001.20180223.0229";		# 初版
#
# BIG-Model ホストプログラム
# プロフィール変更（端末環境変更機能）
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ ChangeProfile ]]\n\n";
		my $self = shift;
		$self->BIGModel::UserSetting::ChangeProfile::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# ** アクセス権がありません **
			$self->BIGModel::UserSetting::ChangeProfile::finalize;
		}
		else {
			$self->sethandler( sub { $self->BIGModel::UserSetting::ChangeProfile::changeprofile(1) } );
		}
	},

	'1' =>	# 新しいプロフィールを入力してください
	sub {
		my $self = shift;
		my $msg = sprintf( qq("%s"\r\n), $self->from_param('HANDLENAME') );
		$self->send( encode('cp932', $msg) );						# 現プロフィールを表示
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );			# 入力モードを設定
			$arg->param( 'line_size'=>80, 'maskchr'=>'#' );			# パラメータを設定
		$self->BIGModel::Generic::sendmsgs('CHANGEPROFILE');			# 「新しいプロフィールを入力してください」
		$self->sethandler( sub { $self->BIGModel::UserSetting::ChangeProfile::changeprofile(2) } );
	},

	'2' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			【終了】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::UserSetting::ChangeProfile::finalize;
		}
		# ●(入力値)		【パラメータに保存→次へ】
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			$self->from_param( '_INPUT', $input );
			$self->sethandler( sub { $self->BIGModel::UserSetting::ChangeProfile::changeprofile(3) } );
		}
	},

	'3' =>	# 入力したハンドルネームを表示
	sub {
		my $self = shift;
		$self->send( sprintf( qq("%s"\r\n), $self->from_param('_INPUT') ) );		# 新プロフィールを表示
		$self->sethandler( sub { $self->BIGModel::UserSetting::ChangeProfile::changeprofile(4) } );
	},

	'4' =>	# 正しいですか
	sub {
		my $self = shift;
		$self->BIGModel::Generic::sendmsgs('YESNO_CORRECT');				# 「正しいですか」
		$self->sethandler( sub { $self->BIGModel::UserSetting::ChangeProfile::changeprofile(5) } );
	},

	'5' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			【終了】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::UserSetting::ChangePassword::finalize;
		}
		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●[Y]				【更新】
			if ($input =~ /Y/i) {
				my $userid = $self->from_param('ID');
				my $new = $self->from_param('_INPUT');				# パラメータを回収
				my $old = $self->from_param('HANDLENAME');
				$self->BIGModel::Generic::sendmsgs('CHANGE_NOW');		# 「変更手続きを実行中です」

				$new = decode('cp932', $new);
				$self->from_param( 'HANDLENAME' => $new );											# パラメータに保存
				$self->BIGModel::UserSetting::ChangeProfile::update( $userid, $new );				# ＤＢを更新
				$self->BIGModel::Generic::accesslog( $userid, '0', '', sprintf("ハンドルネームを変更 ( %s → %s )", $old, $new) );
				$self->BIGModel::UserSetting::ChangeProfile::finalize;
			}
			# ●[N]				【中止】
			elsif ($input =~ /N/i) {
				$self->BIGModel::UserSetting::ChangeProfile::finalize;
			}
			# ●(その他)		【プロンプトを再表示】
			else {
				$self->sethandler( sub { $self->BIGModel::UserSetting::ChangeProfile::changeprofile(4) } );
			}
		}
	},

};

#==================================================
# ■更新
# update( userid, handlename )
#
sub update {
	#print "\n(update)\n";							# ????
	my $self = shift;
	my $userid = shift;
	my $handlename = shift;

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "UPDATE users SET handlename='$handlename' WHERE userid='$userid'";
	#printf "**[q]------------\n%s\n-----------------\n", $q;		# ????
	$dbh->do( $q );
	$dbh->disconnect();
	#print "(update)\n";							# ????
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;
		$self->from_param( 'LOCATION' => 'USER-SETTING' );		# 現在地
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;
	$self->from_param('-_INPUT');						# パラメータの廃棄
	$self->BIGModel::Generic::modoru();						# 戻る
}

#--------------------------------------------------
# ■呼び出し
#
sub changeprofile {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;