package BIGModel::UserSetting::TelegramMuteMode;
our $VERSION = "0001.20180223.0306";		# 初版
#
# BIG-Model ホストプログラム
# 電報受信制限（端末環境変更機能）
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ TelegramMuteMode ]]\n\n";
		my $self = shift;
		$self->BIGModel::UserSetting::TelegramMuteMode::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# ** アクセス権がありません **
			$self->BIGModel::UserSetting::TelegramMuteMode::finalize;
		}
		else {
			$self->sethandler( sub { $self->BIGModel::UserSetting::TelegramMuteMode::telegrammutemode(1) } );
		}
	},

	'1' =>	# 選択してください
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );			# 入力モードを設定
			$arg->param( 'line_size'=>60, 'maskchr'=>'*' );			# パラメータを設定
		$self->BIGModel::Generic::sendmsgs('TELEGRAMMUTEMODE');			# 「選択してください」
		$self->sethandler( sub { $self->BIGModel::UserSetting::TelegramMuteMode::telegrammutemode(2) } );
	},

	'2' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			【終了】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::UserSetting::TelegramMuteMode::finalize;
		}
		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(ヌル)			【終了】
			if ($input eq '') {
				$self->BIGModel::UserSetting::TelegramMuteMode::finalize;
			}
			# ●[1]				【全電報を受信】
			elsif ($input eq '1') {
				#print "*** [normal]\n";										# ????
				$self->from_param( 'TELEGRAM_MUTE' => 1 );
				$self->BIGModel::Generic::sendmsgs('TELEGRAMMUTEMODE.1' );		# ** すべての電報を受信します **
				$self->BIGModel::UserSetting::TelegramMuteMode::update;
				$self->BIGModel::UserSetting::TelegramMuteMode::finalize;
			}
			# ●[2]				【会員からのみ受信】
			elsif ($input eq '2') {
				#print "*** [useronly]\n";										# ????
				$self->from_param( 'TELEGRAM_MUTE' => 2 );
				$self->BIGModel::Generic::sendmsgs('TELEGRAMMUTEMODE.2' );		# ** ゲストからの電報を拒否します **
				$self->BIGModel::UserSetting::TelegramMuteMode::update;
				$self->BIGModel::UserSetting::TelegramMuteMode::finalize;
			}
			# ●[3]				【全電報を拒否】
			elsif ($input eq '3') {
				#print "*** [silent]\n";										# ????
				$self->from_param( 'TELEGRAM_MUTE' => 3 );
				$self->BIGModel::Generic::sendmsgs('TELEGRAMMUTEMODE.3' );		# ** ゲストおよび会員からの電報を拒否します **
				$self->BIGModel::UserSetting::TelegramMuteMode::update;
				$self->BIGModel::UserSetting::TelegramMuteMode::finalize;
			}
			# ●(その他)		【プロンプト再表示】
			else {
				$self->sethandler( sub { $self->BIGModel::UserSetting::TelegramMuteMode::telegrammutemode(1) } );
			}
		}
	},

};

#==================================================
# ■クッキーの更新
# update()
# 
sub update {
	#print "\n(update)\n";					# ????
	my $self = shift;

	# ●設定値をＤＢに反映（ゲストレベル以上）
	$self->from_param_tocookie('TELEGRAM_MUTE') if ( $self->from_param('USERLEVEL') > 1 );
;
	#print "(update)\n";					# ????
}

#------------------------------------------------------
# ■初期処理
# initalize()
#
sub initalize {
	my $self = shift;
	$self->from_param( 'LOCATION' => 'USER-SETTING' );			# 現在地
}

#------------------------------------------------------
# ■終了処理
# finalize()
# 
sub finalize {
	my $self = shift;

	$self->BIGModel::Generic::modoru();							# 戻る
}

#------------------------------------------------------
# ■呼び出し
# login( id )
#
sub telegrammutemode {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;