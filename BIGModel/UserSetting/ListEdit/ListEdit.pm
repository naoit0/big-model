package BIGModel::UserSetting::ListEdit;
our $VERSION = "0001.20180714.2016";		# 着手
#
# BIG-Model ホストプログラム
# 探索リスト編集（端末環境変更機能）
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
use Debug;
use DBI;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ ListEdit ]]\n\n";
		my $self = shift;
		$self->BIGModel::UserSetting::ListEdit::initalize;
		if ( $self->BIGModel::Permit::member ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# 「アクセス権がありません」
			$self->BIGModel::UserSetting::ListEdit::finalize;
		}
		else {
			$self->BIGModel::Generic::sendmsgs('LISTEDIT.1');				# 「探索リストに登録します」
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::UserSetting::ListEdit::listedit(1) });
		}
	},

	'1' =>	# プロンプト
	sub {
		my $self = shift;
		my $ln = $self->comm->param('LINENUM');
		$self->comm->send( sprintf("%d : ", $ln ) );
		$self->arg->mode( '-echo'=>1, '-line'=>1, '-size'=>60, '-mask'=>0 );
		$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::UserSetting::ListEdit::listedit(3) });
	},

	'3' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->arg->arg();
		my $chr = $self->arg->status('-ctrlchr');
			$chr = ( defined($chr) ) ? $chr : '';

		if ( $chr eq "\x03" || $chr eq "\x1b" ) {			# [ETX][ESC]		終了確認
			$self->comm->send("\r\n");
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::UserSetting::ListEdit::listedit(4) });
		}
		elsif ( $chr eq "\x0d" ) {
			$self->comm->send("\r\n");
			if ( $arg eq '' ) {								# (ヌル)			終了確認
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::UserSetting::ListEdit::listedit(4) });
			}
			else {											# (入力値)			保存
				my $ln = $self->comm->param('LINENUM');
				$self->comm->param('ARG' => $arg );
				$self->BIGModel::UserSetting::ListEdit::push();									# 入力値を記憶
				$self->comm->param( 'LINENUM' => ($ln + 1) );
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::UserSetting::ListEdit::listedit(1) });
			}
		}
	},

#--------------------------------------------------
# ■ 終了確認
#--------------------------------------------------
	'4' =>	# この内容でよいですか
	sub {
		my $self = shift;
		$self->BIGModel::Generic::prompt('AUXEDIT_MAIL_YESNO.3', '-echo'=>1, '-line'=>1, '-size'=>60, '-mask'=>0 );		# 「この内容でよいですか」
		$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::UserSetting::ListEdit::listedit(5) });
	},

	'5' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->arg->arg();
		my $chr = $self->arg->status('-ctrlchr');
			$chr = ( defined($chr) ) ? $chr : '';

		if ( $chr eq "\x03" || $chr eq "\x1b" ) {			# [ETX][ESC]		プロンプト再表示
			$self->comm->send("\r\n");
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::UserSetting::ListEdit::listedit(4) });
		}
		elsif ( $chr eq "\x0d" ) {
			$self->comm->send("\r\n");
			if ( $arg =~ /Y/i ) {							# [Y]				保存
				my $ln = $self->comm->param('LINENUM');
				if ( $ln == 1 ) {												# 一行目でキャンセルしたら
					$self->BIGModel::UserSetting::ListEdit::delete();				# リストを消去する
					$self->BIGModel::Generic::sendmsgs('LISTEDIT.3');					# 「探索リストを消去しました」
				}
				else {															# それ以外なら
					$self->BIGModel::UserSetting::ListEdit::save();					# リストを更新する
					$self->BIGModel::Generic::sendmsgs('LISTEDIT.2');					# 「探索リストに登録しました」
				}
				$self->BIGModel::UserSetting::ListEdit::finalize;
			}
			elsif ( $arg =~ /N/i ) {						# [N]				キャンセル
				$self->BIGModel::Generic::sendmsgs('AUXEDIT_SIG.2');				# 「登録をとりやめました」
				$self->BIGModel::UserSetting::ListEdit::remove();				# 編集中のレコードを破棄
				$self->BIGModel::UserSetting::ListEdit::finalize;
			}
			else {											# (その他)			プロンプト再表示
				$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::UserSetting::ListEdit::listedit(4) });
			}
		}
	},

};

#==================================================
# ■巡回リストを記憶
# 保存するときは'_{USERID}'にする
# 一行ずつ処理する
# ※ここで、重複登録のチェックをしたほうがいいかもしれない
#
sub push {
	#printf "\n\n** < %s::push [%s] >\n", __PACKAGE__, Debug::_caller(caller);
	my $self = shift;
	my $userid = $self->comm->param('ID');				# ユーザＩＤ
	my $ln     = $self->comm->param('LINENUM');			# 行番号
	my $value  = $self->comm->param('ARG');				# 入力値
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "INSERT INTO `list` (userid, num, value) VALUES( '_$userid', '$ln', '$value' )";
	printf "** q=[\n%s\n]\n", encode('cp932', $q);
	$dbh->do($q);
	$dbh->disconnect();
}

#--------------------------------------------------
# ■巡回リストを更新する
# '{USERID}'に変更する
#
sub save {
	#printf "\n\n** < %s::save [%s] >\n", __PACKAGE__, Debug::_caller(caller);
	my $self = shift;
	my $userid = $self->comm->param('ID');				# ユーザＩＤ
	$self->BIGModel::UserSetting::ListEdit::delete();						# 既存レコードを削除
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "UPDATE `list` SET userid = '$userid' WHERE userid = '_$userid'";		# 記憶したレコードを保存する
	printf "** q=[\n%s\n]\n", encode('cp932', $q);
	$dbh->do($q);
}

#--------------------------------------------------
# ■巡回リストを削除
#
sub delete {
	#printf "\n\n** < %s::delete [%s] >\n", __PACKAGE__, Debug::_caller(caller);
	my $self = shift;
	my $userid = $self->comm->param('ID');				# ユーザＩＤ
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	my $q = "DELETE FROM `list` WHERE userid = '$userid'";						# 既存レコードを削除
	printf "** q=[\n%s\n]\n", encode('cp932', $q);
	$dbh->do($q);
	$dbh->disconnect();
}

#--------------------------------------------------
# ■編集中の巡回リストを破棄する
# '_{USERID}'に変更する
#
sub remove {
	#printf "\n\n** < %s::remove [%s] >\n", __PACKAGE__, Debug::_caller(caller);
	my $self = shift;
	my $userid = $self->comm->param('ID');			# ログイン中のユーザＩＤ

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");

	my $q = "DELETE FROM `list` WHERE userid = '_$userid'";							# 編集中のレコードを削除
	printf "** q=[\n%s\n]\n", encode('cp932', $q);
	$dbh->do($q);
	$dbh->disconnect();
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	#printf "\n\n** < %s::initalize [%s] >\n", __PACKAGE__, Debug::_caller(caller);
	my $self = shift;
	$self->comm->param( 'LOCATION' => 'USER-SETTING' );			# 現在地
	$self->comm->param( 'LINENUM' => 1 );

}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	#printf "\n\n** < %s::finalize [%s] >\n", __PACKAGE__, Debug::_caller(caller);
	my $self = shift;
	$self->BIGModel::Generic::Path::back();										# 現在のパスがルートでなければひとつ手前のユーティリティを呼び出す
}

#--------------------------------------------------
# ■呼び出し
#
sub listedit {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	if ( ref($func) eq 'CODE') {
		&{ $func }($self);
	}
}
#------------------------------------------------------
# ■ エントリポイントを返す
#
sub start {
	#printf "\n\n** < %s::start [%s] >\n", __PACKAGE__, Debug::_caller(caller);
	return \&BIGModel::UserSetting::ListEdit::listedit;
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;
