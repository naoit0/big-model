package BIGModel::UserSetting::ChangeLastLogin;
our $VERSION = "0001.20180223.0015";		# 初版
#
# BIG-Model ホストプログラム
# 最終ログイン日時仮設定（端末環境変更機能）
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Naoit0::DateTime;
use Data::Dumper;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ ChangeLastLogin ]]\n\n";
		my $self = shift;
		$self->BIGModel::UserSetting::ChangeLastLogin::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# 「アクセス権がありません」
			$self->BIGModel::UserSetting::ChangeLastLogin::finalize;
		}
		else {
			$self->sethandler( sub { $self->BIGModel::UserSetting::ChangeLastLogin::changelastlogin(1) });
		}
	},

	'1' =>	# 最終ログイン日付を表示
	sub {
		my $self = shift;
		my $dt = $self->from_param('LASTLOGIN_DATE');			# 最終ログイン日付を得る
		$dt = '0000-00-00' unless ( defined($dt) );				# 登録がなければ00-00-00(ゲストも00-00-00)
		$dt = substr( $dt, -8 );								# 'yy-mm-dd'形式にする
		$self->send(encode('cp932', "最後にアクセスされた日付は $dt です.\r\n"));
		$self->sethandler( sub { $self->BIGModel::UserSetting::ChangeLastLogin::changelastlogin(2) });
	},

	'2' =>	# 日付を入力してください
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );
			$arg->param( 'line_size'=>8, 'maskchr'=>'*' );
		$self->BIGModel::Generic::sendmsgs( 'CHANGELASTLOGIN.1' );		# 「日付を入力してください」
		$self->sethandler( sub { $self->BIGModel::UserSetting::ChangeLastLogin::changelastlogin(3) });
	},

	'3' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			【終了】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::UserSetting::ChangeLastLogin::finalize;
		}
		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(入力値)		【入力値チェック】
			if ( $input ne '' ) {
				# ●入力値が'99-99-99'または'00-00-00'または文字列が正しい→設定
				if( ($self->Naoit0::DateTime::validate('-', $input)) || ($input eq '99-99-99') || ($input eq '00-00-00') ) {
					$self->from_param( '_LASTLOGIN_DATE' => $input );
					$self->sethandler( sub { $self->BIGModel::UserSetting::ChangeLastLogin::changelastlogin(4) });
				}
				# ●入力値の文字列が間違っている→プロンプト再表示
				else {
					#print "*** (invalid)\n";					# ????
					$self->sethandler( sub { $self->BIGModel::UserSetting::ChangeLastLogin::changelastlogin(2) });
				}
			}
			# ●(ヌル)			【終了】
			else {
				$self->BIGModel::UserSetting::ChangeLastLogin::finalize;
			}
		}
	},

	'4' =>	# 最終ログイン時刻を表示
	sub {
		my $self = shift;
		my $tm = $self->from_param('LASTLOGIN_TIME');			# 最終ログイン時刻を得る
		$tm = '00:00:00' unless ( defined($tm) );				# 登録がなければ00:00:00(ゲストも00:00:00)
		$self->send(encode('cp932', "最後にアクセスされた時刻は $tm です.\r\n"));
		$self->sethandler( sub { $self->BIGModel::UserSetting::ChangeLastLogin::changelastlogin(5) });
	},

	'5' =>	# 時刻を入力してください
	sub {
		my $self = shift;
		$self->BIGModel::Generic::sendmsgs( 'CHANGELASTLOGIN.2' );		# 「時刻を入力してください」
		$self->sethandler( sub { $self->BIGModel::UserSetting::ChangeLastLogin::changelastlogin(6) });
	},

	'6' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			【終了】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::UserSetting::ChangeLastLogin::finalize;
		}
		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(入力値)		【入力値チェック】
			if ($input ne '') {
				# ●入力値が'99:99:99'または'00:00:00'または文字列が正しい→設定
				if( ($self->Naoit0::DateTime::validate(':', $input)) || ($input eq '99:99:99') || ($input eq '00:00:00') ) {
					$self->from_param( '_LASTLOGIN_TIME' => $input );
					$self->BIGModel::UserSetting::ChangeLastLogin::setunreaddate;		# 未読日時を設定
					$self->BIGModel::UserSetting::ChangeLastLogin::finalize;
				}
				# ●入力値の文字列が間違っている→プロンプト再表示
				else {
					#print "*** (invalid)\n";					# ????
					$self->sethandler( sub { $self->BIGModel::UserSetting::ChangeLastLogin::changelastlogin(5) });
				}
			}
			# ●(ヌル)			【終了】
			else {
				$self->BIGModel::UserSetting::ChangeLastLogin::finalize;
			}
		}
	},

};

#==================================================
# ■未読日時を設定
# setunreaddate()
#
sub setunreaddate {
	my $self = shift;
	my $dt = $self->from_param('_LASTLOGIN_DATE');					# 最終ログイン日付
	my $tm = $self->from_param('_LASTLOGIN_TIME');					# 最終ログイン時刻

	if ( defined($dt) && defined($tm) ) {										# 入力値が全て埋まっている→設定
		my ($y, $md) = $dt =~ /(\d+)(-\d+-\d+)/;
			$y = ($y < 60 && $y >= 0) ? ($y+2000) : ($y+1900);					# 1960-2059
		$self->from_param( 'UNREAD_DATETIME' => "$y$md $tm" );
		$self->send(encode('cp932', "\"$dt $tm\" に設定しました.\r\n"));
	}
	else {																		# 入力値がひとつでも未定義→解除
		$self->from_param( 'UNREAD_DATETIME' => undef );
		$self->send(encode('cp932', "最終ログイン日時仮設定を解除しました.\r\n"));
	}
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;
	my ( $dt, $tm ) = split("\x20", $self->from_param('LASTLOGIN'));	# 最終ログイン日時
	$self->from_param( 'LASTLOGIN_DATE' => $dt );						# 最終ログイン日付（ローカル）
	$self->from_param( 'LASTLOGIN_TIME' => $tm );						# 最終ログイン時刻（ローカル）
	#printf "** dt=[%s], tm=[%s]\n", $dt, $tm;							# ????
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;

	# ●パラメータを廃棄
	$self->from_param('-LASTLOGIN_DATE');
	$self->from_param('-_LASTLOGIN_DATE');
	$self->from_param('-LASTLOGIN_TIME');
	$self->from_param('-_LASTLOGIN_TIME');

	# ●戻る
	$self->BIGModel::Generic::modoru();
}

#--------------------------------------------------
# ■呼び出し
#
sub changelastlogin {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;