package BIGModel::UserSetting::Userlist;
our $VERSION = "0003.20180209.1753";
our $VERSION = "0004.20180323.1508";
our $VERSION = "0005.20180623.1603";		# 機能改良
our $VERSION = "0005.20180705.1223";		# 微調整 完了
our $VERSION = "0001.20190221.2233";		# 改訂版 着手
our $VERSION = "0001.20190221.2256";		# 改訂版 完了
#
# BIG-Model ホストプログラム
# 会員リスト（端末環境変更機能）
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ Userlist ]]\n\n";
		my $self = shift;
		$self->BIGModel::UserSetting::Userlist::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# ** アクセス権がありません **
			$self->BIGModel::UserSetting::Userlist::finalize;
		}
		else {
			$self->sethandler( sub { $self->BIGModel::UserSetting::Userlist::userlist(1) });
		}
	},

	'1' =>	# 検索文字列を入力してください
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );												# 入力モードを設定
			$arg->param( 'line_size'=>80, 'maskchr'=>'#' );												# パラメータを設定
		$self->BIGModel::Generic::sendmsgs('PROMPT_SEARCH_STRING');												# 「検索文字列を入力してください」
		$self->sethandler( sub { $self->BIGModel::UserSetting::Userlist::userlist(2) });
	},

	'2' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			【終了】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::UserSetting::Userlist::finalize;
		}
		# ●(入力値)		【表示へ】
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');
			$self->from_param( '_USERLIST_KEYWORD' => $input ) if ( $input ne '' );			# 検索文字列があれば保存
			$arg->mode('-echo'=>0, '-line'=>0 );
			$self->sethandler( sub { $self->BIGModel::UserSetting::Userlist::userlist(3) });
		}
	},

	'3' =>	# 表示
	sub {
		my $self = shift;
		my $r = $self->BIGModel::UserSetting::Userlist::view(5);								# 該当レコードを５件表示
		#printf "** r=(%d)\n", $r;						# ????
		if ( $r == 1 ) {																		# 続きのレコードがある→入力待ち
			$self->BIGModel::Generic::sendmsgs('PROMPT_MORE');											# 「続きを表示します」
			$self->sethandler( sub { $self->BIGModel::UserSetting::Userlist::userlist(4) });		# 入力待ち
		}
		elsif ( $r == -1 ) {																	# 該当レコードがない→終了
			$self->BIGModel::Generic::sendmsgs('USERLIST.1');											# 「該当するユーザが見つかりません」
			$self->BIGModel::UserSetting::Userlist::finalize;										# 終了
		}
		else {																					# 該当レコードを全て表示した→終了
			$self->BIGModel::Generic::sendmsgs('USERLIST.2');											# 「全てを表示しました」
			$self->BIGModel::UserSetting::Userlist::finalize;										# 終了
		}
	},

	'4' =>	# キー操作
	sub {
		my $self = shift;
		my $nonstop = $self->from_param('_NOSTOP');
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		if ( defined($nonstop) ) {																	# ページ表示抑止が有効なら
			$self->send( "\x08\x20\x08" x 22 );															# プロンプトを消去
			$self->sethandler( sub { $self->BIGModel::UserSetting::Userlist::userlist(3) });			# メイン処理に移動
		}
		else {																						# ページ表示抑止が無効なら
			# ●[ETX][ESC]			【ページ表示を終了】
			if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
				$self->send( "\x08\x20\x08" x 22 );															# プロンプトを消去
				$self->BIGModel::Generic::sendmsgs('USERLIST.3');												# 「中止しました」
				$self->BIGModel::UserSetting::Userlist::finalize;											# 終了へ
			}
			# ●[CR]				【次のページを表示】
			elsif ( $inkey eq "\x0d" ) {
				$self->send( "\x08\x20\x08" x 22 );															# プロンプトを消去
				$self->sethandler( sub { $self->BIGModel::UserSetting::Userlist::userlist(3) });			# メイン処理に移動
			}
			# ●[SPACE]				【全てのページを表示】
			elsif ( $inkey eq "\x20" ) {
				$self->send( "\x08\x20\x08" x 22 );															# プロンプトを消去
				$self->from_param( '_NOSTOP' => 1 );														# ページ表示抑止を有効
				$self->sethandler( sub { $self->BIGModel::UserSetting::Userlist::userlist(3) });			# メイン処理に移動
			}
		}
	},

};

#==================================================
# ■リスト表示
# r = view( step );
#	step = 一度に表示する件数
#	r = ( 0:続きなし, 1:続きあり )
#
sub view {
	#print "\n(view)\n";					# ????
	my $self = shift;
	my $step = shift;													# 一度に表示する件数
		$step = 10 unless( defined($step) && ($step =~ /\d/) );				# （デフォルトは１０件）
	my $keyword = $self->from_param('_USERLIST_KEYWORD');				# 検索文字列
		$keyword = '' unless ( defined($keyword) );						# 文字列がなければヌル
	my $data = $self->from_param('_USERLIST_DATA');						# 処理途中のデータ

	my ( $total, $cnt, $lastid );
		if ( defined($data) )											# 途中のデータがあれば
			{ ( $total, $cnt, $lastid ) = @{ $data }; }						# 保存値を定義
		else															# 途中のデータがなければ
			{ $total = -1; $cnt = 1; $lastid = -1 }							# デフォルト値を定義
	#printf "*(1)* cnt=(%d), lastid=(%d), total=(%d)\n", $cnt, $lastid, $total;			# ????

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my ( $q, $sth );



	# (1) クエリ実行・該当レコード取得

	# 出力するクエリを生成
	my @query = ("SELECT id ,userid , handlename FROM `users` "),				# SELECT
					"",															# WHERE
					"",															# AND
					"",
				);

	# 検索文字列があればクエリに盛り込む
	if ( $keyword ne '' ) {
		$query[1] = "WHERE ";
		$query[2] = "( `userid` LIKE '%$keyword%' OR `handlename` LIKE '%$keyword%' ) ";
	}

	# 途中データがなければクエリを実行して該当レコード数を得る
	if ( $total == -1 ) {
		$q = join('', @query);
		#printf "**[q]------------\n%s\n-----------------\n", $q;									# ????
		$sth = $dbh->prepare( $q );
		$sth->execute;
		$total = $sth->rows;
		$sth->finish();
		#printf "** total=(%d)\n", $total;			# ????
	}


	# (2)


	my $exit = 0;	# 終了フラグ


	unless ( $total == 0 ) {											# 該当レコードがある
		unless ( $lastid == -1 ) {											# 最後に出力したレコードＩＤがあれば
			$query[1] = "WHERE id > $lastid ";									# 条件をクエリに盛り込む
			$query[2] = "AND ".$query[2] if ( $query[2] ne '' );				# 検索文字列が定義されていればクエリに盛り込む
		}

		# 残りのクエリを盛り込んで実行する
		$query[3] = "ORDER BY id ASC LIMIT $step";
		$q = join('', @query);
		#printf "**[q]------------\n%s\n-----------------\n", $q;									# ????
		$sth = $dbh->prepare( $q );
		$sth->execute();

		if ( $sth->rows ) {
			while ( my ($id, $userid, $handlename) = $sth->fetchrow_array() ) {
				$lastid = $id;
				$userid = encode('cp932', decode('utf8', $userid));
				$handlename = encode('cp932', decode('utf8', $handlename));
				$self->send( sprintf "%d users\r\n", $total ) if ( $cnt == 1 );			# １件目ならヘッダを付けて
				$self->send( sprintf "%s %s\r\n", $userid, $handlename );					# レコードを表示する
				$cnt++;
			}
			$self->from_param( '_USERLIST_DATA' => [ $total, $cnt, $lastid ] );			# 最終レコードのデータを保存
		}
		else {
			#print "** (no_record)\n";
		}
		$sth->finish();
		$dbh->disconnect;
		$exit = 1 if ( $cnt > $total );										# カウンタが該当レコード数を超えたら終了
	}
	else {																# 該当レコードがない
		$exit = 2;															# 終了
	}
	#printf "*(2)* cnt=(%d), lastid=(%d), total=(%d)\n", $cnt, $lastid, $total;					# ????
	#printf "** exit=(%d)\n", $exit;															# ????

	if ( $exit > 0 ) {										# 終了なら
		$self->from_param( '_USERLIST_DATA' => undef );		# 途中データを廃棄
		return 0 if ( $exit == 1 );								# 該当レコードを全て出力して終了したなら０を返す
		return -1 if ( $exit == 2 );							# 該当レコードがないため終了したなら－１を返す
	}
	return 1;													# 継続なら１を返す
	#print "\n(view)\n";				# ????
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;
	$self->from_param( 'LOCATION' => 'USER-SETTING' );			# 現在地
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;

	# ●パラメータの廃棄
	$self->from_param('-_USERLIST_KEYWORD');
	$self->from_param('-_USERLIST_DATA');
	$self->from_param('-_NOSTOP');

	# ●呼び出し前に戻り先が定義されていたら移動する
	my $return = $self->from_param('_RETURN');						# パラメータを回収
	$self->from_param('-_RETURN');									# パラメータを廃棄
	if ( ref($return) eq 'CODE' ) {									# 戻り先が定義されていれば
		$self->sethandler( sub { $self->$return } );				# そちらにジャンプする
	}
	else {															# 戻り先が定義されていなければ
		$self->BIGModel::Generic::modoru();								# 戻る
	}
}

#--------------------------------------------------
# ■呼び出し
#
sub userlist {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;