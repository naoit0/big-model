package BIGModel::UserSetting::ChangeMenuMode;
our $VERSION = "0001.20180221.2113";		# 初版
#
# BIG-Model ホストプログラム
# 漢字モード変更（端末環境変更機能）
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
use Debug;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ ChangeMenuMode ]]\n\n";
		my $self = shift;
		$self->BIGModel::UserSetting::ChangeMenuMode::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# 「アクセス権がありません」
			$self->BIGModel::UserSetting::ChangeMenuMode::finalize;
		}
		else {
			$self->sethandler( sub { $self->BIGModel::UserSetting::ChangeMenuMode::changemenumode(1) });
		}
	},

	'1' =>	# 選択してください
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
			$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>0 );
			$arg->param( 'line_size'=>60, 'maskchr'=>'*' );
		$self->BIGModel::Generic::sendmsgs('CHANGEMENUMODE');			# 「選択してください」
		$self->sethandler( sub { $self->BIGModel::UserSetting::ChangeMenuMode::changemenumode(2) });
	},

	'2' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			【終了】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::UserSetting::ChangeMenuMode::finalize;
		}
		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(ヌル)			【終了】
			if ( $input eq '' ) {
				$self->BIGModel::UserSetting::ChangeMenuMode::finalize;
			}
			# ●[1]				【モード１（ＡＮＫ）に変更】
			elsif ($input eq '1') {
				#print "*** [ank]\n";											# ????
				$self->from_param( 'MENUMODE' => 1 );
				$self->BIGModel::UserSetting::ChangeMenuMode::update;
				$self->BIGModel::UserSetting::ChangeMenuMode::finalize;
			}
			# ●[2]				【モード２（シフトＪＩＳ）に変更】
			elsif ($input eq '2') {
				#print "*** [sjis]\n";											# ????
				$self->from_param( 'MENUMODE' => 2 );
				$self->BIGModel::UserSetting::ChangeMenuMode::update;
				$self->BIGModel::UserSetting::ChangeMenuMode::finalize;
			}
			# ●[3]				【モード３（ＡＮＳＩ）に変更】
			elsif ($input eq '3') {
				#print "*** [ansi]\n";											# ????
				$self->from_param( 'MENUMODE' => 3 );
				$self->BIGModel::UserSetting::ChangeMenuMode::update;
				$self->BIGModel::UserSetting::ChangeMenuMode::finalize;
			}
			# ●(その他)		【プロンプト再表示】
			else {
				$self->sethandler( sub { $self->BIGModel::UserSetting::ChangeMenuMode::changemenumode(1) });
			}
		}
	},

};

#==================================================
# ■更新
#
sub update {
	#print "\n(update)\n";														# ????
	my $self = shift;

	# ●設定値をＤＢに反映（ゲストレベル以上）
	$self->from_param_tocookie('MENUMODE') if ( $self->from_param('USERLEVEL') > 1 );
	#print "(update)\n";														# ????
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;
	$self->from_param( 'LOCATION' => 'USER-SETTING' );			# 現在地
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;
	$self->BIGModel::Generic::modoru();		# 戻る
}

#--------------------------------------------------
# ■呼び出し
#
sub changemenumode {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;
