package BIGModel::UserSetting::ListView;
our $VERSION = "0001.20180714.2016";		# 着手
#
# BIG-Model ホストプログラム
# 探索リスト読み出し（端末環境変更機能）
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;
use Debug;
use DBI;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ ListView ]]\n\n";
		my $self = shift;
		$self->BIGModel::UserSetting::ListView::initalize;
		if ( $self->BIGModel::Permit::member ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# 「アクセス権がありません」
			$self->BIGModel::UserSetting::ListView::finalize;
		}
		else {
			$self->BIGModel::Generic::sendmsgs('LISTEDIT');				# 「探索リストに登録します」
			$self->handler('onIdle')->add('onIdle1', sub { $self->BIGModel::UserSetting::ListView::listview(1) });
		}
	},

	'1' =>	# レコードを表示
	sub {
		my $self = shift;
		$self->comm->send("\r\n");
		$self->BIGModel::UserSetting::ListView::view();
		$self->comm->send("\r\n");
		$self->BIGModel::UserSetting::ListView::finalize;
	},

};

#==================================================
# ■巡回リストを表示
#
sub view {
	#printf "\n\n** < %s::view [%s] >\n", __PACKAGE__, Debug::_caller(caller);
	my $self = shift;
	my $userid = $self->comm->param('ID');				# ユーザＩＤ
	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "SELECT value FROM `list` WHERE userid = '$userid' ORDER BY num";
	printf "** q=[\n%s\n]\n", encode('cp932', $q);
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	while ( my ($value) = $sth->fetchrow_array() ) {
		$self->comm->send( $value."\r\n" );
	}
	$sth->finish();
	$dbh->disconnect();
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;
	$self->from_param( 'LOCATION' => 'USER-SETTING' );			# 現在地
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;
	$self->BIGModel::Generic::modoru();							# 戻る
}

#--------------------------------------------------
# ■呼び出し
#
sub listview {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	if ( ref($func) eq 'CODE') {
		&{ $func }($self);
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;
