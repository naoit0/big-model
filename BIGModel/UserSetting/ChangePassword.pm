package BIGModel::UserSetting::ChangePassword;
our $VERSION = "0001.20180223.0153";		# 初版
#
# BIG-Model ホストプログラム
# パスワード変更（端末環境変更機能）
#
use strict;
use warnings;
use utf8;
use Encode qw( encode decode );
use Data::Dumper;

my $subs = {

	'0' =>	# 初期設定
	sub {
		print "\n** [[ ChangePassword ]]\n\n";
		my $self = shift;
		$self->BIGModel::UserSetting::ChangePassword::initalize;
		if ( $self->BIGModel::Generic::Permit::member() == 0 ) {
			$self->BIGModel::Generic::sendmsgs('ACCESS_DENIED');		# ** アクセス権がありません **
			$self->BIGModel::UserSetting::ChangePassword::finalize;
		}
		else {
			$self->sethandler( sub { $self->BIGModel::UserSetting::ChangePassword::changepassword(1) });
		}
	},

	'1' =>	# 現パスワード認証
	sub {
		my $self = shift;
		$self->from_param( '_INPUT' => undef );															# 入力用パラメータを廃棄
		my $arg = $self->from_param('_Arg');
		$arg->mode( 'echo'=>1, 'line'=>1, 'mask'=>1 );													# 入力モードを設定
		$arg->param( 'line_size'=>8, 'maskchr'=>'#' );													# パラメータを設定
		$self->BIGModel::Generic::sendmsgs('CHANGEPASSWORD.1');												# 「現在のパスワードを入力してください」
		$self->sethandler( sub { $self->BIGModel::UserSetting::ChangePassword::changepassword(2) });	# 新パスワード入力へ
	},

	'2' =>	# 入力待ち
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			【終了】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::UserSetting::ChangePassword::finalize;
		}
		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(ヌル)			【終了】
			if ($input eq '') {
				$self->BIGModel::UserSetting::ChangePassword::finalize;
			}
			# ●(入力値)		【現パスワード認証→新パスワード入力へ】
			else {
				unless ( $self->BIGModel::UserSetting::ChangePassword::auth( $self->from_param('ID'), $input ) ) {		# パスワード認証に失敗→プロンプト再表示
					$self->BIGModel::Generic::sendmsgs('CHANGEPASSWORD.4');													# 「パスワードが違います」
					$self->sethandler( sub { $self->BIGModel::UserSetting::ChangePassword::changepassword(1) });		# プロンプト再表示
				}
				else {																									# パスワード認証に成功→
					$self->sethandler( sub { $self->BIGModel::UserSetting::ChangePassword::changepassword(3) });		# 新パスワード入力へ
				}
			}
		}
	},

	'3' =>	# 変更するパスワード
	sub {
		my $self = shift;
		$self->from_param( '_INPUT' => undef );																		# 入力用パラメータを廃棄
		$self->BIGModel::Generic::sendmsgs('CHANGEPASSWORD.2');															# 「新しいパスワードを入力してください」
		$self->sethandler( sub { $self->BIGModel::UserSetting::ChangePassword::changepassword(4) });
	},

	'4' =>	# 分岐
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]				【終了】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::UserSetting::ChangePassword::finalize;
		}
		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(ヌル)				【終了】
			if ( $input eq '' ) {
				$self->BIGModel::UserSetting::ChangePassword::finalize;
			}
			else {
				# ●(入力値)		【現パスワード認証→新パスワード入力へ】
				$self->from_param( '_INPUT' => $input );															# 入力値をパラメータに保存
				$self->sethandler( sub { $self->BIGModel::UserSetting::ChangePassword::changepassword(5) });		# 新パスワードの再入力へ
			}
		}
	},

	'5' =>	# 変更するパスワードの再入力
	sub {
		my $self = shift;
		$self->BIGModel::Generic::sendmsgs('CHANGEPASSWORD.3');															# 「確認のため同じパスワードを入力してください」
		$self->sethandler( sub { $self->BIGModel::UserSetting::ChangePassword::changepassword(6) });
	},

	'6' =>	# 分岐
	sub {
		my $self = shift;
		my $arg = $self->from_param('_Arg');
		my $inkey = $arg->arg(); $inkey = defined($inkey) ? $inkey : '';

		# ●[ETX][ESC]			【終了】
		if ( $inkey eq "\x03" || $inkey eq "\x1b" ) {
			$self->send("\r\n");
			$arg->flush();
			$self->BIGModel::UserSetting::ChangePassword::finalize;
		}
		# ●（改行）
		elsif ( $arg->status('line_cr') == 1 ) {
			$self->send("\r\n");
			my $input = $arg->status('line');

			# ●(ヌル)			【終了】
			if ($input eq '') {
				$self->BIGModel::UserSetting::ChangePassword::finalize;
			}
			# ●(入力値)		【新パスワード照合→パスワード更新】
			else {
				#printf "** input=(%d)[%s]\n", length($input), $input;													# ????
				unless ( $input eq $self->from_param('_INPUT') ) {														# 新パスワード照合に失敗
					$self->BIGModel::Generic::sendmsgs('CHANGEPASSWORD.4');													# パスワードが違います
					$self->sethandler( sub { $self->BIGModel::UserSetting::ChangePassword::changepassword(3) });		# プロンプト再表示
				}
				else {
					my $userid = $self->from_param('ID');
					$self->BIGModel::UserSetting::ChangePassword::update( $userid, $input );				# パスワード更新
					$self->BIGModel::Generic::accesslog( $userid, '0', '', 'パスワードを変更' );			# 顛末をアクセスログに記録
					$self->BIGModel::Generic::sendmsgs('CHANGEPASSWORD.5');										# パスワードを変更しました
					$self->BIGModel::UserSetting::ChangePassword::finalize;									# 終了
				}
			}
		}
	},

};

#==================================================
# ■更新
# update( userid, passwd )
#
sub update {
	#print "\n(update)\n";							# ????
	my $self = shift;
	my $userid = shift;
	my $passwd = shift;
	#print encode( 'cp932', sprint("** userid=[%s], passwd=[%s]\n", $userid, $passwd) );		# ????

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "UPDATE `users` SET passwd='$passwd' WHERE userid = BINARY '$userid'";
	#printf "**[q]------------\n%s\n-----------------\n", $q;									# ????
	my $r = $dbh->do( $q );
	$dbh->disconnect();
	#print "(update)\n";							# ????
	return $r;
}

#--------------------------------------------------
# ■現パスワード認証
# auth( userid, passwd )
# 
sub auth {
	#print "\n(auth)\n";							# ????
	my $self = shift;
	my $userid = shift;
	my $passwd = shift;
	#print encode( 'cp932', sprint("** userid=[%s], passwd=[%s]\n", $userid, $passwd) );		# ????

	my $dbh = DBI->connect(@{ $self->{'db'} }, {AutoCommit=>1,RaiseError=>1,PrintError=>0});
	$dbh->do("SET NAMES utf8;");
	my $q = "SELECT * FROM `users` WHERE userid = BINARY '$userid' AND passwd='$passwd'";
	#printf "**[q]------------\n%s\n-----------------\n", $q;									# ????
	my $sth = $dbh->prepare( $q );
	$sth->execute();
	my $r = $sth->rows();
	$sth->finish;
	$dbh->disconnect();
	#print "(/auth)\n";								# ????
	return $r;
}

#--------------------------------------------------
# ■初期処理
#
sub initalize {
	my $self = shift;
	$self->from_param( 'LOCATION' => 'USER-SETTING' );		# 現在地
	#$self->from_param( '_INPUT' => undef );				# 入力値保存
}

#--------------------------------------------------
# ■終了処理
#
sub finalize {
	my $self = shift;

	# ●パラメータを廃棄
	$self->from_param('-_INPUT');
	$self->BIGModel::Generic::modoru();			# 戻る
}

#--------------------------------------------------
# ■呼び出し
#
sub changepassword {
	my $self = shift;
	my $id = shift;
		$id = ( defined($id) ) ? $id : 0;
	my $func = $subs->{$id};
	my @caller_ = caller;
	if ( ref($func) eq 'CODE') {
		#printf "fileno=(%d) : ", $self->from->fileno;		# ???? 応対中ソケットのファイル番号
		#printf "id=[%d], ", $id;							# ???? ステップＩＤ
		#printf "caller=(%s)\n", join(':', @caller_);		# ???? 呼び出し元
		$self->$func;
	}
}

printf("(%s) [ %s ]\n", $VERSION, __PACKAGE__ ) if ($::DEBUG >= 1);
1;